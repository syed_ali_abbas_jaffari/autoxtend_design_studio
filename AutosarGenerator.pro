#-------------------------------------------------
#
# Project created by QtCreator 2015-11-27T09:52:34
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutosarGenerator
TEMPLATE = app

LIBS += -luuid

OBJECTS_DIR = objects

CONFIG += object_parallel_to_source

SOURCES += main.cpp\
        autosargenerator.cpp \
    metamodelviewerdialog.cpp \
    referenceeditdialog.cpp \
    myintvalidator.cpp \
    codegeneratorfactory.cpp \
    generators/generic/osgenerator.cpp \
    generators/generic/ecum_generator.cpp \
    generators/generic/det_generator.cpp \
    generators/generic/rte_generator.cpp \
    generators/generic/pdur_generator.cpp \
    generators/generic/com_generator.cpp \
    generators/generic/comm_generator.cpp \
    generators/generic/linsm_generator.cpp \
    generators/generic/linif_generator.cpp \
    generators/bcm2835/mcu_generator.cpp \
    generators/bcm2835/port_generator.cpp \
    generators/bcm2835/dio_generator.cpp \
    generators/bcm2835/lin_generator.cpp \
    generators/stm32f103/mcu_generator.cpp \
    generators/stm32f103/port_generator.cpp \
    generators/stm32f103/dio_generator.cpp \
    generators/stm32f103/adc_generator.cpp \
    generators/stm32f103/pwm_generator.cpp \
    generators/stm32f103/lin_generator.cpp \
    autosartoolfactory.cpp \
    aboutdialog.cpp

HEADERS  += autosargenerator.h \
    metamodelviewerdialog.h \
    referenceeditdialog.h \
    myintvalidator.h \
    abstractcodegenerator.h \
    codegeneratorfactory.h \
    generators/generic/osgenerator.h \
    generators/generic/ecum_generator.h \
    generators/generic/det_generator.h \
    generators/generic/rte_generator.h \
    generators/generic/pdur_generator.h \
    generators/generic/com_generator.h \
    generators/generic/comm_generator.h \
    generators/generic/linsm_generator.h \
    generators/generic/linif_generator.h \
    generators/bcm2835/mcu_generator.h \
    generators/bcm2835/port_generator.h \
    generators/bcm2835/dio_generator.h \
    generators/bcm2835/lin_generator.h \
    generators/stm32f103/mcu_generator.h \
    generators/stm32f103/port_generator.h \
    generators/stm32f103/dio_generator.h \
    generators/stm32f103/adc_generator.h \
    generators/stm32f103/pwm_generator.h \
    generators/stm32f103/lin_generator.h \
    autosartoolfactory.h \
    aboutdialog.h \
    qdebugstream.h

FORMS    += autosargenerator.ui \
    metamodelviewerdialog.ui \
    referenceeditdialog.ui \
    aboutdialog.ui

RESOURCES += \
    images.qrc
