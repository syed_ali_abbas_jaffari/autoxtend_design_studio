TODO:
- Change the name of the binary to AutosarDesignStudio. Change messagebox title to reflect meaningful information.
- Do we need RTE editor window? Does not exist yet.
- Module configuration view does not show correct hierarchy for ECU extracts. Make sure that it is correct.
- Do not add default value to the project file if it is not available.
- Make sure that the project is saved before validation and generation, otherwise the generator will fail.
- Keep track of QDomElement added for in-case-user-modifies service (possibly by adding an attribute and removing when user modifies). At the end, the file must be searched for added-but-not-modified QDomElements and remove them, if exist.
- Reference type ECUC-SYMBOLIC-NAME-REFERENCE-DEF is implemented as other references. Don't know what is the actual difference. Left for future work.
- When opening/importing files, only ECUC-MODULE-CONFIGURATION-VALUES-REF-CONDITIONAL modules are loaded from meta-model. Provide a functionality to import other modules from other files, if possible.
- Although recommended configurations referenced by ECUC-MODULE-CONFIGURATION-VALUES-REF are imported. The references in there may point to elements in the meta-model. Use recommended configurations from /ArcCore/EcucDefs/Generic/RecommendedConfs for generic modules and /ArcCore/Boards/<BoardName> for MCAL modules.
- Create tree model for reference dialog.
- Add/Remove parameter button (at the right side of parameters with higher multiplicity) is always added to the new parameter (upon pressing add similar parameter). Do it based on the upper and lower multiplicity.
- Upon pressing remove parameter lower multiplicity is assumed to be 1. Do it based on the lower multiplicity defined by the meta-model.
- Private variables are passed to other classes as pointers (e.g. autosarQItemModel to MetaModelViewerDialog, foundReferences to ReferenceEditDialog). Make them public/global/static.
- Only supports module creation folder for non-service components (in RunAllGenerators()). Generate the service components in separate folder defined by GENERATOR_OUTPUT_SERVICE_DIRECTORY Admin data.
- ReplaceMcalInReference() is used more than required.
- Code with comment 'Find the refered MCAL in the meta-model' is repeated. Make function out of it. Refactor the complete code (avoid too huge functions).

Code optimizations:
1. Replace all occurances of GET_VALUE_OF_PARAM and GET_VALUE_REF_OF_REF with GET_VALUE_OF_PARAM_WITH_RETURN and GET_VALUE_REF_OF_REF_WITH_RETURN, respectively, in all code generators.
2. Define a global style of error indication and use it in all code.
3. Move all error statements from the generators to the validators.

Known Bugs:
1. Save as modifies the original file.
2. Locating the meta-model files at the first start of the software doesn't work.

********************************************** ARXML files in ArcCore 12.0.1 **************************************************
core/arxml/generic folder defines module configurations which are independent of hardware (i.e. not part of the MCAL). The files containing 'generic' are (from my perspective) useless as they define a generic module configurations which might not be correct for a hardware and these configurations are already defined by hardware specific module.
core/arxml/autosar folder provides configurations as defined by the Autosar Consortium. When the tool uses ArcCore arxmls, these files are redundant.
core/arxml/<arch> folder defines hardware depenedent module configurations.
examples/boards folder defines hardware dependent default values and possible enums.

All generic module ARXML files (except ArcCore_MOD_BSWServiceInterfaces_IoHwAb.arxml and ArcCore_Types.arxml) have common generic package. The implementation packages of all generic modules are located within generic package. Similarly, all board related ARXML files (except for the McalImplementations_<board>.arxml) have common <board> package. The implemenations (i.e. McalImplementations_<board>.arxml) of all board related packages are located in /ArcCore/Implementations package. All board related modifications of the generic modules are defined in examples/boards arxml files. These files are just values definition for the board, which are not mandatory to be used and are like project files.

The references in generic modules to MCAL modules is made with a reference /ArcCore/EcucDefs/Mcal/.
All implementations are supposed to be in their respective module files.

Elements defining collections in Autosar Meta-model
AR-PACKAGES, ELEMENTS, CONTAINERS, SUB-CONTAINERS
Only in meta-model file: PARAMETERS, REFERENCES, CHOICES, DESTINATION-REFS
Only in project file: PARAMETER-VALUES, REFERENCE-VALUES

ECUC-MODULE-DEF - Defines a new module's meta-model
ECUC-MODULE-CONFIGURATION-VALUES - Defines a module's values (default/reference/current_project)

MCAL Modules: Adc, Can, Dio, Fls, Fr, Gpt, Icu, Lin, Mcu, Ocu, Port, Pwm, Spi, Wdg
Generic Modules not refined from AUTOSAR: Eth, Port, Cal, Crc, Dlt, IoHwAb, J1939Tp, LwIp, SomeIpXf
Generic but not provided by ArcCore: Eep

************************************************************* Issues with BCM2835 Modules **************************************************************
- MCU: Probably, no operaitng modes, PLL. Getting the reset reason are supported by the hardware but not by the module. It is not known how to safely create Ram Sectors in the assembly.

Complete BCM2835 library
https://github.com/vanvught/rpidmx512/tree/master/lib-bcm2835
