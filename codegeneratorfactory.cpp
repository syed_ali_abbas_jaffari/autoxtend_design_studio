/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "codegeneratorfactory.h"
#include "generators/generic/osgenerator.h"
#include "generators/generic/ecum_generator.h"
#include "generators/generic/det_generator.h"
#include "generators/generic/rte_generator.h"
#include "generators/generic/pdur_generator.h"
#include "generators/generic/com_generator.h"
#include "generators/generic/comm_generator.h"
#include "generators/generic/linsm_generator.h"
#include "generators/generic/linif_generator.h"
#include "generators/bcm2835/mcu_generator.h"
#include "generators/bcm2835/port_generator.h"
#include "generators/bcm2835/dio_generator.h"
#include "generators/bcm2835/lin_generator.h"
#include "generators/stm32f103/mcu_generator.h"
#include "generators/stm32f103/port_generator.h"
#include "generators/stm32f103/dio_generator.h"
#include "generators/stm32f103/adc_generator.h"
#include "generators/stm32f103/pwm_generator.h"
#include "generators/stm32f103/lin_generator.h"

CodeGeneratorFactory::CodeGeneratorFactory()
{
    // Key for registering code generator is based on the implementation definition in the Module's Meta-Model ARMXL file
    // USED-CODE-GENERATOR:SW-VERSION:VENDOR-ID:AR-RELEASE-VERSION
    RegisterGenerator("generators::generic::Os::main:1.0.1:60:4.0.3", &OsGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::EcuM::main:2.4.0:60:4.0.3", &EcuMGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::Det::main:0.9.0:60:4.0.3", &DetGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::PduR::main:2.2.0:60:4.0.3", &PdurGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::Com::main:1.1.0:60:4.0.3", &ComGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::ComM::main:2.2.1:60:4.0.3", &ComMGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::LinSM::main:2.4.0:60:4.0.3", &LinSMGenerator::CreateGenerator);
    RegisterGenerator("generators::generic::LinIf::main:2.1.0:60:4.0.3", &LinIfGenerator::CreateGenerator);
    RegisterGenerator("@RteGenerator::60:", &RteGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::bcm2835::Mcu::main:0.9.0:60:4.1.2", &Bcm2835McuGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::bcm2835::Port::main:0.9.0:60:4.1.2", &Bcm2835PortGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::bcm2835::Dio::main:0.9.0:60:4.1.2", &Bcm2835DioGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::bcm2835::Lin::main:0.9.0:60:4.1.2", &Bcm2835LinGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Mcu::main:2.0.0:60:4.1.2", &STM32F103McuGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Port::main:5.0.1:60:4.1.2", &STM32F103PortGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Dio::main:5.0.0:60:4.1.2", &STM32F103DioGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Adc::main:2.0.0:60:4.1.2", &STM32F103AdcGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Pwm::main:2.1.0:60:4.1.2", &STM32F103PwmGenerator::CreateGenerator);
    RegisterGenerator("generators::mcal::stm32f10x::Lin::main:1.0.0:60:4.1.2", &STM32F103LinGenerator::CreateGenerator);
}

void CodeGeneratorFactory::RegisterGenerator(const QString &generatorDetails, CreateGeneratorFunction createGeneratorFunction)
{
    m_FactoryMap[generatorDetails] = createGeneratorFunction;
}

AbstractCodeGenerator *CodeGeneratorFactory::CreateGenerator(const QString &generatorDetails)
{
    QMap<QString, CreateGeneratorFunction>::iterator it = m_FactoryMap.find(generatorDetails);
    if( it != m_FactoryMap.end() )
        return (it.value())();
    return NULL;
}
