/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "autosartoolfactory.h"

QString AutosarToolFactory::CreateReferenceFromDomElement(QDomElement elem) {
    QString reference;
    if (!elem.parentNode().isNull()) {
        reference = CreateReferenceFromDomElement(elem.parentNode().toElement());
    }
    QDomElement shortName = elem.parentNode().toElement().firstChildElement("SHORT-NAME");
    if (!shortName.isNull()) {
        reference += "/" + shortName.text();
    }
    return reference;
}

void AutosarToolFactory::FindReferencesToElement(QList<QDomElement> *returnElements, QDomElement *root, QString refLastItem) {
    //qDebug() << "Called ref search with root " << root->tagName() << " for ref " << refLastItem;
    QDomElement childElement = root->firstChildElement();
    //qDebug() << "Child element  " << childElement.tagName();
    QDomElement definitionRef;
    while (!childElement.isNull()) {
        definitionRef = childElement.firstChildElement("DEFINITION-REF");
        //qDebug() << "Ref " << definitionRef.tagName() << definitionRef.text();
        if (!definitionRef.isNull()) {
            if (definitionRef.text().contains(refLastItem)) {
                QString lastItem = definitionRef.text().split("/").last();
                if (lastItem == refLastItem)
                    returnElements->push_back(childElement);
            }
        }
        if (!childElement.firstChildElement().isNull()) {
            FindReferencesToElement(returnElements, &childElement, refLastItem);
        }
        childElement = childElement.nextSiblingElement();
    }
}

QDomElement AutosarToolFactory::FindFirstReferenceToElement(QDomElement *root, QString refLastItem) {
    //qDebug() << "Called ref search with root " << root->tagName() << " for ref " << refLastItem;
    QDomElement childElement = root->firstChildElement();
    //qDebug() << "Child element  " << childElement.tagName();
    QDomElement definitionRef;
    while (!childElement.isNull()) {
        definitionRef = childElement.firstChildElement("DEFINITION-REF");
        //qDebug() << "Ref " << definitionRef.tagName() << definitionRef.text();
        if (!definitionRef.isNull()) {
            if (definitionRef.text().contains(refLastItem)) {
                QString lastItem = definitionRef.text().split("/").last();
                if (lastItem == refLastItem) {
                    return childElement;
                }
            }
        }
        if (!childElement.firstChildElement().isNull()) {
            QDomElement  retEl = FindFirstReferenceToElement(&childElement, refLastItem);
            if (!retEl.isNull()) {
                return retEl;
            }
        }
        childElement = childElement.nextSiblingElement();
    }
    return QDomElement();
}

QString AutosarToolFactory::GetTextOfFirstChild(QDomElement *element, QString childName) {
    QDomElement value = element->firstChildElement(childName);
    if (value.isNull()) {
        throw QString("Error: Child " + childName + " not found.");
    }
    return value.text();
}

QDomElement AutosarToolFactory::GetDomElementFromReference(QString *reference, QDomElement documentRootElem) {
    QStringList splittedRef = reference->split('/');
    //qDebug() << "Splitted " << splittedRef;
    QString elementToLookFor = splittedRef.at(1);
    QDomElement domChild = documentRootElem.firstChildElement();
    QString newReducedRef;
    while(!domChild.isNull()) {
        //qDebug() << "Looking into " << domChild.tagName();
        if (domChild.tagName() == "SHORT-NAME") {
            if (domChild.text() == elementToLookFor) {
                if (splittedRef.size() == 2) {
                    return documentRootElem;
                } else {
                    splittedRef.erase(splittedRef.begin(),splittedRef.begin()+2);
                    newReducedRef = "/" + splittedRef.join("/");
                    //qDebug() << "\tSplitted " << splittedRef;
                    //qDebug() << "reduced ref: " << newReducedRef;
                    domChild = domChild.nextSiblingElement();
                    while (!domChild.isNull() && (domChild.tagName() == "DESC" ||
                           domChild.tagName() == "ADMIN-DATA" ||
                           domChild.tagName() == "INTRODUCTION" ||
                           domChild.firstChildElement().isNull())) {
                        domChild = domChild.nextSiblingElement();
                    }
                    //qDebug() << "Calling with reduced reference " << newReducedRef;
                    QDomElement retDom = GetDomElementFromReference(&newReducedRef, domChild);
                    if (!retDom.isNull()) {
                        return retDom;
                    }
                }
            } else
                return QDomElement();
        } else if (!domChild.firstChildElement().isNull()) {
            QDomElement retDom;
            if (newReducedRef == "") {
                //qDebug() << "Calling with reference " << *reference;
                retDom = GetDomElementFromReference(reference, domChild);
            } else {
                //qDebug() << "Calling with reduced reference " << newReducedRef;
                retDom = GetDomElementFromReference(&newReducedRef, domChild);
            }
            if (!retDom.isNull()) {
                return retDom;
            }
        }
        domChild = domChild.nextSiblingElement();
    }
    return QDomElement();
}
