/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef ABSTRACTCODEGENERATOR
#define ABSTRACTCODEGENERATOR

#include <QtXml>
#include <QTextEdit>
#include "autosartoolfactory.h"

class AbstractCodeGenerator
{
public:
    virtual bool RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) = 0;
    virtual bool CheckModuleValidity(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot) = 0;
    int GetTrailingZeros(int number, int numberLength) {
        if (number == 0) {
            return numberLength;
        }
        else if (number % 2 == 1) {
            return 0;
        }
        else {
            return 1 + GetTrailingZeros(number/2, numberLength);
        }
    }
    bool isMaskValid(int mask, int maskLength) {
        if (mask == 0)
        {
            return false;
        }
        else if (mask/(int)qPow(2, GetTrailingZeros(mask, maskLength)) != 1)
        {
            return false;
        }
        return true;
    }
    QString GetStdOnOff(QString value) {
        if (value == "0")
        {
            return "STD_OFF";
        }
        else if (value == "1")
        {
            return "STD_ON";
        }
        else
        {
            throw QString("Error: Value \'" + value + "\' can not be converted to STD_ON/STD_OFF.");
        }
    }
    QString GetValueOfParameter(QDomElement& element, QString parameterName, QString fileName, int lineNumber, bool throwError = true) {
        if (element.isNull())
            throw QString("Error: Element passed as argument is Null. FileName: " + fileName + ", Line: " + QString::number(lineNumber));
        QDomElement parameterElem = AutosarToolFactory::FindFirstReferenceToElement(&element, parameterName);
        if (parameterElem.isNull()) {
            if (throwError)
                throw QString("Error: Parameter \'" + parameterName + "\' is not a child of element. FileName: " + fileName + ", Line: " + QString::number(lineNumber));
            else
                return "";
        }
        QString value;
        try {
            value = AutosarToolFactory::GetTextOfFirstChild(&parameterElem, "VALUE");
        } catch (QString err) {
            if (throwError)
                throw err + " FileName: " + fileName + ", Line: " + QString::number(lineNumber);
            else
                return "";
        }
        return value;
    }
    QString GetValueRefOfReference(QDomElement& element, QString referenceName, QString fileName, int lineNumber, bool throwError = true) {
        if (element.isNull())
            throw QString("Error: Element passed as argument is Null. FileName: " + fileName + ", Line: " + QString::number(lineNumber));
        QDomElement referenceElem = AutosarToolFactory::FindFirstReferenceToElement(&element, referenceName);
        if (referenceElem.isNull())
        {
            if (throwError)
                throw QString("Error: Parameter \'" + referenceName + "\' is not a child of element. FileName: " + fileName + ", Line: " + QString::number(lineNumber));
            else {
                //throw QString("Error: reference not found.");
                return "";
            }
        }
        QString valueRef;
        try {
            valueRef = AutosarToolFactory::GetTextOfFirstChild(&referenceElem, "VALUE-REF");
        }
        catch (QString err) {
            if (throwError)
                throw err + " FileName: " + fileName + ", Line: " + QString::number(lineNumber);
            else
                return "";
        }
        return valueRef;
    }

    virtual ~AbstractCodeGenerator() { }
};

#endif // ABSTRACTCODEGENERATOR

