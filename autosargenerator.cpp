/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "autosargenerator.h"
#include "ui_autosargenerator.h"
#include "metamodelviewerdialog.h"
#include "aboutdialog.h"
#include "referenceeditdialog.h"
#include "myintvalidator.h"
#include "codegeneratorfactory.h"
#include "abstractcodegenerator.h"
#include "autosartoolfactory.h"
#include "generators/generic/rte_generator.h"
#include "qdebugstream.h"

#include <QDomElement>
#include <uuid/uuid.h>
Q_DECLARE_METATYPE(QDomElement);
#include <QDir>

AutosarGenerator::AutosarGenerator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AutosarGenerator) {
    ui->setupUi(this);
}

void AutosarGenerator::runGui(bool v) {
    guiMode = v;
    if (guiMode) {
        // Set up QStream output
        new QDebugStream(std::cout, ui->generatorOutput); //Redirect Console output to QTextEdit
        //QDebugStream::registerQDebugMessageHandler(); //Redirect qDebug() output to QTextEdit
    }
    Initialize();
}

void AutosarGenerator::Initialize() {
    // Set the initial sizes of the GUI elements
    QList<int> sizes;
    sizes << 300 << 120;
    ui->moduleSplitter->setSizes(sizes);
    ui->propertiesSplitter->setSizes(sizes);
    sizes.clear();
    sizes << 100 << 300;
    ui->mainSplitter->setSizes(sizes);

    autosarQItemModel = new QStandardItemModel;
    projectQItemModel = new QStandardItemModel;
    availableMcals << "zynq" << "bcm2835" << "stm32f10x";
    mcalName = "";
    generatorOutputDirectory = "";

    // Construct AUTOSAR model
    autosarDomModel.setContent(QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<AUTOSAR xmlns=\"http://autosar.org/schema/r4.0\" \
                                       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://autosar.org/schema/r4.0 autosar_4-0-3.xsd\">\n \
            <AR-PACKAGES>\n<AR-PACKAGE>\n<SHORT-NAME>ArcCore</SHORT-NAME>\n<AR-PACKAGES>\n</AR-PACKAGES>\n</AR-PACKAGE>\n</AR-PACKAGES>\n</AUTOSAR>"));

            // Read AUTOSAR model from all ARXML files
            QString autosarModelDir = "arxml";
                               QDir myDir(autosarModelDir);
            QStringList folderList = myDir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);
    //std::cout << "Folders: " << folderList << endl;

    bool exit = false;
    if (guiMode) {
        while (folderList.size() == 0) {
            QMessageBox msgBox;
            msgBox.setText(trUtf8("AUTOSAR ARXML model files are required in \"arxml\" folder in the executable directory. No folders were found."));
            QAbstractButton *myLocateButton = msgBox.addButton(trUtf8("Locate"), QMessageBox::ActionRole);
            QAbstractButton *myExitButton = msgBox.addButton(trUtf8("Exit"), QMessageBox::YesRole);
            msgBox.setIcon(QMessageBox::Question);
            msgBox.exec();

            if(msgBox.clickedButton() == myLocateButton) {
                autosarModelDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"));
                myDir.setPath(autosarModelDir);
                folderList = myDir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);
            } else if (msgBox.clickedButton() == myExitButton) {
                QTimer::singleShot(0, this, SLOT(close()));
                exit = true;
                break;
            }
        }
    } else {
        if (folderList.size() == 0) {
            std::cout << "Error: Unable to locate AUTOSAR ARXML model files. Make sure that \"arxml\" folder exists in the executable directory.\n";
            exit = true;
        }
    }
    if (!exit) {
        for (int folderIter = 0; folderIter < folderList.size(); folderIter++) {
            autosarModelDir = "arxml/" + folderList[folderIter];
            myDir.setPath(autosarModelDir);
            myDir.setNameFilters(QStringList("*.arxml"));
            QStringList filesList = myDir.entryList(QDir::NoDotAndDotDot | QDir::Files);
            //std::cout << "Files: " << filesList << endl;

            for (int xmlFileIter = 0; xmlFileIter < filesList.size(); xmlFileIter++) {
                QFile file(autosarModelDir + "/" + filesList[xmlFileIter]);
                if(!file.open(QIODevice::ReadOnly)) {
                    std::cout << "Error: Unable to open AUTOSAR model file " << autosarModelDir.toStdString() << "/" << filesList[xmlFileIter].toStdString() << "\n";
                    continue;
                }
                QDomDocument tempDoc;
                QString errorMsg;
                int errorLine, errorColumn;
                bool successful = tempDoc.setContent(&file, false, &errorMsg, &errorLine, &errorColumn);
                if (!successful) {
                    std::cout << "Error: Unable to parse file " + autosarModelDir.toStdString() + "/" + filesList[xmlFileIter].toStdString() + ".\nError Message: " + errorMsg.toStdString() + ", Line: " + QString::number(errorLine).toStdString() + ", Column:" + QString::number(errorColumn).toStdString();
                    continue;
                }
                QDomElement modelDomRoot = autosarDomModel.firstChildElement();
                QDomElement tempDomRoot = tempDoc.firstChildElement();
                AppendArxmls(&modelDomRoot, &tempDomRoot);
            }
        }

        std::cout << "AUTOSAR Design Studio, Copyright (C), 2016, Ali Syed\nThis program comes with ABSOLUTELY NO WARRANTY. This is free software and you are welcome to redistribute it under certain conditions; see \'Help->About\' for details.\n\n";

        QDomElement root = autosarDomModel.documentElement();
        OptimizeArxml(&root);

        QStringList references;
        QList<QDomElement> domElems;
        ParseArxmlAndCreateReferences(&autosarDomModel, 0, &references, &domElems);
        CreateQModel(&references, 0, autosarQItemModel->index(0,0), autosarQItemModel, &domElems);

        // Look for all available generic modules
        QString tempStr = "/ArcCore/EcucDefs/Generic";
        GetAllAvailableModulesInMM(tempStr, &genericModules);
        QString moduleNames = genericModules.join(", ");
        std::cout << "Loaded Generic Modules: " << moduleNames.toStdString() << "\n\n";
        std::cout << "Available MCALs: " << availableMcals.join(", ").toStdString() << "\n\n";

        ui->configurationView->setModel(projectQItemModel);
        ui->configurationView->setContextMenuPolicy(Qt::CustomContextMenu);
        ui->configurationView->setHeaderHidden(true);
        ui->configurationView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->configurationView->setSelectionMode(QAbstractItemView::SingleSelection);
        connect(ui->configurationView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(configurationSelectionChanged(QItemSelection,QItemSelection)));

        // Connect context menu for the configurations
        connect(ui->configurationView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onConfigurationContextMenu(const QPoint &)));
        // Connect handler for the configurations context menu item selection
        connect(&moduleContextMenu, SIGNAL(triggered(QAction*)), this, SLOT(onConfigurationContextMenuItemClicked(QAction*)));
    }
}

AutosarGenerator::~AutosarGenerator() {
    delete ui;
}

// TODO: Removes useless elements from the DOM model
void AutosarGenerator::OptimizeArxml(QDomElement *node) {
    QDomElement domElement = node->firstChildElement();
    while(!domElement.isNull()) {
        //std::cout << "TAG: " << childElement.tagName() << endl;
        // Remove DOM elements which we hope we won't use
        if (domElement.tagName() == "IMPLEMENTATION-CONFIG-CLASSES" ||
                domElement.tagName() == "SUPPORTED-CONFIG-VARIANTS" ||
                domElement.tagName() == "ECUC-FUNCTION-NAME-DEF-VARIANTS" ||
                domElement.tagName() == "REFINED-MODULE-DEF-REF") {
            QDomNodeList nodeList = node->childNodes();
            int childrenCount = nodeList.count();
            int i;
            for( i = 0; i < childrenCount; ++i )
                if( nodeList.at(i).isElement() && nodeList.at(i).toElement() == domElement) {
                    nodeList.at(i).parentNode().removeChild(nodeList.at(i));
                    break;
                }
            if (i == childrenCount-1) {
                return;
            } else {
                domElement = nodeList.at(i+1).toElement();
            }
        }
        // Remove UUIDs from ar-package(s). Not sure if we win anything by it.
        else if ((domElement.tagName() == "AR-PACKAGES" || domElement.tagName() == "AR-PACKAGE") && domElement.attribute("UUID", "") != "") {
            domElement.removeAttribute("UUID");
        }
        if (!domElement.firstChildElement().isNull()) {
            OptimizeArxml(&domElement);
        }
        domElement = domElement.nextSiblingElement();
    }
}

void AutosarGenerator::AppendArxmls(QDomElement *destinationRootElem, QDomElement *sourceRootElem)
{
    QDomNodeList shortNames = sourceRootElem->elementsByTagName("SHORT-NAME");
    for (int elemIter = 1; elemIter < shortNames.size(); elemIter++) {
        QString reference = AutosarToolFactory::CreateReferenceFromDomElement(shortNames.at(elemIter).toElement());
        // The reference WILL not have any '/Mcal/' in it, as the references are generated from the SHORT-NAME tags.
        QDomElement referedElement = AutosarToolFactory::GetDomElementFromReference(&reference, *destinationRootElem);
        if (referedElement.isNull()) {
            //std::cout << reference << endl;
            // Look for the parent which is available
            QStringList splitedRef = reference.split("/");
            QDomElement foundRef;
            QString foundReferenceStr = "";
            for (int parentIter = 1; parentIter < splitedRef.size()-1; parentIter++) {
                QStringList newRefList = splitedRef;
                for (int remIter = 0; remIter < parentIter; remIter++) {
                    newRefList.pop_back();
                }
                QString newRef = newRefList.join("/");
                // The reference WILL not have any '/Mcal/' in it, as the references are generated from the SHORT-NAME tags.
                QDomElement parentElement = AutosarToolFactory::GetDomElementFromReference(&newRef, *destinationRootElem);
                if (!parentElement.isNull()) {
                    //std::cout << "Parent found " << newRef << endl;
                    foundRef = parentElement;
                    foundReferenceStr = newRef;
                    break;
                }
            }
            //std::cout << "\tParent tag: " << foundRef.tagName() << ", Elem tag: " << shortNames.at(elemIter).parentNode().toElement().tagName() << endl;
            if (shortNames.at(elemIter).parentNode().toElement().tagName() == "AR-PACKAGE") {
                QDomElement packagesElem = foundRef.firstChildElement("AR-PACKAGES");
                if (packagesElem.isNull()) {
                    //std::cout << "Packages not found. Not tested.\n";
                    QDomElement tempEl = destinationRootElem->ownerDocument().createElement("AR-PACKAGES");
                    foundRef.appendChild(tempEl);
                    packagesElem = foundRef.firstChildElement("AR-PACKAGES");
                }
                //std::cout << "Adding the element.\n";
                packagesElem.appendChild(shortNames.at(elemIter).parentNode().cloneNode());
            } else if (shortNames.at(elemIter).parentNode().toElement().tagName() == "ECUC-MODULE-DEF" ||
                       shortNames.at(elemIter).parentNode().toElement().tagName() == "BSW-IMPLEMENTATION" ||
                       shortNames.at(elemIter).parentNode().toElement().tagName() == "ECUC-MODULE-CONFIGURATION-VALUES") {
                QDomElement packagesElem = foundRef.firstChildElement("ELEMENTS");
                if (packagesElem.isNull()) {
                    //std::cout << "Elements not found. Not tested.\n";
                    QDomElement tempEl = destinationRootElem->ownerDocument().createElement("ELEMENTS");
                    foundRef.appendChild(tempEl);
                    packagesElem = foundRef.firstChildElement("ELEMENTS");
                }
                //std::cout << "Adding the element.\n";
                packagesElem.appendChild(shortNames.at(elemIter).parentNode().cloneNode());
            } else {
                std::cout << "Element type " << shortNames.at(elemIter).parentNode().toElement().tagName().toStdString() << " encountered and no procedure defined for merging.\n";
            }
        }
    }
}

// Note that when this function is called for project file, the parameter type will not be correct (i.e. Uninitialized)
void AutosarGenerator::GetMetaModelIndicators(QDomElement *modelIndex)
{
    description = introduction = destReference = destReferenceType = definitionReference = definitionReferenceType = shortName = "";
    upperMultiplicity = lowerMultiplicity = InvalidMultiplicity;
    hasReferences = hasChoices = hasParameters = hasContainers = false;
    parameterType = Uninitialized;
    floatMin = floatMax = -0.0f;
    intMin = -1;
    intMax = 0;

    if (modelIndex->tagName() == "ECUC-FUNCTION-NAME-DEF") {
        parameterType = Function;
    } else if (modelIndex->tagName() == "ECUC-REFERENCE-DEF") {
        parameterType = Reference;
    } else if (modelIndex->tagName() == "ECUC-FOREIGN-REFERENCE-DEF") {
        parameterType = ForeignReference;
    } else if (modelIndex->tagName() == "ECUC-EXTERNAL-FILE-REFERENCE-DEF") {
        parameterType = ExternalFileReference;
    } else if (modelIndex->tagName() == "ECUC-ENUMERATION-PARAM-DEF") {
        parameterType = Enumeration;
    } else if (modelIndex->tagName() == "ECUC-BOOLEAN-PARAM-DEF") {
        parameterType = Boolean;
    } else if (modelIndex->tagName() == "ECUC-INTEGER-PARAM-DEF") {
        parameterType = Integer;
    } else if (modelIndex->tagName() == "ECUC-FLOAT-PARAM-DEF") {
        parameterType = Float;
    } else if (modelIndex->tagName() == "ECUC-STRING-PARAM-DEF") {
        parameterType = String;
    } else if (modelIndex->tagName() == "ECUC-SYMBOLIC-NAME-REFERENCE-DEF") {
        parameterType = SymbolicNameReference;
    } else if (modelIndex->tagName() == "ECUC-LINKER-SYMBOL-DEF") {
        parameterType = LinkerSymbol;
    } else if (modelIndex->tagName() == "ECUC-CHOICE-REFERENCE-DEF") {
        parameterType = ChoiceReference;
    }

    QDomElement childElement = modelIndex->firstChildElement();
    QDomElement tempElem;
    while (!childElement.isNull()) {
        if (childElement.tagName() == "SHORT-NAME") {
            shortName = childElement.text();
        }
        else if (childElement.tagName() == "DESC") {
            tempElem = childElement.firstChildElement();
            if (tempElem.tagName() == "L-2") {
                description = tempElem.text();
                description.replace("\\n","\n");
            } else {
                std::cout << "Error: DESC tag of " + shortName.toStdString() + " has an incorrect formulation.\n";
            }
        } else if (childElement.tagName() == "INTRODUCTION") {
            tempElem = childElement.firstChildElement();
            tempElem = tempElem.firstChildElement();
            if (tempElem.tagName() == "L-1") {
                introduction = tempElem.text();
                introduction.replace("\\n","\n");
            } else {
                std::cout << "Error: INTRODUCTION tag of " + shortName.toStdString() + " has an incorrect formulation.\n";
            }
        } else if (childElement.tagName() == "DESTINATION-REF") {      // Model File
            destReference = childElement.text();
            destReferenceType = childElement.attribute("DEST");
        } else if (childElement.tagName() == "DESTINATION-TYPE") {
            destReference = childElement.text();
        } else if (childElement.tagName() == "LOWER-MULTIPLICITY") {
            bool okFlag;
            lowerMultiplicity = childElement.text().toInt(&okFlag);
            if (okFlag == false) {
                std::cout << "Error: Model element\'s LOWER-MULTIPLICITY value can not be converted to integer.\n";
            }
        } else if (childElement.tagName() == "UPPER-MULTIPLICITY") {
            bool okFlag;
            upperMultiplicity = childElement.text().toInt(&okFlag);
            if (okFlag == false) {
                std::cout << "Error: Model element\'s UPPER-MULTIPLICITY value can not be converted to integer.\n";
            }
        } else if (childElement.tagName() == "UPPER-MULTIPLICITY-INFINITE") {
            if (childElement.text() == "true") {
                upperMultiplicity = Infinite;
            }
        } else if (childElement.tagName() == "REFERENCES") {
            hasReferences = true;
        } else if (childElement.tagName() == "PARAMETERS" /* For model */ || childElement.tagName() == "PARAMETER-VALUES" /* For project */) {
            hasParameters = true;
        } else if (childElement.tagName() == "CHOICES") {
            hasChoices = true;
        } else if (childElement.tagName() == "CONTAINERS" || childElement.tagName() == "SUB-CONTAINERS") {
            hasContainers = true;
        } else if (childElement.tagName() == "MAX") {
            if (childElement.text() == "Inf") {
                floatMax = Infinite;
            } else {
                bool okFlag;
                if (parameterType == Integer) {
                    //std::cout << "integer " << childElement.text() << endl;
                    intMax = childElement.text().toULongLong(&okFlag);
                } else if (parameterType == Float) {
                    //std::cout << "float " << childElement.text() << endl;
                    floatMax = childElement.text().toDouble(&okFlag);
                } else {
                    std::cout << "Error: Parameter type for MAX value is unknown.\n";
                }
                if (okFlag == false) {
                    std::cout << "Error: Unable to convert model element\'s MAX value to integer/float.\n";
                }
            }
        } else if (childElement.tagName() == "MIN") {
            bool okFlag;
            if (parameterType == Integer) {
                intMin = childElement.text().toLongLong(&okFlag);
            } else if (parameterType == Float) {
                floatMin = childElement.text().toDouble(&okFlag);
            } else {
                std::cout << "Error: Parameter type for MIN value is unknown.\n";
            }
            if (okFlag == false) {
                std::cout << "Error: Unable to convert model element\'s MIN value to integer/float.\n";
            }
        } else if (childElement.tagName() == "DEFINITION-REF") {      // Project File
            definitionReference = childElement.text();
            definitionReferenceType = childElement.attribute("DEST");
        }
        childElement = childElement.nextSiblingElement();
    }
}

void AutosarGenerator::GetLiterals(QStringList *enums, QDomElement childElement) {
    childElement = childElement.firstChildElement("LITERALS");
    QDomElement literalDef = childElement.firstChildElement();
    QString literal;
    QDomElement tempElem;
    while(!literalDef.isNull()) {
        tempElem = literalDef.firstChildElement("SHORT-NAME");
        if (tempElem.isNull()) {
            std::cout << "Error: Short name of the literal cannot be found.\n";
        } else {
            literal = tempElem.text();
            if (literal != "") {
                enums->push_back(literal);
            } else {
                std::cout << "Error: Literal is empty.\n";
            }
        }
        literalDef = literalDef.nextSiblingElement();
    }
}

void AutosarGenerator::GetChoiceReferences(QStringList *refs, QDomElement childElement) {
    childElement = childElement.firstChildElement("DESTINATION-REFS");
    QDomElement choiceRefDef = childElement.firstChildElement();
    QString choiceRef;
    while(!choiceRefDef.isNull()) {
        choiceRef = choiceRefDef.text();
        if (choiceRef != "") {
            refs->push_back(choiceRef);
        } else {
            std::cout << "Error: Destination reference is empty while searching for choice reference.\n";
        }
        choiceRefDef = choiceRefDef.nextSiblingElement();
    }
}

void AutosarGenerator::ParseArxmlAndCreateReferences(QDomNode *node, int parentIndex, QStringList *refs, QList<QDomElement> *domElems) {
    QDomNode domNode = node->firstChild();
    while(!domNode.isNull()) {
        if(domNode.isElement()) {
            QDomElement domElement = domNode.toElement();
            //std::cout << "TAG: " << childElement.tagName() << endl;
            if (domElement.tagName() == "SHORT-NAME" && node->toElement().tagName() != "ECUC-ENUMERATION-LITERAL-DEF" && node->toElement().tagName() != "APPLICATION-ERROR") {
                if (refs->size()==0) {
                    refs->push_back("/" + domElement.text());
                    parentIndex = 0;
                } else {
                    refs->push_back(refs->at(parentIndex) + "/" + domElement.text());
                    parentIndex = refs->size()-1;
                }
                domElems->push_back(node->toElement());
            }
            if (!domElement.firstChildElement().isNull()) {
                ParseArxmlAndCreateReferences(&domElement, parentIndex, refs, domElems);
            }
        }
        domNode = domNode.nextSibling();
    }
}

// Inputs:
// references = list of references to create hierarchy for,
// refIndex = start index in the references list,
// modelIndex = start qmodelindex in the model 'model'.
// model = model to add the qmodel elements to,
// fileIndex = index of the file (in case of opened project files)
void AutosarGenerator::CreateQModel(QStringList *references, int refIndex, QModelIndex modelIndex, QStandardItemModel *model, QList<QDomElement> *domElems, int fileIndex) {
    QStringList ref = references->at(refIndex).split("/");
    QStandardItem *childItem = new QStandardItem (ref.last());
    //std::cout << ref << endl;
    if (ref.size()>2) {
        //std::cout << "Looking for " << ref.at(ref.size()-parentCount-1) << endl;
        while (modelIndex.data().toString() != ref.at(ref.size()-2) && modelIndex.data().toString() != "") {
            //std::cout << "Nope " << modelIndex.data().toString() << endl;
            modelIndex = modelIndex.parent();
        }
        //std::cout << "Yep " << modelIndex.data().toString() << endl;
        if (modelIndex.data().toString() != "") {
            childItem->setData(QVariant::fromValue(domElems->at(refIndex)), Qt::UserRole + 1);
            if (fileIndex != -1) {
                childItem->setData(references->at(refIndex), Qt::UserRole + 2);
                childItem->setData(QString::number(fileIndex), Qt::UserRole + 3);
            }
            // QDomElement selectedDomElement = modelIndex.data(Qt::UserRole + 1).value<QDomElement*>();
            model->itemFromIndex(modelIndex)->setChild(model->itemFromIndex(modelIndex)->rowCount(), childItem);
        } else {
            std::cout << "Error: Parent node " + ref.at(ref.size()-2).toStdString() + " in the reference " + references->at(refIndex).toStdString() + " not found in the model.\n";
        }
    } else {
        childItem->setData(QVariant::fromValue(domElems->at(refIndex)), Qt::UserRole + 1);
        if (fileIndex != -1) {
            childItem->setData(references->at(refIndex), Qt::UserRole + 2);
            childItem->setData(QString::number(fileIndex), Qt::UserRole + 3);
        }
        model->appendRow(childItem);
    }
    refIndex++;
    if (refIndex < references->size()) {
        CreateQModel(references, refIndex, model->indexFromItem(childItem), model, domElems, fileIndex);
    }
}

void AutosarGenerator::ReadAndAppendImplementations(QDomElement *fileRootElement, bool allImplementations)
{
    // Read all references to implementations from the file
    QDomNodeList implementationReferences = fileRootElement->elementsByTagName("MODULE-DESCRIPTION-REF");
    int addImplementations;
    if (allImplementations) {
        addImplementations = implementationReferences.size();
    } else if (implementationReferences.size() > 0) {
        addImplementations = 1;
    } else {
        addImplementations = 0;
    }
    //std::cout << "Found implementations = " << implementationReferences.size() << ". Adding " << addImplementations << " implementations.\n";
    for (int implIter = 0; implIter < addImplementations; implIter++) {
        QString implRef = implementationReferences.at(implIter).toElement().text();
        // The reference WILL not have any '/Mcal/' in it, as the MCAL implementations are only defined for a specific architecture
        QDomElement tempImplementationReference = AutosarToolFactory::GetDomElementFromReference(&implRef, autosarDomModel.documentElement());
        if (!tempImplementationReference.isNull()) {
            // Append it to the found implementations
            bswImplementations.push_back(tempImplementationReference);
            bswImplementationRefs.push_back(implementationReferences.at(implIter));
            //std::cout << "Implementation " << implRef << " added.\n";
        } else {
            std::cout << "Error: Unable to find the implementation " + implRef.toStdString() + " in the AUTOSAR model.\n";
        }
    }

    // Enable validate and generate buttons, only when there exists an implementation in the opened file. Otherwise, disable them.
    QList<QAction*> toolBarActions = ui->mainToolBar->actions();
    if (toolBarActions.size() > 0) {
        for (int menuIter = 0; menuIter < toolBarActions.size(); menuIter++) {
            if (toolBarActions[menuIter]->text() == "Generate" || toolBarActions[menuIter]->text() == "Validate") {
                if (bswImplementations.size() > 0) {
                    toolBarActions[menuIter]->setEnabled(true);
                    //std::cout << "Buttons on." << endl;
                } else {
                    toolBarActions[menuIter]->setEnabled(false);
                    //std::cout << "Buttons off." << endl;
                }
            }
        }
    }
}

void AutosarGenerator::on_actionE_xit_triggered() {
    this->close();
}

void AutosarGenerator::closeEvent (QCloseEvent *event) {
    if (openedFilesSaved.size() > 0) {
        for (int fileIter = 0; fileIter < openedFilesSaved.size(); fileIter++) {
            if (!openedFilesSaved[fileIter]) {
                QMessageBox::StandardButton reply1;
                if (openedFileNames.at(fileIter) != "") {
                    reply1 = QMessageBox::question(this, "Close AUTOSAR Generator", "The ARXML project file " + openedFileNames.at(fileIter).split("/").last() + " is not saved. Do you want to save it before exiting?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                } else {
                    reply1 = QMessageBox::question(this, "Close AUTOSAR Generator", "The new ARXML project file is not saved. Do you want to save it before exiting?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                }
                if (reply1 == QMessageBox::Yes) {
                    on_action_Save_triggered();
                } else if (reply1 == QMessageBox::Cancel) {
                    event->ignore();
                    return;
                }
            }
        }
    }
    event->accept();
}

void AutosarGenerator::on_action_New_triggered() {
    if (openedFilesSaved.size() > 0) {
        for (int fileIter = 0; fileIter < openedFilesSaved.size(); fileIter++) {
            if (!openedFilesSaved[fileIter]) {
                QMessageBox::StandardButton reply1;
                if (openedFileNames.at(fileIter) != "") {
                    reply1 = QMessageBox::question(this, "New ARXML Project", "The ARXML project file " + openedFileNames.at(fileIter).split("/").last() + " is not saved. Do you want to save it?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                } else {
                    reply1 = QMessageBox::question(this, "New ARXML Project", "The ARXML new project file is not saved. Do you want to save it?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                }
                if (reply1 == QMessageBox::Yes) {
                    on_action_Save_triggered();
                } else if (reply1 == QMessageBox::Cancel) {
                    return;
                }
            }
        }
    }
    bswImplementations.clear();
    bswImplementationRefs.clear();
    projectDomModel.clear();
    projectQItemModel->clear();
    openedFileNames.clear();
    openedFilesSaved.clear();
    moduleContextMenu.clear();
    ui->descriptionLabel->setText("");
    ClearLayout(ui->PropsFormLayout->layout());

    openedFileNames.push_back("");
    openedFilesSaved.push_back(false);
    mcalModules.clear();

    // Disable generate and validate buttons
    QList<QAction*> menuBarActions = ui->menuBar->actions();
    if (menuBarActions.size() > 0) {
        for (int menuIter = 0; menuIter < menuBarActions.size(); menuIter++) {
            if (menuBarActions[menuIter]->text() == "Generate" || menuBarActions[menuIter]->text() == "Validate") {
                menuBarActions[menuIter]->setEnabled(false);
            }
        }
    }

    // Create new ARXML file
    QString newFileContent = "<?xml version='1.0' encoding='UTF-8'?>\n<AUTOSAR xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \
            xsi:schemaLocation=\"http://autosar.org/schema/r4.0 autosar_4-0-3.xsd\" xmlns=\"http://autosar.org/schema/r4.0\"> \
            \n\t<AR-PACKAGES>\n\t\t<AR-PACKAGE UUID=\"2432bc86-2fdb-4e6d-8ec9-7876ed7fea96\">\n\t\t<SHORT-NAME>NewProject</SHORT-NAME> \
            <ELEMENTS><ECUC-VALUE-COLLECTION><SHORT-NAME>NewEcu</SHORT-NAME><ADMIN-DATA><SDGS><SDG> \
            <SD GID=\"_ExtensionKey\">MCAL</SD><SD GID=\"_Type\">java.lang.String</SD> \
            <SD GID=\"_Value\">";
    newFileContent += availableMcals.at(0);
    newFileContent += "</SD></SDG><SDG><SD GID=\"_ExtensionKey\">GENERATOR_OUTPUT_DIRECTORY</SD><SD GID=\"_Type\">java.lang.String</SD> \
            <SD GID=\"_Value\">PROJECT_LOC/config</SD></SDG><SDG><SD GID=\"_ExtensionKey\">GENERATOR_OUTPUT_SERVICE_DIRECTORY</SD><SD GID=\"_Type\">java.lang.String</SD> \
            <SD GID=\"_Value\">PROJECT_LOC/config</SD></SDG></SDGS></ADMIN-DATA></ECUC-VALUE-COLLECTION></ELEMENTS></AR-PACKAGE> \
            </AR-PACKAGES></AUTOSAR>";
            mcalName = availableMcals.at(0);
    generatorOutputDirectory = "PROJECT_LOC/config";
    // Find the refered MCAL in the meta-model
    QString tempStr = "/ArcCore/EcucDefs/" + mcalName;
    GetAllAvailableModulesInMM(tempStr, &mcalModules);
    if (mcalModules.size() > 0) {
        QString moduleNames = mcalModules.join(", ");
        std::cout << "Loaded \'" + mcalName.toStdString() + "\' Modules: " + moduleNames.toStdString() << "\n";
    } else {
        std::cout << "Error: MCAL " + mcalName.toStdString() + " is not supported. Error Code: 00002.\n";
    }

    QDomDocument tempDoc;
    tempDoc.setContent(newFileContent);
    projectDomModel.clear();
    projectDomModel.push_back(tempDoc);
    // Parse and create QStandardItemModel
    QStringList references;
    QList<QDomElement> domElems;
    ParseArxmlAndCreateReferences(&projectDomModel[0], 0, &references, &domElems);
    CreateQModel(&references, 0, projectQItemModel->index(0,0), projectQItemModel, &domElems, 0);
    // Enable import ARXML file menu item.

    // Enable import file option
    QList<QAction*> fileMenuActions = ui->menu_File->actions();
    if (fileMenuActions.size() > 0) {
        for (int menuIter = 0; menuIter < fileMenuActions.size(); menuIter++) {
            if (fileMenuActions[menuIter]->text() == "&Import ARXML File") {
                fileMenuActions[menuIter]->setEnabled(true);
                break;
            }
        }
    }
}

void AutosarGenerator::on_action_Open_triggered()
{
    QString fname;
    if (guiMode) {
        if (openedFilesSaved.size() > 0) {
            for (int fileIter = 0; fileIter < openedFilesSaved.size(); fileIter++) {
                if (!openedFilesSaved[fileIter]) {
                    QMessageBox::StandardButton reply1;
                    if (openedFileNames.at(fileIter) != "")
                        reply1 = QMessageBox::question(this, "Open ARXML Project", "The ARXML project file " + openedFileNames.at(fileIter).split("/").last() + " is not saved. Do you want to save it?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                    else
                        reply1 = QMessageBox::question(this, "Open ARXML Project", "The new ARXML project file is not saved. Do you want to save it?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
                    if (reply1 == QMessageBox::Yes)
                        on_action_Save_triggered();
                    else if (reply1 == QMessageBox::Cancel)
                        return;
                }
            }
        }
        fname = QFileDialog::getOpenFileName(this, tr("Open ARXML File"),QString(),tr("AUTOSAR Configuration File (*.arxml)"));
    } else {
        fname = commandLineFileName;
    }
    if (fname.isEmpty())
        return;

    if (fname.split(".").last().toUpper() == "ARXML") {
        // Open specified ARXML file
        QFile f(fname);
        if (!f.open(QFile::ReadOnly | QFile::Text)) {
            std::cout << "Error: Unable to open file " << fname.toStdString() << "\n";
            return;
        }

        bswImplementations.clear();
        bswImplementationRefs.clear();
        moduleContextMenu.clear();
        ClearLayout(ui->PropsFormLayout->layout());
        openedFileNames.clear();
        openedFileNames.push_back(fname);
        openedFilesSaved.clear();
        openedFilesSaved.push_back(true);
        ui->descriptionLabel->setText("");
        QDomDocument tempDoc;
        tempDoc.setContent(&f);
        projectDomModel.clear();
        projectDomModel.push_back(tempDoc);
        mcalModules.clear();
        mcalName = "";

        // Find the MCAL name and generator output directory
        QDomNodeList tempNode = tempDoc.elementsByTagName("ECUC-VALUE-COLLECTION");
        if (tempNode.size() > 0) {
            QDomElement tempElem = tempNode.at(0).firstChildElement("ADMIN-DATA");
            if (!tempElem.isNull()) {
                tempElem = tempElem.firstChildElement();
                QList<QDomElement> extensionKeys, values;
                ParseSdgs(tempElem, &extensionKeys, &values);
                for (int sdgIter = 0; sdgIter < extensionKeys.size(); sdgIter++) {
                    //std::cout << "EXT: " << extensionKeys.at(sdgIter).text() << endl;
                    if (extensionKeys.at(sdgIter).text() == "MCAL") {
                        mcalName = values.at(sdgIter).text();
                        //std::cout << "MCAL: " << mcalName << endl;

                        // Find the refered MCAL in the meta-model
                        QString tempStr = "/ArcCore/EcucDefs/" + mcalName;
                        GetAllAvailableModulesInMM(tempStr, &mcalModules);
                        if (mcalModules.size() > 0) {
                            QString moduleNames = mcalModules.join(", ");
                            std::cout << "Loaded \'" + mcalName.toStdString() + "\' Modules: " + moduleNames.toStdString() << "\n";
                        } else {
                            QMessageBox::information(this, "Error", "MCAL for hardware \"" + mcalName + "\" is not available.", QMessageBox::Ok);
                            return;
                        }
                    } else if (extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_DIRECTORY") {
                        generatorOutputDirectory = values.at(sdgIter).text();
                    }
                }
            }
        }

        // For each ECUC-MODULE-CONFIGURATION-VALUES-REF-CONDITIONAL in the opened ARXML file, check if reference is available. If not, import the reference from the board package in AUTOSAR model.
        ImportModules(&projectDomModel[0]);
        QDomElement fileRoot = projectDomModel[0].documentElement();
        ReadAndAppendImplementations(&fileRoot);

        // Parse and create QStandardItemModel
        QStringList references;
        QList<QDomElement> domElems;
        ParseArxmlAndCreateReferences(&projectDomModel[0], 0, &references, &domElems);
        //std::cout << references << endl;
        projectQItemModel->clear();
        CreateQModel(&references, 0, projectQItemModel->index(0,0), projectQItemModel, &domElems, 0);
        f.close();

        // Enable import ARXML file menu item.
        QList<QAction*> importMenu = ui->menu_File->actions();
        if (importMenu.size() > 0) {
            for (int menuIter = 0; menuIter < importMenu.size(); menuIter++) {
                if (importMenu[menuIter]->text() == "&Import ARXML File") {
                    importMenu[menuIter]->setEnabled(true);
                    break;
                }
            }
        }
    }
}

// For each ECUC-MODULE-CONFIGURATION-VALUES-REF-CONDITIONAL in the ECU Extract file, check if reference is available, if not, import the reference from the board files.
// Called from Import ARXML file
void AutosarGenerator::ImportModules(QDomDocument* domElement) {
    QDomNodeList configurationValues = domElement->elementsByTagName("ECUC-MODULE-CONFIGURATION-VALUES-REF-CONDITIONAL");
    for (int valuesIter = 0; valuesIter < configurationValues.size(); valuesIter++) {
        QDomElement child = configurationValues.at(valuesIter).firstChildElement("ECUC-MODULE-CONFIGURATION-VALUES-REF");
        if (!child.isNull()) {
            QString childText = child.text();
            // The reference WILL not have any '/Mcal/' in it, as the MCAL references will be for specific architecture
            QDomElement referedModelElement = AutosarToolFactory::GetDomElementFromReference(&childText, autosarDomModel.documentElement());
            if (!referedModelElement.isNull()) {
                //std::cout << configurationValues.at(valuesIter).toElement().tagName() << " -> " << configurationValues.at(valuesIter).parentNode().toElement().tagName()
                //<< " -> " << configurationValues.at(valuesIter).parentNode().parentNode().toElement().tagName()
                //<< " -> " << configurationValues.at(valuesIter).parentNode().parentNode().parentNode().toElement().tagName() << endl;
                configurationValues.at(valuesIter).parentNode().parentNode().parentNode().toElement().appendChild(referedModelElement.cloneNode());
            }
        }
    }
}

void AutosarGenerator::on_action_Save_triggered() {
    if (openedFilesSaved.size() > 0) {
        for (int fileIter = 0; fileIter < openedFilesSaved.size(); fileIter++) {
            if (!openedFilesSaved[fileIter]) {
                if (openedFileNames[fileIter] == "") {
                    QString newFileName;
                    newFileName = QFileDialog::getSaveFileName(this, tr("Save ARXML File"),QString(),tr("AUTOSAR Configuration File (*.arxml)"));
                    if (newFileName.isEmpty())
                        return;
                    QString fileExtension = newFileName.split(".").last();
                    //std::cout << "File extension = " << fileExtension << endl;
                    if (fileExtension.toUpper() != "ARXML") {
                        newFileName += ".arxml";
                    }
                    openedFileNames[fileIter] = newFileName;
                }
                SaveFile(fileIter);
            }
        }
    }
}

void AutosarGenerator::SaveFile(int fileIter) {
    QFile file(openedFileNames[fileIter]);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        projectDomModel[fileIter].save(out, 2);             // Write DOM to XML with 2 spaces as tabs
        file.close();
        openedFilesSaved[fileIter] = true;
        std::cout << "File " + openedFileNames.at(fileIter).split("/").last().toStdString() + " is saved.\n";
    } else {
        std::cout << "Error: Unable to write to file " + openedFileNames.at(fileIter).split("/").last().toStdString() + ".\n";
    }
}

void AutosarGenerator::ClearLayout(QLayout *layout) {
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            ClearLayout(item->layout());
            delete item->layout();
        } else if (item->widget()) {
            //layout->removeWidget(item->widget());
            delete item->widget();
            delete item;
        }
    }
}

void AutosarGenerator::configurationSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected) {
    Q_UNUSED( deselected );
    Q_UNUSED( selected );

    QModelIndex selectedElementInQModel = ui->configurationView->selectionModel()->currentIndex();

    if (selectedElementInQModel.isValid()) {
        QDomElement selectedDomElement = selectedElementInQModel.data(Qt::UserRole + 1).value<QDomElement>();
        selectedContainersFileIndex = selectedElementInQModel.data(Qt::UserRole + 3).toInt();
        int fileIndex = selectedContainersFileIndex;
        //std::cout << "File Index: " << fileIndex << endl;

        editingValues.clear();
        moduleContextMenu.clear();
        ClearLayout(ui->PropsFormLayout->layout());
        int propertiesAdded = 0;

        //std::cout << "Tag: " << selectedDomElement.tagName() << endl;
        if (selectedDomElement.tagName() == "ECUC-VALUE-COLLECTION") {
            // Add "Short Name" property in the PropsFormLayout in properties frame
            QHBoxLayout* singleFormLine = new QHBoxLayout();
            QLabel* label1 = new QLabel(this);
            label1->setText("Short Name");
            singleFormLine->addWidget(label1, 20, Qt::AlignRight);
            QLineEdit* lineEdit1 = new QLineEdit(this);
            QDomElement tempElem = selectedDomElement.firstChildElement("SHORT-NAME");
            lineEdit1->setText(tempElem.text());
            editingValues.push_back(tempElem);
            //std::cout << "Adding values: Short Name\n";
            //lineEdit1->setToolTip("");
            singleFormLine->addWidget(lineEdit1, 60);
            connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
            ui->PropsFormLayout->addLayout(singleFormLine);
            propertiesAdded++;

            //std::cout << "ECUC value collection found.\n";
            tempElem = selectedDomElement.firstChildElement("ADMIN-DATA");
            tempElem = tempElem.firstChildElement();        // SDGS
            QList<QDomElement> extensionKeys, values;
            ParseSdgs(tempElem, &extensionKeys, &values);
            for (int sdgIter = 0; sdgIter < extensionKeys.size(); sdgIter++) {
                // Make label
                QHBoxLayout* singleFormLine = new QHBoxLayout();
                QLabel* label1 = new QLabel(this);
                label1->setText(extensionKeys.at(sdgIter).text());
                singleFormLine->addWidget(label1, 20, Qt::AlignRight);
                if (extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_DIRECTORY" || extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_SERVICE_DIRECTORY") {
                    // Make lineEdit
                    QLineEdit* lineEdit1 = new QLineEdit(this);
                    lineEdit1->setText(values.at(sdgIter).text());
                    editingValues.push_back(values.at(sdgIter));
                    //std::cout << "Adding values: " << label1->text() << endl;
                    //lineEdit1->setReadOnly(true);
                    QAction *action = lineEdit1->addAction(QIcon(":/icons/images/ref.svg"), QLineEdit::TrailingPosition);
                    action->setWhatsThis("Ref SaveFolderDialog");
                    connect(action, SIGNAL(triggered(bool)), this, SLOT(propertiesRefClicked(bool)));
                    connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                    singleFormLine->addWidget(lineEdit1, 60);
                } else if (extensionKeys.at(sdgIter).text() == "MCAL") {
                    // Make ComboBox
                    QComboBox* combo1 = new QComboBox(this);
                    combo1->addItems(availableMcals);
                    //std::cout << "Adding values: " << label1->text() << endl;
                    singleFormLine->addWidget(combo1, 60);
                    int returnedIndex = combo1->findText(values.at(sdgIter).text());
                    if (returnedIndex != -1) {
                        combo1->setCurrentIndex(returnedIndex);
                    } else {
                        std::cout << "Error: MCAL " + values.at(sdgIter).text().toStdString() + " not supported.\n";
                    }
                    editingValues.push_back(values.at(sdgIter));
                    connect(combo1, SIGNAL(currentTextChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                } else {
                    std::cout << "Error: Unknown Extension Key: " + extensionKeys.at(sdgIter).text().toStdString() + ".\n";
                }
                ui->PropsFormLayout->addLayout(singleFormLine);
                propertiesAdded++;
            }
            ui->descriptionLabel->setText("");
        } else if (selectedDomElement.tagName() == "AR-PACKAGE") {
            // Add "Short Name" property in the PropsFormLayout in properties frame
            QHBoxLayout* singleFormLine = new QHBoxLayout();
            QLabel* label1 = new QLabel(this);
            label1->setText("Short Name");
            singleFormLine->addWidget(label1, 20, Qt::AlignRight);
            QLineEdit* lineEdit1 = new QLineEdit(this);
            QDomElement tempElem = selectedDomElement.firstChildElement("SHORT-NAME");
            lineEdit1->setText(tempElem.text());
            editingValues.push_back(tempElem);
            //std::cout << "Adding values: Short Name\n";
            singleFormLine->addWidget(lineEdit1, 60);
            connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
            ui->PropsFormLayout->addLayout(singleFormLine);
            propertiesAdded++;
            ui->descriptionLabel->setText("");
        } else {
            // Get container reference
            GetMetaModelIndicators(&selectedDomElement);
            // Keep a copy of indicators which we use in this function as this function is called multiple times and it can modify the stored indicator values
            QString shortNameProjectLocal = shortName;
            QString definitionReferenceProjectLocal = definitionReference;
            // The rest are not important or perhaps not available in the project file

            if (definitionReferenceProjectLocal != "") {
                //std::cout << "refered link " << definitionReferenceProjectLocal << endl;
                // Get container meta-model element
                // The reference WILL not have any '/Mcal/' in it, as the project file only contains references to architecture specific modules
                QDomElement referedDomElement = AutosarToolFactory::GetDomElementFromReference(&definitionReferenceProjectLocal, autosarDomModel.documentElement());
                GetMetaModelIndicators(&referedDomElement);
                ui->descriptionLabel->setText("Selected Model Element: " + definitionReferenceProjectLocal.split("/").last() + "\n\n" + description + "\n" + introduction);
                //std::cout << "Refered DOM Element: " << shortName << description << endl;
                // Keep a copy of indicators which we use in this function as this function is called multiple times and it can modify the stored indicator values
                bool hasReferencesModelLocal = hasReferences;
                bool hasParametersModelLocal = hasParameters;

                if (shortNameProjectLocal != "") {
                    // Add "Short Name" property in the PropsFormLayout in properties frame
                    QHBoxLayout* singleFormLine = new QHBoxLayout();
                    QLabel* label1 = new QLabel(this);
                    label1->setText("Short Name");
                    singleFormLine->addWidget(label1, 20, Qt::AlignRight);
                    QLineEdit* lineEdit1 = new QLineEdit(this);
                    lineEdit1->setText(shortNameProjectLocal);
                    lineEdit1->setAccessibleDescription(description + "\n" + introduction);
                    lineEdit1->installEventFilter(this);
                    QDomElement tempElem = selectedDomElement.firstChildElement("SHORT-NAME");
                    editingValues.push_back(tempElem);
                    //std::cout << "Adding values: Short Name\n";
                    //lineEdit1->setToolTip("");
                    singleFormLine->addWidget(lineEdit1, 60);
                    connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                    ui->PropsFormLayout->addLayout(singleFormLine);
                    propertiesAdded++;
                }

                // Add all parameters or references in the PropsFormLayout in properties frame
                for (int loopIter = 0; loopIter < 2; loopIter++) {       // 2, once for parameters and once for references
                    QDomElement childElement;
                    if (loopIter == 0) {
                        if (hasParametersModelLocal)
                            childElement = referedDomElement.firstChildElement("PARAMETERS");
                        else
                            continue;
                    } else if (loopIter == 1) {
                        if (hasReferencesModelLocal) {
                            childElement = referedDomElement.firstChildElement("REFERENCES");
                        }
                        else
                            continue;
                    }
                    childElement = childElement.firstChildElement();
                    QDomElement elementRoot;
                    if (loopIter == 0) {
                        elementRoot = selectedDomElement.firstChildElement("PARAMETER-VALUES");
                        if (elementRoot.isNull()) {
                            QDomElement newRoot = projectDomModel[fileIndex].createElement("PARAMETER-VALUES");
                            QDomNode tempNode;
                            tempNode = selectedDomElement.appendChild(newRoot);
                            elementRoot = tempNode.toElement();
                        }
                    } else if (loopIter == 1) {
                        elementRoot = selectedDomElement.firstChildElement("REFERENCE-VALUES");
                        if (elementRoot.isNull()) {
                            QDomElement newRoot = projectDomModel[fileIndex].createElement("REFERENCE-VALUES");
                            QDomNode tempNode;
                            tempNode = selectedDomElement.appendChild(newRoot);
                            elementRoot = tempNode.toElement();
                        }
                    }
                    while (!childElement.isNull()) {
                        GetMetaModelIndicators(&childElement);
                        QList<QDomElement> foundReferences;
                        ManualImmMatchRefDomModel(&foundReferences, &elementRoot, shortName);
                        //std::cout << foundReferences.size() << " paremeters/reference for " + shortName + " found in project file.\n";
                        if (foundReferences.size() == 0) {
                            // The reference does not exist in the project file. Add the parameter/reference to the DOM. This is required for when a user edits the value.
                            QDomElement valueRoot, defRef, valRef;
                            QDomText refText, valRefText;
                            if (loopIter == 0) {
                                if (parameterType == Integer || parameterType == Boolean || parameterType == Float) {
                                    valueRoot = projectDomModel[fileIndex].createElement("ECUC-NUMERICAL-PARAM-VALUE");
                                } else if (parameterType == Enumeration || parameterType == String || parameterType == Function || parameterType == LinkerSymbol) {
                                    valueRoot = projectDomModel[fileIndex].createElement("ECUC-TEXTUAL-PARAM-VALUE");
                                } else {
                                    std::cout << "Error: Creating parameter values for unknown type " + QString::number(parameterType).toStdString() + ".\n";
                                    return;
                                }
                                defRef = projectDomModel[fileIndex].createElement("DEFINITION-REF");
                                if (parameterType == Integer) {
                                    defRef.setAttribute("DEST", "ECUC-INTEGER-PARAM-DEF");
                                } else if (parameterType == Boolean) {
                                    defRef.setAttribute("DEST", "ECUC-BOOLEAN-PARAM-DEF");
                                } else if (parameterType == Float) {
                                    defRef.setAttribute("DEST", "ECUC-FLOAT-PARAM-DEF");
                                } else if (parameterType == Enumeration) {
                                    defRef.setAttribute("DEST", "ECUC-ENUMERATION-PARAM-DEF");
                                } else if (parameterType == String) {
                                    defRef.setAttribute("DEST", "ECUC-STRING-PARAM-DEF");
                                } else if (parameterType == Function) {
                                    defRef.setAttribute("DEST", "ECUC-FUNCTION-NAME-DEF");
                                } else if (parameterType == LinkerSymbol) {
                                    defRef.setAttribute("DEST", "ECUC-LINKER-SYMBOL-DEF");
                                }
                                refText = projectDomModel[fileIndex].createTextNode(definitionReferenceProjectLocal + "/" + shortName);
                                defRef.appendChild(refText);
                                valRef = projectDomModel[fileIndex].createElement("VALUE");
                                QDomElement defaultElement = childElement.firstChildElement("DEFAULT-VALUE");
                                if (!defaultElement.isNull()) {
                                    valRefText = projectDomModel[fileIndex].createTextNode(defaultElement.text());
                                } else {
                                    valRefText = projectDomModel[fileIndex].createTextNode("");
                                }
                                valRef.appendChild(valRefText);
                                valueRoot.appendChild(defRef);
                                valueRoot.appendChild(valRef);
                                elementRoot.appendChild(valueRoot);
                                //std::cout << valueRoot.isNull() << valRef.isNull() << elementRoot.isNull() << shortName << endl;
                            } else if (loopIter == 1) {
                                valueRoot = projectDomModel[fileIndex].createElement("ECUC-REFERENCE-VALUE");
                                defRef = projectDomModel[fileIndex].createElement("DEFINITION-REF");
                                if (parameterType == ChoiceReference) {
                                    defRef.setAttribute("DEST", "ECUC-CHOICE-REFERENCE-DEF");
                                } else if (parameterType == ForeignReference) {
                                    defRef.setAttribute("DEST", "ECUC-FOREIGN-REFERENCE-DEF");
                                } else if (parameterType == ExternalFileReference) {
                                    defRef.setAttribute("DEST", "ECUC-EXTERNAL-FILE-REFERENCE-DEF");
                                } else if (parameterType == SymbolicNameReference) {
                                    defRef.setAttribute("DEST", "ECUC-SYMBOLIC-NAME-REFERENCE-DEF");
                                } else if (parameterType == Reference) {
                                    defRef.setAttribute("DEST", "ECUC-REFERENCE-DEF");
                                }
                                refText = projectDomModel[fileIndex].createTextNode(definitionReferenceProjectLocal + "/" + shortName);
                                defRef.appendChild(refText);
                                valRef = projectDomModel[fileIndex].createElement("VALUE-REF");
                                QDomElement defaultElement = childElement.firstChildElement("DEFAULT-VALUE");
                                if (!defaultElement.isNull()) {
                                    valRefText = projectDomModel[fileIndex].createTextNode(defaultElement.text());
                                } else {
                                    valRefText = projectDomModel[fileIndex].createTextNode("");
                                }
                                if (parameterType != ExternalFileReference)
                                    valRef.setAttribute("DEST", "ECUC-CONTAINER-VALUE");
                                valRef.appendChild(valRefText);
                                valueRoot.appendChild(defRef);
                                valueRoot.appendChild(valRef);
                                elementRoot.appendChild(valueRoot);
                                //std::cout << valueRoot.isNull() << valRef.isNull() << elementRoot.isNull() << shortName << endl;
                            }
                            foundReferences.push_back(valueRoot);
                            //std::cout << foundReferences.size() << endl;
                        }
                        for (int lineIter = 0; lineIter < foundReferences.size(); lineIter++) {
                            QHBoxLayout* singleFormLine = new QHBoxLayout();
                            QLabel* label1 = new QLabel(this);
                            QString tempShortName = shortName;
                            AddSpaces(tempShortName);
                            label1->setText(tempShortName);
                            singleFormLine->addWidget(label1, 20, Qt::AlignRight);
                            QLineEdit* lineEdit1 = new QLineEdit(this);
                            //std::cout << "Property " << shortName << ", type: " << parameterType << endl;
                            // parameterType is Enumeration/Boolean/Integer only for parameters, i.e. not references
                            // TODO: Below code can be optimized. The combo box has 2 different cases. Make them 1.
                            if (parameterType == Enumeration) {                   // If combo box with special values
                                QComboBox* combo1 = new QComboBox(this);
                                QStringList enums;
                                GetLiterals(&enums, childElement);
                                if (enums.size() == 0) {
                                    std::cout << "Error: Enumeration type " << childElement.firstChildElement("SHORT-NAME").text().toStdString() << " contains no literals in the model.\n";
                                    continue;
                                }
                                //std::cout << "Literals " << enums << endl;
                                combo1->addItems(enums);
                                combo1->setAccessibleDescription(description + "\n" + introduction);
                                combo1->installEventFilter(this);
                                singleFormLine->addWidget(combo1, 60);
                                QDomElement defaultElement = childElement.firstChildElement("DEFAULT-VALUE");
                                if (!defaultElement.isNull()) {
                                    //std::cout << "default value enumeration " << defaultElement.text() << endl;
                                    combo1->setCurrentIndex(combo1->findText(defaultElement.text()));
                                }
                                if (foundReferences.size() > 0) {
                                    QDomElement valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                    //std::cout << "enumeration text " << foundReferences[lineIter].text() << endl;
                                    if (valueElem.isNull()) {
                                        QDomElement tempElement = projectDomModel[fileIndex].createElement("VALUE");
                                        QDomText tempText;
                                        if (!defaultElement.isNull()) {
                                            tempText = projectDomModel[fileIndex].createTextNode(defaultElement.text());
                                        } else {
                                            tempText = projectDomModel[fileIndex].createTextNode("");
                                        }
                                        tempElement.appendChild(tempText);
                                        foundReferences[lineIter].appendChild(tempElement);
                                        valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                    }
                                    if (!valueElem.isNull()) {
                                        if (valueElem.text() == "") {
                                            editingValues.push_back(valueElem);
                                            //std::cout << "Adding values: " << shortName << endl;
                                        } else {
                                            int returnedIndex = combo1->findText(valueElem.text());
                                            if (returnedIndex != -1) {
                                                editingValues.push_back(valueElem);
                                                //std::cout << "Adding values: " << shortName << endl;
                                                combo1->setCurrentIndex(returnedIndex);
                                            } else {
                                                std::cout << "Error: Value " + valueElem.text().toStdString() + " cannot be found in " + shortName.toStdString() + ".\n";
                                            }
                                        }
                                    } else {
                                        std::cout << "Error: Enumeration " + shortName.toStdString() + " not found in the project file.\n";
                                        continue;
                                    }
                                }
                                connect(combo1, SIGNAL(currentTextChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                            } else if (parameterType == Boolean) {                 // If combo box with true/false
                                QComboBox* combo1 = new QComboBox(this);
                                QStringList enums;
                                enums << "false" << "true";
                                combo1->addItems(enums);
                                combo1->setAccessibleDescription(description + "\n" + introduction);
                                combo1->installEventFilter(this);
                                singleFormLine->addWidget(combo1, 60);
                                QDomElement defaultElement = childElement.firstChildElement("DEFAULT-VALUE");
                                if (!defaultElement.isNull()) {
                                    //std::cout << "default value boolean " << defaultElement.text() << endl;
                                    if (defaultElement.text() == "1") {
                                        combo1->setCurrentIndex(1);
                                    } else {
                                        combo1->setCurrentIndex(0);
                                    }
                                }
                                QDomElement valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                if (valueElem.isNull()) {
                                    QDomElement tempElement = projectDomModel[fileIndex].createElement("VALUE");
                                    QDomText tempText;
                                    if (!defaultElement.isNull()) {
                                        tempText = projectDomModel[fileIndex].createTextNode(defaultElement.text());
                                    } else {
                                        tempText = projectDomModel[fileIndex].createTextNode("");
                                    }
                                    tempElement.appendChild(tempText);
                                    foundReferences[lineIter].appendChild(tempElement);
                                    valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                }
                                if (!valueElem.isNull()) {
                                    editingValues.push_back(valueElem);
                                    //std::cout << "Adding values: " << shortName << endl;
                                    if (valueElem.text() == "1") {
                                        combo1->setCurrentIndex(1);
                                    } else {
                                        combo1->setCurrentIndex(0);
                                    }
                                } else {
                                    std::cout << "Error: Value for the boolean property " + shortName.toStdString() + " does not exist.\n";
                                }
                                connect(combo1, SIGNAL(currentTextChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                            } else {
                                lineEdit1->setAccessibleDescription(description + introduction);
                                lineEdit1->installEventFilter(this);
                                //std::cout << "Trying " << shortName << endl;
                                QDomElement valueElem;
                                if (loopIter == 0) {
                                    valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                } else if (loopIter == 1) {
                                    valueElem = foundReferences[lineIter].firstChildElement("VALUE-REF");
                                }
                                if (valueElem.isNull()) {
                                    QDomElement tempElement;
                                    if (loopIter == 0) {
                                        tempElement = projectDomModel[fileIndex].createElement("VALUE");
                                    } else if (loopIter == 1) {
                                        tempElement = projectDomModel[fileIndex].createElement("VALUE-REF");
                                        if (parameterType != ExternalFileReference)
                                            tempElement.setAttribute("DEST", "ECUC-CONTAINER-VALUE");
                                    }
                                    QDomText tempText;
                                    QDomElement defaultElement = childElement.firstChildElement("DEFAULT-VALUE");
                                    if (!defaultElement.isNull()) {
                                        tempText = projectDomModel[fileIndex].createTextNode(defaultElement.text());
                                    } else {
                                        tempText = projectDomModel[fileIndex].createTextNode("");
                                    }
                                    tempElement.appendChild(tempText);
                                    foundReferences[lineIter].appendChild(tempElement);
                                    if (loopIter == 0) {
                                        valueElem = foundReferences[lineIter].firstChildElement("VALUE");
                                    } else if (loopIter == 1) {
                                        valueElem = foundReferences[lineIter].firstChildElement("VALUE-REF");
                                    }
                                }
                                //std::cout << "child tag " << foundReferences[lineIter].tagName() << ", text: " << foundReferences[lineIter].text() << "label text " << valueElem.text() << endl;
                                if (!valueElem.isNull()) {
                                    editingValues.push_back(valueElem);
                                    //std::cout << "Adding values: " << shortName << endl;
                                    lineEdit1->setText(valueElem.text());
                                } else {
                                    std::cout << "Error: Value for generic parameter " + shortName.toStdString() + " does not exist.\n";
                                }
                                singleFormLine->addWidget(lineEdit1, 60);
                                if (loopIter == 1) { // Add reference button
                                    //lineEdit1->setReadOnly(true);
                                    QAction *action = lineEdit1->addAction(QIcon(":/icons/images/ref.svg"), QLineEdit::TrailingPosition);

                                    if (parameterType == ChoiceReference) {
                                        QStringList defRefs;
                                        GetChoiceReferences(&defRefs, childElement);
                                        //std::cout << "Refs: " << defRefs << ", Size: " << defRefs.size() << endl;
                                        action->setWhatsThis("Ref " + defRefs.join(" "));
                                    } else if (parameterType == Reference || parameterType == SymbolicNameReference || parameterType == ForeignReference) {
                                        action->setWhatsThis("Ref " + destReference);
                                    } else if (parameterType == ExternalFileReference) {
                                        action->setWhatsThis("Ref OpenFileDialog " + destReference);
                                    }
                                    connect(action, SIGNAL(triggered(bool)), this, SLOT(propertiesRefClicked(bool)));
                                }
                                connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                            }
                            //if (upperMultiplicity == Infinite)
                            if (upperMultiplicity != 1) {
                                QPushButton* butt1 = new QPushButton(this);
                                butt1->setIcon(QIcon(":/icons/images/add_item.png"));
                                butt1->setMaximumWidth(28);
                                butt1->setWhatsThis("+ " + definitionReferenceProjectLocal + "/" + shortName);
                                //singleFormLine->setStretch(1, singleFormLine->stretch(1)-5);
                                connect(butt1, SIGNAL(clicked(bool)), this, SLOT(propertiesButtonClicked(bool)));
                                singleFormLine->addWidget(butt1, 5);
                                if (lineIter >= lowerMultiplicity) {
                                    QPushButton* butt2 = new QPushButton(this);
                                    butt2->setIcon(QIcon(":/icons/images/remove_item.png"));
                                    butt2->setMaximumWidth(28);
                                    butt2->setWhatsThis("- " + definitionReferenceProjectLocal + "/" + shortName);
                                    //singleFormLine->setStretch(1, singleFormLine->stretch(1)-5);
                                    connect(butt2, SIGNAL(clicked(bool)), this, SLOT(propertiesButtonClicked(bool)));
                                    singleFormLine->addWidget(butt2, 5);
                                }
                            } /*else if (upperMultiplicity == 1) {

                            } else {
                                std::cout << "Error: Upper Multiplicity \'" + QString::number(upperMultiplicity) + "\' for property " + shortName + " not supported.\n";
                                return;
                            }*/
                            if (parameterType == Integer) {
                                if (intMin != -1 && intMax != 0) {
                                    lineEdit1->setValidator(new MyIntValidator(intMin, intMax, this));
                                } else {
                                    lineEdit1->setValidator(new QIntValidator(this));
                                }
                            } else if (parameterType == Float) {
                                if (floatMin != -0.0f && floatMax != -0.0f && floatMax != Infinite) {
                                    lineEdit1->setValidator(new QDoubleValidator(floatMin, floatMax, 2, this));
                                } else {
                                    lineEdit1->setValidator(new QDoubleValidator(this));
                                }
                            } else if (parameterType == String ||
                                     parameterType == Enumeration ||
                                     parameterType == Boolean ||
                                     parameterType == Reference ||
                                     parameterType == ForeignReference ||
                                     parameterType == SymbolicNameReference ||
                                     parameterType == ChoiceReference ||
                                     parameterType == Function ||
                                     parameterType == LinkerSymbol ||
                                     parameterType == ExternalFileReference) {

                            } else {
                                std::cout << "Error: Invalid data type specified in the model file for the property \'" + shortName.toStdString() + "\'.\n";
                            }
                            ui->PropsFormLayout->addLayout(singleFormLine);
                            propertiesAdded++;
                        }
                        childElement = childElement.nextSiblingElement();
                    }
                }
            }
        }
        if (propertiesAdded == 0) {
            // Add a label in the PropsFormLayout in properties frame to display that the element has no properties
            QLabel* label1 = new QLabel(this);
            label1->setText("There exists no property for the configuration/module " + selectedElementInQModel.data().toString() + ".");
            ui->PropsFormLayout->addWidget(label1);
        }
        if (propertiesAdded != editingValues.size()) {
            std::cout << "Error: AddedProperties: " + QString::number(propertiesAdded).toStdString() + ", EditingValues: " + QString::number(editingValues.size()).toStdString() + ".\n";
        }
        ui->PropsFormLayout->addStretch();
    }
}

void AutosarGenerator::onConfigurationContextMenu(const QPoint &point) {
    if (ui->configurationView->indexAt(point).isValid()) {
        QModelIndex selectedElementInQModel = ui->configurationView->selectionModel()->currentIndex();
        QDomElement selectedDomElement = selectedElementInQModel.data(Qt::UserRole + 1).value<QDomElement>();
        moduleContextMenu.clear();

        if (selectedDomElement.tagName() != "AR-PACKAGES" && selectedDomElement.tagName() != "AR-PACKAGE" && selectedDomElement.tagName() != "ECUC-VALUE-COLLECTION") {
            // Get container reference
            GetMetaModelIndicators(&selectedDomElement);
            // Keep a copy of indicators which we use in this function as this function is called multiple times and it can modify the stored indicator values
            QString definitionReferenceProjectLocal = definitionReference;

            if (definitionReferenceProjectLocal != "") {
                // Get container meta-model element
                ReplaceMcalInReference(definitionReferenceProjectLocal);
                QDomElement referedDomElement = AutosarToolFactory::GetDomElementFromReference(&definitionReferenceProjectLocal, autosarDomModel.documentElement());
                GetMetaModelIndicators(&referedDomElement);
                // Keep a copy of indicators which we use in this function as this function is called multiple times and it can modify the stored indicator values
                QString shortNameModelLocal = shortName;
                int lowerMultiplicityModelLocal = lowerMultiplicity;
                bool hasChoicesModelLocal = hasChoices;
                bool hasContainersModelLocal = hasContainers;

                if (hasContainersModelLocal) {
                    QDomElement childContainer = referedDomElement.firstChildElement("CONTAINERS");
                    if (childContainer.isNull()) {
                        childContainer = referedDomElement.firstChildElement("SUB-CONTAINERS");
                    }
                    if (!childContainer.isNull()) {
                        childContainer = childContainer.firstChildElement();
                        while (!childContainer.isNull()) {
                            GetMetaModelIndicators(&childContainer);            // Get the upper and lower multiplicity
                            QList<QDomElement> foundReferences;
                            QDomElement elementRoot = selectedDomElement.firstChildElement("CONTAINERS");
                            if (elementRoot.isNull()) {
                                elementRoot = selectedDomElement.firstChildElement("SUB-CONTAINERS");
                            }
                            if (!elementRoot.isNull()) {
                                ManualImmMatchRefDomModel(&foundReferences, &elementRoot, shortName);
                                //std::cout << foundReferences.size() << " paremeters/reference for " + shortName + " found in project file upon right click.\n";
                            }
                            if (upperMultiplicity == Infinite || (upperMultiplicity > 0 && foundReferences.size() < upperMultiplicity)) {
                                QAction *action = moduleContextMenu.addAction("Add " + shortName);
                                action->setWhatsThis(definitionReferenceProjectLocal + "/" + shortName);
                            }
                            childContainer = childContainer.nextSiblingElement();
                        }
                    }
                } else if (hasChoicesModelLocal) {
                    QDomElement choiceContiner = referedDomElement.firstChildElement("CHOICES");
                    QStringList allChoices;
                    bool found = false;
                    while (!choiceContiner.isNull()) {
                        QDomElement childContainer = choiceContiner.firstChildElement();            // individual choice
                        //std::cout << "Choices found, Child container " << childContainer.firstChildElement("SHORT-NAME").text() << endl;
                        while (!childContainer.isNull()) {
                            //std::cout << "Child containers " << childContainer.firstChildElement("SHORT-NAME").text() << endl;
                            allChoices << childContainer.firstChildElement("SHORT-NAME").text();
                            // Upper and lower multiplicities of choice containers are always 1 and 0, respectively.
                            QList<QDomElement> foundReferences;
                            QDomElement elementRoot = selectedDomElement.firstChildElement("CONTAINERS");
                            if (elementRoot.isNull()) {
                                elementRoot = selectedDomElement.firstChildElement("SUB-CONTAINERS");
                            }
                            if (!elementRoot.isNull()) {
                                ManualImmMatchRefDomModel(&foundReferences, &elementRoot, shortName);
                                //std::cout << foundReferences.size() << " paremeters/reference for " + shortName + " found in project file upon right click choice.\n";
                                if (foundReferences.size() > 0) {
                                    found = true;
                                }
                            }
                            childContainer = childContainer.nextSiblingElement();
                        }
                        choiceContiner = choiceContiner.nextSiblingElement("CHOICES");
                    }
                    if (!found) {
                        for (int choiceIter = 0; choiceIter < allChoices.size(); choiceIter++) {
                            QAction *action = moduleContextMenu.addAction("Add " + allChoices.at(choiceIter));
                            action->setWhatsThis(definitionReferenceProjectLocal + "/" + allChoices.at(choiceIter));
                        }
                    }
                }

                // Check all occurances of selected DOM element and add a remove button when necessary
                QList<QDomElement> foundReferences;
                QDomElement elementRoot = selectedDomElement.parentNode().toElement();
                if (!elementRoot.isNull()) {
                    //std::cout << "Checking " << shortNameModelLocal << endl;
                    ManualImmMatchRefDomModel(&foundReferences, &elementRoot, shortNameModelLocal);
                    //std::cout << foundReferences.size() << " same paremeters/reference for " + shortNameModelLocal + " found in project file upon right click.\n";
                }
                if (foundReferences.size() > lowerMultiplicityModelLocal) {
                    QAction *action = moduleContextMenu.addAction("Remove " + selectedDomElement.firstChildElement("SHORT-NAME").text());
                    action->setWhatsThis(selectedElementInQModel.data(Qt::UserRole + 2).toString());
                }
            }
        } else if (selectedDomElement.tagName() == "AR-PACKAGE") {
            // Check if its ELEMENTS have ECUC-VALUE-COLLECTION. If it has, then create model menu entry.
            // If the ADMIN-DATA of the respective ECUC-VALUE-COLLECTION contains _ExtensionKey of MCAL, than create MCAL specific module menu items
            // Else if it contains just the _ExtensionKey of GENERATOR_OUTPUT_DIRECTORY, then create generic module menu items
            bool hasGenOutDir = false;//, hasGenOutSerDir = false;
            QDomElement potentialSdgElement = selectedDomElement.firstChildElement("ELEMENTS");
            if (potentialSdgElement.isNull()) {
                return;
            }
            potentialSdgElement = potentialSdgElement.firstChildElement("ECUC-VALUE-COLLECTION");
            if (potentialSdgElement.isNull()) {
                return;
            }
            potentialSdgElement = potentialSdgElement.firstChildElement("ADMIN-DATA");
            if (potentialSdgElement.isNull()) {
                return;
            }
            potentialSdgElement = potentialSdgElement.firstChildElement("SDGS");
            if (potentialSdgElement.isNull()) {
                return;
            }
            QList<QDomElement> extensionKeys, values;
            ParseSdgs(potentialSdgElement, &extensionKeys, &values);
            for (int sdgIter = 0; sdgIter < extensionKeys.size(); sdgIter++) {
                if (extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_DIRECTORY") {
                    hasGenOutDir = true;
                } /*else if (extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_SERVICE_DIRECTORY") {
                    hasGenOutSerDir = true;
                }*/
            }
            // TODO: Below part can be optimized. The code is going through the opened files more than once.
            if (hasGenOutDir) {
                // Create generic modules menu entries
                QStringList availableModules = genericModules;

                // Check which modules are available in opened files (assuming that all modules have 0 and 1 lower and upper multiplicities, respectively.
                QDomNodeList modules;
                for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                    modules = projectDomModel[fileIter].elementsByTagName("ECUC-MODULE-CONFIGURATION-VALUES");
                    //std::cout << modules.size() << endl;
                    for (int moduleIter = 0; moduleIter < modules.size(); moduleIter++) {
                        QDomElement shortName = modules.at(moduleIter).firstChildElement("SHORT-NAME");
                        if (shortName.isNull()) {
                            std::cout << "Error: Module's name can not be found.\n";
                            continue;
                        }
                        QString nameStr = shortName.text();
                        for (int availIter = 0; availIter < availableModules.size(); availIter++) {
                            if (availableModules.at(availIter) == nameStr) {
                                //std::cout << "Module " << nameStr << " found in the opened files." << endl;
                                availableModules.erase(availableModules.begin()+availIter);
                                break;
                            }
                        }
                    }
                }

                for (int moduleIter = 0; moduleIter < availableModules.size(); moduleIter++) {
                    QAction *action = moduleContextMenu.addAction("Create Generic " + availableModules.at(moduleIter) + " Module");
                    action->setWhatsThis("/ArcCore/EcucDefs/Generic/" + availableModules.at(moduleIter));
                }
            }
            if (mcalName != "") {
                // Create MCAL specific module entries
                QStringList availableModules = mcalModules;

                // Check which modules are available in opened files (assuming that all modules have 0 and 1 lower and upper multiplicities, respectively.
                QDomNodeList modules;
                for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                    modules = projectDomModel[fileIter].elementsByTagName("ECUC-MODULE-CONFIGURATION-VALUES");
                    //std::cout << modules.size() << endl;
                    for (int moduleIter = 0; moduleIter < modules.size(); moduleIter++) {
                        QDomElement shortName = modules.at(moduleIter).firstChildElement("SHORT-NAME");
                        if (shortName.isNull()) {
                            std::cout << "Error: Module's name can not be found.\n";
                            continue;
                        }
                        QString nameStr = shortName.text();
                        for (int availIter = 0; availIter < availableModules.size(); availIter++) {
                            if (availableModules.at(availIter) == nameStr) {
                                //std::cout << "Module " << nameStr << " found in the opened files.\n";
                                availableModules.erase(availableModules.begin()+availIter);
                                break;
                            }
                        }
                    }
                }
                for (int moduleIter = 0; moduleIter < availableModules.size(); moduleIter++) {
                    QAction *action = moduleContextMenu.addAction("Create " + mcalName + " " + availableModules.at(moduleIter) + " Module");
                    action->setWhatsThis("/ArcCore/EcucDefs/" + mcalName + "/" + availableModules.at(moduleIter));
                }
            }
        }
    }
    if (!moduleContextMenu.isEmpty())
        moduleContextMenu.exec(ui->configurationView->mapToGlobal(point));
}

void AutosarGenerator::ManualImmMatchRefDomModel(QList<QDomElement> *returnItemList, QDomElement *root, QString refLastItem) {
    //std::cout << "Called ref search with root " << root->tagName() << " for ref " << reference << endl;
    QDomElement childElement = root->firstChildElement();
    //std::cout << "Child element  " << childElement.tagName() << endl;
    QDomElement definitionRef;
    while (!childElement.isNull()) {
        definitionRef = childElement.firstChildElement("DEFINITION-REF");
        //std::cout << "Ref " << definitionRef.tagName() << definitionRef.text() << endl;
        if (!definitionRef.isNull()) {
            if (definitionRef.text().contains(refLastItem)) {
                QString lastItem = definitionRef.text().split("/").last();
                if (lastItem == refLastItem)
                    returnItemList->push_back(childElement);
            }
        } /*else {
            QMessageBox::information(this, "Error", "Element DEFINITON-REF not found in the " + childElement.tagName() + " element.", QMessageBox::Ok);
        }*/
        childElement = childElement.nextSiblingElement();
    }
}

void AutosarGenerator::onConfigurationContextMenuItemClicked(QAction *action)
{
    // The type of button is defined by action's text (Add/Remove/Create(Module))
    // When the button type is add or create, the WhatsThis text refers to the reference in the meta-model to create
    // When the button type is remove, the WhatsThis text refers to the reference in the project file
    QModelIndex selectedElementInQModel = ui->configurationView->selectionModel()->currentIndex();

    QString what = action->text().split(' ').first();
    if (selectedElementInQModel.isValid()) {
        if (what == "Remove") {
            QMessageBox::StandardButton reply1;
            reply1 = QMessageBox::question(this, action->text(), "Are you sure you want to remove the configuration " + action->text().split(' ').last() + "?", QMessageBox::Yes|QMessageBox::No);
            if (reply1 == QMessageBox::Yes) {
                //QMessageBox::information(this, "Test", "Yes clicked. Config " + sourceIndex.data().toString(), QMessageBox::Ok);
                // Find the file index and remove the selected configuration from the project file
                for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                    QString configurationRef = action->whatsThis();
                    ReplaceMcalInReference(configurationRef);
                    QDomElement referedDomElement = AutosarToolFactory::GetDomElementFromReference(&configurationRef, projectDomModel[fileIter].documentElement());
                    if (!referedDomElement.isNull()) {
                        QDomNodeList implementation = referedDomElement.elementsByTagName("MODULE-DESCRIPTION-REF");
                        std::cout << "Suggestion: Functionality 1 not tested. Please check the bswImplementations if it is removed.\n";
                        if (implementation.size() == 1) {    // Can never be greater than 1
                            QString implRef = implementation.at(0).toElement().text();
                            ReplaceMcalInReference(implRef);
                            QDomElement tempImplementationReference = AutosarToolFactory::GetDomElementFromReference(&implRef, autosarDomModel.documentElement());
                            if (!tempImplementationReference.isNull()) {
                                for (int implIter = 0; implIter < bswImplementations.size(); implIter++) {
                                    if (bswImplementations[implIter] == tempImplementationReference) {
                                        bswImplementations.erase(bswImplementations.begin() + implIter);
                                        bswImplementationRefs.erase(bswImplementationRefs.begin() + implIter);
                                        std::cout << "Suggestion: Functionality 2 not tested. Please check the bswImplementations if it is removed.\n";
                                        break;
                                    }
                                }
                            }
                        } else if (implementation.size() > 1) {
                            std::cout << "Error: More than 1 implementations found while removing the element.\n";
                        }
                        //std::cout << "Element Name: " << referedDomElement.tagName() << endl;
                        referedDomElement.parentNode().removeChild(referedDomElement);
                        break;
                    }
                }
                projectQItemModel->removeRow(selectedElementInQModel.row(), selectedElementInQModel.parent());
            }
            openedFilesSaved[selectedContainersFileIndex] = false;
        } else if (what == "Add" || what == "Create") {
            //qDebug() << "Button: " << action->text() << ", Ref: " << action->whatsThis() << endl;
            QString xml;
            AddRecursiveItemFromMetaModel(action->whatsThis(), QDomElement(), &xml, "");
            if (xml.isEmpty()) {
                std::cout << "Error: The reference to the model element " << action->whatsThis().toStdString() << " is invalid. Please contact the software developer.\n";
                return;
            }
            //qDebug() << "Text: " << xml;
            QDomElement selectedDomElement = selectedElementInQModel.data(Qt::UserRole + 1).value<QDomElement>();
            //qDebug() << "Selected Element: " << AutosarToolFactory::CreateReferenceFromDomElement(selectedDomElement) << "/" << selectedDomElement.firstChildElement("SHORT-NAME").text();
            QDomElement destinationElement;
            if (what == "Create") {
                destinationElement = selectedDomElement.firstChildElement("ELEMENTS");          // assuming that ELEMENTS already exist in the model
                if (destinationElement.isNull()) {
                    std::cout << "Error: ELEMENTS XML element does not exist. The added container might not be complete. Assumption is violated.\n";
                    return;
                }
            } else {
                if (selectedDomElement.tagName() == "ECUC-MODULE-CONFIGURATION-VALUES") {
                    destinationElement = selectedDomElement.firstChildElement("CONTAINERS");
                    if (destinationElement.isNull()) {
                        xml = "<CONTAINERS>" + xml + "</CONTAINERS>";
                        destinationElement = selectedDomElement;
                    }
                } else {
                    destinationElement = selectedDomElement.firstChildElement("SUB-CONTAINERS");
                    if (destinationElement.isNull()) {
                        xml = "<SUB-CONTAINERS>" + xml + "</SUB-CONTAINERS>";
                        destinationElement = selectedDomElement;
                    }
                }
            }
            QDomDocument tempDoc;
            tempDoc.setContent(xml);
            //qDebug() << "Dest: " << AutosarToolFactory::CreateReferenceFromDomElement(destinationElement);
            QDomNode tempNode = destinationElement.appendChild(tempDoc.documentElement().cloneNode());
            //qDebug() << "tempNode = " << AutosarToolFactory::CreateReferenceFromDomElement(tempNode.toElement());

            // Create QModel
            QStringList references;
            QList<QDomElement> domElems;
            ParseArxmlAndCreateReferences(&tempNode, 0, &references, &domElems);
            //qDebug() << "Refers: " << references;
            QString selectedElementReference = selectedElementInQModel.data(Qt::UserRole + 2).toString();
            for (int refIter = 0; refIter < references.size(); refIter++) {
                references[refIter] = selectedElementReference + references[refIter];
            }
            //std::cout << references << ", size: " << domElems.size() << endl;
            int fileIndex = selectedElementInQModel.data(Qt::UserRole + 3).toInt();
            CreateQModel(&references, 0, selectedElementInQModel, projectQItemModel, &domElems, fileIndex);
            openedFilesSaved[selectedContainersFileIndex] = false;

            if (what == "Create") {
                // Add the new implementation
                QDomElement tempElem = tempNode.toElement();
                ReadAndAppendImplementations(&tempElem, false);
            }
            //qDebug() << "";
        } else {
            std::cout << "Error: Invalid button action. Contact the software designer with the error report.\n";
        }
    }
}

// TODO: Not a very good implementation, but works. In case of a problem, will be difficult to debug.
void AutosarGenerator::AddRecursiveItemFromMetaModel(QString reference, QDomElement elementToAdd, QString *projectXml, QString parentRef) {
    if (reference != "") {
        ReplaceMcalInReference(reference);
        elementToAdd = AutosarToolFactory::GetDomElementFromReference(&reference, autosarDomModel.documentElement());
    }
    //std::cout << elementToAdd.tagName() << endl;
    if (!elementToAdd.isNull()) {
        if (elementToAdd.tagName() == "ECUC-MODULE-DEF") {               // A module needs to be created
            QString moduleName = reference.split("/").last();
            *projectXml = "<ECUC-MODULE-CONFIGURATION-VALUES UUID=\"" + GenerateUUID() + "\">\n\t<SHORT-NAME>" + moduleName + "</SHORT-NAME>\n" +
                    "\t<ADMIN-DATA>\n\t\t<SDGS>\n\t\t\t<SDG>\n\t\t\t\t<SD GID=\"_ExtensionKey\">GENERATE_AND_VALIDATE</SD>\n\t\t\t\t" +
                    "<SD GID=\"_Type\">java.lang.String</SD>\n\t\t\t\t<SD GID=\"_Value\">TRUE</SD>\n\t\t\t</SDG>\n\t\t</SDGS>\n\t\t</ADMIN-DATA>\n\t" +
                    "<DEFINITION-REF DEST=\"ECUC-MODULE-DEF\">" + reference + "</DEFINITION-REF>\n\t<IMPLEMENTATION-CONFIG-VARIANT>" +
                    "VARIANT-PRE-COMPILE</IMPLEMENTATION-CONFIG-VARIANT>\n\t<MODULE-DESCRIPTION-REF DEST=\"BSW-IMPLEMENTATION\">";
            QString implementation;
            QDomElement testElem;
            if (!reference.contains("Generic")) {                         // if module is a MCAL module
                implementation = "/ArcCore/Implementations/" + mcalName + "/" + moduleName;
                testElem = AutosarToolFactory::GetDomElementFromReference(&implementation, autosarDomModel.documentElement());
                if (testElem.isNull()) {
                    implementation = "/ArcCore/EcucDefs/Generic/Implementations/" + moduleName;
                    testElem = AutosarToolFactory::GetDomElementFromReference(&implementation, autosarDomModel.documentElement());
                    if (testElem.isNull()) {
                        std::cout << "Error: Implementation for the generic module not found.\n";
                        return;
                    }
                }
            } else {
                implementation = "/ArcCore/EcucDefs/Generic/Implementations/" + moduleName;
                testElem = AutosarToolFactory::GetDomElementFromReference(&implementation, autosarDomModel.documentElement());
                if (testElem.isNull()) {
                    std::cout << "Error: Implementation for the MCAL module not found.\n";
                    return;
                }
            }
            *projectXml += implementation + "</MODULE-DESCRIPTION-REF>\n\t<CONTAINERS>\n";
            if (!elementToAdd.firstChildElement("CONTAINERS").isNull()) {
                AddRecursiveItemFromMetaModel("", elementToAdd.firstChildElement("CONTAINERS").firstChildElement(), projectXml, reference);
            }
            *projectXml += "\n\t</CONTAINERS>\n</ECUC-MODULE-CONFIGURATION-VALUES>";
        } else {
            while (!elementToAdd.isNull()) {
                bool create = false;
                GetMetaModelIndicators(&elementToAdd);
                if (parentRef != "") {
                    //std::cout << lowerMultiplicity << endl;
                    if (lowerMultiplicity > 0) {
                        create = true;
                    }
                } else {
                    create = true;
                }
                if (create) {
                    *projectXml += "\t\t<ECUC-CONTAINER-VALUE UUID=\"" + GenerateUUID() + "\">\n\t\t\t<SHORT-NAME>" + shortName + "</SHORT-NAME>" +
                            "\n\t\t\t<DEFINITION-REF DEST=\"ECUC-PARAM-CONF-CONTAINER-DEF\">";
                    if (reference == "") {
                        *projectXml += parentRef;
                    } else {
                        *projectXml += reference;
                    }
                    if (parentRef != "")
                        *projectXml += "/" + shortName;
                    *projectXml += "</DEFINITION-REF>\n";
                    if (hasContainers) {
                        *projectXml += "\t\t<SUB-CONTAINERS>\n";
                        if (parentRef == "")
                            AddRecursiveItemFromMetaModel("", elementToAdd.firstChildElement("SUB-CONTAINERS").firstChildElement(), projectXml, reference);
                        else
                            AddRecursiveItemFromMetaModel("", elementToAdd.firstChildElement("SUB-CONTAINERS").firstChildElement(), projectXml, parentRef + "/" + shortName);
                        *projectXml += "\t\t</SUB-CONTAINERS>\n";
                    }
                    *projectXml += "\t\t</ECUC-CONTAINER-VALUE>\n";
                    if (parentRef == "" && reference != "") {
                        break;
                    }
                }
                elementToAdd = elementToAdd.nextSiblingElement();
            }
        }
    } else {
        std::cout << "Error: Unable to find the model element while adding the item.\n";
    }
}

QString AutosarGenerator::GenerateUUID() {
    uuid_t uuid;
    char uuid_str[37];
    uuid_generate(uuid);
    uuid_unparse(uuid, uuid_str);
    //QString uuidStr(uuid_str);
    return uuid_str;
}

void AutosarGenerator::AddSpaces(QString &string) {
    for (int charIter = 1; charIter < string.size(); charIter++) {
        if (string.at(charIter).isUpper() && !string.at(charIter-1).isUpper()) {
            string.insert(charIter, " ");
            charIter++;
        }
    }
}

void AutosarGenerator::SetCommandLineFileName(QString clfn) {
    commandLineFileName = clfn;
}

void AutosarGenerator::propertiesRefClicked(bool value) {
    Q_UNUSED(value);

    QAction *act = (QAction *)sender();
    //int buttIndexInPropLayout = -1;
    QString propType;
    QLineEdit* propValueLineEdit;
    for (int propIter = 0; propIter < ui->PropsFormLayout->count(); propIter++) {
        if (ui->PropsFormLayout->itemAt(propIter)->layout()->indexOf(act->parentWidget()) != -1) {
            propType = ((QLabel*)ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(0)->widget())->text();
            propValueLineEdit = ((QLineEdit*)ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(1)->widget());
            //buttIndexInPropLayout = propIter;
            break;
        }
    }

    QString referenceItem = act->whatsThis().split(' ').at(1);
    if (referenceItem == "SaveFolderDialog") {
        QMessageBox::StandardButton reply1 = QMessageBox::No;
        while (reply1 == QMessageBox::No) {
            QString folderName = QFileDialog::getExistingDirectory(this, "Specify " + propType,QString());
            if (folderName.isEmpty())
                return;

            if (QDir(folderName).entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() > 0) {      // Directory is not empty
                reply1 = QMessageBox::question(this, "Warning", "The directory \'" + folderName + "\' is not empty. Upon module generation, the files will be overwritten. Do you want to continue?", QMessageBox::Yes|QMessageBox::No);
                if (reply1 == QMessageBox::No) {
                    continue;
                }
            }
            if (openedFileNames.at(selectedContainersFileIndex) != "") {
                QString projectLocation = openedFileNames.at(selectedContainersFileIndex);
                projectLocation.truncate(projectLocation.lastIndexOf("/"));
                if (folderName.contains(projectLocation)) {
                    folderName.remove(0, projectLocation.size());
                    folderName = "PROJECT_LOC" + folderName;
                }
            }
            propValueLineEdit->setText(folderName+"/");
            break;
        }
    } else if (referenceItem == "OpenFileDialog") {
        QString fileExtension = act->whatsThis().split(' ').at(2);
        QString fname = QFileDialog::getOpenFileName(this, tr("Open TTEthernet File"),QString(),"TTEthernet File (*." + fileExtension + ")");
        if (fname.isEmpty())
            return;
        propValueLineEdit->setText(fname);
    } else {
        QStringList referenceList = act->whatsThis().split(' ');
        referenceList.pop_front();
        for (int refIter = 0; refIter < referenceList.size(); refIter++) {
            ReplaceMcalInReference(referenceList[refIter]);
        }
        //std::cout << "Button Index " << buttIndexInPropLayout << " PropType " << propType << "Ref " << referenceList << endl;
        QStringList foundReferences;
        for (int refIter = 0; refIter < referenceList.size(); refIter++) {
            if (referenceList.at(refIter).contains("/")) {               // parametertype = Reference|ChoiceReference|SymbolicNameReference,
                for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                    QDomNodeList tempNodeList = projectDomModel[fileIter].elementsByTagName("DEFINITION-REF");
                    for (int nodeIter = 0; nodeIter < tempNodeList.size(); nodeIter++) {
                        if (tempNodeList.at(nodeIter).toElement().text() == referenceList[refIter]) {
                            foundReferences.push_back(AutosarToolFactory::CreateReferenceFromDomElement(tempNodeList.at(nodeIter).toElement()));
                        }
                    }
                }
            } else {               // parametertype = ForeignReference
                for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                    //std::cout << referenceList[refIter] << endl;
                    QDomNodeList tempNodeList = projectDomModel[fileIter].elementsByTagName(referenceList[refIter]);
                    for (int nodeIter = 0; nodeIter < tempNodeList.size(); nodeIter++) {
                        foundReferences.push_back(AutosarToolFactory::CreateReferenceFromDomElement(tempNodeList.at(nodeIter).firstChildElement().toElement()));
                    }
                }
            }
        }
        // Create the reference selection dialog
        if (foundReferences.size() > 0) {
            ReferenceEditDialog newDialog;
            newDialog.setModal(true);
            newDialog.setData(foundReferences);
            if (newDialog.exec() == QDialog::Accepted) {
                QString selectedReference;
                selectedReference = newDialog.getSelectedReference();
                if (selectedReference != "") {
                    propValueLineEdit->setText(selectedReference);
                }
            }
        } else {
            std::cout << "Information: No reference found for the refered element.\n";
        }
        //std::cout << "Found references: " << foundReferences << endl;
    }
}

// Upon changing the focus of the properties view, change the description
bool AutosarGenerator::eventFilter(QObject* object, QEvent* event) {
    if(strcmp(object->metaObject()->className(), "QLineEdit") == 0 && event->type() == QEvent::MouseButtonPress) {
        ui->descriptionLabel->setText(((QLineEdit*)object)->accessibleDescription());
        return false; // lets the event continue to the edit
    } else if(strcmp(object->metaObject()->className(), "QComboBox") == 0 && event->type() == QEvent::MouseButtonPress) {
        ui->descriptionLabel->setText(((QComboBox*)object)->accessibleDescription());
        return false; // lets the event continue to the edit
    }
    return false;
}

void AutosarGenerator::propertiesButtonClicked(bool value) {
    Q_UNUSED(value);
    QPushButton *button = (QPushButton *)sender();
    int buttIndexInPropLayout = -1;
    QString propType, propValue;
    for (int propIter = 0; propIter < ui->PropsFormLayout->count(); propIter++) {
        if (ui->PropsFormLayout->itemAt(propIter)->layout()->indexOf(button) != -1) {
            propType = ((QLabel*)ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(0)->widget())->text();
            propValue = ((QLineEdit*)ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(1)->widget())->text();
            buttIndexInPropLayout = propIter;
            break;
        }
    }
    //std::cout << "Button Index " << buttIndexInPropLayout << " PropType " << propType << " PropValue " << propValue << "ButtonType" << button->whatsThis() << endl;

    QStringList info = button->whatsThis().split(" ");
    if (info[0] == "+") {
        if (!((QLineEdit*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget())->text().isEmpty()) {
            // Add a property
            QHBoxLayout* singleFormLine = new QHBoxLayout();
            QLabel* label1 = new QLabel(this);
            label1->setText(propType);
            singleFormLine->addWidget(label1, 20, Qt::AlignRight);
            QLineEdit* lineEdit1 = new QLineEdit(this);
            if (strcmp(ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget()->metaObject()->className(), "QComboBox") == 0) {
                QComboBox* previousCombo = (QComboBox*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget();
                QComboBox* combo1 = new QComboBox(this);
                for (int itemIter = 0; itemIter < previousCombo->count(); itemIter++) {
                    combo1->addItem(previousCombo->itemText(itemIter));
                }
                singleFormLine->addWidget(combo1, 60);
                connect(combo1, SIGNAL(currentTextChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
                combo1->setCurrentIndex(0);
            } else if (strcmp(ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget()->metaObject()->className(), "QLineEdit") == 0) {
                // Create DOM element
                //std::cout << editingValues.at(buttIndexInPropLayout).tagName() << endl;
                QDomNode addedNode = editingValues.at(buttIndexInPropLayout).parentNode().parentNode().appendChild(editingValues.at(buttIndexInPropLayout).parentNode().cloneNode());
                //std::cout << addedNode.toElement().tagName() << endl;
                QDomElement addedValue = addedNode.firstChildElement(editingValues.at(buttIndexInPropLayout).tagName());
                addedValue.firstChild().toText().setData("");
                editingValues.insert(editingValues.begin() + buttIndexInPropLayout + 1, addedValue);
                QLineEdit* previousLineEdit = (QLineEdit*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget();
                singleFormLine->addWidget(lineEdit1, 60);
                if (previousLineEdit->actions().size() != 0) {  // Field is a reference
                    lineEdit1->setReadOnly(true);
                    QAction *action =
                            lineEdit1->addAction(QIcon(":/icons/images/ref.svg"), QLineEdit::TrailingPosition);
                    connect(action, SIGNAL(triggered(bool)), this, SLOT(propertiesRefClicked(bool)));
                    action->setWhatsThis(previousLineEdit->actions().at(0)->whatsThis());
                } else {
                    lineEdit1->setValidator(previousLineEdit->validator());
                }
                connect(lineEdit1, SIGNAL(textChanged(QString)), this, SLOT(propertiesTextChanged(QString)));
            } else {
                std::cout << "Error: Widget addition not supported. Unreachable code.\n";
                return;
            }
            if (ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->count() > 2) {
                QPushButton* previousButton = (QPushButton*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(2)->widget();
                QPushButton* butt1 = new QPushButton(this);
                butt1->setIcon(QIcon(":/icons/images/add_item.png"));
                butt1->setMaximumWidth(28);
                butt1->setWhatsThis(previousButton->whatsThis());
                //singleFormLine->setStretch(1, singleFormLine->stretch(1)-5);
                connect(butt1, SIGNAL(clicked(bool)), this, SLOT(propertiesButtonClicked(bool)));
                singleFormLine->addWidget(butt1, 5);
                if (ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->count() > 3) {
                    QPushButton* previousButton2 = (QPushButton*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(3)->widget();
                    QPushButton* butt2 = new QPushButton(this);
                    butt2->setIcon(QIcon(":/icons/images/remove_item.png"));
                    butt2->setMaximumWidth(28);
                    butt2->setWhatsThis(previousButton2->whatsThis());
                    //singleFormLine->setStretch(1, singleFormLine->stretch(1)-5);
                    connect(butt2, SIGNAL(clicked(bool)), this, SLOT(propertiesButtonClicked(bool)));
                    singleFormLine->addWidget(butt2, 5);
                }
            }
            ui->PropsFormLayout->insertLayout(buttIndexInPropLayout+1, singleFormLine);
            openedFilesSaved[selectedContainersFileIndex] = false;
        }
    } else if (info[0] == "-") {
        // Remove the parameter
        editingValues.at(buttIndexInPropLayout).parentNode().parentNode().removeChild(editingValues.at(buttIndexInPropLayout).parentNode());
        editingValues.erase(editingValues.begin() + buttIndexInPropLayout);

        // Remove the widgets from the PropsFormLayout only if it is not the only property
        int propCount = 0;
        for (int childIter2 = 0; childIter2 < ui->PropsFormLayout->count()-1; childIter2++) {
            if (((QLabel*)ui->PropsFormLayout->itemAt(childIter2)->layout()->itemAt(0)->widget())->text() == propType) {
                propCount++;
            }
        }
        //std::cout << "PropCount " << propCount << endl;
        if (propCount != 1) {
            //std::cout << "Removing layout\n";
            QLayoutItem *item = ui->PropsFormLayout->takeAt(buttIndexInPropLayout);
            ClearLayout(item->layout());            // Item is always a layout
            delete item->layout();
        } else {
            if (strcmp(ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget()->metaObject()->className(), "QComboBox") == 0) {
                ((QComboBox*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget())->setCurrentIndex(0);
            } else if (strcmp(ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget()->metaObject()->className(), "QLineEdit") == 0) {
                ((QLineEdit*)ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget())->setText("");
            } else {
                std::cout << "Error: Widget type \'" + QString(ui->PropsFormLayout->itemAt(buttIndexInPropLayout)->layout()->itemAt(1)->widget()->metaObject()->className()).toStdString() + "\' is not supported for removal. Unreachable code.\n";
            }
        }
        openedFilesSaved[selectedContainersFileIndex] = false;
    } else {
        std::cout << "Error: Invalid button type. Please contact the software designer with the error report.\n";
    }
}

void AutosarGenerator::propertiesTextChanged(QString propValue) {
    QLineEdit *line = (QLineEdit *)sender();
    int propIndexInPropLayout = -1;
    QString propWidgetName;
    QString propType;
    for (int propIter = 0; propIter < ui->PropsFormLayout->count(); propIter++) {
        if (ui->PropsFormLayout->itemAt(propIter)->layout()->indexOf(line) != -1) {
            propType = ((QLabel*)ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(0)->widget())->text();
            propWidgetName = ui->PropsFormLayout->itemAt(propIter)->layout()->itemAt(1)->widget()->metaObject()->className();
            propIndexInPropLayout = propIter;
            break;
        }
    }
    if (propIndexInPropLayout == -1) {
        std::cout << "Error: Property index not found. Unreachable code.\n";
        return;
    }
    //std::cout << "PropType " << propType << " PropValue " << propValue << "propIndex " << propIndexInPropLayout << endl;
    //std::cout << "ElemTag:" << editingValues.at(propIndexInPropLayout).tagName() << "Text: " << editingValues.at(propIndexInPropLayout).text() << endl;
    if (propType == "Short Name") {
        QModelIndex selectedElementInQModel = ui->configurationView->selectionModel()->currentIndex();
        projectQItemModel->setData(selectedElementInQModel, QVariant::fromValue(propValue), Qt::DisplayRole);
        QDomElement selectedDomElement = selectedElementInQModel.data(Qt::UserRole + 1).value<QDomElement>();
        QString tempRef = AutosarToolFactory::CreateReferenceFromDomElement(selectedDomElement);
        tempRef += "/" + propValue;
        //std::cout << "New reference = " << tempRef << endl;
        projectQItemModel->setData(selectedElementInQModel, QVariant::fromValue(tempRef), Qt::UserRole + 2);
    } else if (propType == "MCAL") {
        mcalName = propValue;
        mcalModules.clear();
        //std::cout << "MCAL set to " << mcalName << endl;

        // Find the refered MCAL in the meta-model
        QString tempStr = "/ArcCore/EcucDefs/" + mcalName;
        GetAllAvailableModulesInMM(tempStr, &mcalModules);
        if (mcalModules.size() > 0) {
            QString moduleNames = mcalModules.join(", ");
            std::cout << "Loaded \'" + mcalName.toStdString() + "\' Modules: " + moduleNames.toStdString() << "\n";
        } else {
            std::cout << "Error: MCAL " + mcalName.toStdString() + " is not supported. Error Code: 00004.\n";
        }
    } else if (propType == "GENERATOR_OUTPUT_DIRECTORY") {
        generatorOutputDirectory = propValue;
    }
    for (int childIter = 0; childIter < editingValues[propIndexInPropLayout].childNodes().size(); childIter++) {     // usually just 1 iteration is enough
        if (editingValues[propIndexInPropLayout].childNodes().at(childIter).isText()) {
            if (propWidgetName == "QComboBox") {
                if (propValue == "true")
                    editingValues[propIndexInPropLayout].childNodes().at(childIter).setNodeValue("1");
                else if (propValue == "false")
                    editingValues[propIndexInPropLayout].childNodes().at(childIter).setNodeValue("0");
                else
                    editingValues[propIndexInPropLayout].childNodes().at(childIter).setNodeValue(propValue);
            } else
                editingValues[propIndexInPropLayout].childNodes().at(childIter).setNodeValue(propValue);
            break;
        }
    }
    openedFilesSaved[selectedContainersFileIndex] = false;
}

void AutosarGenerator::on_actionMeta_Model_Editor_triggered() {
    MetaModelViewerDialog* newDialog = new MetaModelViewerDialog(this);
    newDialog->setData(autosarQItemModel);
    newDialog->show();
}

void AutosarGenerator::on_actionImport_ARXML_triggered() {
    QString fname;
    if (guiMode)
        fname = QFileDialog::getOpenFileName(this, tr("Import ARXML File"),QString(),tr("AUTOSAR Configuration File (*.arxml)"));
    else
        fname = commandLineFileName;
    if (fname.isEmpty())
        return;

    if (fname.split(".").last().toUpper() == "ARXML") {
        // Open specified ARXML file
        QFile f(fname);
        if (!f.open(QFile::ReadOnly | QFile::Text)) {
            std::cout << "Error: Unable to open file " + fname.toStdString() + ".\n";
            return;
        }

        // Check if it is an ECU Extract or ECU-VALUEs file
        QTextStream inputFile(&f);
        QString fileContent = inputFile.readAll();
        if (fileContent.contains("ECUC-VALUE-COLLECTION")) {              // Its an ECU-VALUEs file
            f.close();
            // Open specified file again
            if (!f.open(QFile::ReadOnly | QFile::Text)) {
                std::cout << "Error: Unable to open file " + fname.toStdString() + ".\n";
                return;
            }

            QDomDocument tempDoc;
            tempDoc.setContent(&f);

            // Find the MCAL name
            QString tempMcalName = "";
            QDomNodeList tempNode = tempDoc.elementsByTagName("ECUC-VALUE-COLLECTION");
            if (tempNode.size() > 0) {
                QDomElement tempElem = tempNode.at(0).firstChildElement("ADMIN-DATA");
                if (!tempElem.isNull()) {
                    tempElem = tempElem.firstChildElement();
                    QList<QDomElement> extensionKeys, values;
                    ParseSdgs(tempElem, &extensionKeys, &values);
                    for (int sdgIter = 0; sdgIter < extensionKeys.size(); sdgIter++) {
                        //std::cout << "EXT: " << extensionKeys.at(sdgIter).text() << endl;
                        if (extensionKeys.at(sdgIter).text() == "MCAL") {
                            tempMcalName = values.at(sdgIter).text();
                            //std::cout << "MCAL: " << mcalName << endl;
                        } else if (extensionKeys.at(sdgIter).text() == "GENERATOR_OUTPUT_DIRECTORY") {
                            generatorOutputDirectory = values.at(sdgIter).text();
                        }
                    }
                }
            }
            if (tempMcalName != "") {
                if (mcalName != "") {
                    if (mcalName != tempMcalName) {
                        std::cout << "Error: Importing different MCAL files is not supported.\n";
                        return;
                    }
                } else {
                    mcalName = tempMcalName;
                    // Find the refered MCAL in the meta-model
                    QString tempStr = "/ArcCore/EcucDefs/" + mcalName;
                    GetAllAvailableModulesInMM(tempStr, &mcalModules);
                    if (mcalModules.size() > 0) {
                        QString moduleNames = mcalModules.join(", ");
                        std::cout << "Loaded \'" + mcalName.toStdString() + "\' Modules: " + moduleNames.toStdString() << "\n";
                    } else {
                        std::cout << "Error: MCAL " + mcalName.toStdString() + " is not supported. Error Code: 00001.\n";
                        return;
                    }
                }
            }

            ImportModules(&tempDoc);
            QDomElement fileRoot = tempDoc.documentElement();
            ReadAndAppendImplementations(&fileRoot);

            projectDomModel.push_back(tempDoc);
            openedFileNames.push_back(fname);
            openedFilesSaved.push_back(true);

            QStringList references;
            QList<QDomElement> domElems;
            ParseArxmlAndCreateReferences(&projectDomModel[projectDomModel.size()-1], 0, &references, &domElems);
            CreateQModel(&references, 0, projectQItemModel->index(0,0), projectQItemModel, &domElems, projectDomModel.size()-1);
            f.close();

            ui->configurationView->setModel(projectQItemModel);
        } else if (fileContent.contains("ROOT-SW-COMPOSITION-PROTOTYPE")) {           // Its an ECU Extract file
            f.close();
            // Open specified file again
            if (!f.open(QFile::ReadOnly | QFile::Text)) {
                std::cout << "Error: Unable to open file " + fname.toStdString() + ".\n";
                return;
            }

            QDomDocument tempDoc;
            tempDoc.setContent(&f);

            projectDomModel.push_back(tempDoc);
            openedFileNames.push_back(fname);
            openedFilesSaved.push_back(true);

            QStringList references;
            QList<QDomElement> domElems;
            ParseArxmlAndCreateReferences(&projectDomModel[projectDomModel.size()-1], 0, &references, &domElems);
            CreateQModel(&references, 0, projectQItemModel->index(0,0), projectQItemModel, &domElems, projectDomModel.size()-1);
            f.close();

            ui->configurationView->setModel(projectQItemModel);
        } else {
            f.close();
            std::cout << "Error: Not a valid ECU Extract or ECU Values file.\n";
        }
    }
}

void AutosarGenerator::ParseSdgs(QDomElement sdgsElement, QList<QDomElement> *extensionKeys, QList<QDomElement> *values) {
    QDomElement sdgElement = sdgsElement.firstChildElement();
    while (!sdgElement.isNull()) {
        QDomElement sdElement = sdgElement.firstChildElement();
        while (!sdElement.isNull()) {
            QString gid = sdElement.attribute("GID", "");
            if (gid != "") {
                if (gid == "_ExtensionKey") {
                    extensionKeys->push_back(sdElement);
                    //std::cout << "Ext: " << sdElement.text() << endl;
                } else if (gid == "_Value") {
                    values->push_back(sdElement);
                    //std::cout << "Val: " << sdElement.text() << endl;
                }
            }
            sdElement = sdElement.nextSiblingElement();
        }
        sdgElement = sdgElement.nextSiblingElement();   // SDG
    }
}

void AutosarGenerator::GetAllAvailableModulesInMM(QString targetLocation, QStringList *availableModules) {
    QDomElement mMElement = AutosarToolFactory::GetDomElementFromReference(&targetLocation, autosarDomModel.documentElement());
    if (mMElement.isNull()) {
        return;
    }
    mMElement = mMElement.firstChildElement("ELEMENTS");
    if (mMElement.isNull()) {
        return;
    }
    mMElement = mMElement.firstChildElement("ECUC-MODULE-DEF");
    while (!mMElement.isNull()) {
        //std::cout << genericElementsMMElement.tagName() << endl;
        if (mMElement.tagName() == "ECUC-MODULE-DEF") {
            availableModules->append(mMElement.firstChildElement("SHORT-NAME").text());
        }
        mMElement = mMElement.nextSiblingElement();
    }
}

void AutosarGenerator::on_actionS_ave_As_triggered() {
    if (openedFileNames.size() > 0) {
        for (int fileIter = 0; fileIter < openedFilesSaved.size(); fileIter++) {
            QString newFileName;
            QString title;
            if (openedFileNames[fileIter] != "") {
                title = "Save " + openedFileNames.at(fileIter).split("/").last() + " File As ...";
            } else {
                title = "Save File";
            }
            newFileName = QFileDialog::getSaveFileName(this, title,QString(),tr("AUTOSAR Configuration File (*.arxml)"));
            if (newFileName.isEmpty())
                return;
            openedFileNames[fileIter] == newFileName;
            SaveFile(fileIter);
        }
    }
}

void AutosarGenerator::on_moduleSplitter_splitterMoved(int pos, int index) {
    Q_UNUSED(pos);
    Q_UNUSED(index);
    ui->propertiesSplitter->setSizes(ui->moduleSplitter->sizes());
}

void AutosarGenerator::on_propertiesSplitter_splitterMoved(int pos, int index) {
    Q_UNUSED(pos);
    Q_UNUSED(index);
    ui->moduleSplitter->setSizes(ui->propertiesSplitter->sizes());
}

void AutosarGenerator::on_ExpandAllButton_clicked() {
    if (projectQItemModel->hasChildren()) {
        ui->configurationView->expandAll();
    }
}

void AutosarGenerator::on_CollapseAllButton_clicked() {
    if (projectQItemModel->hasChildren()) {
        ui->configurationView->collapseAll();
    }
}

void AutosarGenerator::on_actionValidate_triggered() {
    RunAllGenerators(false);
}

QString AutosarGenerator::ConstructGeneratorString(QDomNode bswImplementation) {
    QString generatorString;
    //std::cout << "Element Tag: " << bswImplementation.toElement().tagName() << endl;
    QDomElement tempElem = bswImplementation.firstChildElement("USED-CODE-GENERATOR");
    if (tempElem.isNull()) {
        std::cout << "Error: Unable to find generator name.\n";
        return "";
    } else {
        //std::cout << "Appending name";
        generatorString += tempElem.text() + ":";
    }
    tempElem = bswImplementation.firstChildElement("SW-VERSION");
    if (tempElem.isNull()) {
        std::cout << "Error: Unable to find generator version.\n";
        return "";
    } else {
        //std::cout << "Appending generator version\n";
        generatorString += tempElem.text() + ":";
    }
    tempElem = bswImplementation.firstChildElement("VENDOR-ID");
    if (tempElem.isNull()) {
        std::cout << "Error: Unable to find generator vendor ID.\n";
        return "";
    } else {
        //std::cout << "Appending generator id\n";
        generatorString += tempElem.text() + ":";
    }
    tempElem = bswImplementation.firstChildElement("AR-RELEASE-VERSION");
    if (tempElem.isNull()) {
        std::cout << "Error: Unable to find AUTOSAR version for generator.\n";
        return "";
    } else {
        //std::cout << "Appending autosar version\n";
        generatorString += tempElem.text();
    }
    return generatorString;
}

void AutosarGenerator::on_actionGenerate_triggered() {
    RunAllGenerators(true);
}

void AutosarGenerator::RunAllGenerators(bool generate)
{
    std::cout << "Running Validator/Generator ----------------------------------------------------------------------------------\n\n";
    int generatedModules = 0;
    for (int implIter = 0; implIter < bswImplementations.size(); implIter++) {
        QString generatorString = ConstructGeneratorString(bswImplementations.at(implIter));;
        //std::cout << "Generator string " << generatorString << endl;
        if (generatorString != "") {
            AbstractCodeGenerator *tempGen = CodeGeneratorFactory::GetGenerator()->CreateGenerator(generatorString);
            QString moduleName = bswImplementations.at(implIter).firstChildElement("SHORT-NAME").text();
            if( tempGen ) {
                // Find the module definition
                QDomElement moduleDef = bswImplementations.at(implIter).firstChildElement("VENDOR-SPECIFIC-MODULE-DEF-REFS");
                if (moduleDef.isNull()) {
                    std::cout << "Error: Unable to find the vendor specific module definition.\n";
                    delete tempGen;
                    continue;
                }
                moduleDef = moduleDef.firstChildElement("VENDOR-SPECIFIC-MODULE-DEF-REF");
                if (moduleDef.isNull()) {
                    std::cout << "Error: Unable to find the vendor specific module definition.\n";
                    delete tempGen;
                    continue;
                }
                QString moduleDefRef = moduleDef.text();
                ReplaceMcalInReference(moduleDefRef);
                moduleDef = AutosarToolFactory::GetDomElementFromReference(&moduleDefRef, autosarDomModel.documentElement());
                if (moduleDef.isNull()) {
                    std::cout << "Error: Unable to find module definition in the AUTOSAR model.\n";
                    delete tempGen;
                    continue;
                }

                std::cout << "Validating " + moduleName.toStdString() + " module...\n";
                QDomElement tempElement = bswImplementationRefs.at(implIter).parentNode().toElement();
                bool success;
                int compositionFileIndex = -1;
                if (generatorString == "@RteGenerator::60:") {
                    // Get extract
                    for (int fileIter = 0; fileIter < projectDomModel.size(); fileIter++) {
                        QDomNodeList compositionElement = projectDomModel[fileIter].documentElement().elementsByTagName("ROOT-SW-COMPOSITION-PROTOTYPE");
                        if (compositionElement.size() > 0) {
                            if (compositionFileIndex == -1)
                                compositionFileIndex = fileIter;
                            else {
                                std::cout << "Error: Multiple root software compositions found for generating RTE.\n";
                                delete tempGen;
                                continue;
                            }
                        }
                    }
                    if (compositionFileIndex == -1) {
                        std::cout << "Error: No root software composition found for generating RTE.\n";
                        delete tempGen;
                        continue;
                    }
                    QDomElement ecuExtractElement = projectDomModel[compositionFileIndex].documentElement();
                    success = dynamic_cast<RteGenerator*>(tempGen)->CheckRteValidity(&tempElement, &moduleDef, &ecuExtractElement);
                } else
                    success = tempGen->CheckModuleValidity(&tempElement, &moduleDef);
                if (!success) {
                    std::cout << "Error: Module " + moduleName.toStdString() + " is invalid.\n\n";
                    delete tempGen;
                    continue;
                }
                std::cout << "Module " + moduleName.toStdString() + " is valid.\n";
                if (generate) {
                    // Look for folder to generate file into
                    if (generatorOutputDirectory.isEmpty()) {
                        std::cout << "Error: Unable to find the key GENERATOR_OUTPUT_DIRECTORY in the opened project files.\n\n";
                        delete tempGen;
                        continue;
                    }
                    if (generatorOutputDirectory.contains("PROJECT_LOC")) {
                        QString projectLocation = openedFileNames.at(0);                        // TODO: Do it based on the containing file index.
                        projectLocation.truncate(projectLocation.lastIndexOf("/"));
                        generatorOutputDirectory.replace("PROJECT_LOC", projectLocation);
                    }
                    std::cout << "\nGenerating " + moduleName.toStdString() + " module...\n";
                    success = false;
                    if (generatorString == "@RteGenerator::60:") {
                        QDomElement ecuExtractElement = projectDomModel[compositionFileIndex].documentElement();
                        // Find COM/OS modules
                        QList <int> comImpIndex, osImpIndex;
                        for (int impIter = 0; impIter < bswImplementations.size(); impIter++) {
                            if (bswImplementations.at(impIter).firstChildElement("SHORT-NAME").text() == "Com") {
                                comImpIndex.push_back(impIter);
                            } else if (bswImplementations.at(impIter).firstChildElement("SHORT-NAME").text() == "Os") {
                                osImpIndex.push_back(impIter);
                            }
                        }
                        if (comImpIndex.size() > 1 || osImpIndex.size() > 1) {
                            QMessageBox::information(this, "Error", "Multiple instances of COM/OS module found in the model. Either merge the modules manually or implement a merging function.", QMessageBox::Ok);
                        } else {
                            QDomElement comModuleRef, osModuleRef;
                            if (comImpIndex.last() < bswImplementations.size())
                                comModuleRef = bswImplementationRefs.at(comImpIndex.last()).parentNode().toElement();
                            else
                                comModuleRef = QDomElement();
                            if (osImpIndex.last() < bswImplementations.size())
                                osModuleRef = bswImplementationRefs.at(osImpIndex.last()).parentNode().toElement();
                            else
                                osModuleRef = QDomElement();
                            success = dynamic_cast<RteGenerator*>(tempGen)->RunRteGenerator(&tempElement, &moduleDef, &comModuleRef, &osModuleRef, &ecuExtractElement, generatorOutputDirectory);
                        }
                    } else
                        success = tempGen->RunCodeGenerator(&tempElement, &moduleDef, generatorOutputDirectory);
                    if (!success) {
                        std::cout << "Error: Unable to generate module " + moduleName.toStdString() + ".\n\n";
                        delete tempGen;
                        continue;
                    }
                    std::cout << "Module " + moduleName.toStdString() + " is successfully generated.\n\n";
                    generatedModules++;
                }
                delete tempGen;
            } else {
                if (moduleName != "EcuC") {
                    std::cout << "Error: Validator/Generator for " + moduleName.toStdString() + " module does not exist.\n\n";
                } else {
                    generatedModules++;
                }
            }
            tempGen = NULL;
        }
    }
    std::cout << "Validation/Generation report: " << QString::number(generatedModules).toStdString() << " of " << QString::number(bswImplementations.size()).toStdString() << " modules validated/generated.\n";
}

void AutosarGenerator::ReplaceMcalInReference(QString &reference) {
    if (reference.contains("/Mcal/")) {
        if (mcalName != "") {
            reference.replace("/Mcal/","/" + mcalName + "/");
        }
    }
}

void AutosarGenerator::on_actionA_bout_triggered() {
    AboutDialog* newDialog = new AboutDialog(this);
    newDialog->setVersion(VERSION);
    newDialog->show();
}
