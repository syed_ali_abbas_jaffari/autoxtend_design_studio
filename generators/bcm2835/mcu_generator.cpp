/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/bcm2835/mcu_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool Bcm2835McuGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of MCU Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Mcu.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Mcu.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += MCU\n";
        makeFile.close();

        // ----------------------------------------- Create Cfg.h -----------------------------------------
        QString configHeaderName = location + "/Mcu_Cfg.h";
        QFile headerFile(configHeaderName);
        if (!headerFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Mcu_Cfg.h file.\n";
            return false;
        }
        QTextStream headerStream( &headerFile );
        headerStream << "\n#ifndef MCU_CFG_H_\n#define MCU_CFG_H_\n\n";

        QDomElement McuGeneralConfiguration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "McuGeneralConfiguration");
        QString errorDetect = GET_VALUE_OF_PARAM(McuGeneralConfiguration, "McuDevErrorDetect");
        headerStream << "#define MCU_DEV_ERROR_DETECT " << GetStdOnOff(errorDetect) << endl;
        QString initClock = GET_VALUE_OF_PARAM(McuGeneralConfiguration, "McuInitClock");
        headerStream << "#define MCU_INIT_CLOCK " << GetStdOnOff(initClock) << "\n\n";
        headerStream << "// Mcu Modes (Symbolic name)\n";
        QDomElement McuModuleConfiguration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "McuModuleConfiguration");
        QList<QDomElement> modeConfigs;
        AutosarToolFactory::FindReferencesToElement(&modeConfigs, &McuModuleConfiguration, "McuModeSettingConf");
        for (int modeIter = 0; modeIter < modeConfigs.size(); modeIter++)
        {
            QString modeName = AutosarToolFactory::GetTextOfFirstChild(&modeConfigs[modeIter], "SHORT-NAME");
            headerStream << "#define McuConf_McuModeSettingConf_" << modeName << " (Mcu_ModeType)" << modeIter << "u\n";
        }
        headerStream << "\n";
        QList<QDomElement> clockConfigs;
        AutosarToolFactory::FindReferencesToElement(&clockConfigs, &McuModuleConfiguration, "McuClockSettingConfig");
        if (clockConfigs.size() > 1)
        {
            std::cout << "Info: This version of generator only supports 1 clock configuration. Only writing the default clock configuration to the generated code.\n";
        }
        QString clockSettingConfigName = AutosarToolFactory::GetTextOfFirstChild(&clockConfigs[0], "SHORT-NAME");
        QString ArcDefaultClockRef = GET_VALUE_REF_OF_REF(McuModuleConfiguration, "ArcDefaultClockRef");
        QDomElement defaultClockConfig = AutosarToolFactory::GetDomElementFromReference(&ArcDefaultClockRef, projectModuleRoot->ownerDocument().documentElement());
        QString defaultClockConfigName = AutosarToolFactory::GetTextOfFirstChild(&defaultClockConfig, "SHORT-NAME");
        headerStream << "// Mcu Clock Types (Symbolic name)\n#define McuConf_" << clockSettingConfigName << "_" << defaultClockConfigName << " (Mcu_ClockType)0u\n\n";
        headerStream << "#endif // MCU_CFG_H_\n";
        headerFile.close();

        // ----------------------------------------- Create LCfg.c -----------------------------------------
        QString lcfgFileName = location + "/Mcu_LCfg.c";
        QFile lcfgFile(lcfgFileName);
        if (!lcfgFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Mcu_LCfg.c file.\n";
            return false;
        }
        QTextStream lcfgStream( &lcfgFile );
        lcfgStream << "\n#include \"Mcu.h\"\n\n/* This file is not used - Mcu_PBcfg.h contains the configuration regardless\n * if post build is used or not.\n */\n";
        lcfgFile.close();

        // ----------------------------------------- Create PBCfg.c -----------------------------------------
        QString pbcfgFileName = location + "/Mcu_PBcfg.c";
        QFile pbcfgFile(pbcfgFileName);
        if (!pbcfgFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Mcu_PBcfg.c file.\n";
            pbcfgFile.close();
            return false;
        }
        QTextStream pbcfgStream( &pbcfgFile );
        pbcfgStream << "\n#ifndef MCU_CFG_C_\n#define MCU_CFG_C_\n\n#include \"Mcu.h\"\n\n";

        QString McuClockSettingId = GET_VALUE_OF_PARAM(defaultClockConfig, "McuClockSettingId");
        Q_UNUSED(McuClockSettingId);
        QString McuClockReferencePointFrequency = GET_VALUE_OF_PARAM(defaultClockConfig, "McuClockReferencePointFrequency");
        pbcfgStream << "Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] =\n{\n\t{\n";
        pbcfgStream << "\t\t.McuClockReferencePointFrequency = " << McuClockReferencePointFrequency << "UL,\n";
        pbcfgStream << "\t\t.Pll1\t= 0,\t// Not used\n\t\t.Pll2\t= 0,\t// Not used\n\t\t.Pll3\t= 0,\t// Not used\n\t},\n};\n\n" <<
                       "const Mcu_ConfigType McuConfigData[] = {\n\t{\n\t\t.McuRamSectors = 0u,\n\t\t.McuClockSettings = 1u," <<
                       "\n\t\t.McuDefaultClockSettings = McuConf_" << clockSettingConfigName << "_" << defaultClockConfigName << ",\n";
        pbcfgStream << "\t\t.McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],\n\t\t.McuRamSectorSettingConfig = NULL,\n\t}\n};\n\n";
        pbcfgStream << "#endif // MCU_CFG_C_\n";
        pbcfgFile.close();
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool Bcm2835McuGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }
    // Make sure that the ArcDefaultClockRef is defined
    QDomElement McuModuleConfiguration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "McuModuleConfiguration");
    QString ArcDefaultClockRef = GET_VALUE_REF_OF_REF_WITH_RETURN(McuModuleConfiguration, "ArcDefaultClockRef");
    if (ArcDefaultClockRef == "")
    {
        std::cout << "Error: Mcu::McuModuleConfiguration::ArcDefaultClockRef not defined.\n";
        returnValue = false;
    }
    // Make sure that there is at least 1 McuClockSettingConfig
    QList<QDomElement> clockConfigs;
    AutosarToolFactory::FindReferencesToElement(&clockConfigs, &McuModuleConfiguration, "McuClockSettingConfig");
    if (clockConfigs.size() == 0)
    {
        std::cout << "Error: Mcu::McuModuleConfiguration::McuClockSettingConfig not found.\n";
        returnValue = false;
    }
    // Make sure that there is at least 1 McuModeSettingConf
    QList<QDomElement> modeConfigs;
    AutosarToolFactory::FindReferencesToElement(&modeConfigs, &McuModuleConfiguration, "McuModeSettingConf");
    if (modeConfigs.size() == 0)
    {
        std::cout << "Error: At least 1 Mcu::McuModuleConfiguration::McuModeSettingConf required.\n";
        returnValue = false;
    }

    return returnValue;
}
