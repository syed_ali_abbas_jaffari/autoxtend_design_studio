/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/bcm2835/dio_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool Bcm2835DioGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Dio Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
		// ----------------------------------------- Create Makefile -----------------------------------------
		QString makeFileName = location + "/Dio.mk";
		QFile makeFile(makeFileName);
		if (!makeFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Dio.mk file.\n";
		    return false;
		}
		QTextStream makeFileStream( &makeFile );
		makeFileStream << "MOD_USE += DIO\n";
		makeFile.close();

		// ----------------------------------------- Create Dio_Cfg.h -----------------------------------------
		QString configHeaderName = location + "/Dio_Cfg.h";
		QFile headerFile(configHeaderName);
		if (!headerFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Dio_Cfg.h file.\n";
		    return false;
		}
		QTextStream headerStream( &headerFile );
        QDomElement DioGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "DioGeneral");
        QString DioDevErrorDetect = GET_VALUE_OF_PARAM(DioGeneral, "DioDevErrorDetect");
        QString DioVersionInfoApi = GET_VALUE_OF_PARAM(DioGeneral, "DioVersionInfoApi");
        headerStream << "\n#ifndef DIO_CFG_H_\n#define DIO_CFG_H_\n\n";
        headerStream << "#define DIO_VERSION_INFO_API\t\t" << GetStdOnOff(DioDevErrorDetect) << "\n";
        headerStream << "#define DIO_DEV_ERROR_DETECT\t\t" << GetStdOnOff(DioVersionInfoApi) << "\n\n";
        headerStream << "#define DIO_END_OF_LIST\t\t(0xFFu)\n\n// Physical ports for BCM2835 (GPIO)\ntypedef enum {\n";
        headerStream << "\tDIO_BANK_0 = 0,\n\tDIO_BANK_1 = 1,\n} Dio_PortTypesType;\n\n// Ports";
        QDomElement DioConfig = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "DioConfig");
        QList<QDomElement> DioPorts;
        AutosarToolFactory::FindReferencesToElement(&DioPorts, &DioConfig, "DioPort");
        for (int portIter = 0; portIter < DioPorts.size(); portIter++)
        {
            QString portName = AutosarToolFactory::GetTextOfFirstChild(&DioPorts[portIter], "SHORT-NAME");
            QString DioPortId = GET_VALUE_OF_PARAM(DioPorts[portIter], "DioPortId");
            headerStream << "\n#define DioConf_DioPort_" << portName << "\t\t(DIO_BANK_" << DioPortId << ")\n";
            headerStream << "#define Dio_" << portName << "\t\tDioConf_DioPort_" << portName << "\n";
        }
        headerStream << "\n// Channels";
        for (int portIter = 0; portIter < DioPorts.size(); portIter++)
        {
            QList<QDomElement> DioChannels;
            AutosarToolFactory::FindReferencesToElement(&DioChannels, &DioPorts[portIter], "DioChannel");
            for (int chanIter = 0; chanIter < DioChannels.size(); chanIter++)
            {
                QString channelName = AutosarToolFactory::GetTextOfFirstChild(&DioChannels[chanIter], "SHORT-NAME");
                QString DioChannelId = GET_VALUE_OF_PARAM(DioPorts[portIter], "DioChannelId");
                headerStream << "\n#define DioConf_DioChannel_" << channelName << "\t\t" << DioChannelId << "\n";
                headerStream << "#define Dio_" << channelName << "\t\tDioConf_DioChannel_" << channelName << "\n";
            }
        }
        headerStream << "\n// Configuration Set Handles\n#define DioConfig (DioConfigData)\n#define Dio_DioConfig (DioConfigData)\n\n";
		headerStream << "#endif // DIO_CFG_H_\n";
		headerFile.close();

		// ----------------------------------------- Create Dio_Lcfg.c -----------------------------------------
		QString configFileName = location + "/Dio_Lcfg.c";
		QFile configFile(configFileName);
		if (!configFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Dio_Lcfg.c file.\n";
		    return false;
		}
		QTextStream configStream( &configFile );
        configStream << "\n#include \"Dio.h\"\n#include \"Dio_Cfg.h\"\n\nconst Dio_ChannelType DioChannelConfigData[] = {\n";
        for (int portIter = 0; portIter < DioPorts.size(); portIter++)
        {
            QList<QDomElement> DioChannels;
            AutosarToolFactory::FindReferencesToElement(&DioChannels, &DioPorts[portIter], "DioChannel");
            for (int chanIter = 0; chanIter < DioChannels.size(); chanIter++)
            {
                QString channelName = AutosarToolFactory::GetTextOfFirstChild(&DioChannels[chanIter], "SHORT-NAME");
                configStream << "\tDioConf_DioChannel_" << channelName << ",\n";
            }
        }
        configStream << "\tDIO_END_OF_LIST\n};\n\nconst Dio_PortType DioPortConfigData[] = {\n";
        for (int portIter = 0; portIter < DioPorts.size(); portIter++)
        {
            QString portName = AutosarToolFactory::GetTextOfFirstChild(&DioPorts[portIter], "SHORT-NAME");
            configStream << "\tDioConf_DioPort_" << portName << ",\n";
        }
        configStream << "\tDIO_END_OF_LIST\n};\n\nconst Dio_ConfigType DioConfigData = {\n\t.ChannelConfig = DioChannelConfigData,\n";
        configStream << "\t.GroupConfig = NULL,\n\t.PortConfig = DioPortConfigData\n};\n\n";
		configFile.close();
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool Bcm2835DioGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                return false;
            }
        }
    }
    return true;
}

