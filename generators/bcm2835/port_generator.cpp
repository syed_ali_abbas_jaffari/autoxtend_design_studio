/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/bcm2835/port_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool Bcm2835PortGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Port Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
		// ----------------------------------------- Create Port_Cfg.h -----------------------------------------
		QString configHeaderName = location + "/Port_Cfg.h";
		QFile headerFile(configHeaderName);
		if (!headerFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Port_Cfg.h file.\n";
		    return false;
		}
		QTextStream headerStream( &headerFile );
		headerStream << "\n#ifndef PORT_CFG_H_\n#define PORT_CFG_H_\n\n";
        headerStream << "#include \"PortDefs.h\"\n#include \"bcm2835.h\"\n\n";
        QDomElement PortGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortGeneral");
        QString PortDevErrorDetect = GET_VALUE_OF_PARAM(PortGeneral, "PortDevErrorDetect");
        QString PortSetPinDirectionApi = GET_VALUE_OF_PARAM(PortGeneral, "PortSetPinDirectionApi");
        QString PortSetPinModeApi = GET_VALUE_OF_PARAM(PortGeneral, "PortSetPinModeApi");
        QString PortVersionInfoApi = GET_VALUE_OF_PARAM(PortGeneral, "PortVersionInfoApi");
        headerStream << "#define PORT_VERSION_INFO_API\t\t" << GetStdOnOff(PortVersionInfoApi) << "\n";
        headerStream << "#define PORT_DEV_ERROR_DETECT\t\t" << GetStdOnOff(PortDevErrorDetect) << "\n";
        headerStream << "#define PORT_SET_PIN_MODE_API\t\t" << GetStdOnOff(PortSetPinModeApi) << "\n";
        headerStream << "#define PORT_SET_PIN_DIRECTION_API\t\t" << GetStdOnOff(PortSetPinDirectionApi) << "\n\n";
        headerStream << "#define PORT_INVALID_REG\t\t255\n\n";
        headerStream << "typedef struct {\n\tuint8_t gpioPin;\n";
        headerStream << "\tuint16_t config;\t\t// bits 0:2 -> functionSelect (i.e. input, output, or ALT0-5)\n";
        headerStream << "\t\t\t\t\t\t\t// bit 3 -> rising edge detect enable\n\t\t\t\t\t// bit 4 -> falling edge detect enable\n";
        headerStream << "\t\t\t\t\t\t\t// bit 5 -> high level detect enable\n\t\t\t\t\t// bit 6 -> low level detect enable\n";
        headerStream << "\t\t\t\t\t\t\t// bit 7 -> async. rising edge detect enable (bits 3 and 7 can not be high at the same time.\n\t\t\t\t\t\t\t// When set async detection will be enabled.)\n";
        headerStream << "\t\t\t\t\t\t\t// bit 8 -> async. falling edge detect enable (bits 4 and 8 can not be high at the same time.\n\t\t\t\t\t\t\t// When set async detection will be enabled.)\n";
        headerStream << "\t\t\t\t\t\t\t// bit 9:10 -> pull-up/down\n\t\t\t\t\t// bits 11:15 -> Unused\n} ArcPort_ConfigType;\t\t\t\t\t// Defines the GPIO pin settings\n\n";
        headerStream << "typedef struct {\n\tuint8_t gpioPin;\n\tuint8_t level;\n} ArcPort_OutConfigType;\t\t\t\t\t// Defines the initial level (i.e. High/low) written on the GPIO pin\n\n";
        headerStream << "typedef struct {\n\tconst ArcPort_ConfigType *PortConfig;\n\tconst ArcPort_OutConfigType *OutConfig;\n";
        headerStream << "} Port_ConfigType;\t\t\t\t\t// Defines the complete data structure\n\n";
        QDomElement PortConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortConfigSet");
        QList <QDomElement> PortContainers;
        AutosarToolFactory::FindReferencesToElement(&PortContainers, &PortConfigSet, "PortContainer");
        for (int contIter = 0; contIter < PortContainers.size(); contIter++)
        {
            QList <QDomElement> PortPins;
            AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
            for (int pinIter = 0; pinIter < PortPins.size(); pinIter++)
            {
                QString PortPinId = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinId");
                QString PortPinName = AutosarToolFactory::GetTextOfFirstChild(&PortPins[pinIter], "SHORT-NAME");
                headerStream << "#define PortConf_PortPin_" << PortPinName << "\t((Port_PinType)" << PortPinId << ")\n";
            }
        }
        headerStream << "\nextern const Port_ConfigType PortConfigData;\n\n";
        headerStream << "// Configuration Set Handles\n#define PortConfigSet (PortConfigData)\n#define Port_PortConfigSet (PortConfigData)\n\n";
		headerStream << "#endif // PORT_CFG_H_\n";
		headerFile.close();

		// ----------------------------------------- Create Port_PBcfg.c -----------------------------------------
		QString configFileName = location + "/Port_PBcfg.c";
		QFile configFile(configFileName);
		if (!configFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Port_PBcfg.c file.\n";
		    return false;
		}
		QTextStream configStream( &configFile );
        configStream << "#include \"Port.h\"\n\n#define PIN_CONTROL_REG(nr, mode) PIN_CONTROL_REG_ ## nr ## _## mode\n\n";
        configStream << "// lint -save -e835 A zero has been given as argument to operator\nstatic const ArcPort_ConfigType PortConfig[] = {\n";
        for (int contIter = 0; contIter < PortContainers.size(); contIter++)
        {
            QList <QDomElement> PortPins;
            AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
            for (int pinIter = 0; pinIter < PortPins.size(); pinIter++)
            {
                QString PortPinId = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinId");
                configStream << "\t{" << PortPinId << ", 0 ";
                QString ArcPortPinPull = GET_VALUE_OF_PARAM(PortPins[pinIter], "ArcPortPinPull");
                if (ArcPortPinPull != "UNDEFINED")
                    configStream << "| ENABLE_" << ArcPortPinPull << " ";
                QString PortPinEdgeDetect = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinEdgeDetect");
                if (PortPinEdgeDetect != "UNDEFINED")
                    configStream << "| " << PortPinEdgeDetect << " ";
                QString PortPinLevelDetect = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinLevelDetect");
                if (PortPinLevelDetect != "UNDEFINED")
                    configStream << "| " << PortPinLevelDetect << " ";
                QString PortPinFunction = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinFunction");
                if (PortPinFunction == "INPUT" || PortPinFunction == "OUTPUT")
                    configStream << "| " << "PIN_CONTROL_REG(" << PortPinId << ", " << PortPinFunction << ")},\n";
                else
                {
                    QStringList splittedFunction = PortPinFunction.split("_");
                    splittedFunction.removeAt(0);splittedFunction.removeAt(0);
                    configStream << "| PIN_CONTROL_REG(" << PortPinId << ", " << splittedFunction.join("_") << ")},\n";
                }
            }
        }
        configStream << "\t{PORT_INVALID_REG, 0},\n};\n\nstatic const ArcPort_OutConfigType GpioOutConfig[] = {\n";
        for (int contIter = 0; contIter < PortContainers.size(); contIter++)
        {
            QList <QDomElement> PortPins;
            AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
            for (int pinIter = 0; pinIter < PortPins.size(); pinIter++)
            {
                QString PortPinInitialLevel = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinInitialLevel");
                if (PortPinInitialLevel != "UNDEFINED")
                {
                    QString PortPinId = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinId");
                    configStream << "\t{" << PortPinId << ", ";
                    if (PortPinInitialLevel == "HIGH")
                        configStream << "1},\n";
                    else
                        configStream << "0},\n";
                }
            }
        }
        configStream << "\t{PORT_INVALID_REG, 0},\n};\n\nconst Port_ConfigType PortConfigData =\n{\n\t.PortConfig = & PortConfig[0],\n\t.OutConfig  = & GpioOutConfig[0]\n};\n\n";
        configFile.close();

		// ----------------------------------------- Create Port_Lcfg.c -----------------------------------------
		QString lConfigFileName = location + "/Port_Lcfg.c";
		QFile lConfigFile(lConfigFileName);
		if (!lConfigFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Port_Lcfg.c file.\n";
		    return false;
		}
		QTextStream lConfigStream( &lConfigFile );
        lConfigStream << "\n#include \"Port.h\"\n/* This file is not used - Port_PBcfg.c contains the configuration regardless\n";
        lConfigStream << " * if post build is used or not. \n */";
		lConfigFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Port.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Port.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += PORT\n";
        makeFile.close();
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool Bcm2835PortGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);

    bool returnValue = true;
    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }
    // Make sure that port pin correspand to the respective function
    QDomElement PortConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortConfigSet");
    QList <QDomElement> PortContainers;
    AutosarToolFactory::FindReferencesToElement(&PortContainers, &PortConfigSet, "PortContainer");
    QStringList pins;
    for (int contIter = 0; contIter < PortContainers.size(); contIter++)
    {
        QList <QDomElement> PortPins;
        AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
        for (int pinIter = 0; pinIter < PortPins.size(); pinIter++)
        {
            QString PortPinFunction, PortPinId;
            try {
                PortPinFunction = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinFunction");
                PortPinId = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinId");
            } catch (QString err) {
                std::cout << err.toStdString() << "\n";
                return false;
            }
            if (PortPinFunction == "INPUT" || PortPinFunction == "OUTPUT")
            {
                if (PortPinId == "")
                {
                    std::cout << "Error: Port::PortContainer::PortPin::PortPinId can not be empty when PortPinFunction is either INPUT or OUTPUT.\n";
                    returnValue = false;
                }
            }
            else
            {
                QStringList splittedFunction = PortPinFunction.split("_");
                if (splittedFunction[1] != PortPinId)
                {
                    std::cout << "Error: Port::PortContainer::PortPin::PortPinId " + PortPinId.toStdString() + " is invalid for function " + PortPinFunction.toStdString() + ".\n";
                    returnValue = false;
                }
            }
            // Make sure that the port pin numbers are unique
            for (int pinIter = 0; pinIter < pins.size(); pinIter++)
            {
                if (pins[pinIter] == PortPinId)
                {
                    std::cout << "Error: Port::PortContainer::PortPin::PortPinId " + PortPinId.toStdString() + " is not unique.\n";
                    returnValue = false;
                }
            }
            pins << PortPinId;
        }
    }
    return returnValue;
}

