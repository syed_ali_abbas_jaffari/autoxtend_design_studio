/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef RTE_GENERATOR
#define RTE_GENERATOR

#include <QtXml>
#include <iostream>
#include "abstractcodegenerator.h"

class RteGenerator : public AbstractCodeGenerator
{
public:
    ~RteGenerator() { }

    bool RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location);
    bool CheckModuleValidity(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot);
    bool RunRteGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QDomElement *comModuleRoot, QDomElement *osModuleRoot, QDomElement* ecuExtract, QString location);
    bool CheckRteValidity(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QDomElement* ecuExtract);

    static AbstractCodeGenerator *CreateGenerator() { return new RteGenerator(); }
private:
    // File generator functions for MemMap
    bool CreateMemMapH(QString compName, QString location);
    void MemMapSectionEntry(QTextStream &file, QString &compName, QString section);
    void MemMapSectionPolicyEntry (QTextStream &file, QString &compName, QString section, QString policy = "");
    void MemMapSectionAlignmentPolicyEntry (QTextStream &file, QString &compName, const QString section, QString alignment = "", QString policy = "");

    // File generator functions for Config
    bool CreateIocC(QString location);
    bool CreateIocH(QString location);
    bool CreateRteAssertH(QString location);
    bool CreateRteC(QString location, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes, QDomElement &projectModuleRoot, QList<QDomElement> &osTasks, QList<bool> &isOsTaskExtended);
    bool CreateRteCallbacks(QString location, QDomElement* projectModuleRoot, QDomElement* ecuExtract, QDomElement* osModuleRoot);
    bool CreateRteCalprmsC(QString location);
    bool CreateRteCalprmsH(QString location);
    bool CreateRteCfgH(QString location, QDomElement *projectModuleRoot);
    bool CreateRteDataHandleTypeH(QString location, QDomElement* ecuExtract);
    bool CreateRteFifoC(QString location);
    bool CreateRteFifoH(QString location);
    bool CreateRteH(QString location, QDomElement* projectModuleRoot);
    bool CreateRteHookH(QString location);
    bool CreateRteInternalC(QString location, QList<QDomElement> &ModeMachineSwcs, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes, QDomElement &projectModuleRoot, QDomElement &osModuleRoot, QList <QDomElement> &osTasks, QList <bool> &isOsTaskExtended, QList<QDomElement> &comSignals);
    bool CreateRteInternalH(QString location, QList<QDomElement> &ModeMachineSwcs, QList<QList<QDomElement> > &ModeMachineComps, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes);
    bool CreateRteMainC(QString location, QList<QDomElement> &ModeMachineSwcs, QList<QList<QDomElement> > &ModeMachineComps, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract);
    bool CreateRteMainH(QString location);
    bool CreateRteMakeFile(QString location, QList<QDomElement> &swcs);
    bool CreateRteSwcTypeH(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs);
    bool CreateRteSwcH(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes);
    bool CreateRteSwcC(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes);
    bool CreateRteTagsTxt(QString location);
    bool CreateRteTypeH(QString location, QList<QDomElement> &structureDataTypes, QList<QDomElement> &otherDataTypes, QDomElement &ecuExtract);
    bool CreateRteTypeWorkaroundsH(QString location, QList <QDomElement> &structureDataTypes, QList <QDomElement> &otherDataTypes);

    // Helper functions
    void CreateSRArguments(QDomElement &Type, QString &outString, bool isRead);
    bool CreateRteFunctionCallCPrototype(QStringList &protoList, QString &interfaceRef, QDomElement &swc, QDomElement &componentPrototype, QDomElement &port, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, bool isPPort);
    bool CreateCSArguments(QStringList &dataType, QStringList &variable, QString &referedTargetRef, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract);
    bool CreateServerCallCPrototype(QString &apiString, QDomElement &event, QDomElement &comp, int &numOfPortArgs, int &numOfInterfaceArgs, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract);
    QDomElement CreateServerPortArguments(QDomElement &event, int &numOfPortArgs, QString &apiString);
};

#endif // RTE_GENERATOR

