/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "osgenerator.h"

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool OsGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " << location.toStdString() << " for generation of MCU Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
        // ----------------------------------------- Create Os_Cfg.h -----------------------------------------
        QString configHeaderName = location + "/Os_Cfg.h";
        QFile headerFile(configHeaderName);
        if (!headerFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Os_Cfg.h file.\n";
            return false;
        }
        QTextStream headerStream( &headerFile );
        headerStream << "\n#ifndef OS_CFG_H_\n#define OS_CFG_H_\n\n";

        headerStream << "// Application Ids\n";
        QList<QDomElement> OsApplications;
        AutosarToolFactory::FindReferencesToElement(&OsApplications, projectModuleRoot, "OsApplication");
        QString firstApplicationName = "";
        for (int appIter = 0; appIter < OsApplications.size(); appIter++)
        {
            QString osAppName = AutosarToolFactory::GetTextOfFirstChild(&OsApplications[appIter], "SHORT-NAME");
            headerStream << "#define APPLICATION_ID_" << osAppName << "\t\t" << appIter << "u\n";
            if (firstApplicationName == "")
            {
                firstApplicationName = osAppName;
            }
        }
        headerStream << "\n// Alarm Ids\n";
        QList<QDomElement> OsAlarms;
        AutosarToolFactory::FindReferencesToElement(&OsAlarms, projectModuleRoot, "OsAlarm");
        for (int alarmIter = 0; alarmIter < OsAlarms.size(); alarmIter++)
        {
            QString osAlarmName = AutosarToolFactory::GetTextOfFirstChild(&OsAlarms[alarmIter], "SHORT-NAME");
            headerStream << "#define ALARM_ID_" << osAlarmName << "\t\t((AlarmType)" << alarmIter << ")\n";
        }
        headerStream << "\n// Counter Ids\n";
        QList<QDomElement> OsCounters;
        AutosarToolFactory::FindReferencesToElement(&OsCounters, projectModuleRoot, "OsCounter");
        for (int alarmIter = 0; alarmIter < OsCounters.size(); alarmIter++)
        {
            QString osCounterName = AutosarToolFactory::GetTextOfFirstChild(&OsCounters[alarmIter], "SHORT-NAME");
            headerStream << "#define COUNTER_ID_" << osCounterName << "\t\t((CounterType)" << alarmIter << ")\n";
        }
        headerStream << "\n// System counter\n"
                     << "#define OSMAXALLOWEDVALUE\t\tUINT_MAX\t// NOT CONFIGURABLE IN TOOLS\n"
                     << "#define OSTICKSPERBASE\t\t\t1u\t// NOT CONFIGURABLE IN TOOLS\n"
                     << "#define OSMINCYCLE\t\t\t\t1u\t// NOT CONFIGURABLE IN TOOLS\n";
        QDomElement OsOS = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "OsOS");
        QString cores = GET_VALUE_OF_PARAM_WITH_RETURN(OsOS, "OsNumberOfCores");
        if (cores != "1" && cores != "")
            std::cout << "Info: This version of OS generator only supports 1 core.\n";
        QString freq = GET_VALUE_OF_PARAM(OsOS, "ArcOsTickFrequency");
        if (freq == "")
        {
            freq == "1000";
        }
        bool ok;
        int freqInt = freq.toInt(&ok);
        Q_ASSERT(ok);
        headerStream << "#define OSTICKDURATION\t\t\t" << 1000000000/freqInt << "UL\t// Time between ticks in nano seconds\n\n";
        headerStream << "// Counter macros\n";
        for (int counterIter = 0; counterIter < OsCounters.size(); counterIter++)
        {
            QString osCounterName = AutosarToolFactory::GetTextOfFirstChild(&OsCounters[counterIter], "SHORT-NAME");
            QString type = GET_VALUE_OF_PARAM(OsCounters[counterIter], "OsCounterType");
            headerStream << "#define OSMAXALLOWEDVALUE_" << osCounterName << "\t\t";
            if (type == "HARDWARE" || type == "SOFTWARE")
            {
                QDomElement OsCounterMaxAllowedValue = AutosarToolFactory::FindFirstReferenceToElement(&OsCounters[counterIter], "OsCounterMaxAllowedValue");
                headerStream << OsCounterMaxAllowedValue << "u\n";
            }
            else
            {
                headerStream << "OSMAXALLOWEDVALUE\n";
            }
            headerStream << "#define OSTICKSPERBASE_" << osCounterName << "\t\t\t1u // NOT CONFIGURABLE IN TOOLS (counter.Os CounterTicksPerBase.value)\n#define OSMINCYCLE_"
                         << osCounterName << "\t\t\t\t";
            QString minCycle = GET_VALUE_OF_PARAM(OsCounters[counterIter], "OsCounterMinCycle");
            if (minCycle == "")
            {
                minCycle = "0";
            }
            headerStream << minCycle << "u\n";
            if (type != "SOFTWARE")
                headerStream << "// @req OS393\n#define OS_TICKS2SEC_" << osCounterName << "(_ticks)\t\t( (OSTICKDURATION * _ticks)/1000000000UL )\n"
                    << "#define OS_TICKS2MS_" << osCounterName << "(_ticks)\t\t( (OSTICKDURATION * _ticks)/1000000UL )\n"
                    << "#define OS_TICKS2US_" << osCounterName << "(_ticks)\t\t( (OSTICKDURATION * _ticks)/1000UL )\n"
                    << "#define OS_TICKS2NS_" << osCounterName << "(_ticks)\t\t(OSTICKDURATION * _ticks)\n";

        }
        headerStream << "\n// Event masks\n";
        QList<QDomElement> OsEvents;
        AutosarToolFactory::FindReferencesToElement(&OsEvents, projectModuleRoot, "OsEvent");
        for (int eventIter = 0; eventIter < OsEvents.size(); eventIter++)
        {
            QString osEventName = AutosarToolFactory::GetTextOfFirstChild(&OsEvents[eventIter], "SHORT-NAME");
            QString mask = GET_VALUE_OF_PARAM_WITH_RETURN(OsEvents[eventIter], "OsEventMask");
            if (mask != "")
            {
                bool ok;
                int integerMask = mask.toInt(&ok);
                Q_ASSERT(ok);
                headerStream << "#define EVENT_MASK_" << osEventName << "\t\t\t(EventMaskType)((EventMaskType)1u<<" << GetTrailingZeros(integerMask, 64) << ")\n";
            }
            else
            {
                // TODO: Might be same when some event masks are defined but some not. Fix it.
                headerStream << "#define EVENT_MASK_" << osEventName << "\t\t\t(EventMaskType)((EventMaskType)1u<<" << eventIter << ")\n";
            }
        }
        headerStream << "\n// Isr Ids\n";
        QList<QDomElement> OsIsrs;
        int cat1OsIsrs = 0, cat2OsIsrs = 0;
        AutosarToolFactory::FindReferencesToElement(&OsIsrs, projectModuleRoot, "OsIsr");
        for (int isrIter = 0; isrIter < OsIsrs.size(); isrIter++)
        {
            QString osIsrName = AutosarToolFactory::GetTextOfFirstChild(&OsIsrs[isrIter], "SHORT-NAME");
            headerStream << "#define ISR_ID_" << osIsrName << "\t\t((ISRType)" << isrIter << ")\n";
            QString category = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "OsIsrCategory");
            if (category != "CATEGORY_1")
                cat1OsIsrs++;
            else if (category != "CATEGORY_2")
                cat2OsIsrs++;
        }
        headerStream << "\n// Resource Ids\n";
        QList<QDomElement> OsResources;
        int nonLinkedResources = 0, linkedResources = 0;
        AutosarToolFactory::FindReferencesToElement(&OsResources, projectModuleRoot, "OsResource");
        for (int resIter = 0; resIter < OsResources.size(); resIter++)
        {
            QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
            if (prop != "LINKED")
            {
                nonLinkedResources++;
                QString osResourcesName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                headerStream << "#define RES_ID_" << osResourcesName << "\t\t((ResourceType)" << resIter << ")\n";
            }
        }
        headerStream << "\n// Linked resource Ids\n";
        for (int resIter = 0; resIter < OsResources.size(); resIter++)
        {
            QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
            if (prop == "LINKED")
            {
                linkedResources++;
                QString osResourcesName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                QString ref = GET_VALUE_REF_OF_REF(OsResources[resIter], "OsResourceLinkedResourceRef");
                QDomElement referedResource = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
                QString referedOsResourcesName = AutosarToolFactory::GetTextOfFirstChild(&referedResource, "SHORT-NAME");
                headerStream << "#define RES_ID_" << osResourcesName << "\t\tRES_ID_" << referedOsResourcesName << ")\n";
            }
        }
        headerStream << "\n// Resource masks\n";
        for (int resIter = 0; resIter < OsResources.size(); resIter++)
        {
            QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
            if (prop != "LINKED")
            {
                QString osResourcesName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                headerStream << "#define RES_MASK_" << osResourcesName << "\t\t(1u << " << resIter << "u)\n";
            }
        }
        headerStream << "\n// Task Ids\n";
        headerStream << "#define TASK_ID_OsIdle\t\t((TaskType)0)\n";
        QList<QDomElement> OsTasks;
        AutosarToolFactory::FindReferencesToElement(&OsTasks, projectModuleRoot, "OsTask");
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            headerStream << "#define TASK_ID_" << osTaskName << "\t\t((TaskType)" << taskIter+1 << ")\n";
        }
        headerStream << "\n// Task entry points\nvoid OsIdle(void);\n";
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            headerStream << "void " << osTaskName << "(void);\n";
        }
        headerStream << "\n// Schedule Table Ids\n";
        QList<QDomElement> OsScheduleTables;
        AutosarToolFactory::FindReferencesToElement(&OsScheduleTables, projectModuleRoot, "OsScheduleTable");
        for (int schedIter = 0; schedIter < OsScheduleTables.size(); schedIter++)
        {
            QString osSchedName = AutosarToolFactory::GetTextOfFirstChild(&OsScheduleTables[schedIter], "SHORT-NAME");
            headerStream << "#define SCHTBL_ID_" << osSchedName << "\t\t((ScheduleTableType)" << schedIter << ")\n";
        }
        headerStream << "\n// Spinlock Ids\n";
        QList<QDomElement> OsSpinlocks;
        AutosarToolFactory::FindReferencesToElement(&OsSpinlocks, projectModuleRoot, "OsSpinlock");
        for (int lockIter = 0; lockIter < OsSpinlocks.size(); lockIter++)
        {
            QString osSpinlockName = AutosarToolFactory::GetTextOfFirstChild(&OsSpinlocks[lockIter], "SHORT-NAME");
            headerStream << "#define SPINLOCK_ID_" << osSpinlockName << "\t\t" << lockIter << "\n";
        }
        headerStream << "#define OS_SPINLOCK\t\t\t" << OsSpinlocks.size() << "\n#define OS_RTE_SPINLOCK\t\t" << OsSpinlocks.size()+1 << "\n\n";
        headerStream << "// Stack size\n#define OS_INTERRUPT_STACK_SIZE\t\t";
        QString intStackSize = GET_VALUE_OF_PARAM(OsOS, "ArcOsInterruptStackSize");
        headerStream << intStackSize << "u\n#define OS_OSIDLE_STACK_SIZE\t\t";
        QString idleStackSize = GET_VALUE_OF_PARAM(OsOS, "ArcOsIdleStackSize");
        headerStream << idleStackSize << "u\n\n";
        headerStream << "#define OS_ALARM_CNT\t\t\t" << OsAlarms.size() << "u\n";
        headerStream << "#define OS_TASK_CNT\t\t\t\t" << OsTasks.size()+1 << "u\n";
        headerStream << "#define OS_SCHTBL_CNT\t\t\t" << OsScheduleTables.size() << "u\n";
        headerStream << "#define OS_COUNTER_CNT\t\t\t" << OsCounters.size() << "u\n";
        headerStream << "#define OS_EVENTS_CNT\t\t\t" << OsEvents.size() << "u\n";
        headerStream << "#define OS_ISRS_CNT\t\t\t\t" << OsIsrs.size() << "u\n";
        headerStream << "#define OS_RESOURCE_CNT\t\t\t" << nonLinkedResources << "u\n";
        headerStream << "#define OS_LINKED_RESOURCE_CNT\t" << linkedResources << "u\n";
        headerStream << "#define OS_APPLICATION_CNT\t\t" << OsApplications.size() << "u\n";
        headerStream << "#define OS_SPINLOCK_CNT\t\t\t" << OsSpinlocks.size() + 2 << "u\n";
        headerStream << "#define OS_SERVICE_CNT\t\t\t0u\n\n";
        QDomElement ArcOsDebug = AutosarToolFactory::FindFirstReferenceToElement(&OsOS, "ArcOsDebug");
        QString debugValue = GET_VALUE_OF_PARAM(ArcOsDebug, "OsDebugActivated");
        headerStream << "#define CFG_OS_DEBUG\t\t" << GetStdOnOff(debugValue) << "\n\n";
        QString sC = GET_VALUE_OF_PARAM(OsOS, "OsScalabilityClass");
        headerStream << "#define OS_SC1\t\t\t\t\t";
        if (sC == "SC1")
            headerStream << "STD_ON";
        else
            headerStream << "STD_OFF";
        headerStream << "\n#define OS_SC2\t\t\t\t\tSTD_OFF\n#define OS_SC3\t\t\t\t\t";
        if (sC == "SC3")
            headerStream << "STD_ON";
        else
            headerStream << "STD_OFF";
        headerStream << "\n#define OS_SC4\t\t\t\t\tSTD_OFF\n\n#define OS_USE_APPLICATIONS\t\t\tSTD_ON\n#define OS_USE_MEMORY_PROT\t\t\tSTD_OFF\t// NOT CONFIGURABLE IN TOOLS\n";
        headerStream << "#define OS_USE_TASK_TIMING_PROT\t\tSTD_OFF\t// NOT CONFIGURABLE IN TOOLS\n#define OS_USE_ISR_TIMING_PROT\t\tSTD_OFF\t// NOT CONFIGURABLE IN TOOLS\n";
        headerStream << "#define OS_STACK_MONITORING\t\t\t";
        QString stackMon = GET_VALUE_OF_PARAM(OsOS, "OsStackMonitoring");
        if (stackMon == "")
        {
            stackMon = "1";
        }
        headerStream << GetStdOnOff(stackMon) << "\n\n#define OS_STATUS_EXTENDED\t\t\t";
        QString status = GET_VALUE_OF_PARAM(OsOS, "OsStatus");
        if (status == "EXTENDED")
            headerStream << "STD_ON";
        else
            headerStream << "STD_OFF";
        headerStream << "\n#define OS_USE_GET_SERVICE_ID\t\tSTD_ON\t// NOT CONFIGURABLE IN TOOLS\n"
                     << "#define OS_USE_PARAMETER_ACCESS\t\tSTD_ON\t// NOT CONFIGURABLE IN TOOLS\n"
                     << "#define OS_RES_SCHEDULER\t\t\tSTD_ON\t// NOT CONFIGURABLE IN TOOLS\n\n"
                     << "#define OS_ISR_CNT\t\t\t\t\t" << OsIsrs.size() << "u\n"
                     << "#define OS_ISR1_CNT\t\t\t\t\t" << cat1OsIsrs << "u\n"
                     << "#define OS_ISR2_CNT\t\t\t\t\t" << cat2OsIsrs << "u\n"
                     << "#define OS_ISR_MAX_CNT\t\t\t\t";
        QString maxIsr = GET_VALUE_OF_PARAM(OsOS, "ArcOsMaxIsr");
        if (maxIsr == "")
        {
            maxIsr = "10";
        }
        headerStream << maxIsr << "u\n#define OS_NUM_CORES\t\t\t\t1u\n#define OS_CORE_0_MAIN_APPLICATION\tAPPLICATION_ID_" << firstApplicationName;
        headerStream << "\n\n#if defined(__CWCC__)\nint write(  int fd, const void *buf, uint32 nbytes);\n#endif\n\n";
        headerStream << "#endif // OS_CFG_H_\n";
        headerFile.close();

        // ----------------------------------------- Create Os_Cfg.c -----------------------------------------
        QString configFileName = location + "/Os_Cfg.c";
        QFile configFile(configFileName);
        if (!configFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Os_Cfg.c file.\n";
            return false;
        }
        QTextStream configStream( &configFile );
        configStream << "#include \"os_i.h\"\n\n// ###############################\tEXTERNAL REFERENCES\t#############################\n\n";
        configStream << "// Set the os tick frequency\nOsTickType OsTickFreq = " << freq << ";\n\n";
        configStream << "// ###############################\tDEBUG OUTPUT\t#############################\nuint32 os_dbg_mask = 0;\n\n";
        configStream << "// ###############################    APPLICATIONS     #############################\nGEN_APPLICATION_HEAD = {\n";
        for (int appIter = 0; appIter < OsApplications.size(); appIter++)
        {
            QString osAppName = AutosarToolFactory::GetTextOfFirstChild(&OsApplications[appIter], "SHORT-NAME");
            QString trusted = GET_VALUE_OF_PARAM(OsApplications[appIter], "OsTrusted");
            configStream << "\tGEN_APPLICATION(\n\t\t\tAPPLICATION_ID_" << osAppName << ",\n\t\t\t\"" << osAppName << "\",\n\t\t\t";
            if (trusted == "0")
                configStream << "false";
            else if (trusted == "1")
                configStream << "true";
            configStream << ",\n\t\t\t0,\n\t\t\tNULL,\n\t\t\tNULL,\n\t\t\tNULL,\n\t\t\t0\n\t)\n";
        }
        configStream << "};\n\n// #################################\tCOUNTERS\t###############################\n";
        if (OsCounters.size() >0)
        {
            configStream << "GEN_COUNTER_HEAD = {\n";
            for (int counterIter = 0; counterIter < OsCounters.size(); counterIter++)
            {
                QString osCounterName = AutosarToolFactory::GetTextOfFirstChild(&OsCounters[counterIter], "SHORT-NAME");
                QString type = GET_VALUE_OF_PARAM(OsCounters[counterIter], "OsCounterType");
                QString max = GET_VALUE_OF_PARAM_WITH_RETURN(OsCounters[counterIter], "OsCounterMaxAllowedValue");
                if (max == "")
                    max = "65535";
                int maxValue = max.toInt(&ok);
                Q_ASSERT(ok);
                QString min = GET_VALUE_OF_PARAM(OsCounters[counterIter], "OsCounterMinCycle");
                if (min == "")
                    min = "0";
                int minValue = min.toInt(&ok);
                Q_ASSERT(ok);
                configStream << "\tGEN_COUNTER(\n\t\t\tCOUNTER_ID_" << osCounterName << ",\n\t\t\t\"" << osCounterName << "\",\n";
                if (type == "HARDWARE" || type == "OS_TICK")
                    configStream << "\t\t\tCOUNTER_TYPE_HARD,\n";
                else
                    configStream << "\t\t\tCOUNTER_TYPE_SOFT\n";
                configStream << "\t\t\tCOUNTER_UNIT_NANO,\n\t\t\t";
                if (type == "SOFTWARE")
                    configStream << maxValue;
                else
                    configStream << "0xffff";
                configStream << ",\n\t\t\t1,\n\t\t\t" << minValue << ",\n\t\t\t0,\n";
                QString owningAppName = "";
                GetOwningApplication(owningAppName, "OsAppCounterRef", OsApplications, OsCounters[counterIter], *projectModuleRoot);
                if (owningAppName == "")
                {
                    std::cout << "Error: OsApplication not found for the OsCounter \'" + osCounterName.toStdString() + "\'.\n";
                    return false;
                }
                configStream << "\t\t\tAPPLICATION_ID_" << owningAppName << ",\n\t\t\t(1 << APPLICATION_ID_" << owningAppName << ")\n";
                configStream << GetAccessingApplications("OsCounterAccessingApplication", OsCounters[counterIter], *projectModuleRoot);
                configStream << "\t)\n";
            }
            configStream << "};\n";
        }
        QString counterName = "";
        for (int counterIter = 0; counterIter < OsCounters.size(); counterIter++)
        {
            QString type = GET_VALUE_OF_PARAM(OsCounters[counterIter], "OsCounterType");
            if (type == "OS_TICK")
            {
                counterName = AutosarToolFactory::GetTextOfFirstChild(&OsCounters[counterIter], "SHORT-NAME");
                break;
            }
        }
        configStream << "CounterType Os_Arc_OsTickCounter = ";
        if (counterName != "")
            configStream << "COUNTER_ID_" << counterName << ";\n";
        else
            configStream << "-1;\n";
        configStream << "// ##################################\tALARMS\t################################\n";
        if (OsAlarms.size() >0)
        {
            for (int alarmIter = 0; alarmIter < OsAlarms.size(); alarmIter++)
            {
                QDomElement OsAlarmAutostart = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarms[alarmIter], "OsAlarmAutostart");
                if (!OsAlarmAutostart.isNull())
                {
                    configStream << "GEN_ALARM_AUTOSTART(\n";
                    QString osAlarmName = AutosarToolFactory::GetTextOfFirstChild(&OsAlarms[alarmIter], "SHORT-NAME");
                    QString type = GET_VALUE_OF_PARAM(OsAlarmAutostart, "OsAlarmAutostartType");
                    QString alarmTime = GET_VALUE_OF_PARAM(OsAlarmAutostart, "OsAlarmAlarmTime");
                    QString cycleTime = GET_VALUE_OF_PARAM(OsAlarmAutostart, "OsAlarmCycleTime");
                    configStream << "\t\t\tALARM_ID_" << osAlarmName << ",\n";
                    configStream << "\t\t\tALARM_AUTOSTART_" << type << ",\n";
                    configStream << "\t\t\t" << alarmTime << ",\n";
                    configStream << "\t\t\t" << cycleTime << ",\n";
                    configStream << "\t\t\tOSDEFAULTAPPMODE );\n";
                }
            }
            configStream << "\nGEN_ALARM_HEAD = {\n";
            for (int alarmIter = 0; alarmIter < OsAlarms.size(); alarmIter++)
            {
                QString osAlarmName = AutosarToolFactory::GetTextOfFirstChild(&OsAlarms[alarmIter], "SHORT-NAME");
                configStream << "\tGEN_ALARM(\tALARM_ID_" << osAlarmName << ",\n";
                configStream << "\t\t\t\"" << osAlarmName << "\",\n";
                QString counterRef = GET_VALUE_REF_OF_REF(OsAlarms[alarmIter], "OsAlarmCounterRef");
                QDomElement referedCounter = AutosarToolFactory::GetDomElementFromReference(&counterRef, projectModuleRoot->ownerDocument().documentElement());
                QString counterName = AutosarToolFactory::GetTextOfFirstChild(&referedCounter, "SHORT-NAME");
                configStream << "\t\t\tCOUNTER_ID_" << counterName << ",\n";
                QDomElement OsAlarmAutostart = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarms[alarmIter], "OsAlarmAutostart");
                if (!OsAlarmAutostart.isNull())
                    configStream << "\t\t\tGEN_ALARM_AUTOSTART_NAME(ALARM_ID_" << osAlarmName << "),\n";
                else
                    configStream << "\t\t\tNULL,\n";
                QDomElement OsAlarmAction = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarms[alarmIter], "OsAlarmAction");
                QDomElement OsAlarmActivateTask = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmActivateTask");
                if (!OsAlarmActivateTask.isNull())
                {
                    configStream << "\t\t\tALARM_ACTION_ACTIVATETASK,\n";
                    QString taskRef = GET_VALUE_REF_OF_REF(OsAlarmActivateTask, "OsAlarmActivateTaskRef");
                    QDomElement referedTask = AutosarToolFactory::GetDomElementFromReference(&taskRef, projectModuleRoot->ownerDocument().documentElement());
                    QString taskName = AutosarToolFactory::GetTextOfFirstChild(&referedTask, "SHORT-NAME");
                    configStream << "\t\t\tTASK_ID_" << taskName << ",\n\t\t\t0,\n\t\t\t0,\n";
                }
                QDomElement OsAlarmSetEvent = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmSetEvent");
                if (!OsAlarmSetEvent.isNull())
                {
                    configStream << "\t\t\tALARM_ACTION_SETEVENT,\n";
                    QString taskRef = GET_VALUE_REF_OF_REF(OsAlarmSetEvent, "OsAlarmSetEventTaskRef");
                    QDomElement referedTask = AutosarToolFactory::GetDomElementFromReference(&taskRef, projectModuleRoot->ownerDocument().documentElement());
                    QString taskName = AutosarToolFactory::GetTextOfFirstChild(&referedTask, "SHORT-NAME");
                    configStream << "\t\t\tTASK_ID_" << taskName << ",\n";
                    QString eventRef = GET_VALUE_REF_OF_REF(OsAlarmSetEvent, "OsAlarmSetEventRef");
                    QDomElement referedEvent = AutosarToolFactory::GetDomElementFromReference(&eventRef, projectModuleRoot->ownerDocument().documentElement());
                    QString eventName = AutosarToolFactory::GetTextOfFirstChild(&referedEvent, "SHORT-NAME");
                    configStream << "\t\t\tEVENT_MASK_" << eventName << ",\n\t\t\t0,\n";
                }
                QDomElement OsAlarmIncrementCounter = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmIncrementCounter");
                if (!OsAlarmIncrementCounter.isNull())
                {
                    configStream << "\t\t\tALARM_ACTION_INCREMENTCOUNTER,\n\t\t\t0,\n\t\t\t0,\n";
                    QString counterRef = GET_VALUE_REF_OF_REF(OsAlarmIncrementCounter, "OsAlarmIncrementCounterRef");
                    QDomElement referedCounter = AutosarToolFactory::GetDomElementFromReference(&counterRef, projectModuleRoot->ownerDocument().documentElement());
                    QString counterName = AutosarToolFactory::GetTextOfFirstChild(&referedCounter, "SHORT-NAME");
                    configStream << "\t\t\tCOUNTER_ID_" << counterName << ",\n";
                }
                QString owningAppName = "";
                GetOwningApplication(owningAppName, "OsAppAlarmRef", OsApplications, OsAlarms[alarmIter], *projectModuleRoot);
                if (owningAppName == "")
                {
                    std::cout << "Error: OsApplication not found for the OsAlarm \'" + osAlarmName.toStdString() + "\'.\n";
                    return false;
                }
                configStream << "\t\t\tAPPLICATION_ID_" << owningAppName << ",\n\t\t\t(1 << APPLICATION_ID_" << owningAppName << ")\n";
                configStream << GetAccessingApplications("OsAlarmAccessingApplication", OsAlarms[alarmIter], *projectModuleRoot);
                configStream << "\t)\n";
                if (alarmIter != OsAlarms.size()-1)
                {
                    configStream << "\t,\n";
                }
            }
            configStream << "};\n\n";
        }
        configStream << "// ################################\tRESOURCES\t###############################\n";
        if (OsResources.size())
        {
            configStream << "GEN_RESOURCE_HEAD = {\n";
            for (int resIter = 0; resIter < OsResources.size(); resIter++)
            {
                QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
                if (prop != "LINKED")
                {
                    configStream << "\tGEN_RESOURCE(\n";
                    QString osResourceName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                    configStream << "\t\t\tRES_ID_" << osResourceName << ",\n";
                    configStream << "\t\t\tRESOURCE_TYPE_" << prop << ",\n\t\t\t0,\n";
                    QString owningAppName = "", streamText;
                    QList<QDomElement> accessingApps;
                    AutosarToolFactory::FindReferencesToElement(&accessingApps, &OsResources[resIter], "OsResourceAccessingApplication");
                    for (int appIter = 0; appIter< accessingApps.size(); appIter++)
                    {
                        QString appRef = AutosarToolFactory::GetTextOfFirstChild(&accessingApps[appIter], "VALUE-REF");
                        if (appRef != "")
                        {
                            QDomElement referedApp = AutosarToolFactory::GetDomElementFromReference(&appRef, projectModuleRoot->ownerDocument().documentElement());
                            QString appName = AutosarToolFactory::GetTextOfFirstChild(&referedApp, "SHORT-NAME");
                            if (owningAppName == "")
                            {
                                owningAppName = appName;
                            }
                            streamText += "\t\t\t| ((1 << APPLICATION_ID_" + appName + ")\n";
                        }
                    }
                    if (owningAppName == "")
                    {
                        std::cout << "Error: OsApplication not found for the OsResource \'" + osResourceName.toStdString() + "\'.\n";
                        return false;
                    }
                    configStream << "\t\t\tAPPLICATION_ID_" << owningAppName << ",\n\t\t\t(1 << APPLICATION_ID_" << owningAppName << ")\n";
                    configStream << streamText;
                    configStream << "\t)\n";
                    if (resIter != OsResources.size()-1)
                    {
                        configStream << "\t,\n";
                    }
                }
            }
            configStream << "};\n\n";
        }
        configStream << "// ##############################\tSTACKS (TASKS)\t############################\n";
        configStream << "DECLARE_STACK(OsIdle, OS_OSIDLE_STACK_SIZE);\n";
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            QString stackSize = GET_VALUE_OF_PARAM(OsTasks[taskIter], "ArcOsTaskStackSize");
            if (stackSize == "")
            {
                std::cout << "Error: OsTask::ArcOsTaskStackSize is not defined for OsTask \'" + osTaskName.toStdString() + "\'.\n";
                return false;
            }
            configStream << "DECLARE_STACK(" << osTaskName << ", " << stackSize << ");\n";
        }
        configStream << "\n\n// ##################################\tTASKS\t#################################\n";
        configStream << "GEN_TASK_HEAD = {\n";
        configStream << "\tGEN_BTASK(\n";
        configStream << "\t\tOsIdle,\n";
        configStream << "\t\t\"OsIdle\",\n";
        configStream << "\t\t0,\n";
        configStream << "\t\tFULL,\n";
        configStream << "\t\tTRUE,\n";
        configStream << "\t\tNULL,\n";
        configStream << "\t\t0,\n";
        configStream << "\t\t1,\n";
        configStream << "\t\tOS_CORE_0_MAIN_APPLICATION,\n";
        configStream << "\t\t(1u << OS_CORE_0_MAIN_APPLICATION)\n";
        configStream << "\t),\n\n";
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            configStream << "\t{\n";
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            configStream << "\t\t.pid = TASK_ID_" << osTaskName << ",\n";
            configStream << "\t\t.name = \"" << osTaskName << "\",\n";
            configStream << "\t\t.entry = " << osTaskName << ",\n";
            QString priority = GET_VALUE_OF_PARAM(OsTasks[taskIter], "OsTaskPriority");
            if (priority == "")
            {
                priority = "1";
            }
            configStream << "\t\t.prio = " << priority << ",\n";
            QString schedule = GET_VALUE_OF_PARAM(OsTasks[taskIter], "OsTaskSchedule");
            if (schedule == "")
            {
                schedule = "FULL";
            }
            configStream << "\t\t.scheduling = " << schedule << ",\n";
            QList<QDomElement> events;
            AutosarToolFactory::FindReferencesToElement(&events, &OsTasks[taskIter], "OsTaskEventRef");
            if (events.size() > 0)
                configStream << "\t\t.proc_type = PROC_EXTENDED,\n";
            else
                configStream << "\t\t.proc_type = PROC_BASIC,\n";
            configStream << "\t\t.stack = {\n";
            configStream << "\t\t\t.size = sizeof stack_" << osTaskName << ",\n";
            configStream << "\t\t\t.top = stack_" << osTaskName << ",\n";
            configStream << "\t\t},\n";
            QDomElement OsTaskAutostart = AutosarToolFactory::FindFirstReferenceToElement(&OsTasks[taskIter], "OsTaskAutostart");
            if (!OsTaskAutostart.isNull())
                configStream << "\t\t.autostart = TRUE,\n";
            else
                configStream << "\t\t.autostart = FALSE,\n";
            configStream << "\t\t.resourceAccess = 0";
            QList<QDomElement> OsTaskResourceRefs;
            AutosarToolFactory::FindReferencesToElement(&OsTaskResourceRefs, &OsTasks[taskIter], "OsTaskResourceRef");
            if (OsTaskResourceRefs.size() > 0)
            {
                for (int resIter = 0; resIter < OsTaskResourceRefs.size(); resIter++)
                {
                    QString resourceRef = AutosarToolFactory::GetTextOfFirstChild(&OsTaskResourceRefs[resIter], "VALUE-REF");
                    if (resourceRef != "")
                    {
                        QDomElement referedResource = AutosarToolFactory::GetDomElementFromReference(&resourceRef, projectModuleRoot->ownerDocument().documentElement());
                        QString resourceName = AutosarToolFactory::GetTextOfFirstChild(&referedResource, "SHORT-NAME");
                        configStream << "| (1 << RES_ID_" << resourceName << ") ";
                    }
                }
            }
            configStream << ",\n";
            QString activation = GET_VALUE_OF_PARAM(OsTasks[taskIter], "OsTaskActivation");
            if (activation == "")
            {
                activation = "1";
            }
            configStream << "\t\t.activationLimit = " << activation  << ",\n";
            configStream << "\t\t.eventMask = 0";
            QList<QDomElement> OsTaskEventRefs;
            AutosarToolFactory::FindReferencesToElement(&OsTaskEventRefs, &OsTasks[taskIter], "OsTaskEventRef");
            if (OsTaskEventRefs.size() > 0)
            {
                for (int eventIter = 0; eventIter < OsTaskEventRefs.size(); eventIter++)
                {
                    QString eventRef = AutosarToolFactory::GetTextOfFirstChild(&OsTaskEventRefs[eventIter], "VALUE-REF");
                    if (eventRef != "")
                    {
                        QDomElement referedEvent = AutosarToolFactory::GetDomElementFromReference(&eventRef, projectModuleRoot->ownerDocument().documentElement());
                        QString eventName = AutosarToolFactory::GetTextOfFirstChild(&referedEvent, "SHORT-NAME");
                        configStream << " | EVENT_MASK_" << eventName;
                    }
                }
            }
            configStream << ",\n";
            QString owningAppName = "";
            GetOwningApplication(owningAppName, "OsAppTaskRef", OsApplications, OsTasks[taskIter], *projectModuleRoot);
            if (owningAppName == "")
            {
                std::cout << "Error: OsApplication not found for the OsTask \'" + osTaskName.toStdString() + "\'.\n";
                return false;
            }
            configStream << "\t\t.applOwnerId = APPLICATION_ID_" << owningAppName << ",\n\t\t.accessingApplMask = (1u << APPLICATION_ID_" << owningAppName << ")\n";
            configStream << GetAccessingApplications("OsTaskAccessingApplication", OsTasks[taskIter], *projectModuleRoot);
            configStream << "\t},\n\n";
        }
        configStream << "};\n\n// ##################################\tHOOKS\t#################################\n";
        QDomElement OsHooks = AutosarToolFactory::FindFirstReferenceToElement(&OsOS, "OsHooks");
        if (!OsHooks.isNull())
        {
            configStream << "GEN_HOOKS(\n";
            QString hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsStartupHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tStartupHook";
            configStream << ",\n";
            hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsProtectionHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tProtectionHook";
            configStream << ",\n";
            hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsShutdownHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tShutdownHook";
            configStream << ",\n";
            hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsErrorHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tErrorHook";
            configStream << ",\n";
            hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsPreTaskHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tPreTaskHook";
            configStream << ",\n";
            hookValue = GET_VALUE_OF_PARAM_WITH_RETURN(OsHooks, "OsPostTaskHook");
            if (hookValue == "")
            {
                hookValue = "0";
            }
            if (hookValue == "0")
                configStream << "\tNULL";
            else
                configStream << "\tPostTaskHook";
            configStream << "\n);\n\n";
        }
        configStream << "// ##################################\tISRS\t##################################\n";
        if (OsIsrs.size() > 0)
        {
            configStream << "GEN_ISR_HEAD = {\n";
            for (int isrIter = 0; isrIter < OsIsrs.size(); isrIter++)
            {
                configStream << "\tGEN_ISR";
                QString category = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "OsIsrCategory");
                if (category == "CATEGORY_1")
                    configStream << "1(\n";
                else if (category == "CATEGORY_2")
                    configStream << "2(\n";
                QString osIsrName = AutosarToolFactory::GetTextOfFirstChild(&OsIsrs[isrIter], "SHORT-NAME");
                configStream << "\t\t\"" << osIsrName << "\",\n";
                QString isrVector = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "ArcOsIsrVector");
                configStream << "\t\t" << isrVector << ",\n";
                QString isrPriority = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "ArcOsIsrPriority");
                configStream << "\t\t" << isrPriority << ",\n";
                QString isrHandler = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "ArcOsIsrHandlerFunction");
                configStream << "\t\t" << isrHandler << ",\n";
                QString owningAppName = "";
                GetOwningApplication(owningAppName, "OsAppIsrRef", OsApplications, OsIsrs[isrIter], *projectModuleRoot);
                if (owningAppName == "")
                {
                    std::cout << "Error: OsApplication not found for the OsIsr \'" + osIsrName.toStdString() + "\'.\n";
                    return false;
                }
                configStream << "\t\tAPPLICATION_ID_" << owningAppName << ",\n";
                if (category == "CATEGORY_2")
                {
                    configStream << "\t\t(0";
                    QList<QDomElement> OsIsrResourceRefs;
                    AutosarToolFactory::FindReferencesToElement(&OsIsrResourceRefs, &OsIsrs[isrIter], "OsIsrResourceRef");
                    if (OsIsrResourceRefs.size() > 0)
                    {
                        for (int resIter = 0; resIter < OsIsrResourceRefs.size(); resIter++)
                        {
                            QString resRef = AutosarToolFactory::GetTextOfFirstChild(&OsIsrResourceRefs[resIter], "VALUE-REF");
                            if (resRef != "")
                            {
                                QDomElement referedResource = AutosarToolFactory::GetDomElementFromReference(&resRef, projectModuleRoot->ownerDocument().documentElement());
                                QString resourceName = AutosarToolFactory::GetTextOfFirstChild(&referedResource, "SHORT-NAME");
                                configStream << " | (1 << RES_ID_" << resourceName << ") ";
                            }
                        }
                    }
                    configStream << "\t)\n";
                }
            }
            configStream << "};\n";
        }
        configStream << "\nGEN_ISR_MAP = {\n";
        if (OsIsrs.size() > 0)
        {
            for (int isrIter = 0; isrIter < OsIsrs.size(); isrIter++)
            {
                QString isrVector = GET_VALUE_OF_PARAM(OsIsrs[isrIter], "ArcOsIsrVector");
                configStream << "\t[" << isrVector << "] = " << isrIter;
                if (isrIter != OsIsrs.size() - 1)
                {
                    configStream << ",";
                }
            }
        }
        else
            configStream << "\t0";
        configStream << "\n};\n\n// ############################\tSCHEDULE TABLES\t#############################\n";
        configFile.close();

        // ----------------------------------------- Create Os.orti -----------------------------------------
        QString ortiFileName = location + "/Os.orti";
        QFile ortiFile(ortiFileName);
        if (!ortiFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Os.orti file.\n";
            return false;
        }
        QTextStream ortiStream( &ortiFile );
        ortiStream << "VERSION\n{\n\tKOIL = \"2.2\";\n\tOSSEMANTICS = \"ORTI\", \"2.2\";\n};\n\nIMPLEMENTATION ArcCore_ORTI {\n";
        ortiStream << "\tOS {\n";
        ortiStream << "\t\tENUM [\n\t\t\t\"NO_TASK\" = 0x0,\n\t\t\t\"OsIdle\" = \"&(Os_TaskVarList[0])\",\n";
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            ortiStream << "\t\t\t\"" << osTaskName << "\" = \"&(Os_TaskVarList[" << taskIter + 1 << "])\"";
            if (taskIter != OsTasks.size()-1)
                ortiStream << ",";
            ortiStream << "\n";
        }
        ortiStream << "\t\t] RUNNINGTASK, \"Running Task Identification\";\n";
        ortiStream << "\t\tENUM [\n\t\t\t\"NO_ISR2\" = 0x0,\n\t\t\t\"ISR_0\" = \"&(Os_IsrVarList[0])\"\n\t\t] RUNNINGISR2, \"ISR Identification\";\n";
        ortiStream << "\t\tENUM \"unsigned char\" [\n\t\t] RUNNINGTASKPRIORITY, \"Priority of running task\";\n";
        ortiStream << "\t}; // OS\n\n";
        ortiStream << "\tTASK {\n\t\tENUM \"unsigned short\" [\n\t\t] PRIORITY, \"Actual Prio\";\n\t\tENUM \"void*\" [\n";
        ortiStream << "\t\t] STACK, \"Task Stack\";\n\t\tCTYPE \"unsigned char\" EvWait, \"Event Wait\";\n\t\tCTYPE \"unsigned char\" EvSet, \"Event Set\";\n";
        ortiStream << "\t\tSTRING TaskType, \"Task Type\";\n\t\tCTYPE \"unsigned char\" MaxActivations, \"Max. Activations\";\n";
        ortiStream << "\t\tCTYPE \"unsigned char\" Activations, \"Activations\";\n\t\tENUM \"unsigned char\" [\n\t\t\t\"ST_READY\"=1,\n";
        ortiStream << "\t\t\t\"ST_WAITING\"=2,\n\t\t\t\"ST_SUSPENDED\"=4,\n\t\t\t\"ST_RUNNING\"=8,\n\t\t\t\"ST_NOT_STARTED\"=16,\n";
        ortiStream << "\t\t\t\"ST_SLEEPING\"=32,\n\t\t\t\"ST_WAITING_SEM\"=64\n\t\t] STATE, \"State\";\n";
        ortiStream << "\t}; // TASK\n\n";
        ortiStream << "\tSTACK {\n\t\tCTYPE \"unsigned short\" SIZE, \"Stack Size (Byte)\";\n\t\tCTYPE \"unsigned short*\" BASEADDRESS, \"Stack Start Address\";\n";
        ortiStream << "\t\tSTRING STACKDIRECTION, \"Stack Direction\";\n\t}, \"Stacks\"; // STACK\n\n\tEVENT {\n\t\tSTRING EvMask, \"Event Mask\";\n";
        ortiStream << "\t}, \"Events\"; // EVENT\n\n\tALARM {\n\t\tENUM \"unsigned char\" [\n\t\t\t\"STOPPED\" = 0,\n\t\t\t\"RUNNING\" = 1\n";
        ortiStream << "\t\t] STATE, \"Alarm State\";\n\t\tCTYPE \"unsigned long\" CYCLETIME, \"Cycle Time (Ticks)\";\n";
        ortiStream << "\t\tCTYPE \"unsigned long\" ALARMTIME, \"Alarm Time (Ticks)\";\n\t\tSTRING ACTION, \"Action\";\n\t\tSTRING COUNTER, \"Counter\";\n";
        ortiStream << "\t}, \"Alarms\"; // ALARM\n\n\tCOUNTER {\n\t\tCTYPE \"unsigned long\" VALUE, \"Current value\";\n\t}, \"Counters\"; // COUNTER\n\n";
        ortiStream << "\tRESOURCE {\n\t\tENUM \"unsigned char\" [\n\t\t\t\"LOCKED\"=1,\n\t\t\t\"UNLOCKED\"=0\n\t\t] STATE, \"Resource state\";\n";
        ortiStream << "\t\tENUM \"unsigned short\" [\n\t\t\t\"-\" = 0xFFFF\n\t\t] OWNER, \"Resource owner\";\n";
        ortiStream << "\t\tENUM \"unsigned short\" [\n\t\t\t\"-\" = 0xFFFF\n\t\t] PRIORITY, \"Ceil. priority\";\n\t}, \"Resources\"; // RESOURCE\n\n";
        ortiStream << "}; //IMPLEMENTATION\n\nOS ArcCore {\n\tRUNNINGTASK = \"Os_Sys[0].currTaskPtr\";\n\tRUNNINGISR2 = \"Os_Sys[0].currIsrPtr\";\n";
        ortiStream << "};\n\nTASK OsIdle {\n\tPRIORITY = \"Os_TaskVarList[0].activePriority\";\n\tSTATE = \"Os_TaskVarList[0].state\";\n";
        ortiStream << "\tSTACK = \"stack_OsIdle\";\n};\n\nSTACK OsIdle {\n\tSIZE = \"" << idleStackSize << "\";\n\tBASEADDRESS = \"stack_OsIdle[1]\";\n";
        ortiStream << "\tSTACKDIRECTION = \"DOWN\";\n};\n\n";
        for (int taskIter = 0; taskIter < OsTasks.size(); taskIter++)
        {
            QString osTaskName = AutosarToolFactory::GetTextOfFirstChild(&OsTasks[taskIter], "SHORT-NAME");
            ortiStream << "TASK " << osTaskName << " {\n\tPRIORITY = \"Os_TaskVarList[" << taskIter + 1 << "].activePriority\";\n";
            ortiStream << "\tSTATE = \"Os_TaskVarList[" << taskIter + 1 << "].state\";\n";
            QList<QDomElement> events;
            AutosarToolFactory::FindReferencesToElement(&events, &OsTasks[taskIter], "OsTaskEventRef");
            if (events.size() > 0)
            {
                ortiStream << "\tTaskType = \"EXTENDED\";\n\tEvWait = \"Os_TaskVarList[" << taskIter + 1 << "].ev_wait\";\n";
                ortiStream << "\tEvSet = \"Os_TaskVarList[" << taskIter + 1 << "].ev_set\";\n";
            }
            else
            {
                ortiStream << "\tTaskType = \"BASIC\";\n\tActivations = \"Os_TaskVarList[" << taskIter + 1 << "].activations\";\n";
                ortiStream << "\tMaxActivations = \"Os_TaskVarList[" << taskIter + 1 << "].constPtr->activationLimit\";\n";
            }
            ortiStream << "\tSTACK = \"stack_" << osTaskName << "\";\n};\n\nSTACK " << osTaskName << " {\n";
            QString stackSize = GET_VALUE_OF_PARAM(OsTasks[taskIter], "ArcOsTaskStackSize");
            ortiStream << "\tSIZE = \"" << stackSize;
            ortiStream << "\";\n\tBASEADDRESS = \"stack_" << osTaskName << "[1]\";\n\tSTACKDIRECTION = \"DOWN\";\n};\n\n";
        }
        ortiStream << "\n";
        for (int counterIter = 0; counterIter < OsCounters.size(); counterIter++)
        {
            QString osCounterName = AutosarToolFactory::GetTextOfFirstChild(&OsCounters[counterIter], "SHORT-NAME");
            ortiStream << "COUNTER " << osCounterName << " {\n\tVALUE = \"counter_list[" << counterIter << "].val\";\n};\n";
        }
        ortiStream << "\n";
        for (int alarmIter = 0; alarmIter < OsAlarms.size(); alarmIter++)
        {
            QString osAlarmName = AutosarToolFactory::GetTextOfFirstChild(&OsAlarms[alarmIter], "SHORT-NAME");
            ortiStream << "ALARM " << osAlarmName << " {\n\tSTATE = \"(alarm_list[" << alarmIter << "].active) ? 1 : 0\";\n";
            ortiStream << "\tALARMTIME = \"alarm_list[" << alarmIter << "].alarmtime\";\n";
            ortiStream << "\tCYCLETIME = \"alarm_list[" << alarmIter << "].cycletime\";\n";
            QDomElement OsAlarmAction = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarms[alarmIter], "OsAlarmAction");
            QDomElement OsAlarmActivateTask = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmActivateTask");
            if (!OsAlarmActivateTask.isNull())
            {
                QString taskRef = GET_VALUE_REF_OF_REF(OsAlarmActivateTask, "OsAlarmActivateTaskRef");
                QDomElement referedTask = AutosarToolFactory::GetDomElementFromReference(&taskRef, projectModuleRoot->ownerDocument().documentElement());
                QString taskName = AutosarToolFactory::GetTextOfFirstChild(&referedTask, "SHORT-NAME");
                ortiStream << "\tACTION = \"ActivateTask " << taskName << "\";\n";
            }
            QDomElement OsAlarmSetEvent = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmSetEvent");
            if (!OsAlarmSetEvent.isNull())
            {
                QString taskRef = GET_VALUE_REF_OF_REF(OsAlarmSetEvent, "OsAlarmSetEventTaskRef");
                QDomElement referedTask = AutosarToolFactory::GetDomElementFromReference(&taskRef, projectModuleRoot->ownerDocument().documentElement());
                QString taskName = AutosarToolFactory::GetTextOfFirstChild(&referedTask, "SHORT-NAME");
                ortiStream << "\tACTION = \"SetEvent " << taskName << "\";\n";
            }
            QDomElement OsAlarmIncrementCounter = AutosarToolFactory::FindFirstReferenceToElement(&OsAlarmAction, "OsAlarmIncrementCounter");
            if (!OsAlarmIncrementCounter.isNull())
            {
                QString counterRef = GET_VALUE_REF_OF_REF(OsAlarmIncrementCounter, "OsAlarmIncrementCounterRef");
                QDomElement referedCounter = AutosarToolFactory::GetDomElementFromReference(&counterRef, projectModuleRoot->ownerDocument().documentElement());
                QString counterName = AutosarToolFactory::GetTextOfFirstChild(&referedCounter, "SHORT-NAME");
                ortiStream << "\tACtION = \"IncreaseCounter " << counterName << "\";\n";
            }
            QString counterRef = GET_VALUE_REF_OF_REF(OsAlarms[alarmIter], "OsAlarmCounterRef");
            QDomElement referedCounter = AutosarToolFactory::GetDomElementFromReference(&counterRef, projectModuleRoot->ownerDocument().documentElement());
            QString counterName = AutosarToolFactory::GetTextOfFirstChild(&referedCounter, "SHORT-NAME");
            ortiStream << "\tCOUNTER = \"" << counterName << "\";\n";
            ortiStream << "};\n";
        }
        ortiStream << "\n";
        if (OsResources.size() > 0)
        {
            ortiStream << "RESOURCE RES_SCHEDULER {\n\tSTATE = \"(Os_Sys[0].resScheduler.owner != 0xffff) ? 1 : 0\";\n";
            ortiStream << "\tOWNER = \"Os_Sys[0].resScheduler.owner\";\n\tPRIORITY = \"(Os_Sys[0].resScheduler.ceiling_priority)\";\n};\n\n";
            for (int resIter = 0; resIter < OsResources.size(); resIter++)
            {
                QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
                if (prop != "LINKED")
                {
                    QString osResourceName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                    ortiStream << "RESOURCE " << osResourceName << " {\n\tSTATE = \"(resource_list[" << resIter << "].owner != 0xffff) ? 1 : 0\";\n";
                    ortiStream << "\tOWNER = \"resource_list[" << resIter << "].owner\";\n\tPRIORITY = \"resource_list[" << resIter << "].ceiling_priority\";\n};\n";
                }
            }
            ortiStream << "\n";
        }

        ortiFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Os.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create Os.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += KERNEL\n";
        makeFile.close();
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

void OsGenerator::GetOwningApplication(QString& returnAppName, QString searchText, QList<QDomElement>& applications, QDomElement& referedElement, QDomElement &modeuleRoot)
{
    for (int appIter = 0; appIter < applications.size(); appIter++)
    {
        QList<QDomElement> OsAppRefs;
        AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &applications[appIter], searchText);
        for (int refIter = 0; refIter < OsAppRefs.size(); refIter++)
        {
            QString ref = AutosarToolFactory::GetTextOfFirstChild(&OsAppRefs[refIter], "VALUE-REF");
            QDomElement referedElem = AutosarToolFactory::GetDomElementFromReference(&ref, modeuleRoot.ownerDocument().documentElement());
            if (referedElem == referedElement)
            {
                returnAppName = AutosarToolFactory::GetTextOfFirstChild(&applications[appIter], "SHORT-NAME");
                break;
            }
        }
    }
}

QString OsGenerator::GetAccessingApplications(QString searchText, QDomElement& referedElement, QDomElement &modeuleRoot)
{
    QString streamText;
    QList<QDomElement> accessingApps;
    AutosarToolFactory::FindReferencesToElement(&accessingApps, &referedElement, searchText);
    for (int appIter = 0; appIter< accessingApps.size(); appIter++)
    {
        QString appRef = AutosarToolFactory::GetTextOfFirstChild(&accessingApps[appIter], "VALUE-REF");
        if (appRef != "")
        {
            QDomElement referedApp = AutosarToolFactory::GetDomElementFromReference(&appRef, modeuleRoot.ownerDocument().documentElement());
            streamText += "\t\t\t| (1 << APPLICATION_ID_" + AutosarToolFactory::GetTextOfFirstChild(&referedApp, "SHORT-NAME") + ")\n";
        }
    }
    return streamText;
}

bool OsGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;
    // Incomprehensible requirements: SWS_Os_00050, 00562, 00177

    // SWS_Os_00173: Provide consistency checks
    try
    {
        // Miscellaneous Checks
        // Make sure that all the references within the module are valid
        QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
        for (int refIter = 0; refIter < refs.size(); refIter++)
        {
            QString ref = refs.at(refIter).toElement().text();
            if (ref != "")
            {
                QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
                if (tempElem.isNull())
                {
                    std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                                 AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                    returnValue =  false;
                }
            }
        }
        // Make sure that linked OsResources have valid reference to other OsResources
        QList<QDomElement> OsResources;
        AutosarToolFactory::FindReferencesToElement(&OsResources, projectModuleRoot, "OsResource");
        for (int resIter = 0; resIter < OsResources.size(); resIter++)
        {
            QString prop = GET_VALUE_OF_PARAM(OsResources[resIter], "OsResourceProperty");
            if (prop == "LINKED")
            {
                QString osResourcesName = AutosarToolFactory::GetTextOfFirstChild(&OsResources[resIter], "SHORT-NAME");
                QString ref = GET_VALUE_REF_OF_REF_WITH_RETURN(OsResources[resIter], "OsResourceLinkedResourceRef");
                if (ref == "")
                {
                    std::cout << "Error: OsResourceLinkedResourceRef of OsResource \'" + osResourcesName.toStdString() + "\' is not defined.\n";
                    returnValue =  false;
                }
            }
        }
        // Make sure that the OsScalabilityClass is either SC1 or SC3. SC2 and SC4 are, so far, not supported.
        QDomElement OsOS = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "OsOS");
        QString sC = GET_VALUE_OF_PARAM(OsOS, "OsScalabilityClass");
        if (!(sC == "SC1" || sC == "SC3"))
        {
            std::cout << "Error: OsOS::OsScalabilityClass \'" + sC.toStdString() + "\' is not supported by this version of generator.\n";
            returnValue =  false;
        }
        // Make sure that the ArcOsInterruptStackSize & ArcOsIdleStackSize are valid
        QString intStackSize = GET_VALUE_OF_PARAM(OsOS, "ArcOsInterruptStackSize");
        bool ok;
        int tempInt = intStackSize.toInt(&ok);
        if (!ok)
        {
            std::cout << "Error: OsOS::ArcOsInterruptStackSize \'" + intStackSize.toStdString() + "\' is not a valid Stack Size.\n";
            returnValue =  false;
        }
        tempInt++; // To avoid compile warning
        QString idleStackSize = GET_VALUE_OF_PARAM(OsOS, "ArcOsIdleStackSize");
        tempInt = idleStackSize.toInt(&ok);
        if (!ok)
        {
            std::cout << "Error: OsOS::ArcOsIdleStackSize \'" + idleStackSize.toStdString() + "\' is not a valid Stack Size.\n";
            returnValue =  false;
        }
        // Make sure that the OsEventMasks are valid (i.e. only 1 bit set)
        QList<QDomElement> OsEvents;
        AutosarToolFactory::FindReferencesToElement(&OsEvents, projectModuleRoot, "OsEvent");
        for (int eventIter = 0; eventIter < OsEvents.size(); eventIter++)
        {
            QString mask = GET_VALUE_OF_PARAM_WITH_RETURN(OsEvents[eventIter], "OsEventMask");
            if (mask != "")
            {
                int integerMask = mask.toInt(&ok);
                Q_ASSERT(ok);
                if (!isMaskValid(integerMask, 64))
                {
                    std::cout << "Error: \'" + mask.toStdString() + "\' is not a valid OsEventMask.\n";
                    returnValue =  false;
                }
                else
                {
                    // Make sure that all OsEvents have different OsEventMask
                    for (int eventIter2 = eventIter+1; eventIter2 < OsEvents.size(); eventIter2++)
                    {
                        QString mask2 = GET_VALUE_OF_PARAM(OsEvents[eventIter2], "OsEventMask");
                        if (mask == mask2)
                        {
                            std::cout << "Error: Multiple OsEvents have same OsEventMasks \'" + mask.toStdString() + "\'.\n";
                            returnValue =  false;
                        }
                    }
                }
            }
        }

        // Make sure that there is at least 1 OsApplication
        QList<QDomElement> OsApplications;
        AutosarToolFactory::FindReferencesToElement(&OsApplications, projectModuleRoot, "OsApplication");
        //std::cout << "Info: " + QString::number(OsApplications.size()) + " application found.\n";
        if (OsApplications.size() == 0)
        {
            std::cout << "Error: No OsApplication is defined.\n";
            returnValue =  false;
        }
        // Make sure that the OsTrusted is true for scalability class SC1 and SC2
        if (sC == "SC1" || sC == "SC2")
        {
            for (int appIter = 0; appIter < OsApplications.size(); appIter++)
            {
                QString trusted = GET_VALUE_OF_PARAM(OsApplications[appIter], "OsTrusted");
                if (trusted == "0")
                {
                    std::cout << "Error: OsScalabilityClass \'" + sC.toStdString() + "\' requires that the OsApplications are trusted.\n";
                    returnValue =  false;
                }
            }
        }

        // SWS_Os_00311: If OsScalabilityClass is SC3 or SC4 AND a Task OR Category 2 ISR OR Counters OR Alarms OR Schedule tables does not
        // belong to exactly one OSApplication the consistency check shall issue an error.
        // SWS_Os_00361: If OsScalabilityClass is SC3ArcOsInterruptStackSize or SC4 AND a Category 1 ISR does not belong to exactly one trusted OS Application the
        // consistency check shall issue an error.(Comment: Already covered by miscellaneous check)
        // Comment: Checking for all OsScalabilityClasses
        QList<QDomElement> notOwnedOsElements;
        for (int osElemIter = 0; osElemIter < 5; osElemIter++)
        {
            if (osElemIter == 0)
            {
                AutosarToolFactory::FindReferencesToElement(&notOwnedOsElements, projectModuleRoot, "OsTask");
            }
            else if (osElemIter == 1)
            {
                AutosarToolFactory::FindReferencesToElement(&notOwnedOsElements, projectModuleRoot, "OsAlarm");
            }
            else if (osElemIter == 2)
            {
                AutosarToolFactory::FindReferencesToElement(&notOwnedOsElements, projectModuleRoot, "OsConter");
            }
            else if (osElemIter == 3)
            {
                AutosarToolFactory::FindReferencesToElement(&notOwnedOsElements, projectModuleRoot, "OsIsr");
            }
            else if (osElemIter == 4)
            {
                AutosarToolFactory::FindReferencesToElement(&notOwnedOsElements, projectModuleRoot, "OsScheduleTable");
            }
            for (int appIter = 0; appIter < OsApplications.size(); appIter++)
            {
                QList<QDomElement> OsAppRefs;
                if (osElemIter == 0)
                {
                    AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &OsApplications[appIter], "OsAppTaskRef");
                }
                else if (osElemIter == 1)
                {
                    AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &OsApplications[appIter], "OsAppAlarmRef");
                }
                else if (osElemIter == 2)
                {
                    AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &OsApplications[appIter], "OsAppCounterRef");
                }
                else if (osElemIter == 3)
                {
                    AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &OsApplications[appIter], "OsAppIsrRef");
                }
                else if (osElemIter == 4)
                {
                    AutosarToolFactory::FindReferencesToElement(&OsAppRefs, &OsApplications[appIter], "OsAppScheduleTableRef");
                }
                //std::cout << "Info: OsApplication " + QString::number(appIter) + " has " + QString::number(OsAppRefs.size()) + " refs for " + QString::number(osElemIter) + ".\n";
                for (int elemRefIter = 0; elemRefIter < OsAppRefs.size(); elemRefIter++)
                {
                    for (int elemIter = 0; elemIter < notOwnedOsElements.size(); elemIter++)
                    {
                        QString osElemName = AutosarToolFactory::GetTextOfFirstChild(&notOwnedOsElements[elemIter], "SHORT-NAME");
                        if (AutosarToolFactory::GetTextOfFirstChild(&OsAppRefs[elemRefIter], "VALUE-REF").contains(osElemName))
                        {
                            //std::cout << "Info: " + osElemName + " is referenced.\n";
                            notOwnedOsElements.removeAt(elemIter);
                            break;
                        }
                    }
                }
            }
            if (notOwnedOsElements.size() > 0)
            {
                for (int taskIter = 0; taskIter < notOwnedOsElements.size(); taskIter++)
                {
                    std::cout << "Error: " + AutosarToolFactory::GetTextOfFirstChild(&notOwnedOsElements[taskIter], "SHORT-NAME").toStdString() + " is not owned by any OsApplication (SWS_Os_00311, SWS_Os_00361).\n";
                }
                notOwnedOsElements.clear();
                returnValue =  false;
            }
        }

        // SWS_Os_00045: If timing protection is configured together with OSEK OS Category 1 interrupts, the consistency check shall issue a warning.
        QList<QDomElement> OsIsr;
        AutosarToolFactory::FindReferencesToElement(&OsIsr, projectModuleRoot, "OsIsr");
        for (int isrIter = 0; isrIter < notOwnedOsElements.size(); isrIter++)
        {
            QString category = GET_VALUE_OF_PARAM(OsIsr[isrIter], "OsIsrCategory");
            if (category == "CATEGORY_1")
            {
                QDomElement OsIsrTimingProtection = AutosarToolFactory::FindFirstReferenceToElement(&OsIsr[isrIter], "OsIsrTimingProtection");
                if (!OsIsrTimingProtection.isNull())
                {
                    std::cout << "Error: OsIsrTimingProtection is not allowed with CATEGORY_1 OsIsrs (SWS_Os_00045).\n";
                    returnValue =  false;
                }
            }
        }

        // SWS_Os_00328: If OsStatus is STANDARD and OsScalabilityClass is SC3 or SC4 the consistency check shall issue an error.
        if (sC == "SC3" || sC == "SC4")
        {
            QString status = GET_VALUE_OF_PARAM(OsOS, "OsStatus");
            if (status == "STANDARD")
            {
                std::cout << "Error: STANDARD OsStatus is not supported by OsScalabilityClass \'" + sC.toStdString() + "\' (SWS_Os_00328).\n";
                returnValue =  false;
            }
        }

        // SWS_Os_00343: If OsScalabilityClass is SC3 or SC4 AND a task is referenced within a schedule table object AND the OS Application
        // of the schedule table has no access to the task, the consistency check shall issue an error.

        // SWS_Os_00344: If OsScalabilityClass is SC3 or SC4 AND a task is referenced  within an alarm object AND the OS Application of
        // the  alarm  has  no  access  to  the task, the consistency check shall issue an error.

        // SWS_Os_00440: If a schedule table has OsScheduleTblSyncStrategy = IMPLICIT and the OsCounterMaxAllowedValue+1 of the associated  counter
        // is not equal to the duration of the schedule table then the consitency check shall issue an error.

        // SWS_Os_00461: If OsScalabilityClass is SC2, SC3 or SC4 AND Alarm Callbacks are configured the conistency check shall isuue an error.

        // SWS_Os_00303: If OsAlarmIncrementCounter is configured as action on alarm expiry AND the alarm is driven directly or indirectly
        // (a cyclic chain of alarm actions with OsAlarmIncrementCounter) by that counter, the consistency check shall issue a warning

    }
    catch(QString err)
    {
        std::cout << err.toStdString() << "\n";
        returnValue =  false;
    }

    return returnValue;
}
