/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "comm_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool ComMGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of ComM Module Configuration files does not exist.\n";
        return false;
    }

    try {
			// ----------------------------------------- Create Makefile -----------------------------------------
            QString makeFileName = location + "/ComM.mk";
			QFile makeFile(makeFileName);
			if (!makeFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create ComM.mk file.\n";
				  return false;
			}
			QTextStream makeFileStream( &makeFile );
            makeFileStream << "MOD_USE += COMM\n";
			makeFile.close();

            // ----------------------------------------- Create ComM_Cfg.h -----------------------------------------
            QString configHeaderName = location + "/ComM_Cfg.h";
			QFile headerFile(configHeaderName);
			if (!headerFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create ComM_Cfg.h file.\n";
				  return false;
			}
            QTextStream stream( &headerFile );
            stream << "\n#if !(((COMM_SW_MAJOR_VERSION == 2) && (COMM_SW_MINOR_VERSION == 2)) )\n";
            stream << "#error ComM: Configuration file expected BSW module version to be 2.2.*\n#endif\n\n";
            stream << "#if !(((COMM_AR_RELEASE_MAJOR_VERSION == 4) && (COMM_AR_RELEASE_MINOR_VERSION == 0)) )\n";
            stream << "#error ComM: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n";
            stream << "#ifndef COMM_CFG_H_\n#define COMM_CFG_H_\n\n";
            QDomElement ComMGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "ComMGeneral");
            QString parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMDevErrorDetect");
            stream << "#define COMM_DEV_ERROR_DETECT\t";
            if (parameterValue == "1") {
                stream << "STD_ON";
            } else {
                stream << "STD_OFF";
            }
            stream << "\n#define COMM_VERSION_INFO_API\t";
            parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMVersionInfoApi");
            if (parameterValue == "1") {
                stream << "STD_ON";
            } else {
                stream << "STD_OFF";
            }
            stream << "\n#define COMM_MODE_LIMITATION_ENABLED\t";
            parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMModeLimitationEnabled");
            if (parameterValue == "1") {
                stream << "STD_ON";
            } else {
                stream << "STD_OFF";
            }
            stream << "\n#define COMM_NO_COM\t";
            parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMNoCom");
            if (parameterValue == "1") {
                stream << "STD_ON";
            } else {
                stream << "STD_OFF";
            }
            stream << "\n#define COMM_RESET_AFTER_FORCING_NO_COMM\tSTD_OFF\t\t// Not supported\n";
            stream << "#define COMM_SYNCHRONOUS_WAKE_UP\tSTD_OFF\t\t// Not supported\n";
            stream << "#define COMM_PNC_SUPPORT\t";
            parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMPncSupport");
            if (parameterValue == "1") {
                stream << "STD_ON";
            } else {
                stream << "STD_OFF";
            }
            QDomElement ComMConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "ComMConfigSet");
            QList <QDomElement> ComMPncs;
            stream << "\n#define COMM_PNC_NUM\t";
            if (parameterValue == "1") {
                AutosarToolFactory::FindReferencesToElement(&ComMPncs, &ComMConfigSet, "ComMPnc");
                stream << ComMPncs.size();
            } else {
                stream << "0u";
            }
            stream << "\n#define COMM_PNC_COMSIGNAL_VECTOR_LEN\t";
            if (parameterValue == "1") {
                // No clue
            } else {
                stream << "0";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComMGeneral, "ComMTMinFullComModeDuration");
            bool ok;
            float duration = parameterValue.toFloat(&ok);;
            Q_ASSERT(ok);
            stream << "\n#define COMM_T_MIN_FULL_COM_MODE_DURATION\t" << duration * 1000.0f << "\n";
            stream << "#define COMM_T_PNC_PREPARE_SLEEP\t";
            parameterValue = GET_VALUE_OF_PARAM_WITH_RETURN(ComMGeneral, "ComMPncPrepareSleepTimer");
            if (parameterValue.isEmpty()) {
                stream << "0\n";
            } else {
                // No clue
            }
            stream << "#define COMM_PNC_GATEWAY_ENABLED\t";
            parameterValue = GET_VALUE_OF_PARAM_WITH_RETURN(ComMGeneral, "ComMPncGatewayEnabled");
            if (parameterValue.isEmpty() || parameterValue == "0") {
                stream << "STD_OFF";
            } else {
                stream << "STD_ON";
            }
            QList <QDomElement> ComMChannels;
            AutosarToolFactory::FindReferencesToElement(&ComMChannels, &ComMConfigSet, "ComMChannel");
            stream << "\n\n#define COMM_CHANNEL_COUNT\t" << ComMChannels.size() << "\n";
            QList <QDomElement> ComMUsers;
            AutosarToolFactory::FindReferencesToElement(&ComMUsers, &ComMConfigSet, "ComMUser");
            stream << "#define COMM_USER_COUNT\t" << ComMUsers.size() << "\n\n";
            // MODIFY_POINT from LinSM/LinIf when indices are modified.
            for (int chIter = 0; chIter < ComMChannels.size(); chIter++) {
                stream << "#define COMM_NETWORK_HANDLE_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << " " << chIter << "u\n";
                stream << "#define ComMConf_ComMChannel_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << " " << chIter << "u\n";
            }
            stream << "\n#define INVALID_NETWORK_HANDLE 0xFFu\n#define INVALID_SIGNAL_HANDLE 0xFFFFu\n";
            for (int userIter = 0; userIter < ComMUsers.size(); userIter++) {
                stream << "#define COMM_USER_HANDLE_" << ComMUsers[userIter].firstChildElement("SHORT-NAME").text() << " ((ComM_UserHandleType)" << userIter << ")\n";
                stream << "#define ComMConf_ComMUser_" << ComMUsers[userIter].firstChildElement("SHORT-NAME").text() << " " << userIter << "u\n";
            }
            stream << "\n";
            for (int chIter = 0; chIter < ComMChannels.size(); chIter++) {
                stream << "void ComM_MainFunction_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << "(void);\n";
            }
            stream << "\n/* Call back function declaration */\n\n";
            stream << "/* Call back function declaration */\n/** @req ComM986 */\n/** @req ComM971 */\n\n";
            stream << "/* ComM module configuration. */\nextern const ComM_ConfigType ComM_Config;\n\n";
            stream << "#endif /* COMM_CFG_H_ */\n";
			headerFile.close();

            // ----------------------------------------- Create ComM_Cfg.c -----------------------------------------
            QString configFileName = location + "/ComM_Cfg.c";
			QFile configFile(configFileName);
			if (!configFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create ComM_Cfg.c file.\n";
				  return false;
			}
			QTextStream configStream( &configFile );
            configStream << "\n#include \"ComM.h\"\n\n";
            configStream << "#if defined(USE_CANSM)\n#include \"CanSM.h\"\n#endif\n\n";
            configStream << "#if defined(USE_NM)\n#include \"Nm.h\"\n#endif\n\n";
            configStream << "#if defined(USE_LINSM)\n#include \"LinSM.h\"\n#endif\n\n";
            configStream << "#if defined(USE_ETHSM)\n#include \"EthSM.h\"\n#endif\n\n";
            configStream << "const ComM_ChannelType ComM_Channels[COMM_CHANNEL_COUNT] = {\n";
            QString tempStr;
            for (int chIter = 0; chIter < ComMChannels.size(); chIter++) {
                configStream << "\t{\n";
                tempStr = GET_VALUE_OF_PARAM(ComMChannels[chIter], "ComMBusType");
                configStream << "\t\t.BusType = " << tempStr << ",\n";
                configStream << "\t\t.ComMChannelId = ComMConf_ComMChannel_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << ",\n";
                QDomElement ComMNetworkManagement = AutosarToolFactory::FindFirstReferenceToElement(&ComMChannels[chIter], "ComMNetworkManagement");
                if (ComMNetworkManagement.isNull()) {
                    std::cout << "Error: Undefined model element ComM::ComMConfigSet::ComMChannel::ComMNetworkManagement in " <<
                                 ComMChannels[chIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                    return false;
                }
                tempStr = GET_VALUE_OF_PARAM(ComMNetworkManagement, "ComMNmVariant");
                configStream << "\t\t.NmVariant = COMM_NM_VARIANT_" << tempStr << ",\n";
                tempStr = GET_VALUE_OF_PARAM(ComMChannels[chIter], "ComMMainFunctionPeriod");
                duration = tempStr.toFloat(&ok);
                Q_ASSERT(ok);
                configStream << "\t\t.MainFunctionPeriod = " << duration * 1000 << ",\n";
                tempStr = GET_VALUE_OF_PARAM_WITH_RETURN(ComMNetworkManagement, "ComMNmLightTimeout");
                if (tempStr.isEmpty())
                    tempStr = "10.0";
                duration = tempStr.toFloat(&ok);
                Q_ASSERT(ok);
                configStream << "\t\t.LightTimeout = " << duration * 1000 << ",\n";
                configStream << "\t\t.PncGatewayType = COMM_GATEWAY_TYPE_NONE\n";
                configStream << "\t}";
                if (chIter != ComMChannels.size()-1)
                    configStream << ",";
                configStream << "\n";
            }
            configStream << "};\n\n";
            QList <int> userChannelCount;
            for (int userIter = 0; userIter < ComMUsers.size(); userIter++) {           // TODO optimize the loops
                int channelCount = 0;
                configStream << "const ComM_ChannelType* ComM_" << ComMUsers[userIter].firstChildElement("SHORT-NAME").text() << "_Channels[] = {\n";
                for (int chIter = 0; chIter < ComMChannels.size(); chIter++) {
                    QList <QDomElement> ComMUserPerChannels;
                    AutosarToolFactory::FindReferencesToElement(&ComMUserPerChannels, &ComMChannels[chIter], "ComMUserPerChannel");
                    for (int upcIter = 0; upcIter < ComMUserPerChannels.size(); upcIter++) {
                        QString ref = GET_VALUE_REF_OF_REF(ComMUserPerChannels[upcIter], "ComMUserChannel");
                        if (ref.isEmpty()) {
                            std::cout << "Error: Undefined reference ComM::ComMConfigSet::ComMChannel::ComMUserPerChannel::ComMUserChannel in " <<
                                         ComMUserPerChannels[upcIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                            return false;
                        }
                        QDomElement user = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
                        if (user.isNull()) {
                            std::cout << "Error: Invalid reference ComM::ComMConfigSet::ComMChannel::ComMUserPerChannel::ComMUserChannel in " <<
                                         ComMUserPerChannels[upcIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                            return false;
                        }
                        if (user.firstChildElement("SHORT-NAME").text() == ComMUsers[userIter].firstChildElement("SHORT-NAME").text()) {
                            configStream << "\t&ComM_Channels[" << chIter << "]\n";
                            channelCount++;
                            break;
                        }
                    }
                    // TODO What to do if no user is specified??? Is it important?
                }
                userChannelCount.push_back(channelCount);
                configStream << "};\n";
            }
            configStream << "const ComM_UserType ComM_Users[COMM_USER_COUNT] = {\n";
            for (int userIter = 0; userIter < ComMUsers.size(); userIter++) {
               configStream << "\t{\n\t\t/** @req ComM995 */\n";
               configStream << "\t\t.ChannelList = ComM_" << ComMUsers[userIter].firstChildElement("SHORT-NAME").text() << "_Channels,\n";
               configStream << "\t\t.ChannelCount = " << userChannelCount[userIter] << ",\n";
               configStream << "\t\t.PncChnlList = NULL,\n\t\t.PncChnlCount = 0,\n";
               configStream << "\t}";
               if (userIter != ComMUsers.size()-1)
                   configStream << ",";
               configStream << "\n";
            }
            configStream << "};\nconst ComM_ConfigType ComM_Config = {\n\t.Channels = ComM_Channels,\n\t.Users = ComM_Users,\n\t.ComMPncConfig = NULL\n};\n\n";
            configStream << "extern void ComM_MainFunction(NetworkHandleType Channel);\n\n/* @req COMM818 */\n";
            for (int chIter = 0; chIter < ComMChannels.size(); chIter++) {
                configStream << "void ComM_MainFunction_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << "(void) {\n";
                configStream << "\tComM_MainFunction(ComMConf_ComMChannel_" << ComMChannels[chIter].firstChildElement("SHORT-NAME").text() << ");\n}\n";
            }
            configStream << "\n/** @req ComM986 */\n/** @req ComM971 */\n\n";
            configFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool ComMGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
		bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
					QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
						if (tempElem.isNull()) {
						    std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
				                         AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
						    returnValue = false;
						}
				}
    }

    return returnValue;
}

