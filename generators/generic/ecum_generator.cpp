/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "ecum_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool EcuMGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " << location.toStdString() << " for generation of EcuM Module Configuration files does not exist.\n";
        return false;
    }

    try {
        // ----------------------------------------- Create EcuM_GeneratedCallouts.c -----------------------------------------
        QString calloutsFileName = location + "/EcuM_GeneratedCallouts.c";
        QFile calloutsFile(calloutsFileName);
        if (!calloutsFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create EcuM_GeneratedCallouts.c file.\n";
            return false;
        }
        QTextStream calloutsStream( &calloutsFile );
        calloutsStream << "\n/*\n * No EcuMDriverInitListZero, EcuMDriverInitListOne or EcuMDriverRestartList. Using callouts\n";
        calloutsStream << " * EcuM_AL_DriverInitZero, EcuM_AL_DriverInitOne and EcuM_AL_DriverRestart from EcuM_CalloutStubs.c.\n */\n";
        calloutsFile.close();

        // ----------------------------------------- Create EcuM_Cfg.h -----------------------------------------
		QString configHeaderName = location + "/EcuM_Cfg.h";
		QFile headerFile(configHeaderName);
        if (!headerFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create EcuM_Cfg.h file.\n";
		    return false;
		}
		QTextStream headerStream( &headerFile );
        headerStream << "\n#ifndef ECUM_CFG_H_\n#define ECUM_CFG_H_\n\n";
        QDomElement EcuMGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "EcuMGeneral");
        QString EcuMVersionInfoApi = GET_VALUE_OF_PARAM(EcuMGeneral, "EcuMVersionInfoApi");
        headerStream << "#define ECUM_VERSION_INFO_API\t" << GetStdOnOff(EcuMVersionInfoApi) << "\n// @req EcuM2983\n";
        QString EcuMDevErrorDetect = GET_VALUE_OF_PARAM(EcuMGeneral, "EcuMDevErrorDetect");
        headerStream << "#define ECUM_DEV_ERROR_DETECT\t" << GetStdOnOff(EcuMDevErrorDetect) << "\n\n";
        QString EcuMMainFunctionPeriod = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMGeneral, "EcuMMainFunctionPeriod");
        if (EcuMMainFunctionPeriod == "")
            EcuMMainFunctionPeriod = "0.0";
        bool ok;
        float periodSeconds = EcuMMainFunctionPeriod.toFloat(&ok);
        Q_ASSERT(ok);
        headerStream << "#define ECUM_MAIN_FUNCTION_PERIOD\t(" << ceil(periodSeconds*1000.0f) << ")\n";
        QDomElement EcuMConfiguration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "EcuMConfiguration");
        QDomElement EcuMFixedConfiguration = AutosarToolFactory::FindFirstReferenceToElement(&EcuMConfiguration, "EcuMFixedConfiguration");
        QString EcuMNvramReadallTimeout = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMFixedConfiguration, "EcuMNvramReadallTimeout");
        if (EcuMNvramReadallTimeout == "")
            EcuMNvramReadallTimeout = "0.0";
        float readTimeoutSeconds = EcuMNvramReadallTimeout.toFloat(&ok);
        Q_ASSERT(ok);
        headerStream << "#define ECUM_NVRAM_READALL_TIMEOUT\t(" << (int)(readTimeoutSeconds*1000.0f) << ")\n";
        QString EcuMNvramWriteallTimeout = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMFixedConfiguration, "EcuMNvramWriteallTimeout");
        if (EcuMNvramWriteallTimeout == "")
            EcuMNvramWriteallTimeout = "0.0";
        float writeTimeoutSeconds = EcuMNvramWriteallTimeout.toFloat(&ok);
        Q_ASSERT(ok);
        headerStream << "#define ECUM_NVRAM_WRITEALL_TIMEOUT\t(" << (int)(writeTimeoutSeconds*1000.0f) << ")\n";
        QString EcuMRunMinimumDuration = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMFixedConfiguration, "EcuMRunMinimumDuration");
        if (EcuMRunMinimumDuration == "")
            EcuMRunMinimumDuration = "0.0";
        float durationSeconds = EcuMRunMinimumDuration.toFloat(&ok);
        Q_ASSERT(ok);
        headerStream << "#define ECUM_NVRAM_MIN_RUN_DURATION\t(" << (int)(durationSeconds*1000.0f) << ")\n";
        headerStream << "#define ECUM_RESET_LOOP_DETECTION\tSTD_OFF\n";
        QDomElement EcuMCommonConfiguration = AutosarToolFactory::FindFirstReferenceToElement(&EcuMConfiguration, "EcuMCommonConfiguration");
        QList<QDomElement> EcuMWakeupSources;
        AutosarToolFactory::FindReferencesToElement(&EcuMWakeupSources, &EcuMCommonConfiguration, "EcuMWakeupSource");
        QString firstEcuMValidationTimeout = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMWakeupSources[0], "EcuMValidationTimeout");
        if (firstEcuMValidationTimeout == "")
            firstEcuMValidationTimeout = "0.0";
        float validationTimeout = firstEcuMValidationTimeout.toFloat(&ok);
        Q_ASSERT(ok);
        headerStream << "#define ECUM_VALIDATION_TIMEOUT\t" << (int)(validationTimeout*1000.0f) << "\n\n";
        QList<QDomElement> EcuMFixedUserConfigs;
        AutosarToolFactory::FindReferencesToElement(&EcuMFixedUserConfigs, &EcuMFixedConfiguration, "EcuMFixedUserConfig");
        for (int configIter = 0; configIter < EcuMFixedUserConfigs.size(); configIter++) {
            QString configName = AutosarToolFactory::GetTextOfFirstChild(&EcuMFixedUserConfigs[configIter], "SHORT-NAME");
            headerStream << "#define EcuMConf_EcuMFixedUserConfig_" << configName << "\t" << configIter << "u\n";
            headerStream << "#define ECUM_USER_" << configName << "\t" << "EcuMConf_EcuMFixedUserConfig_" << configName << "\n";
        }
        headerStream << "#define ECUM_USER_ENDMARK\t" << EcuMFixedUserConfigs.size() << "u\n\n";
        headerStream << "// EcuM Sleep Mode IDs\n// @req EcuM2957, @req EcuM2959\n";
        QList<QDomElement> EcuMSleepModes;
        AutosarToolFactory::FindReferencesToElement(&EcuMSleepModes, &EcuMCommonConfiguration, "EcuMSleepMode");
        for (int modeIter = 0; modeIter < EcuMSleepModes.size(); modeIter++) {
            QString modeName = AutosarToolFactory::GetTextOfFirstChild(&EcuMSleepModes[modeIter], "SHORT-NAME");
            headerStream << "#define ECUM_SLEEP_MODE_" << modeName.toUpper() << "\t" << modeIter << "\n";
            headerStream << "#define EcuMConf_EcuMSleepMode_" << modeName << "\tECUM_SLEEP_MODE_" << modeName.toUpper() << "\n";
        }
        headerStream << "#define ECUM_SLEEP_MODE_CNT\t" << EcuMSleepModes.size() << "\n";
        headerStream << "#define ECUM_SLEEP_MODE_FIRST\t0\n";
        QList<QDomElement> EcuMComMCommunicationAllowedLists;
        AutosarToolFactory::FindReferencesToElement(&EcuMComMCommunicationAllowedLists, &EcuMFixedConfiguration, "EcuMComMCommunicationAllowedList");
        if (EcuMComMCommunicationAllowedLists.size() > 0) {
            QString listRef = AutosarToolFactory::GetTextOfFirstChild(&EcuMComMCommunicationAllowedLists[0], "VALUE-REF");
            if (listRef == "") {
                Q_ASSERT(EcuMComMCommunicationAllowedLists.size() == 1);
                EcuMComMCommunicationAllowedLists.clear();
            }
        }
        headerStream << "#define ECUM_COMM_NETWORKS_CNT\t" << EcuMComMCommunicationAllowedLists.size() << "\n\n";
        headerStream << "#define ECUM_WKSOURCE_POWER\t\t(1uL)\n#define ECUM_WKSOURCE_RESET\t\t(1uL<<1u)\n";
        headerStream << "#define ECUM_WKSOURCE_INTERNAL_RESET\t\t(1uL<<2u)\n#define ECUM_WKSOURCE_INTERNAL_WDG\t\t(1uL<<3u)\n";
        headerStream << "#define ECUM_WKSOURCE_EXTERNAL_WDG\t\t(1uL<<4u)\n\n// @req EcuM2165\n";
        for (int wusIter = 0; wusIter < EcuMWakeupSources.size(); wusIter++) {
            QString wusName = AutosarToolFactory::GetTextOfFirstChild(&EcuMWakeupSources[wusIter], "SHORT-NAME");
            headerStream << "#define EcuMConf_EcuMWakeupSource_" << wusName << "\t(1uL << " << wusIter + 5 << "u)\n";
        }
        headerStream << "\n#define ECUM_WKSOURCE_SYSTEM_CNT\t5\n#define ECUM_WKSOURCE_USER_CNT\t" << EcuMWakeupSources.size() << "\n\n";
        headerStream << "#define ECUM_BACKWARD_COMPATIBLE\n\n";
        headerStream << "#endif // ECUM_CFG_H_\n";
		headerFile.close();

		// ----------------------------------------- Create EcuM_PBcfg.c -----------------------------------------
		QString configFileName = location + "/EcuM_PBcfg.c";
		QFile configFile(configFileName);
        if (!configFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create EcuM_PBcfg.c file.\n";
		    return false;
		}
		QTextStream configStream( &configFile );
        configStream << "\n#include \"EcuM.h\"\n#include \"EcuM_Generated_Types.h\"\n\n";
        configStream << "#if defined(USE_PDUR) || defined(USE_COM) || defined(USE_CANIF) || defined(USE_CANTP) || defined (USE_CANNM)\n";
        configStream << "extern const PostbuildConfigType Postbuild_Config;\n#endif\n\n#if defined(USE_UDPNM)\n";
        configStream << "extern const UdpNm_ConfigType UdpNm_Config;\n#endif\n\nconst EcuM_SleepModeType EcuM_SleepModeConfig[] = {\n";
        for (int modeIter = 0; modeIter < EcuMSleepModes.size(); modeIter++) {
            QString modeName = AutosarToolFactory::GetTextOfFirstChild(&EcuMSleepModes[modeIter], "SHORT-NAME");
            configStream << "\t{ // " << modeName << "\n\t\t.EcuMSleepModeId = ECUM_SLEEP_MODE_" << modeName.toUpper() << ",\n";
            QList <QDomElement> EcuMWakeupSourceMasks;
            AutosarToolFactory::FindReferencesToElement(&EcuMWakeupSourceMasks, &EcuMSleepModes[modeIter], "EcuMWakeupSourceMask");
            QString masks;
            for (int maskIter = 0; maskIter < EcuMWakeupSourceMasks.size(); maskIter++) {
                QString maskRef = AutosarToolFactory::GetTextOfFirstChild(&EcuMWakeupSourceMasks[maskIter], "VALUE-REF");
                if (maskRef == "") {
                    std::cout << "Error: EcuM::EcuMConfiguration::EcuMCommonConfiguration::EcuMSleepMode::EcuMWakeupSourceMask not defined.\n";
                    configFile.close();
                    return false;
                }
                QDomElement source = AutosarToolFactory::GetDomElementFromReference(&maskRef, projectModuleRoot->ownerDocument().documentElement());
                QString sourceName = AutosarToolFactory::GetTextOfFirstChild(&source, "SHORT-NAME");
                masks += "EcuMConf_EcuMWakeupSource_" + sourceName + " ";
                if (maskIter != EcuMWakeupSourceMasks.size()-1)
                    masks += "| ";
            }
            configStream << "\t\t.EcuMWakeupSourceMask = " << masks << ",\n";
            QString EcuMSleepModeMcuModeRef = GET_VALUE_REF_OF_REF(EcuMSleepModes[modeIter], "EcuMSleepModeMcuModeRef");
            QDomElement referedMcuMode = AutosarToolFactory::GetDomElementFromReference(&EcuMSleepModeMcuModeRef, projectModuleRoot->ownerDocument().documentElement());
            QString mcuModeName = AutosarToolFactory::GetTextOfFirstChild(&referedMcuMode, "SHORT-NAME");
            configStream << "\t\t.EcuMSleepModeMcuMode = McuConf_McuModeSettingConf_" << mcuModeName << ",\t\t// @req EcuM2958\n\t}\n";
            if (modeIter != EcuMSleepModes.size() - 1)
                configStream << ",\n";
        }
        configStream << "};\n\nconst EcuM_WakeupSourceConfigType EcuM_WakeupSourceConfig[] = {\n";
        for(int wusIter = 0; wusIter < EcuMWakeupSources.size(); wusIter++) {
            QString wusName = AutosarToolFactory::GetTextOfFirstChild(&EcuMWakeupSources[wusIter], "SHORT-NAME");
            configStream << "\t{ // " << wusName << "\n\t\t.EcuMWakeupSourceId = EcuMConf_EcuMWakeupSource_" << wusName << ",\n";
            QString EcuMValidationTimeout = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMWakeupSources[wusIter], "EcuMValidationTimeout");
            if (EcuMValidationTimeout == "")
                configStream << "\t\t.EcuMValidationTimeout = ECUM_VALIDATION_TIMEOUT_ILL,\n";
            else {
                float validationTimeout = EcuMValidationTimeout.toFloat(&ok);
                Q_ASSERT(ok);
                configStream << "\t\t.EcuMValidationTimeout = " << ceil(validationTimeout*1000.0f) << ",\n";
            }
            configStream << "#if defined(USE_COMM)\n";
            QString EcuMComMChannelRef = GET_VALUE_OF_PARAM_WITH_RETURN(EcuMWakeupSources[wusIter], "EcuMComMChannelRef");
            if (EcuMComMChannelRef == "")
                configStream << "\t\t.EcuMComMChannel = ECUM_COMM_CHANNEL_ILL\n";
            else {
                QDomElement referedChannel = AutosarToolFactory::GetDomElementFromReference(&EcuMComMChannelRef, projectModuleRoot->ownerDocument().documentElement());
                QString channelName = AutosarToolFactory::GetTextOfFirstChild(&referedChannel, "SHORT-NAME");
                configStream << "\t\t.EcuMComMChannel = COMM_NETWORK_HANDLE_" << channelName << "\n\n";
            }
            configStream << "#endif\n";
            if (wusIter != EcuMWakeupSources.size() - 1)
                configStream << "\t},\n";
            else
                configStream << "\t}\n";
        }
        configStream << "};\n\n";
        if (EcuMComMCommunicationAllowedLists.size() > 0) {
            configStream << "#if defined(USE_COMM)\nconst EcuM_ComMConfigType EcuM_ComMConfig[] = {\n";
            for (int listIter = 0; listIter < EcuMComMCommunicationAllowedLists.size(); listIter++) {
                QString listRef = AutosarToolFactory::GetTextOfFirstChild(&EcuMComMCommunicationAllowedLists[listIter], "VALUE-REF");
                if (listRef != "") {
                    QDomElement referedChannel = AutosarToolFactory::GetDomElementFromReference(&listRef, projectModuleRoot->ownerDocument().documentElement());
                    QString channelName = AutosarToolFactory::GetTextOfFirstChild(&referedChannel, "SHORT-NAME");
                    QString busType = GET_VALUE_OF_PARAM(EcuMComMCommunicationAllowedLists[listIter], "ComMBusType");
                    configStream << "\t{\n\t\t.EcuMComMNetworkHandle =  COMM_NETWORK_HANDLE_" << channelName << ",\n";
                    configStream << "\t\t.EcuMComBusType = " << busType << "\n\t}\n";
                }
            }
            configStream << "};\n#endif\n\n";
        }
        configStream << "const EcuM_ConfigType EcuMConfig = {\n\t.EcuMPostBuildVariant = 1,\n\t.EcuMConfigConsistencyHashLow = PRE_COMPILED_DATA_HASH_LOW, \n";
        configStream << "\t.EcuMConfigConsistencyHashHigh = PRE_COMPILED_DATA_HASH_HIGH,\t// @req EcuM2795\n\t.EcuMDefaultShutdownTarget = ";
        QDomElement EcuMDefaultShutdownTarget = AutosarToolFactory::FindFirstReferenceToElement(&EcuMCommonConfiguration, "EcuMDefaultShutdownTarget");
        QString EcuMDefaultState = GET_VALUE_OF_PARAM(EcuMDefaultShutdownTarget, "EcuMDefaultState");
        if (EcuMDefaultState == "EcuMStateOff")
            configStream << "ECUM_STATE_OFF";
        else if (EcuMDefaultState == "EcuMStateSleep")
            configStream << "ECUM_STATE_SLEEP";
        else if (EcuMDefaultState == "EcuMStateReset")
            configStream << "ECUM_STATE_RESET";
        configStream << ",\n\t.EcuMDefaultSleepMode = ECUM_SLEEP_MODE_";
        QString sleepModeRef = GET_VALUE_REF_OF_REF_WITH_RETURN(EcuMDefaultShutdownTarget, "EcuMDefaultSleepModeRef");
        if (sleepModeRef != "") {
            QDomElement referedSleepMode = AutosarToolFactory::GetDomElementFromReference(&sleepModeRef, projectModuleRoot->ownerDocument().documentElement());
            QString modeName = AutosarToolFactory::GetTextOfFirstChild(&referedSleepMode, "SHORT-NAME");
            configStream << modeName.toUpper();
        } else
            configStream << "FIRST";
        configStream << ",\n\t.EcuMDefaultAppMode = OSDEFAULTAPPMODE,\n\t.EcuMNvramReadAllTimeout = ECUM_NVRAM_READALL_TIMEOUT,\n";
        configStream << "\t.EcuMNvramWriteAllTimeout = ECUM_NVRAM_WRITEALL_TIMEOUT,\n\t.EcuMRunMinimumDuration = ECUM_NVRAM_MIN_RUN_DURATION,\n";
        QString mcuModeRef = GET_VALUE_REF_OF_REF(EcuMFixedConfiguration, "EcuMNormalMcuModeRef");
        QDomElement referedMcuMode = AutosarToolFactory::GetDomElementFromReference(&mcuModeRef, projectModuleRoot->ownerDocument().documentElement());
        QString mcuModeName = AutosarToolFactory::GetTextOfFirstChild(&referedMcuMode, "SHORT-NAME");
        configStream << "\t.EcuMNormalMcuMode = McuConf_McuModeSettingConf_" << mcuModeName << ",\n";
        configStream << "\t.EcuMSleepModeConfig = EcuM_SleepModeConfig,\n\t.EcuMWakeupSourceConfig = EcuM_WakeupSourceConfig,\n";
        configStream << "#if defined(USE_DEM)\n\t.EcuMDemInconsistencyEventId = DEM_EVENT_ID_NULL,\n";
        configStream << "\t.EcuMDemRamCheckFailedEventId = DEM_EVENT_ID_NULL,\n\t.EcuMDemAllRunRequestsKilledEventId = DEM_EVENT_ID_NULL,\n";
        configStream << "#endif\n#if defined(USE_COMM)\n";
        if (EcuMComMCommunicationAllowedLists.size() > 0)
            configStream << "\t.EcuMComMConfig = EcuM_ComMConfig,\n";
        else
            configStream << "\t.EcuMComMConfig = NULL,\n";
        configStream << "#endif\n#if defined(USE_MCU)\n\t.McuConfigPtr = McuConfigData,\n#endif\n";
        configStream << "#if defined(USE_PORT)\n\t.PortConfigPtr = &PortConfigData,\n#endif\n";
        configStream << "#if defined(USE_CAN)\n\t.CanConfigPtr = &CanConfigData,\n#endif\n";
        configStream << "#if defined(USE_DIO)\n\t.DioConfigPtr = &DioConfigData,\n#endif\n";
        configStream << "#if defined(USE_CANSM)\n\t.CanSMConfigPtr = &CanSM_Config,\n#endif\n";
        configStream << "#if defined(USE_LIN)\n\t.LinConfigPtr = &Lin_Config,\n#endif\n";
        configStream << "#if defined(USE_FR)\n\t.FrConfigPtr = &FrConfigData,\n#endif\n";
        configStream << "#if defined(USE_LINIF)\n\t.LinIfConfigPtr = &LinIf_Config,\n#endif\n";
        configStream << "#if defined(USE_LINSM)\n\t.LinSMConfigPtr = &LinSM_Config,\n#endif\n";
        configStream << "#if defined(USE_UDPNM)\n\t.UdpNmConfigPtr = &UdpNm_Config,\n#endif\n";
        configStream << "#if defined(USE_COMM)\n\t.ComMConfigPtr = &ComM_Config,\n#endif\n";
        configStream << "#if defined(USE_BSWM)\n\t.BswMConfigPtr = &BswM_Config,\n#endif\n";
        configStream << "#if defined(USE_NM)\n\t.NmConfigPtr = &Nm_Config,\n#endif\n";
        configStream << "#if defined(USE_TCPIP)\n\t.TcpIpConfigPtr = &TcpIp_Config,\n#endif\n";
        configStream << "#if defined(USE_J1939TP)\n\t.J1939TpConfigPtr = &J1939Tp_Config,\n#endif\n";
        configStream << "#if defined(USE_SD)\n\t.SdConfigPtr = &SdConfig,\n#endif\n";
        configStream << "#if defined(USE_SOAD)\n\t.SoAdConfigPtr = &SoAd_Config,\n#endif\n";
        configStream << "#if defined(USE_LDCOM)\n\t.LdComConfigPtr = &LdCom_Config,\n#endif\n";
        configStream << "#if defined(USE_DMA)\n\t.DmaConfigPtr = DmaConfig,\n#endif\n";
        configStream << "#if defined(USE_ADC)\n#if defined(CFG_ZYNQ)\n\t.AdcConfigPtr = NULL,\n#else\n\t.AdcConfigPtr = AdcConfig,\n#endif\n#endif\n";
        configStream << "#if defined(USE_PWM)\n\t.PwmConfigPtr = &PwmConfig,\n#endif\n";
        configStream << "#if defined(USE_OCU)\n\t.OcuConfigPtr = &OcuConfig,\n#endif\n";
        configStream << "#if defined(USE_ICU)\n\t.IcuConfigPtr = &IcuConfig,\n#endif\n";
        configStream << "#if defined(USE_WDG)\n\t.WdgConfigPtr = &WdgConfig,\n#endif\n";
        configStream << "#if defined(USE_WDGM)\n\t.WdgMConfigPtr = &WdgMConfig,\n#endif\n";
        configStream << "#if defined(USE_WDGIF)\n\t.WdgIfConfigPtr = &WdgIfConfig,\n#endif\n";
        configStream << "#if defined(USE_GPT)\n\t.GptConfigPtr = GptConfigData,\n#endif\n";
        configStream << "#if defined(USE_FLS)\n\t.FlsConfigPtr = FlsConfigSet,\n#endif\n";
        configStream << "#if defined(USE_EEP)\n\t.EepConfigPtr = EepConfigData,\n#endif\n";
        configStream << "#if defined(USE_SPI)\n\t.SpiConfigPtr = &SpiConfigData,\n#endif\n";
        configStream << "#if defined(USE_DCM)\n\t.DcmConfigPtr = &DCM_Config,\n#endif\n";
        configStream << "#if defined(USE_DEM)\n\t.DemConfigPtr = &DEM_Config,\n#endif\n";
        configStream << "#if defined(USE_XCP)\n\t.XcpConfigPtr = &XcpConfig,\n#endif\n";
        configStream << "#if defined(USE_STBM)\n\t.StbMConfigPtr = &StbMConfigData,\n#endif\n";
        configStream << "#if defined(USE_ETHTSYN)\n\t.EthTSynConfigPtr = &EthTSynConfigData,\n#endif\n";
        configStream << "#if defined(USE_ETHSM)\n\t.EthSMConfigPtr = &EthSMConfig,\n#endif\n";
        configStream << "#if defined(USE_ETHIF)\n\t.EthIfConfigPtr = &EthIfConfigData,\n#endif\n";
        configStream << "#if defined(USE_ETH)\n\t.EthConfigPtr = &EthConfig,\n#endif\n";
        configStream << "#if defined(USE_IPDUM)\n\t.IpduMConfigPtr = &IpduMConfigData,\n#endif\n";
        configStream << "#if defined(USE_DLT)\n\t.DltConfigPtr = &Dlt_Config,\n#endif\n";
        configStream << "#if defined(USE_PDUR) || defined(USE_COM) || defined(USE_CANIF) || defined(USE_CANTP) || defined(USE_CANNM)\n\t.PostBuildConfig = &Postbuild_Config,\n#endif\n";
        configStream << "#if defined(USE_FR)\n\t.FrConfigPtr = &FrConfigData,\n#endif\n";
        configStream << "#if defined (USE_FRIF)\n\t.FrIfConfigPtr = &FrIf_Config,\n#endif\n";
        configStream << "#if defined (USE_FRNM)\n\t.FrNmConfigPtr = &FrNmConfigData,\n#endif\n";
        configStream << "#if defined (USE_FRSM)\n\t.FrSMConfigPtr = &FrSMConfigData,\n#endif\n";
        configStream << "#if defined (USE_FRTP)\n\t.FrTpConfigPtr = &FrTpConfig,\n#endif\n};\n";
        configStream << endl;
		configFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/EcuM.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create EcuM.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += ECUM_FIXED\n";
        makeFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool EcuMGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot) {
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
		    }
		}
    }
    if (returnValue == false) {
        std::cout << "Suggestion: Please make sure that the ECUM Module is self-contained.\n";
    }
    // Make sure that EcuM is FIXED
    QDomElement EcuMConfiguration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "EcuMConfiguration");
    QDomElement EcuMFixedConfiguration = AutosarToolFactory::FindFirstReferenceToElement(&EcuMConfiguration, "EcuMFixedConfiguration");
    if (EcuMFixedConfiguration.isNull()) {
        std::cout << "Error: This version of EcuM generator only supports fixed EcuM architecture.\n";
        returnValue = false;
    }
    return returnValue;
}

