/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "rte_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

#define CREATE_SECTION(fileStream, module, section, init, align, code) \
    fileStream << "#define " << module << "_START_SEC" << section << init << align << "\n#include <" << module << "_MemMap.h>\n" << code << "\n"; \
    fileStream << "#define " << module << "_STOP_SEC" << section << init << align << "\n#include <" << module << "_MemMap.h>\n";

bool RteGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);
    Q_UNUSED(location);

    return true;
}

bool RteGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    return true;
}

bool RteGenerator::RunRteGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QDomElement* comModuleRoot, QDomElement* osModuleRoot, QDomElement* ecuExtract, QString location)
{
    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " << location.toStdString() << " for generation of Rte Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
        // ----------------------------------------- Create Config -----------------------------------------
        QString configFolder = location + "/Rte/Config";
        bool success;
        QDir configDir(configFolder);
        if (!configDir.exists())
            success = configDir.mkpath(".");
        else
            success = true;
        if (!success) {
            std::cout << "Error: Unable to create \'Config\' folder.\n";
            return false;
        }

        // Get all software component types and respective prototypes
        QList <QDomElement> softwareComponentTypes;
        QDomNodeList tempDomNodeList = ecuExtract->elementsByTagName("ECU-ABSTRACTION-SW-COMPONENT-TYPE");
        for (int listIter = 0; listIter < tempDomNodeList.size(); listIter++)
            softwareComponentTypes.push_back(tempDomNodeList.at(listIter).toElement());
        tempDomNodeList = ecuExtract->elementsByTagName("SERVICE-SW-COMPONENT-TYPE");
        for (int listIter = 0; listIter < tempDomNodeList.size(); listIter++)
            softwareComponentTypes.push_back(tempDomNodeList.at(listIter).toElement());
        tempDomNodeList = ecuExtract->elementsByTagName("APPLICATION-SW-COMPONENT-TYPE");
        for (int listIter = 0; listIter < tempDomNodeList.size(); listIter++)
            softwareComponentTypes.push_back(tempDomNodeList.at(listIter).toElement());
        QList < QList <QDomElement> > componentPrototypes;
        QDomNodeList tempPrototypes = ecuExtract->elementsByTagName("SW-COMPONENT-PROTOTYPE");
        for (int compIter = 0; compIter < softwareComponentTypes.size(); compIter++) {
            QList <QDomElement> tempPrototypesForComp;
            for (int protoIter = 0; protoIter < tempPrototypes.size(); protoIter++) {
                QDomElement typeRef = tempPrototypes.at(protoIter).firstChildElement("TYPE-TREF");
                QString ref = typeRef.text();
                QDomElement type = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
                if (type == softwareComponentTypes.at(compIter)) {
                    tempPrototypesForComp.push_back(tempPrototypes.at(protoIter).toElement());
                }
            }
            componentPrototypes.push_back(tempPrototypesForComp);
        }
        //qDebug() << "Software component types = " << softwareComponentTypes.size() << ", component prototypes = " << componentPrototypes.size();
        Q_ASSERT(softwareComponentTypes.size() == componentPrototypes.size());

        // Get all system signals referenced by COM
        QList<QDomElement> SystemSignals;
        QList<QDomElement> ISignals;
        QList<QDomElement> ComSignals;
        //QList<QDomElement> DataPrototypes;
        QDomElement tempElem;
        //qDebug() << "Com: " << comModuleRoot->tagName() << ", name: " << comModuleRoot->firstChildElement("SHORT-NAME").text();
        AutosarToolFactory::FindReferencesToElement(&ComSignals, comModuleRoot, "ComSignal");
        //qDebug() << "\nComSignals = " << ComSignals.size() << ", rootDoc: " << comModuleRoot->parentNode().parentNode().firstChildElement("SHORT-NAME").text();
        for (int sigIter = 0; sigIter < ComSignals.size(); sigIter++) {
            //qDebug() << "IT: " << ComSignals[sigIter].firstChildElement("SHORT-NAME").text();
            QString ref = GET_VALUE_REF_OF_REF_WITH_RETURN(ComSignals[sigIter], "ComSystemTemplateSystemSignalRef");
            if (ref == "") {
                std::cout << "Error: System signal not referenced for a ComSignal (Error Code: 0001, " << ComSignals[sigIter].firstChildElement("SHORT-NAME").text().toStdString() << ").\n";
                return false;
            }
            tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference to a ComSignal in ECU Extract.\n";
                return false;
            }
            tempElem = tempElem.firstChildElement("I-SIGNAL-REF");
            if (tempElem.isNull()) {
                std::cout << "Error: No signal reference found for a ComSignal.\n";
                return false;
            }
            ref = tempElem.text();
            if (ref == "") {
                std::cout << "Error: ISignal not referenced for a ComSignal.\n";
                return false;
            }
            tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference to an ISignal in ECU Extract.\n";
                return false;
            }
            ISignals.push_back(tempElem);
            tempElem = tempElem.firstChildElement("SYSTEM-SIGNAL-REF");
            if (tempElem.isNull()) {
                std::cout << "Error: No system signal reference found for a ComSignal.\n";
                return false;
            }
            ref = tempElem.text();
            if (ref == "") {
                std::cout << "Error: System Signal not referenced for a ComSignal (Error Code: 0002).\n";
                return false;
            }
            tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
            if (tempElem.isNull()) {
                std::cout << "Error: System signal not found for a ComSignal.\n";
                return false;
            }
            SystemSignals.push_back(tempElem);
        }
        //qDebug() << "ComSignals = " << ComSignals.size() << ", SystemSignals = " << SystemSignals.size() << ", ISignals = " << ISignals.size() << ", DataPrototypes = " << DataPrototypes.size();

        // Get all data types from RTE module and ECU extract
        QList <QDomElement> structureDataTypes;
        QList <QDomElement> otherDataTypes;
        QDomNodeList ImplementationDataTypes = autosarModelModuleRoot->ownerDocument().documentElement().elementsByTagName("IMPLEMENTATION-DATA-TYPE");
        bool found = false;
        for (int dtIter = 0; dtIter < ImplementationDataTypes.size(); dtIter++) {
            QDomElement category = ImplementationDataTypes.at(dtIter).firstChildElement("CATEGORY");
            if (category.isNull()) {
                std::cout << "Category not found in " << ImplementationDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text().toStdString() << "\n";
            } else {
                if (category.text() == "STRUCTURE") {
                    found = false;
                    for (int strIter = 0; strIter < structureDataTypes.size(); strIter++) {
                        if (structureDataTypes.at(strIter).firstChildElement("SHORT-NAME").text() == ImplementationDataTypes.at(dtIter).toElement().firstChildElement("SHORT-NAME").text()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        structureDataTypes.push_back(ImplementationDataTypes.at(dtIter).toElement());
                } else if (category.text() == "TYPE_REFERENCE" || category.text() == "DATA_REFERENCE" || category.text() == "ARRAY") {
                    found = false;
                    for (int strIter = 0; strIter < otherDataTypes.size(); strIter++) {
                        if (otherDataTypes.at(strIter).firstChildElement("SHORT-NAME").text() == ImplementationDataTypes.at(dtIter).toElement().firstChildElement("SHORT-NAME").text()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        otherDataTypes.push_back(ImplementationDataTypes.at(dtIter).toElement());
                } else if (category.text() == "VALUE") {
                    // Do nothing
                } else {
                    std::cout << "Error: Unknown category for IMPLEMENTATION-DATA-TYPE " << ImplementationDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text().toStdString() << " = " << category.text().toStdString() << "\n";
                }
            }
        }
        ImplementationDataTypes = ecuExtract->elementsByTagName("IMPLEMENTATION-DATA-TYPE");
        for (int dtIter = 0; dtIter < ImplementationDataTypes.size(); dtIter++) {
            QDomElement category = ImplementationDataTypes.at(dtIter).firstChildElement("CATEGORY");
            if (category.isNull()) {
                std::cout << "Category not found in " << ImplementationDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text().toStdString() << "\n";
            } else {
                if (category.text() == "STRUCTURE") {
                    found = false;
                    for (int strIter = 0; strIter < structureDataTypes.size(); strIter++) {
                        if (structureDataTypes.at(strIter).firstChildElement("SHORT-NAME").text() == ImplementationDataTypes.at(dtIter).toElement().firstChildElement("SHORT-NAME").text()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        structureDataTypes.push_back(ImplementationDataTypes.at(dtIter).toElement());
                } else if (category.text() == "TYPE_REFERENCE" || category.text() == "DATA_REFERENCE" || category.text() == "ARRAY") {
                    found = false;
                    for (int strIter = 0; strIter < otherDataTypes.size(); strIter++) {
                        if (otherDataTypes.at(strIter).firstChildElement("SHORT-NAME").text() == ImplementationDataTypes.at(dtIter).toElement().firstChildElement("SHORT-NAME").text()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        otherDataTypes.push_back(ImplementationDataTypes.at(dtIter).toElement());
                } else if (category.text() == "VALUE") {
                    // Do nothing
                } else {
                    std::cout << "Error: Unknown category for IMPLEMENTATION-DATA-TYPE " << ImplementationDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text().toStdString() << " = " << category.text().toStdString() << "\n";
                }
            }
        }

        // Get all mode machine components
        QList <QDomElement> ModeMachineSwcs;
        QList< QList <QDomElement> > ModeMachineComps;
        for (int swcIter = 0; swcIter < softwareComponentTypes.size(); swcIter++) {
            QDomElement port = softwareComponentTypes.at(swcIter).firstChildElement("PORTS").firstChildElement();
            while (!port.isNull()) {
                if (port.tagName() == "P-PORT-PROTOTYPE") {
                    QDomElement interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF");
                    if (interfaceRef.attribute("DEST") == "MODE-SWITCH-INTERFACE") {
                        ModeMachineSwcs.push_back(softwareComponentTypes.at(swcIter));
                        ModeMachineComps.push_back(componentPrototypes.at(swcIter));
                        break;
                    }
                }
                port = port.nextSiblingElement();
            }
        }
        //qDebug() << "ModeMachineSwcs = " << ModeMachineSwcs.size();

        // Get all tasks from OS
        QList <QDomElement> osTasks;
        AutosarToolFactory::FindReferencesToElement(&osTasks, osModuleRoot, "OsTask");
        //qDebug() << "Tasks = " << osTasks.size();
        // Check which ones are extended
        QList <bool> isOsTaskExtended;
        for (int tIter = 0; tIter < osTasks.size(); tIter++) {
            QDomElement task = osTasks.at(tIter);
            QDomElement tempEventRef = AutosarToolFactory::FindFirstReferenceToElement(&task, "OsTaskEventRef");
            if (tempEventRef.isNull()) {
                isOsTaskExtended.push_back(false);
            } else {
                isOsTaskExtended.push_back(true);
            }
        }
        //qDebug() << "isOsTaskExtended = " << isOsTaskExtended;

        // Get data mappings
        /*QDomNodeList AllDataMappings = ecuExtract->elementsByTagName("DATA-MAPPINGS");
        for (int aMapIter = 0; aMapIter < AllDataMappings.size(); aMapIter++) {
            QDomElement MapElement = AllDataMappings.at(aMapIter).firstChildElement();
            while (!MapElement.isNull()) {
                if (MapElement.tagName() == "SENDER-RECEIVER-TO-SIGNAL-MAPPING") {
                    QDomElement refElem = MapElement.firstChildElement("SYSTEM-SIGNAL-REF");
                    if (refElem.text() == ref) {
                        QDomElement dataElement = MapElement.firstChildElement("DATA-ELEMENT-IREF");
                        //QDomElement contextPortRef = dataElement.firstChildElement("CONTEXT-PORT-REF");
                        QDomElement targetDataPrototypeRef = dataElement.firstChildElement("TARGET-DATA-PROTOTYPE-REF");
                        ref = targetDataPrototypeRef.text();
                        QDomElement targetDataPrototype = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
                        if (targetDataPrototype.isNull()) {
                            std::cout << "Error: Target Data Protoype " << ref.toStdString() << " not found.\n";
                            return false;
                        }
                        DataPrototypes.push_back(targetDataPrototype);
                        QDomNodeList DataReceivedEvents = ecuExtract->elementsByTagName("DATA-RECEIVED-EVENT");
                        for (int evIter = 0; evIter < DataReceivedEvents.size(); evIter++) {
                            QDomElement DataRef = DataReceivedEvents.at(evIter).firstChildElement("DATA-IREF");
                            QDomElement targetDataElementRef = DataRef.firstChildElement("TARGET-DATA-ELEMENT-REF");
                            if (targetDataElementRef.text() == ref) {
                                QDomElement portRef = DataRef.firstChildElement("CONTEXT-R-PORT-REF");
                                QDomElement componentType = DataReceivedEvents.at(evIter).parentNode().parentNode().parentNode().parentNode().toElement();
                                QDomElement component;
                                for (int typeIter = 0; typeIter < softwareComponentTypes.size(); typeIter++) {
                                    if (componentType == softwareComponentTypes.at(typeIter)) {
                                        component = componentPrototypes.at(typeIter);
                                        break;
                                    }
                                }
                                ref = portRef.text();
                                QDomElement port = AutosarToolFactory::GetDomElementFromReference(&ref, *ecuExtract);
                                std::cout << "Rte_DataReceived_" << componentType.firstChildElement("SHORT-NAME").text().toStdString() << "_" <<
                                             component.firstChildElement("SHORT-NAME").text().toStdString() << "_" <<
                                             port.firstChildElement("SHORT-NAME").text().toStdString() << "_" <<
                                             targetDataPrototype.firstChildElement("SHORT-NAME").text().toStdString() << "\n";

                                break;
                            }
                        }
                        break;
                    }
                } else {
                    std::cout << "Don\'t know what to do.\n";
                    return false;
                }
                MapElement = MapElement.nextSiblingElement();
            }
        }*/
        success = CreateIocC(configFolder);
        if (!success)
            return false;
        success = CreateIocH(configFolder);
        if (!success)
            return false;
        success = CreateRteAssertH(configFolder);
        if (!success)
            return false;
        success = CreateRteC(configFolder, *ecuExtract, softwareComponentTypes, componentPrototypes, *projectModuleRoot, osTasks, isOsTaskExtended);
        if (!success)
            return false;
        success = CreateRteCallbacks(configFolder, projectModuleRoot, ecuExtract, osModuleRoot);
        if (!success)
            return false;
        success = CreateRteCalprmsC(configFolder);
        if (!success)
            return false;
        success = CreateRteCalprmsH(configFolder);
        if (!success)
            return false;
        success = CreateRteCfgH(configFolder, projectModuleRoot);
        if (!success)
            return false;
        success = CreateRteDataHandleTypeH(configFolder, ecuExtract);
        if (!success)
            return false;
        success = CreateRteFifoC(configFolder);
        if (!success)
            return false;
        success = CreateRteFifoH(configFolder);
        if (!success)
            return false;
        success = CreateRteH(configFolder, projectModuleRoot);
        if (!success)
            return false;
        success = CreateRteHookH(configFolder);
        if (!success)
            return false;
        success = CreateRteInternalC(configFolder, ModeMachineSwcs, *autosarModelModuleRoot, *ecuExtract, softwareComponentTypes, componentPrototypes, *projectModuleRoot, *osModuleRoot, osTasks, isOsTaskExtended, ComSignals);
        if (!success)
            return false;
        success = CreateRteInternalH(configFolder, ModeMachineSwcs, ModeMachineComps, *autosarModelModuleRoot, *ecuExtract, softwareComponentTypes, componentPrototypes);
        if (!success)
            return false;
        success = CreateRteMainC(configFolder, ModeMachineSwcs, ModeMachineComps, *autosarModelModuleRoot, *ecuExtract);
        if (!success)
            return false;
        success = CreateRteMainH(configFolder);
        if (!success)
            return false;
        success = CreateRteMakeFile(configFolder, softwareComponentTypes);
        if (!success)
            return false;
        success = CreateRteTagsTxt(configFolder);
        if (!success)
            return false;
        success = CreateRteTypeH(configFolder, structureDataTypes, otherDataTypes, *ecuExtract);
        if (!success)
            return false;
        success = CreateRteTypeWorkaroundsH(configFolder, structureDataTypes, otherDataTypes);
        if (!success)
            return false;
        success = CreateRteSwcTypeH(configFolder, *autosarModelModuleRoot, *ecuExtract, softwareComponentTypes);
        if (!success)
            return false;
        success = CreateRteSwcC(configFolder, *autosarModelModuleRoot, *ecuExtract, softwareComponentTypes, componentPrototypes);
        if (!success)
            return false;

        // ----------------------------------------- Create MemMap -----------------------------------------
        QString memMapFolder = location + "/Rte/MemMap";
        QDir memMapDir(memMapFolder);
        if (!memMapDir.exists())
            success = memMapDir.mkpath(".");
        else
            success = true;
        if (!success) {
            std::cout << "Error: Unable to create \'MemMap\' folder.\n";
            return false;
        }
        success = CreateMemMapH("Rte", memMapFolder);
        if (!success)
            return false;
        for (int componentIter = 0; componentIter < softwareComponentTypes.size(); componentIter++) {
            success = CreateMemMapH(softwareComponentTypes.at(componentIter).firstChildElement("SHORT-NAME").text(), memMapFolder);
            if (!success)
                return false;
        }


        // ----------------------------------------- Create Contract -----------------------------------------
        QString contractFolder = location + "/Rte/Contract";
        QDir configDir2(contractFolder);
        success = false;
        if (!configDir2.exists())
            success = configDir2.mkpath(".");
        else
            success = true;
        if (!success)
        {
            std::cout << "Error: Unable to create \'Contract\' folder.\n";
            return false;
        }
        success = CreateRteSwcH(contractFolder, *autosarModelModuleRoot, *ecuExtract, softwareComponentTypes, componentPrototypes);
        if (!success)
            return false;
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool RteGenerator::CheckRteValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot, QDomElement* ecuExtract)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract->ownerDocument().documentElement());
                if (tempElem.isNull())
                {
                    std::cout << "Error: Invalid reference \'" << refs.at(refIter).toElement().text().toStdString() << "\' in " <<
                                 AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                    returnValue = false;
                }
            }
        }
    }

    try
    {
        // AUTOSAR 4.0.3
        // rte_sws_1257: In compatibility mode, the Generated RTE module shall include Os.h.

        // rte_sws_1277: In compatibility mode rte_sws_1257, the RTE module shall define all task bodies created by the RTE generator.

        // rte_sws_1287: A DataSendCompletedEvent that references a RunnableEntity and is referenced by a WaitPoint shall be an invalid configuration which is
        // rejected by the RTE generator.
        // rte_sws_1313: A DataReceivedEvent that references a runnable entity and is referenced by a WaitPoint shall be an invalid configuration.
        // rte_sws_2730: A ModeSwitchedAckEvent that references a RunnableEntity and is referenced by a WaitPoint shall be an invalid configuration which is
        // rejected by the RTE generator.
        QDomNodeList RunnableEntity;
        RunnableEntity = ecuExtract->elementsByTagName("RUNNABLE-ENTITY");
        for (int draIter = 0; draIter < RunnableEntity.size(); draIter++)
        {
            QDomElement waitPoints = RunnableEntity.at(draIter).firstChildElement("WAIT-POINTS");
            while (!waitPoints.isNull())
            {
                QDomElement tempDomElement = waitPoints.firstChildElement("TRIGGER-REF");
                if (!tempDomElement.isNull())
                {
                    QString triggerRef = tempDomElement.text();
                    QDomElement referedTrigger = AutosarToolFactory::GetDomElementFromReference(&triggerRef, ecuExtract->ownerDocument().documentElement());
                    if (referedTrigger.isNull())
                    {
                        std::cout << "Error getting refered trigger " << triggerRef.toStdString() << "\n";
                        returnValue = false;
                        break;
                    }
                    // Don't know how to proceed. Need sample extract.
                }
                waitPoints = waitPoints.nextSiblingElement();
            }
        }

        // rte_sws_1358: The RTE shall raise an error if [constr_1091] is violated, so if RunnableEntity has WaitPoint connected to any of the following RTE-Events:
        // OperationInvokedEvent, SwcModeSwitchEvent, TimingEvent, BackgroundEvent, DataReceiveErrorEvent, ExternalTriggerOccurredEvent, InternalTriggerOccurredEvent,
        // DataWriteCompletedEvent. These events can only start a runnable.
        // Don't know how to proceed. Need sample extract.

        // rte_sws_2500: The RTE generator shall reject configurations with category 2 runnables connected to SwcModeSwitchEvents and RTEEvents / BswEvents with
        // ModeDisablingDependencys if the mode machine instance is synchronous. The rejection may be reduced to a warning when the RTE generator is explicitly set
        // to a non strict mode.

        // rte_sws_2662: The RTE generator shall reject configurations with OnEntry, OnTransition, or OnExit ExecutableEntity’s of the same mode machine instance that
        // are mapped to different tasks in case of synchronous mode switching procedure.

        // rte_sws_2663: The RTE generator shall reject configurations with mode disabling dependent ExecutableEntitys that are mapped to a task with lower priority than
        // the task that contains the OnEntry ExecutableEntitys and OnExit ExecutableEntitys of that mode machine instance supporting a synchronous mode switching procedure.

        // rte_sws_2664: The RTE generator shall reject configurations of a task with
        // - OnExit ExecutableEntitys mapped after OnEntry ExecutableEntitys or
        // - OnTransition ExecutableEntitys mapped after OnEntry ExecutableEntitys or
        // - OnExit ExecutableEntitys mapped after OnTransition ExecutableEntitys
        // of the same mode machine instance supporting a synchronous mode switching procedure.

        // rte_sws_2670: RTE shall not support connections with multiple senders (n:1 communication) of mode switch notifications connected to the same receiver. The RTE
        // generator shall reject configurations with multiple senders of mode switch notifications connected to the same receiver.

        // rte_sws_2706: RTE shall reject configurations that contain OperationInvokedEvents with a ModeDisablingDependency.

        // rte_sws_2724: RTE shall reject configurations where one ModeDeclarationGroupPrototype of a provide port is connected to ModeDeclarationGroupPrototypes of
        // require ports from more than one partition.

        // rte_sws_2733: The RTE Generator shall reject a configuration where a runnable has the attribute canBeInvokedConcurrently set to true and the attribute
        // minimumStartInterval set to greater zero.

        // rte_sws_2738: RTE shall reject a configuration, in which there is not exactly one ModeRequestTypeMap referencing the ModeDeclarationGroup used in a
        // ModeDeclarationGroupPrototype of the SW-C’s ports.

        // rte_sws_2750: The RTE Generator shall reject configurations where an r-port typed with a ParameterInterface is not connected and an initValue of a
        // ParameterRequireComSpec is not provided for each ParameterDataPrototypes of this ParameterInterface.

        // rte_sws_3010: One runnable entity shall only be resumed by one single RTEEvent on its WaitPoint. The RTE doesn’t support the WaitPoint of one runnable
        // entity connected to several RTEEvents.

        // rte_sws_3012: Access with VariableAccesses in the dataReadAccess role is only allowed for VariableDataPrototypes whose swImplPolicy attribute is not set to queued.
        // rte_sws_3018: RTE does not support receiving with WaitPoint for VariableDataPrototypes with their swImplPolicy attribute is not set to queued.
        for (int restrictIter = 0; restrictIter < 2; restrictIter++)
        {
            QDomNodeList DataReadAccess;
            if (restrictIter == 0)
                DataReadAccess = ecuExtract->elementsByTagName("DATA-READ-ACCESSS");
            else
                DataReadAccess = ecuExtract->elementsByTagName("DATA-RECEIVE-POINT-BY-ARGUMENTS");
            for (int draIter = 0; draIter < DataReadAccess.size(); draIter++)
            {
                QDomElement VariableAccess = DataReadAccess.at(draIter).firstChildElement();
                while (!VariableAccess.isNull())
                {
                    QDomElement tempDomElement = VariableAccess.firstChildElement("ACCESSED-VARIABLE");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0001.";
                    tempDomElement = tempDomElement.firstChildElement("AUTOSAR-VARIABLE-IREF");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0002.";
                    tempDomElement = tempDomElement.firstChildElement("TARGET-DATA-PROTOTYPE-REF");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0003.";
                    QString referenceForPrototype = tempDomElement.text();
                    QDomElement referedVariableDataPrototype = AutosarToolFactory::GetDomElementFromReference(&referenceForPrototype, ecuExtract->ownerDocument().documentElement());
                    if (referedVariableDataPrototype.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0004.";
                    tempDomElement = referedVariableDataPrototype.firstChildElement("SW-DATA-DEF-PROPS");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0005.";
                    tempDomElement = tempDomElement.firstChildElement("SW-DATA-DEF-PROPS-VARIANTS");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0006.";
                    tempDomElement = tempDomElement.firstChildElement("SW-DATA-DEF-PROPS-CONDITIONAL");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0007.";
                    tempDomElement = tempDomElement.firstChildElement("SW-IMPL-POLICY");
                    if (tempDomElement.isNull())
                        std::cout << "Error during validation of rte_sws_3012/3018. Code 0007.";
                    QString swImplPolicy = tempDomElement.text();
                    if (swImplPolicy == "QUEUED")
                    {
                        if (restrictIter == 0)
                            std::cout << "Error: Violated rte_sws_3012 (Access with VariableAccesses in the dataReadAccess role is only allowed for VariableDataPrototypes whose swImplPolicy attribute is not set to queued.)\n";
                        else
                            std::cout << "Error: Violated rte_sws_3018 (RTE does not support receiving with WaitPoint for VariableDataPrototypes with their swImplPolicy attribute is not set to queued.)\n";
                        returnValue = false;
                    }
                    VariableAccess = VariableAccess.nextSiblingElement();
                }
            }
        }
        // rte_sws_3014: All the ServerCallPoints referring to one ClientServerOperation through one RPortPrototype are considered to have the same behavior
        // by calling service. The RTE generator shall reject configuration where this is violated.



        // rte_sws_3019: If strict checking has been enabled (see rte_sws_5099) there shall not be unconnected r-port. The RTE generator shall in this case reject the
        // configuration with unconnected r-port.

        // rte_sws_3526: The RTE generator shall reject configurations in which a RunnableEntity is triggered by multiple OperationInvokedEvents but violating the
        // constraint [2000] Compatibility of ClientServerOperations triggering the same RunnableEntity.

        // rte_sws_3527: The RTE does NOT support multiple Runnable Entities sharing the same entry point (symbol attribute of RunnableEntity).

        // rte_sws_3605: If several require ports of a software component are categorized by the same client/server interface, all invocations of the same operation
        // of this client/server interface have to be either synchronous, or all invocations of the same operation have to be asynchronous.  This restriction
        // applies under the following conditions:
        // - the usage of the indirect API is specified for at least one of the respective port prototypes and/or
        // - the software component supports multiple instantiation, and the RTE generation shall be performed in compatibility mode.

        // rte_sws_3790: The <typeDefinition> attribute of a PerInstanceMemory is not allowed to contain a function pointer.

        // rte_sws_ext_3811: If an exclusive area’s configuration value for ExclusiveAreaImplMechanism is InterruptBlocking or OsResource, no runnable entity shall
        // contain any WaitPoint inside this exclusive area.

        // rte_sws_3951: RTE does not support measurement of queued communication.

        // rte_sws_3970: The RTE generator shall reject configurations containing require ports attached to ParameterSwComponentTypes.

        // rte_sws_4525: All VariableDataPrototype that are connected to the same sender, or connected to the same receiver, or mapped to the same SystemSignal, must
        // have identical init values.

        // rte_sws_7005: The RTE generator shall reject a configuration with a WaitPoint applied to a RunnableEntity which is using the ExclusiveArea in the
        // role runsInsideExclusiveArea.



        // rte_sws_7007: The RTE generator shall reject configurations where different execution instances of a runnable entity, which use implicit data access, are
        // mapped to different Preemption Areas.

        // rte_sws_7045: The RTE generator shall reject configurations where the type attribute of a ’C’ typed PerInstanceMemory is equal to the name of a
        // ImplementationDataType contained in the input configuration.

        // rte_sws_7101: The RTE does not support configurations in which a PortAPIOption with enableTakeAddress = TRUE is defined by a software-component supporting
        // multiple instantiation.

        // rte_sws_7157: The RTE generator shall reject configurations with
        // - OnExit ExecutableEntitys mapped after OnEntry ExecutableEntitys or
        // - OnTransition ExecutableEntitys mapped after OnEntry ExecutableEntitys or
        // - OnExit ExecutableEntitys mapped after OnTransition ExecutableEntitys
        // of the same software component or Basic Software Module for a mode machine instance supporting an asynchronous mode switching procedure.

        // rte_sws_7170: An AsynchronousServerCallPoint shall be referenced by one AsynchronousServerCallResultPoints only. The RTE generator shall reject
        // configuration where this is violated.



        // rte_sws_7357: The RTE Generator shall reject configurations where a DataReceivedEvent is referenced by a WaitPoint and references a VariableDataPrototype
        // referenced by a NvDataInterface.



        // rte_sws_7402: The RTE generator shall reject a model where two (or more) different RunnableEntitys in the same internal behavior each have a WaitPoint
        // referencing the same DataReceivedEvent, and the runnables are mapped to different tasks.



        // rte_sws_7403: The RTE generator shall reject a model where in the same SwcInternalBehavior two (or more) different DataReceivedEvents, that reference the same
        // VariableDataPrototype with event semantics, trigger different runnable entities mapped to different tasks.

        // rte_sws_7642: When the external configuration switch strictInitialValuesCheck is enabled, the RTE Generator shall reject configurations where a SwAddrMethod
        // has a sectionInitializationPolicy set to init but no initValues are specified on the sender or receiver side.

        // rte_sws_7681: If strict checking of initial values is enabled (see rte_sws_7680), the RTE Generator shall reject configurations where a ParameterDataPrototype
        // has no initValues.

        // rte_sws_7686: The RTE Generator shall reject configurations where an AtomicSwComponentType does not contain a SwcInternalBehavior.

        // rte_sws_7190: The RTE generator shall reject configurations where multiple SwComponentTypes have the same component type symbol regardless of the ARPackage
        // hierarchy.

        // rte_sws_7191: The RTE generator shall reject configurations where a SwComponentType has PortPrototypes typed by different PortInterfaces with equal short
        // name but conflicting ApplicationErrors. ApplicationErrors are conflicting if ApplicationErrors with same name do have different errorCodes.

        // rte_sws_7654: The RTE Generator shall reject configurations where a VariableDataPrototype is referenced by an NonqueuedReceiverComSpec with the
        // enableUpdate attribute enabled, when this VariableDataPrototype is referenced by a VariableAccess in the dataReadAccess role.

        // rte_sws_7670: The RTE shall reject the configuration if an ImplementationDataType with category DATA_REFERENCE is used in a PortInterface and neither
        // sender nor receiver component is a service, complex device driver or ECU abstraction.

        // rte_sws_7810: The RTE shall reject the configuration if a dataElement with an ImplementationDataType with subElements with arraySizeSemantics equal to
        // variableSize resolves to another type than uint8[n:.

        // rte_sws_7811: The RTE shall reject the configuration if a dataElement mapped to a PDU with ComIPduType equal to TP has a swImplPolicy different from queued.

        // rte_sws_7812: The RTE shall reject the configuration if a dataElement with an ImplementationDataType with subElements with arraySizeSemantics equal to
        // variableSize has a swImplPolicy different from queued.

    }
    catch(QString err)
    {
        std::cout << err.toStdString() << "\n";
        returnValue = false;
    }
    return returnValue;
}

bool RteGenerator::CreateIocC(QString location)     // No example found. Writing static code
{
    bool returnValue = true;
    QString fileName = location + "/Ioc.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Ioc.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#include <Ioc.h>\n#include <Os.h>\n#include <Rte_Internal.h>\n\nIocBuffersType IocBuffers;\n\nvoid IocInit(void) {\n\n}\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateIocH(QString location)     // No example found. Writing static code
{
    bool returnValue = true;
    QString fileName = location + "/Ioc.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Ioc.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// IOC Header File\n\n#include <Rte_Type.h>\n\n#define IOC_E_OK\tRTE_E_OK\n#define IOC_E_NOK\tRTE_E_NOK\n#define IOC_E_LIMIT\tRTE_E_LIMIT\n";
    fileStream << "#define IOC_E_LOST_DATA\tRTE_E_LOST_DATA\n#define IOC_E_NO_DATA\tRTE_E_NO_DATA\n\n#ifndef IOC_H_\n#define IOC_H_\n\ntypedef struct {";
    fileStream << "\n\tuint8 _dummy;\n} IocBuffersType;\n\n// === SwcReaderType ===\n// === SwcWriterType ===\n\nvoid IocInit(void);\n\n#endif // IOC_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteAssertH(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Assert.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Assert.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// Rte Runtime Assert\n\n#ifndef RTE_ASSERT_H_\n#define RTE_ASSERT_H_\n\n#include <Det.h>\n#include <Os.h>\n#include <Modules.h>\n\n";
    fileStream << "#define RTE_E_DET_ILLEGAL_SIGNAL_ID\t(0x01) // @req SWS_Rte_06633\n";
    fileStream << "#define RTE_E_DET_ILLEGAL_VARIANT_CRITERION_VALUE\t(0x02) // @req SWS_Rte_06634 @req SWS_Rte_07684\n";
    fileStream << "#define RTE_E_DET_ILLEGAL_INVOCATION\t(0x03) // @req SWS_Rte_06635\n";
    fileStream << "#define RTE_E_DET_WAIT_IN_EXCLUSIVE_AREA\t(0x04) // @req SWS_Rte_06637\n";
    fileStream << "#define RTE_E_DET_ILLEGAL_NESTED_EXCLUSIVE_AREA\t(0x05) // @req SWS_Rte_07675\n";
    fileStream << "#define RTE_E_DET_SEG_FAULT\t(0x06) // @req SWS_Rte_07685\n";
    fileStream << "#define RTE_E_DET_UNINIT\t(0x07) // @req SWS_Rte_07682 @req SWS_Rte_07683\n\n// @req SWS_Rte_06632\n";
    fileStream << "#define RTE_SERVICEID_API_PORTS\t(0x10)\n#define RTE_SERVICEID_API_NPORTS\t(0x11)\n#define RTE_SERVICEID_API_PORT\t(0x12)\n";
    fileStream << "#define RTE_SERVICEID_API_SEND\t(0x13)\n#define RTE_SERVICEID_API_WRITE\t(0x14)\n";
    fileStream << "#define RTE_SERVICEID_API_SWITCH\t(0x15)\n#define RTE_SERVICEID_API_INVALIDATE\t(0x16)\n";
    fileStream << "#define RTE_SERVICEID_API_FEEDBACK\t(0x17)\n#define RTE_SERVICEID_API_SWITCHACK\t(0x18)\n#define RTE_SERVICEID_API_READ\t(0x19)\n";
    fileStream << "#define RTE_SERVICEID_API_DREAD\t(0x1A)\n#define RTE_SERVICEID_API_RECEIVE\t(0x1B)\n#define RTE_SERVICEID_API_CALL\t(0x1C)\n";
    fileStream << "#define RTE_SERVICEID_API_RESULT\t(0x1D)\n#define RTE_SERVICEID_API_PIM\t(0x1E)\n#define RTE_SERVICEID_API_CDATA\t(0x1F)\n";
    fileStream << "#define RTE_SERVICEID_API_PRM\t(0x20)\n#define RTE_SERVICEID_API_IREAD\t(0x21)\n#define RTE_SERVICEID_API_IWRITE\t(0x22)\n";
    fileStream << "#define RTE_SERVICEID_API_IWRITEREF\t(0x23)\n#define RTE_SERVICEID_API_IINVALIDATE\t(0x24)\n#define RTE_SERVICEID_API_ISTATUS\t(0x25)\n";
    fileStream << "#define RTE_SERVICEID_API_IRVIREAD\t(0x26)\n#define RTE_SERVICEID_API_IRVIWRITE\t(0x27)\n#define RTE_SERVICEID_API_IRVREAD\t(0x28)\n";
    fileStream << "#define RTE_SERVICEID_API_IRVWRITE\t(0x29)\n#define RTE_SERVICEID_API_ENTER\t(0x2A)\n#define RTE_SERVICEID_API_EXIT\t(0x2B)\n";
    fileStream << "#define RTE_SERVICEID_API_MODE\t(0x2C)\n#define RTE_SERVICEID_API_TRIGGER\t(0x2D)\n#define RTE_SERVICEID_API_IRTRIGGER\t(0x2E)\n";
    fileStream << "#define RTE_SERVICEID_API_IFEEDBACK\t(0x2F)\n#define RTE_SERVICEID_API_ISUPDATED\t(0x30)\n\n";
    fileStream << "#define RTE_SERVICEID_EVENT_TIMINGEVENT\t(0x50)\n#define RTE_SERVICEID_EVENT_BACKGROUNDEVENT\t(0x51)\n";
    fileStream << "#define RTE_SERVICEID_EVENT_SWCMODESWITCHEVENT\t(0x52)\n#define RTE_SERVICEID_EVENT_ASYNCHRONOUSSERVERCALLRETURNSEVENT\t(0x53)\n";
    fileStream << "#define RTE_SERVICEID_EVENT_DATARECEIVEERROREVENT\t(0x54)\n#define RTE_SERVICEID_EVENT_OPERATIONINVOKEDEVENT\t(0x55)\n";
    fileStream << "#define RTE_SERVICEID_EVENT_DATARECEIVEDEVENT\t(0x56)\n#define RTE_SERVICEID_EVENT_DATASENDCOMPLETEDEVENT\t(0x57)\n";
    fileStream << "#define RTE_SERVICEID_EVENT_EXTERNALTRIGGEROCCURREDEVENT\t(0x58)\n#define RTE_SERVICEID_EVENT_INTERNALTRIGGEROCCURREDEVENT\t(0x59)\n";
    fileStream << "#define RTE_SERVICEID_EVENT_DATAWRITECOMPLETEDEVENT\t(0x5A)\n\n#define RTE_SERVICEID_API_START\t(0x70)\n";
    fileStream << "#define RTE_SERVICEID_API_STOP\t(0x71)\n#define RTE_SERVICEID_API_PARTITIONTERMINATED\t(0x72)\n#define RTE_SERVICEID_API_PARTITIONRESTARTING\t(0x73)\n";
    fileStream << "#define RTE_SERVICEID_API_RESTARTPARTITION\t(0x74)\n#define RTE_SERVICEID_API_INIT\t(0x75)\n#define RTE_SERVICEID_API_STARTTIMING\t(0x76)\n\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKTACK_SIGNAL\t(0x90)\n#define RTE_SERVICEID_CBK_COMCBKTERR_SIGNAL\t(0x91)\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKINV_SIGNAL\t(0x92)\n#define RTE_SERVICEID_CBK_COMCBKRXTOUT_SIGNAL\t(0x93)\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKTXTOUT_SIGNAL\t(0x94)\n#define RTE_SERVICEID_CBK_COMCBK_GROUP\t(0x95)\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKTACK_GROUP\t(0x96)\n#define RTE_SERVICEID_CBK_COMCBKTERR_GROUP\t(0x97)\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKINV_GROUP\t(0x98)\n#define RTE_SERVICEID_CBK_COMCBKRXTOUT_GROUP\t(0x99)\n";
    fileStream << "#define RTE_SERVICEID_CBK_COMCBKTXTOUT_GROUP\t(0x9A)\n#define RTE_SERVICEID_CBK_SETMIRROR\t(0x9B)\n";
    fileStream << "#define RTE_SERVICEID_CBK_GETMIRROR\t(0x9C)\n#define RTE_SERVICEID_CBK_NVMNOTIFYJOBFINISHED\t(0x9D)\n";
    fileStream << "#define RTE_SERVICEID_CBK_NVMNOTIFYINITBLOCK\t(0x9E)\n\n#define SCHM_SERVICEID_API_INIT\t(0x00)\n";
    fileStream << "#define SCHM_SERVICEID_API_DEINIT\t(0x01)\n#define SCHM_SERVICEID_API_GETVERSIONINFO\t(0x02)\n";
    fileStream << "#define SCHM_SERVICEID_API_ENTER\t(0x03)\n#define SCHM_SERVICEID_API_EXIT\t(0x04)\n#define SCHM_SERVICEID_API_ACTMAINFUNCTION\t(0x05)\n";
    fileStream << "#define SCHM_SERVICEID_API_SWITCH\t(0x06)\n#define SCHM_SERVICEID_API_MODE\t(0x07)\n#define SCHM_SERVICEID_API_SWITCHACK\t(0x08)\n";
    fileStream << "#define SCHM_SERVICEID_API_TRIGGER\t(0x09)\n\n";
    fileStream << "#define Rte_Assert_Enter(cond)\tRte_Assert(cond, RTE_SERVICEID_API_ENTER, RTE_E_DET_ILLEGAL_NESTED_EXCLUSIVE_AREA)\n\n";
    fileStream << "// @req SWS_Rte_06630, @req SWS_Rte_07676, @req SWS_Rte_06632, !req SWS_Rte_06631\n#if (RTE_DEV_ERROR_DETECT == STD_ON)\n";
    fileStream << "\t#define Rte_Assert(cond, service, error) \\\n\t\tdo { \\\n\t\t\tif (!(cond)) { \\\n\t\t\t\tDet_ReportError( MODULE_ID_RTE, 0, service, error ); \\\n";
    fileStream << "\t\t\t\tShutdownOS( E_OS_RTE ); \\\n\t\t\t} \\\n\t\t} while(0)\n#else\n\t#define Rte_Assert(cond, service, error)\n#endif\n";
    fileStream << "\n#endif // RTE_ASSERT_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteC(QString location, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes, QDomElement &projectModuleRoot, QList <QDomElement> &osTasks, QList <bool> &isOsTaskExtended)
{
    bool returnValue = true;
    QString fileName = location + "/Rte.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// Generated RTE\n// @req SWS_Rte_01169\n\n// === HEADER ====================================================================================\n";
    fileStream << "\n//@req SWS_Rte_01279\n#include <Rte.h>\n\n//@req SWS_Rte_01257\n#include <Os.h>";
    fileStream << "\n\n#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))\n";
    fileStream << "#error Os version mismatch\n#endif\n//@req SWS_Rte_03794\n#include <Com.h>\n\n";
    fileStream << "#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))\n";
    fileStream << "#error Com version mismatch\n#endif\n\n//@req SWS_Rte_01326\n#include <Rte_Hook.h>\n\n#include <Rte_Internal.h>\n\n#include <Ioc.h>\n\n";
    fileStream << "// === Os Macros ===\n\n#define END_OF_TASK(taskName)\tSYS_CALL_TerminateTask()\n\n#define ARC_STRINGIFY(value)\tARC_STRINGIFY2(value)\n";
    fileStream << "#define ARC_STRINGIFY2(value)\t#value\n#if defined(ARC_INJECTED_HEADER_RTE_C)\n#define  THE_INCLUDE ARC_STRINGIFY(ARC_INJECTED_HEADER_RTE_C)\n";
    fileStream << "#include THE_INCLUDE\n#undef THE_INCLUDE\n#endif\n\n#if !defined(RTE_EXTENDED_TASK_LOOP_CONDITION)\n#define RTE_EXTENDED_TASK_LOOP_CONDITION\t1\n";
    fileStream << "#endif\n\n";

    // Check which SWCs provide the modes
    QList <QDomElement> modeProviderSwcs;
    QStringList rteEventRefs;
    QList <int> rteEventToMMMapping;
    QStringList modeProviderPorts;
    QStringList modeProviderGroupPrototypes;
    QStringList modeProviderCompNames;
    QList < QStringList > onExitRunnableEvents, onExitRunnableComps, onExitTargetModes;
    QList < QStringList > onEntryRunnableEvents, onEntryRunnableComps, onEntryTargetModes;

    QDomNodeList swcModeSwitchEvents = ecuExtract.elementsByTagName("SWC-MODE-SWITCH-EVENT");
    for (int eIter = 0; eIter < swcModeSwitchEvents.size(); eIter++) {
        QDomElement modeRef = swcModeSwitchEvents.at(eIter).firstChildElement("MODE-IREFS").firstChildElement("MODE-IREF");
        QString modeGroupName = modeRef.firstChildElement("CONTEXT-MODE-DECLARATION-GROUP-PROTOTYPE-REF").text();
        QString modeName = modeGroupName.split("/").last();
        int lastSlash = modeGroupName.lastIndexOf("/");
        modeGroupName.remove(lastSlash, modeGroupName.size()-lastSlash);
        QString modePort = modeRef.firstChildElement("CONTEXT-PORT-REF").text();
        //qDebug() << "ModeName: " << modeName;
        bool swcFound = false;
        for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
            QDomNodeList provideInterfaces = swcs.at(swcIter).elementsByTagName("PROVIDED-INTERFACE-TREF");
            for (int iIter = 0; iIter < provideInterfaces.size(); iIter++) {
                if (provideInterfaces.at(iIter).toElement().text() == modeGroupName) {
                    swcFound = true;
                    QString activation = swcModeSwitchEvents.at(eIter).firstChildElement("ACTIVATION").text();
                    QString startOnEvent = swcModeSwitchEvents.at(eIter).firstChildElement("START-ON-EVENT-REF").text();
                    rteEventRefs.push_back(AutosarToolFactory::CreateReferenceFromDomElement(swcModeSwitchEvents.at(eIter).firstChildElement("SHORT-NAME")));
                    if (!modeProviderSwcs.contains(swcs.at(swcIter))) {
                        modeProviderSwcs.push_back(swcs.at(swcIter));
                        rteEventToMMMapping.push_back(modeProviderSwcs.size()-1);
                        modeProviderPorts.push_back(provideInterfaces.at(iIter).parentNode().firstChildElement("SHORT-NAME").text());
                        modeProviderGroupPrototypes.push_back(modeName);
                        modeProviderCompNames.push_back(componentPrototypes.at(swcIter).at(0).firstChildElement("SHORT-NAME").text());      // Get the correct component name: TODO
                        QStringList tempRunnableEvent, tempRunnableComp, tempMode;
                        tempRunnableEvent.push_back(startOnEvent.split("/").last());
                        tempRunnableComp.push_back(modePort);
                        tempMode.push_back(modeRef.firstChildElement("TARGET-MODE-DECLARATION-REF").text().split("/").last());          // TODO Check if the mode exists
                        if (activation == "ON-EXIT") {
                            onExitRunnableEvents.push_back(tempRunnableEvent);
                            onExitRunnableComps.push_back(tempRunnableComp);
                            onExitTargetModes.push_back(tempMode);
                            onEntryRunnableComps.push_back(QStringList());
                            onEntryRunnableEvents.push_back(QStringList());
                            onEntryTargetModes.push_back(QStringList());
                        } else if (activation == "ON-ENTRY") {
                            onEntryRunnableEvents.push_back(tempRunnableEvent);
                            onEntryRunnableComps.push_back(tempRunnableComp);
                            onEntryTargetModes.push_back(tempMode);
                            onExitRunnableComps.push_back(QStringList());
                            onExitRunnableEvents.push_back(QStringList());
                            onExitTargetModes.push_back(QStringList());
                        }
                    } else {
                        int sIndex = modeProviderSwcs.indexOf(swcs.at(swcIter));
                        rteEventToMMMapping.push_back(sIndex);
                        if (activation == "ON-EXIT") {
                            onExitRunnableEvents[sIndex].push_back(startOnEvent.split("/").last());
                            onExitRunnableComps[sIndex].push_back(modePort);
                            onExitTargetModes[sIndex].push_back(modeRef.firstChildElement("TARGET-MODE-DECLARATION-REF").text().split("/").last());         // TODO Check if the mode exists
                        } else if (activation == "ON-ENTRY") {
                            onEntryRunnableEvents[sIndex].push_back(startOnEvent.split("/").last());
                            onEntryRunnableComps[sIndex].push_back(modePort);
                            onEntryTargetModes[sIndex].push_back(modeRef.firstChildElement("TARGET-MODE-DECLARATION-REF").text().split("/").last());         // TODO Check if the mode exists
                        }
                    }
                    break;
                }
            }
            if (swcFound) {
                break;
            }
        }
    }

    // Get task and os event mappings
    QList <QDomElement> RteEventToTaskMapping;
    AutosarToolFactory::FindReferencesToElement(&RteEventToTaskMapping, &projectModuleRoot, "RteEventToTaskMapping");
    QStringList osTaskRefs;
    QStringList osEventRefs;            // Should be defined for each osTaskRef. TODO
    QList <int> modeEventIndex, noModeEventIndex;
    QStringList noModeEventComps, noModeEventRunnables;
    QList <int> positionInTask;
    for (int evIter = 0; evIter < RteEventToTaskMapping.size(); evIter++) {
        QDomElement mapping = RteEventToTaskMapping.at(evIter);
        QString taskRef = GET_VALUE_REF_OF_REF(mapping, "RteMappedToTaskRef");
        if (!osTaskRefs.contains(taskRef)) {
            osTaskRefs.push_back(taskRef);
        }
        QString osEventRef = GET_VALUE_REF_OF_REF(mapping, "RteUsedOsEventRef");
        if (!osEventRefs.contains(osEventRef)) {                                            // TODO: Limitation, one event can only be mapped to one task
            osEventRefs.push_back(osEventRef);
            QString rteEventRef = GET_VALUE_REF_OF_REF(mapping, "RteEventRef");
            QDomElement rteEvent = AutosarToolFactory::GetDomElementFromReference(&rteEventRef, ecuExtract);
            if (!rteEvent.isNull() && rteEvent.tagName() == "SWC-MODE-SWITCH-EVENT") {
                // Get mode machine
                int mmIndex;
                for (int eIter = 0; eIter < rteEventRefs.size(); eIter++) {
                    if (rteEventRefs.at(eIter) == rteEventRef) {
                        mmIndex = rteEventToMMMapping.at(eIter);
                    }
                }
                modeEventIndex.push_back(mmIndex);
                noModeEventIndex.push_back(-1);
            } else {
                modeEventIndex.push_back(-1);
                QString runnableRef = rteEvent.firstChildElement("START-ON-EVENT-REF").text();
                noModeEventRunnables.push_back(runnableRef.split("/").last());
                int lastSlash = runnableRef.lastIndexOf("/");
                runnableRef.remove(lastSlash, runnableRef.size()-lastSlash);                // Get rid of runnable name
                lastSlash = runnableRef.lastIndexOf("/");
                runnableRef.remove(lastSlash, runnableRef.size()-lastSlash);                // Get rid of behaviour name
                noModeEventComps.push_back(runnableRef);
                noModeEventIndex.push_back(noModeEventComps.size()-1);
            }
            QString positionValue = GET_VALUE_OF_PARAM_WITH_RETURN(mapping, "RtePositionInTask");
            if (positionValue == "") {
                positionInTask.push_back(1);
            } else {
                positionInTask.push_back(positionValue.toInt());
            }
        }
        QDomElement event = AutosarToolFactory::GetDomElementFromReference(&osEventRef, ecuExtract);
        if (event.tagName() == "DATA-RECEIVED-EVENT") {
        }
    }
    //qDebug() << "OsTaskRefs: " << osTaskRefs << ", OsEventRefs: " << osEventRefs << ", Pos: " << positionInTask << ", modeEv: " << modeEventIndex << ", noModeEv: " << noModeEventIndex;

    // Check that all tasks referenced in RTE exist in os
    QList <int> taskIndex;
    for (int refIter = 0; refIter < osTaskRefs.size(); refIter++) {
        for (int tIter = 0; tIter < osTasks.size(); tIter++) {
            if (osTasks.at(tIter).firstChildElement("SHORT-NAME").text() == osTaskRefs.at(refIter).split("/").last()) {
                taskIndex.push_back(tIter);
                break;
            }
        }
    }
    if (osTaskRefs.size() != taskIndex.size()) {
        std::cout << "Error: All tasks referenced in RTE mapping not found in OS module.\n";
        returnValue = false;
    }

    // Prepare assembly connector list (TODO remove copy of below code)
    QDomNodeList assemblyConnectors = ecuExtract.elementsByTagName("ASSEMBLY-SW-CONNECTOR");
    QStringList assemblyConnectorRequesterComponentRefs, assemblyConnectorProviderComponentRefs;
    QStringList assemblyConnectorRequesterPortRefs, assemblyConnectorProviderPortRefs;
    for (int assemIter = 0; assemIter < assemblyConnectors.size(); assemIter++) {
        QDomElement RRef = assemblyConnectors.at(assemIter).firstChildElement("REQUESTER-IREF");
        assemblyConnectorRequesterPortRefs.append(RRef.firstChildElement("TARGET-R-PORT-REF").text());
        assemblyConnectorRequesterComponentRefs.append(RRef.firstChildElement("CONTEXT-COMPONENT-REF").text());
        QDomElement PRef = assemblyConnectors.at(assemIter).firstChildElement("PROVIDER-IREF");
        assemblyConnectorProviderPortRefs.append(PRef.firstChildElement("TARGET-P-PORT-REF").text());
        assemblyConnectorProviderComponentRefs.append(PRef.firstChildElement("CONTEXT-COMPONENT-REF").text());
    }
    // Get correct runnable component names
    for (int cIter = 0; cIter < onExitRunnableComps.size(); cIter++) {
        for (int scIter = 0; scIter < onExitRunnableComps.at(cIter).size(); scIter++) {
            for (int connIter = 0; connIter < assemblyConnectorRequesterPortRefs.size(); connIter++) {
                if (assemblyConnectorRequesterPortRefs.at(connIter) == onExitRunnableComps.at(cIter).at(scIter)) {
                    onExitRunnableComps[cIter][scIter] = assemblyConnectorRequesterComponentRefs.at(connIter).split("/").last();
                }
            }
        }
    }
    for (int cIter = 0; cIter < onEntryRunnableComps.size(); cIter++) {
        for (int scIter = 0; scIter < onEntryRunnableComps.at(cIter).size(); scIter++) {
            for (int connIter = 0; connIter < assemblyConnectorRequesterPortRefs.size(); connIter++) {
                if (assemblyConnectorRequesterPortRefs.at(connIter) == onEntryRunnableComps.at(cIter).at(scIter)) {
                    onEntryRunnableComps[cIter][scIter] = assemblyConnectorRequesterComponentRefs.at(connIter).split("/").last();
                }
            }
        }
    }
    /*qDebug() << "P: " << modeProviderPorts
             << ", GP: " << modeProviderGroupPrototypes
             << ", CN: " << modeProviderCompNames
             << ", RTE.R: " << rteEventRefs
             << ", RTE.m: " << rteEventToMMMapping
             << ", ExitRE: " << onExitRunnableEvents
             << ", ExitRC: " << onExitRunnableComps
             << ", ExitTM: " << onExitTargetModes
             << ", EntryRE: " << onEntryRunnableEvents
             << ", EntryRC: " << onEntryRunnableComps
             << ", EntryTM: " << onEntryTargetModes;*/
    // Check the component type for runnables of non-mode events
    QDomNodeList protos = ecuExtract.elementsByTagName("SW-COMPONENT-PROTOTYPE");
    for (int eventIter = 0; eventIter < noModeEventIndex.size(); eventIter++) {
        if (noModeEventIndex.at(eventIter) >= 0) {
            for (int pIter = 0; pIter < protos.size(); pIter++) {
                if (protos.at(pIter).firstChildElement("TYPE-TREF").text() == noModeEventComps.at(noModeEventIndex.at(eventIter))) {
                    noModeEventComps[noModeEventIndex.at(eventIter)] = protos.at(pIter).firstChildElement("SHORT-NAME").text();
                }
            }
        }
    }
    //qDebug() << "NMC: " << noModeEventComps << ", NMR: " << noModeEventRunnables;

    for (int mmIter = 0; mmIter < modeProviderSwcs.size(); mmIter++) {
        fileStream << "extern " << modeProviderSwcs.at(mmIter).firstChildElement("SHORT-NAME").text() << "_ModeMachinesType " << modeProviderSwcs.at(mmIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines;\n";
    }
    fileStream << "\n//=== Generated API =============================================================================\n\n\n//=== Runnables =================================================================================\n\n";

    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        if (swcs.at(swcIter).tagName() == "APPLICATION-SW-COMPONENT-TYPE") {
            QDomElement firstRteEvent = swcs.at(swcIter).elementsByTagName("EVENTS").at(0).toElement().firstChildElement();
            for (int protoIter = 0; protoIter < componentPrototypes.at(swcIter).size(); protoIter++) {
                QDomElement rteEvent = firstRteEvent;
                while (!rteEvent.isNull()) {
                    if (rteEvent.tagName() != "OPERATION-INVOKED-EVENT") {
                        QString runnableEntityRef = rteEvent.firstChildElement("START-ON-EVENT-REF").text();
                        //qDebug() << "Ref: " << runnableEntityRef;
                        QDomElement runnableEntity = AutosarToolFactory::GetDomElementFromReference(&runnableEntityRef, ecuExtract);
                        if (runnableEntity.isNull()) {
                            std::cout << "Error: Unable to find runnable entity \"" << runnableEntityRef.toStdString() << "\" in the extract.\n";
                            returnValue = false;
                        }
                        fileStream << "extern void Rte_" << componentPrototypes.at(swcIter).at(protoIter).firstChildElement("SHORT-NAME").text() << "_" <<
                                      runnableEntity.firstChildElement("SHORT-NAME").text() << "(void);\n";
                    }
                    rteEvent = rteEvent.nextSiblingElement();
                }
            }
        }
    }

    fileStream << "\n//=== Tasks =====================================================================================\n\n";
    for (int refIter = 0; refIter < osTaskRefs.size(); refIter++) {
        if (isOsTaskExtended.at(taskIndex.at(refIter))) {
            fileStream << "void " << osTasks.at(taskIndex.at(refIter)).firstChildElement("SHORT-NAME").text() << "(void) { //@req SWS_Rte_02251\n";
            fileStream << "\tEventMaskType Event;\n\tdo {\n\t\tSYS_CALL_WaitEvent(";
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                if (eIter != 0) {
                    fileStream << " | ";
                }
                fileStream << "EVENT_MASK_" << osEventRefs.at(eIter).split("/").last();
            }
            fileStream << ");\n\t\tSYS_CALL_GetEvent(TASK_ID_" << osTaskRefs.at(refIter).split("/").last() << ", &Event);\n\n";
            // Set new mode to transition and execute exit runnables
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                int mmIndex = modeEventIndex.at(eIter);
                if (mmIndex >= 0) {
                    fileStream << "\t\tif (Event & (EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ")) {\n";
                    QString modeMachineName = modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_ModeMachines." +
                            modeProviderCompNames.at(mmIndex) + "." + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                    QString modeTransitionName = "RTE_TRANSITION_" + modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_" +
                            modeProviderCompNames.at(mmIndex) + "_" + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                    fileStream << "\t\t\t/* Check that a switch has been requested (nextMode is not a transition) */\n";
                    fileStream << "\t\t\tif (" << modeMachineName << ".nextMode != " << modeTransitionName << ") {\n";
                    fileStream << "\t\t\t\t" << modeMachineName << ".previousMode = " << modeMachineName << ".currentMode;\n";
                    fileStream << "\t\t\t\t" << modeMachineName << ".currentMode = " << modeTransitionName << "; /* Indicate ongoing transition */\n\n";
                    if (onExitRunnableComps.size() > mmIndex && onExitRunnableComps.at(mmIndex).size() > 0) {
                        fileStream << "\t\t\t\t/* Activate runnables ON-EXIT */\n";
                        for (int mrIter = 0; mrIter < onExitRunnableComps.at(mmIndex).size(); mrIter++) {
                            QString modeName = "RTE_MODE_" + modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_" +
                                    modeProviderCompNames.at(mmIndex) + "_" + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                            fileStream << "\t\t\t\tif (" << modeMachineName << ".previousMode == " << modeName << "_" << onExitTargetModes.at(mmIndex).at(mrIter) << ") {\n";
                            fileStream << "\t\t\t\t\tRte_" << onExitRunnableComps.at(mmIndex).at(mrIter) << "_" << onExitRunnableEvents.at(mmIndex).at(mrIter) << "();\n\t\t\t\t}\n";
                        }
                    }
                    fileStream << "\t\t\t}\n\t\t}\n";
                }
            }
            // Clear events for non-mode-switch events
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                if (noModeEventIndex.at(eIter) >= 0) {
                    fileStream << "\t\tif (Event & EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ") {\n";
                    fileStream << "\t\t\tSYS_CALL_ClearEvent (EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ");\n";
                    fileStream << "\t\t\tRte_" << noModeEventComps.at(noModeEventIndex.at(eIter)) << "_" << noModeEventRunnables.at(noModeEventIndex.at(eIter)) << "();\n\t\t}\n";
                }
            }
            // Set transition mode to new mode and execute entry runnables
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                int mmIndex = modeEventIndex.at(eIter);
                if (mmIndex >= 0) {
                    fileStream << "\t\tif (Event & (EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ")) {\n";
                    QString modeMachineName = modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_ModeMachines." +
                            modeProviderCompNames.at(mmIndex) + "." + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                    QString modeTransitionName = "RTE_TRANSITION_" + modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_" +
                            modeProviderCompNames.at(mmIndex) + "_" + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                    fileStream << "\t\t\t/* Check that a transition is ongoing */\n";
                    fileStream << "\t\t\tif (" << modeMachineName << ".currentMode == " << modeTransitionName << ") {\n";
                    if (onEntryRunnableComps.size() > mmIndex && onEntryRunnableComps.at(mmIndex).size() > 0) {
                        fileStream << "\t\t\t\t/* Activate runnables ON-ENTRY */\n";
                        for (int mrIter = 0; mrIter < onEntryRunnableComps.at(mmIndex).size(); mrIter++) {
                            QString modeName = "RTE_MODE_" + modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_" +
                                    modeProviderCompNames.at(mmIndex) + "_" + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                            fileStream << "\t\t\t\tif (" << modeMachineName << ".nextMode == " << modeName << "_" << onEntryTargetModes.at(mmIndex).at(mrIter) << ") {\n";
                            fileStream << "\t\t\t\t\tRte_" << onEntryRunnableComps.at(mmIndex).at(mrIter) << "_" << onEntryRunnableEvents.at(mmIndex).at(mrIter) << "();\n\t\t\t\t}\n";
                        }
                    }
                    fileStream << "\t\t\t\t" << modeMachineName << ".currentMode = " << modeMachineName << ".nextMode;\n";
                    fileStream << "\t\t\t\t" << modeMachineName << ".nextMode = " << modeTransitionName << "; /* Indicate that no transition is requested (nextMode can be a transition) */\n";
                    fileStream << "\t\t\t\t" << modeMachineName << ".transitionCompleted = TRUE;\n";
                    fileStream << "\t\t\t}\n\t\t}\n";
                }
            }
            // Clear mode switch event
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                int mmIndex = modeEventIndex.at(eIter);
                if (mmIndex >= 0) {
                    fileStream << "\t\t/* Clear event set by ModeSwitch */\n\t\tif (Event & EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ") {\n";
                    fileStream << "\t\t\tSYS_CALL_ClearEvent (EVENT_MASK_" << osEventRefs.at(eIter).split("/").last() << ");\n\t\t}\n";
                }
            }
            // Set mode transitions to completed
            for (int eIter = 0; eIter < osEventRefs.size(); eIter++) {
                int mmIndex = modeEventIndex.at(eIter);
                if (mmIndex >= 0) {
                    QString modeMachineName = modeProviderSwcs.at(mmIndex).firstChildElement("SHORT-NAME").text() + "_ModeMachines." +
                            modeProviderCompNames.at(mmIndex) + "." + modeProviderPorts.at(mmIndex) + "_" + modeProviderGroupPrototypes.at(mmIndex);
                    fileStream << "\t\t" << modeMachineName << ".transitionCompleted = FALSE;\n";
                }
            }
            fileStream << "\t} while (RTE_EXTENDED_TASK_LOOP_CONDITION);\n}\n";
        } else {

        }
    }

    fileStream << "\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteCallbacks(QString location, QDomElement* projectModuleRoot, QDomElement* ecuExtract, QDomElement* osModuleRoot)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Cbk.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Cbk.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#include <Rte_Cbk.h>\n";
    QString fileNameH = location + "/Rte_Cbk.h";
    QFile fileH(fileNameH);
    if (!fileH.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Cbk.h file.\n";
        return false;
    }
    QTextStream fileStreamH( &fileH );
    fileStreamH << "\n#include <Rte_Internal.h>\n#include <Os.h>\n#include <Com.h>\n";

    // Get RteEventToTaskMapping
    QList <QDomElement> RteEventToTaskMapping;
    AutosarToolFactory::FindReferencesToElement(&RteEventToTaskMapping, projectModuleRoot, "RteEventToTaskMapping");
    //qDebug() << "Mappings = " << RteEventToTaskMapping.size();
    for (int evIter = 0; evIter < RteEventToTaskMapping.size(); evIter++) {
        QDomElement mapping = RteEventToTaskMapping.at(evIter);
        QString eventRef = GET_VALUE_REF_OF_REF(mapping, "RteEventRef");
        QDomElement event = AutosarToolFactory::GetDomElementFromReference(&eventRef, *ecuExtract);
        if (event.tagName() == "DATA-RECEIVED-EVENT") {
            QString portRef = event.firstChildElement("DATA-IREF").firstChildElement("CONTEXT-R-PORT-REF").text();
            QDomElement port = AutosarToolFactory::GetDomElementFromReference(&portRef, *ecuExtract);
            QString osTaskRef = GET_VALUE_REF_OF_REF(mapping, "RteMappedToTaskRef");
            QDomElement osTask = AutosarToolFactory::GetDomElementFromReference(&osTaskRef, osModuleRoot->ownerDocument().documentElement());
            if (osTask.isNull())
                std::cout << "Task not found.\n";
            QString osEventRef = GET_VALUE_REF_OF_REF(mapping, "RteUsedOsEventRef");
            QDomElement osEvent = AutosarToolFactory::GetDomElementFromReference(&osEventRef, osModuleRoot->ownerDocument().documentElement());
            if (osEvent.isNull())
                std::cout << "Event not found.\n";
            fileStreamH << "\nvoid Rte_COMCbk_" << port.firstChildElement("SHORT-NAME").text() << "_Request(void);\n";
            fileStream << "\nvoid Rte_COMCbk_" << port.firstChildElement("SHORT-NAME").text() << "_Request(void) {\n";
            fileStream << "\tSYS_CALL_SetEvent(TASK_ID_" << osTask.firstChildElement("SHORT-NAME").text() << ", EVENT_MASK_" <<
                          osEvent.firstChildElement("SHORT-NAME").text() << ");\n}\n";
        }
    }
    fileStream << "\n";
    fileStreamH << "\n";
    file.close();
    fileH.close();

    return returnValue;
}

bool RteGenerator::CreateRteCalprmsC(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Calprms.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Calprms.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#include \"Rte_Type.h\"\n#include \"CalibrationData.h\"\n#include \"Rte_Calprms.h\"\n\n// Shared calibration parameters\n\n";
    fileStream << "// Per Instance Calibration Parameters\n\n// Parameter Component Parameters\n\nvoid Rte_Init_Calprms(void) {\n\n}\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteCalprmsH(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Calprms.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Calprms.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#include \"Rte_Type.h\"\n// This function initializes online calibration parameters with strategy initRam. It will copy ROM data\n";
    fileStream << "// for these parameters to the respective RAM mirror. Function should be called during RTE startup.\n\n";
    fileStream << "void Rte_Init_Calprms(void);\n\n// Shared calibration parameters\n\n// Per Instance Calibration Parameters\n\n";
    fileStream << "// Parameter Component Parameters\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteCfgH(QString location, QDomElement* projectModuleRoot)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Cfg.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Cfg.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    QDomElement RteGeneration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "RteGeneration");
    QString rded = GET_VALUE_OF_PARAM(RteGeneration, "RteDevErrorDetect");
    if (rded == "1")
        rded = "STD_ON";
    else
        rded = "STD_OFF";
    fileStream << "\n// RTE Configuration Header File\n\n// === HEADER ===\n\n// --- C++ guard ---\n\n#ifdef __cplusplus\n\nextern \"C\" {\n";
    fileStream << "#endif // __cplusplus\n\n// --- Normal include guard ---\n\n#ifndef RTE_CFG_H_\n#define RTE_CFG_H_\n\n";
    fileStream << "// --- Includes ---\n// @req SWS_Rte_07641\n\n#include <Std_Types.h>\n\n// === BODY ===\n\n";
    fileStream << "#define RTE_DEV_ERROR_DETECT\t" << rded << "\n\n// === FOOTER ===\n\n#endif // RTE_CFG_H_\n\n// @req SWS_Rte_07126\n#ifdef __cplusplus\n";
    fileStream << "} // extern \"C\"\n#endif // __cplusplus\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteDataHandleTypeH(QString location, QDomElement* ecuExtract)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_DataHandleType.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        std::cout << "Error: Unable to create Rte_DataHandleType.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    QList<QDomElement> VariableAccess;
    QDomNodeList tempNodeList = ecuExtract->elementsByTagName("DATA-WRITE-ACCESSS");
    for (int waIter = 0; waIter < tempNodeList.size(); waIter++) {
        QDomElement tempElem = tempNodeList.at(waIter).firstChildElement();
        while (!tempElem.isNull()) {
            VariableAccess.push_back(tempElem);
            tempElem = tempElem.nextSiblingElement();
        }
    }
    tempNodeList = ecuExtract->elementsByTagName("DATA-READ-ACCESSS");
    for (int waIter = 0; waIter < tempNodeList.size(); waIter++) {
        QDomElement tempElem = tempNodeList.at(waIter).firstChildElement();
        while (!tempElem.isNull()) {
            VariableAccess.push_back(tempElem);
            tempElem = tempElem.nextSiblingElement();
        }
    }
    QList<QString> AccessedVariableRefs;
    for (int varIter = 0; varIter < VariableAccess.size(); varIter++) {
        QDomElement tempElem = VariableAccess.at(varIter).firstChildElement("ACCESSED-VARIABLE");
        tempElem = tempElem.firstChildElement("AUTOSAR-VARIABLE-IREF");
        tempElem = tempElem.firstChildElement("TARGET-DATA-PROTOTYPE-REF");
        bool found = false;
        for (int refIter = 0; refIter < AccessedVariableRefs.size(); refIter++) {
            if (tempElem.text() == AccessedVariableRefs[refIter]) {
                found = true;
                break;
            }
        }
        if (!found)
        {
            //qDebug() << "Adding " << tempElem.text();
            AccessedVariableRefs.push_back(tempElem.text());
        }/* else {
            qDebug() << "Not adding " << tempElem.text();
        }*/
    }
    for (int varIter = 0; varIter < AccessedVariableRefs.size(); varIter++) {
        QString tempStr = AccessedVariableRefs.at(varIter);
        QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&tempStr, *ecuExtract);
        if (tempElem.isNull()) {
            std::cout << "Error: " << AccessedVariableRefs.at(varIter).toStdString() << " reference not found.";
            returnValue = false;
        } else {
            tempElem = tempElem.firstChildElement("TYPE-TREF");
            bool found = false;
            for (int refIter = 0; refIter < AccessedVariableRefs.size(); refIter++) {
                if (tempElem.text() == AccessedVariableRefs[refIter]) {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                //qDebug() << "Changing " << tempElem.text();
                AccessedVariableRefs[varIter] = tempElem.text();
            } else {
                //qDebug() << "Removing " << tempElem.text();
                AccessedVariableRefs.removeAt(varIter);
                varIter--;
            }
        }
    }
    fileStream << "\n/// Data Handle Types Header File\n\n// @req SWS_Rte_07920, @req SWS_Rte_07921, @req SWS_Rte_07922, @req SWS_Rte_07923, @req SWS_Rte_07136\n\n";
    fileStream << "#ifndef RTE_DATAHANDLETYPE_H_\n#define RTE_DATAHANDLETYPE_H_\n\n#include <Rte_Type.h>\n\n// --- Data Element without Status ---\n\n";
    fileStream << "// @req SWS_Rte_01363, @req SWS_Rte_01364, @req SWS_Rte_02607\n\n";
    for (int varIter = 0; varIter < AccessedVariableRefs.size(); varIter++) {
        QString varName = AccessedVariableRefs.at(varIter).split("/").last();
        fileStream << "typedef struct {\n\t" << varName << " value;\n} Rte_DE_" << varName << ";\n";
    }
    fileStream << "\n// --- Data Element with Status ---\n// @req SWS_Rte_01365, @req SWS_Rte_01366, @req SWS_Rte_03734, @req SWS_Rte_02666";
    fileStream << ", @req SWS_Rte_02589, @req SWS_Rte_02590, @req SWS_Rte_02609, @req SWS_Rte_03836\n\n#endif // RTE_DATAHANDLETYPE_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteFifoC(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Fifo.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Fifo.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// A circular buffer implementation for Rte fifos.*\n\n#include <stdlib.h>\n#include <string.h>\n#include <stdint.h>\n";
    fileStream << "#include \"Os.h\"\n#include \"Rte_Fifo.h\"\n\n#if defined(__GNUC__)\n#define MEMCPY(_x,_y,_z)\t__builtin_memcpy(_x,_y,_z)\n";
    fileStream << "#else\n#define MEMCPY(_x,_y,_z)\tmemcpy(_x,_y,_z)\n#endif\n\n// IMPROVMENT: Make it threadsafe, add DisableAllInterrts()/EnableAllInterrupts()\n";
    fileStream << "void Rte_Fifo_Init(RteFifoType *fBuf, void *buffer, int maxCnt, size_t dataSize) {\n\tfBuf->bufStart = buffer;\n\tfBuf->maxCnt = maxCnt;\n";
    fileStream << "\tfBuf->bufEnd = (char *)fBuf->bufStart + dataSize * maxCnt;\n\tfBuf->head = fBuf->bufStart;\n\tfBuf->tail = fBuf->bufStart;\n\t";
    fileStream << "fBuf->dataSize = dataSize;\n\tfBuf->currCnt = 0;\n}\n\nStd_ReturnType Rte_Fifo_Push(RteFifoType *fPtr, void const *dataPtr) {\n";
    fileStream << "\tSYS_CALL_SuspendOSInterrupts();\n\tif (fPtr == NULL) {\n\t\tSYS_CALL_ResumeOSInterrupts();\n\t\treturn RTE_E_NOK; // Faulty pointer into method\n";
    fileStream << "\t}\n\tif (fPtr->currCnt == fPtr->maxCnt) {\n\t\tSYS_CALL_ResumeOSInterrupts();\n\t\tfPtr->bufFullFlag = TRUE;\n\t\treturn RTE_E_LIMIT; // No more room\n";
    fileStream << "\t}\n\tMEMCPY(fPtr->head, dataPtr, fPtr->dataSize);\n\tfPtr->head = (char *) fPtr->head + fPtr->dataSize;\n\tif (fPtr->head == fPtr->bufEnd) {";
    fileStream << "\n\t\tfPtr->head = fPtr->bufStart;\n\t}\n\t++fPtr->currCnt;\n\tSYS_CALL_ResumeOSInterrupts();\n\treturn RTE_E_OK;\n}\n\n// Pop an entry from the buffer.";
    fileStream << "\n// @param fPtr\tPointer to the queue created with CirqBuffStatCreate, etc.\n// @param dataPtr\n// @return RTE_E_OK - if successfully popped.\n";
    fileStream << "//\t\t\tRTE_E_NO_DATA - nothing popped (it was empty)\n//\t\t\tRTE_E_LOST_DATA - if a buffer overflow has occurred previously\n";
    fileStream << "Std_ReturnType Rte_Fifo_Pop(RteFifoType *fPtr, void *dataPtr) {\n\tSYS_CALL_SuspendOSInterrupts();\n\tif ((fPtr == NULL) || (fPtr->currCnt == 0)) {\n";
    fileStream << "\t\tSYS_CALL_ResumeOSInterrupts();\n\t\treturn RTE_E_NO_DATA;\n\t}\n\tMEMCPY(dataPtr, fPtr->tail, fPtr->dataSize);\n";
    fileStream << "\tfPtr->tail = (char *) fPtr->tail + fPtr->dataSize;\n\tif (fPtr->tail == fPtr->bufEnd) {\n\t\tfPtr->tail = fPtr->bufStart;\n\t}\n";
    fileStream << "\t--fPtr->currCnt;\n\tif (fPtr->bufFullFlag) {\n\t\tfPtr->bufFullFlag = FALSE;\n\t\tSYS_CALL_ResumeOSInterrupts();\n\t\treturn RTE_E_LOST_DATA;";
    fileStream << "\n\t}\n\tSYS_CALL_ResumeOSInterrupts();\n\treturn RTE_E_OK;\n}\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteFifoH(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Fifo.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Fifo.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#ifndef RTE_FIFO_H_\n#define RTE_FIFO_H_\n\n#include <stddef.h>\n#include \"Platform_Types.h\"\n#include \"Rte.h\"\n\ntypedef struct {\n";
    fileStream << "\t// The max number of elements in the list\n\tuint8 maxCnt;\n\tuint8 currCnt;\n\n\t// Error flag\n\tboolean bufFullFlag;\n\n";
    fileStream << "\t// Size of the elements in the list\n\tsize_t dataSize;\n\t// List head and tail\n\tvoid *head;\n\tvoid *tail;\n\n\t// Buffer start/stop\n";
    fileStream << "\tvoid *bufStart;\n\tvoid *bufEnd;\n} RteFifoType;\n\nStd_ReturnType Rte_Fifo_Push(RteFifoType *fPtr, void const *dataPtr);\n";
    fileStream << "Std_ReturnType Rte_Fifo_Pop(RteFifoType *fPtr, void *dataPtr);\nvoid Rte_Fifo_Init(RteFifoType *fPtr, void *buffer, int maxCnt, size_t dataSize);\n";
    fileStream << "\nstatic inline boolean Rte_Fifo_Is_Empty(RteFifoType *fPtr) {\n\treturn (fPtr->currCnt == 0);\n}\n\n#endif // RTE_FIFO_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteH(QString location, QDomElement* projectModuleRoot)   // Finished.
{
    bool returnValue = true;
    QString rteHFileName = location + "/Rte.h";
    QFile rteHFile(rteHFileName);
    if (!rteHFile.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte.h file.\n";
        return false;
    }
    QTextStream rteHStream( &rteHFile );
    rteHStream << "\n// RTE Header File\n//@req SWS_Rte_01157\n\n#ifndef RTE_H_\n#define RTE_H_\n\n// --- Includes ---\n//@req SWS_Rte_01164\n#include <Std_Types.h>";
    rteHStream << "\n#include <Rte_Cfg.h>\n#include <string.h>\n\n// --- Versions ---\n#define RTE_AR_RELEASE_MAJOR_VERSION\t4\n#define RTE_AR_RELEASE_MINOR_VERSION\t0";
    QDomElement RteGeneration = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "RteGeneration");
    if (RteGeneration.isNull()) {
        std::cout << "Error: Unable to find RteGeneration container.\n";
        returnValue = false;
    }
    QString status = GET_VALUE_OF_PARAM(RteGeneration, "RteGenerationMode");
    if (status == "VENDOR_MODE")
        status = "STD_ON";
    else
        status = "STD_OFF";
    if (status == "STD_ON") {
        rteHStream << "\n\n// === VENDOR GENERATION MODE ===\n";
        // TODO Get these values from the tool settings (so far, there exist no tool settings)
        rteHStream << "#define RTE_VENDOR_OPT_EXCL_AREAS\t" << status << "\n";
        rteHStream << "#define RTE_VENDOR_OPT_CLEAR_EVENT_CALLS\t" << status << "\n";
        rteHStream << "#define RTE_VENDOR_OPT_CALPRMS\t" << status << "\n";
        rteHStream << "#define RTE_VENDOR_OPT_AVOID_ENTER_EXIT\t" << status << "\n";
        rteHStream << "#define RTE_VENDOR_OPT_SENDER_RECEIVER\t" << status << "\n";
    }
    rteHStream << "\n//=== ERROR CODES ===\n// @req SWS_Rte_07404\n#define Rte_IsInfrastructureError(status)\t((status & 128U) !=0)\n";
    rteHStream << "\n// @req SWS_Rte_07405\n#define Rte_HasOverlayedError(status)\t((status & 64U) != 0)\n";
    rteHStream << "\n// @req SWS_Rte_07406\n#define Rte_ApplicationError(status) (status & 63U)\n";
    rteHStream << "\n// --- Error values ---\n// @req SWS_Rte_01269\n\n//No error occurred. @req SWS_Rte_01058\n#define RTE_E_OK\t0";
    rteHStream << "\n// Error occured. Without error specification\n#define RTE_E_NOK\t1\n\n// --- Standard Application Error Values:\n";
    rteHStream << "\n#define RTE_E_INVALID\t1\n#define RTE_E_COM_STOPPED\t128\n#define RTE_E_TIMEOUT\t129\n#define RTE_E_LIMIT\t130";
    rteHStream << "\n#define RTE_E_NO_DATA\t131\n#define RTE_E_TRANSMIT_ACK\t132\n#define RTE_E_NEVER_RECEIVED\t133\n#define RTE_E_UNCONNECTED\t134";
    rteHStream << "\n#define RTE_E_IN_EXCLUSIVE_AREA\t135\n#define RTE_E_SEG_FAULT\t136\n#define RTE_E_OUT_OF_RANGE\t137\n#define RTE_E_SERIALIZATION_ERROR\t138";
    rteHStream << "\n#define RTE_E_SERIALIZATION_LIMIT\t139\n#define RTE_E_HARD_TRANSFORMER_ERROR\t138\n#define RTE_E_SOFT_TRANSFORMER_ERROR\t140\n";
    rteHStream << "#define RTE_E_TRANSFORMER_LIMIT\t139\n#define RTE_E_COM_BUSY\t141\n#define RTE_E_LOST_DATA\t64\n#define RTE_E_MAX_AGE_EXCEEDED\t64\n\n#endif // RTE_H_\n";
    rteHFile.close();
    return returnValue;
}

bool RteGenerator::CreateRteHookH(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Hook.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Hook.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#ifndef RTE_HOOK_H_\n#define RTE_HOOK_H_\n\n#endif // RTE_HOOK_H_\n";
    file.close();
    return returnValue;
}

QDomElement RteGenerator::CreateServerPortArguments(QDomElement &event, int &numOfPortArgs, QString &apiString)
{
    QDomElement operation = event.firstChildElement("OPERATION-IREF");
    QDomElement apiOptions = event.parentNode().parentNode().firstChildElement("PORT-API-OPTIONS");
    numOfPortArgs = 0;
    if (!apiOptions.isNull()) {
        apiOptions = apiOptions.firstChildElement();
        while (!apiOptions.isNull()) {
            if (operation.firstChildElement("CONTEXT-P-PORT-REF").text() == apiOptions.firstChildElement("PORT-REF").text()) {
                break;
            }
            apiOptions = apiOptions.nextSiblingElement();
        }
        QDomElement arg = apiOptions.firstChildElement("PORT-ARG-VALUES").firstChildElement();
        while (!arg.isNull()) {
            if (numOfPortArgs != 0)
                apiString += ", ";
            numOfPortArgs++;
            apiString += "/*IN*/" + arg.firstChildElement("VALUE-TYPE-TREF").text().split("/").last() + " portDefArg" + QString::number(numOfPortArgs); // Assuming only 'in' type
            arg = arg.nextSiblingElement();
        }
    }
    return operation;
}

bool RteGenerator::CreateServerCallCPrototype(QString &apiString, QDomElement &event, QDomElement &comp, int &numOfPortArgs, int &numOfInterfaceArgs, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract) {
    apiString += "Std_ReturnType Rte_" + comp.firstChildElement("SHORT-NAME").text() + "_" + event.firstChildElement("START-ON-EVENT-REF").text().split("/").last() + "(";
    QDomElement operation = CreateServerPortArguments(event, numOfPortArgs, apiString);
    QString targetRef = operation.firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
    QStringList dataTypes, variables;
    bool rv = CreateCSArguments(dataTypes, variables, targetRef, autosarModelModuleRoot, ecuExtract);
    if (!rv) {
        return false;
    }
    if (dataTypes.size() > 0) {
        for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
            if (argIter+numOfPortArgs > 0) {
                apiString += ", ";
            }
            apiString += dataTypes.at(argIter) + variables.at(argIter);
        }
    } else if (dataTypes.size()+numOfPortArgs == 0){
        apiString += "void";
    }
    numOfInterfaceArgs = dataTypes.size();
    apiString += ")";
    return true;
}

bool RteGenerator::CreateRteInternalC(QString location, QList<QDomElement> &ModeMachineSwcs, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes, QDomElement &projectModuleRoot, QDomElement &osModuleRoot, QList <QDomElement> &osTasks, QList <bool> &isOsTaskExtended, QList<QDomElement> &comSignals)
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Internal.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Internal.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );

    // Get RteEventToTaskMapping
    QList <QDomElement> RteEventToTaskMapping;
    AutosarToolFactory::FindReferencesToElement(&RteEventToTaskMapping, &projectModuleRoot, "RteEventToTaskMapping");

    fileStream << "\n#include <Rte_Internal.h>\n#include <Rte_Calprms.h>\n#include <Rte_Assert.h>\n#include <Rte_Fifo.h>\n#include <Com.h>\n";
    fileStream << "#include <Os.h>\n#include <Ioc.h>\n\n// --- EXTERNALS ---\n";
    // Define external event APIs (defined in Rte_<component name>.c)
    QStringList createdApiNames;
    QStringList createdApis;
    QList <int> numOfInterfaceArgsForApi;
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        for (int ptIter = 0; ptIter < componentPrototypes.at(swcIter).size(); ptIter++) {
            QDomElement comp = componentPrototypes.at(swcIter).at(ptIter);
            QDomElement event = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").firstChildElement("SWC-INTERNAL-BEHAVIOR").firstChildElement("EVENTS").firstChildElement();           // Only one behaviour per component is supported. TODO: Fix it
            while (!event.isNull()) {
                if (event.tagName() == "OPERATION-INVOKED-EVENT") {
                    QString tempApi;
                    int numOfPortArgs, numOfInterfaceArgs;
                    bool rv = CreateServerCallCPrototype(tempApi, event, comp, numOfPortArgs, numOfInterfaceArgs, autosarModelModuleRoot, ecuExtract);
                    if (!rv) {
                        event = event.nextSiblingElement();
                        continue;
                    }
                    QString tempApiName = "Rte_" + comp.firstChildElement("SHORT-NAME").text() + "_" + event.firstChildElement("START-ON-EVENT-REF").text().split("/").last();
                    if (createdApiNames.contains(tempApiName)) {
                        // For duplicate APIs, create only one API with more interface arguments
                        int duplicateIndex = createdApiNames.indexOf(tempApiName);
                        if (numOfInterfaceArgsForApi.at(duplicateIndex) < numOfInterfaceArgs) {
                            createdApiNames.replace(duplicateIndex, tempApiName);
                            createdApis.replace(duplicateIndex, tempApi);
                            numOfInterfaceArgsForApi.replace(duplicateIndex, numOfInterfaceArgs);
                        }
                    } else {
                        createdApiNames.push_back(tempApiName);
                        createdApis.push_back(tempApi);
                        numOfInterfaceArgsForApi.push_back(numOfInterfaceArgs);
                    }
                }
                event = event.nextSiblingElement();
            }
        }
    }
    for (int apiIter = 0; apiIter < createdApis.size(); apiIter++) {
        fileStream << "extern " << createdApis.at(apiIter) << ";\n";
    }
    fileStream << "\n// --- RTE INTERNAL DATA ---\n";
    CREATE_SECTION(fileStream, "Rte", "_VAR", "_INIT", "_UNSPECIFIED", "boolean RteInitialized = FALSE;");

    // Prepare assembly connector list
    QDomNodeList assemblyConnectors = ecuExtract.elementsByTagName("ASSEMBLY-SW-CONNECTOR");
    QStringList assemblyConnectorRequesterComponentRefs, assemblyConnectorProviderComponentRefs;
    QStringList assemblyConnectorRequesterPortRefs, assemblyConnectorProviderPortRefs;
    for (int assemIter = 0; assemIter < assemblyConnectors.size(); assemIter++) {
        QDomElement RRef = assemblyConnectors.at(assemIter).firstChildElement("REQUESTER-IREF");
        assemblyConnectorRequesterPortRefs.append(RRef.firstChildElement("TARGET-R-PORT-REF").text());
        assemblyConnectorRequesterComponentRefs.append(RRef.firstChildElement("CONTEXT-COMPONENT-REF").text());
        QDomElement PRef = assemblyConnectors.at(assemIter).firstChildElement("PROVIDER-IREF");
        assemblyConnectorProviderPortRefs.append(PRef.firstChildElement("TARGET-P-PORT-REF").text());
        assemblyConnectorProviderComponentRefs.append(PRef.firstChildElement("CONTEXT-COMPONENT-REF").text());
    }
    // Prepare delegate connector list
    QDomNodeList delegateConnectors = ecuExtract.elementsByTagName("DELEGATION-SW-CONNECTOR");
    QStringList delegateConnectorRequesterComponentRefs, delegateConnectorRequesterPortRefs, delegateConnectorOuterPortRefs;
    for (int assemIter = 0; assemIter < delegateConnectors.size(); assemIter++) {
        QDomElement outerPort = delegateConnectors.at(assemIter).firstChildElement("OUTER-PORT-REF");
        QDomElement RRef;
        if (outerPort.attribute("DEST") == "R-PORT-PROTOTYPE") {
            RRef = delegateConnectors.at(assemIter).firstChildElement("INNER-PORT-IREF").firstChildElement("R-PORT-IN-COMPOSITION-INSTANCE-REF");
            delegateConnectorRequesterPortRefs.append(RRef.firstChildElement("TARGET-R-PORT-REF").text());
        } else {
            RRef = delegateConnectors.at(assemIter).firstChildElement("INNER-PORT-IREF").firstChildElement("P-PORT-IN-COMPOSITION-INSTANCE-REF");
            delegateConnectorRequesterPortRefs.append(RRef.firstChildElement("TARGET-P-PORT-REF").text());
        }
        delegateConnectorRequesterComponentRefs.append(RRef.firstChildElement("CONTEXT-COMPONENT-REF").text());
        delegateConnectorOuterPortRefs.append(outerPort.text());
    }
    // Prepare list for sender-receiver to signal mapping
    QDomNodeList signalMappings = ecuExtract.elementsByTagName("SENDER-RECEIVER-TO-SIGNAL-MAPPING");
    QStringList signalMappingPortRefs, signalMappingDataPrototypeRefs, signalMappingSystemSignalRefs;
    for (int assemIter = 0; assemIter < signalMappings.size(); assemIter++) {
        QDomElement RRef = signalMappings.at(assemIter).firstChildElement("DATA-ELEMENT-IREF");
        signalMappingPortRefs.append(RRef.firstChildElement("CONTEXT-PORT-REF").text());
        signalMappingDataPrototypeRefs.append(RRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text());
        signalMappingSystemSignalRefs.append(signalMappings.at(assemIter).firstChildElement("SYSTEM-SIGNAL-REF").text());
    }
    // Prepare list of I signals
    QDomNodeList iSignals = ecuExtract.elementsByTagName("I-SIGNAL");
    QStringList iSignalNames, iSignalSystemSignalRefs;
    for (int assemIter = 0; assemIter < iSignals.size(); assemIter++) {
        iSignalNames.append(iSignals.at(assemIter).firstChildElement("SHORT-NAME").text());
        iSignalSystemSignalRefs.append(iSignals.at(assemIter).firstChildElement("SYSTEM-SIGNAL-REF").text());
    }
    // Prepare list of signal pdus
    QDomNodeList iPdus = ecuExtract.elementsByTagName("I-SIGNAL-I-PDU");
    QList <QStringList> iPduMappingRefs, iPduSignalRefs;
    for (int assemIter = 0; assemIter < iPdus.size(); assemIter++) {
        QDomElement mapping = iPdus.at(assemIter).firstChildElement("I-SIGNAL-TO-PDU-MAPPINGS").firstChildElement();
        QStringList tempList1, tempList2;
        while (!mapping.isNull()) {
            tempList1.append(AutosarToolFactory::CreateReferenceFromDomElement(mapping.firstChildElement("SHORT-NAME")));
            tempList2.append(mapping.firstChildElement("I-SIGNAL-REF").text());
            mapping = mapping.nextSiblingElement();
        }
        iPduMappingRefs.push_back(tempList1);
        iPduSignalRefs.push_back(tempList2);
    }

    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QString compName = swcs.at(swcIter).firstChildElement("SHORT-NAME").text();
        fileStream << "\n// === " << compName << " Data ===============================================================\n";
        // Create mode machine data types
        if (ModeMachineSwcs.contains(swcs.at(swcIter))) {
            QString tempStr = compName + "_ModeMachinesType " + compName + "_ModeMachines;";
            CREATE_SECTION(fileStream, compName, "_VAR", "_CLEARED", "_UNSPECIFIED", tempStr);
        }
        // Create buffer sections for R-Ports (which are SENDER-RECEIVER-INTERFACE type and have an ASSEMBLY-SW-CONNECTOR in top level composition).
        QDomElement port = swcs.at(swcIter).firstChildElement("PORTS").firstChildElement();
        while (!port.isNull()) {
            if (port.tagName() == "R-PORT-PROTOTYPE") {
                QDomElement interfaceRef = port.firstChildElement("REQUIRED-INTERFACE-TREF");
                if (interfaceRef.attribute("DEST") == "SENDER-RECEIVER-INTERFACE") {
                    QString portAddress = AutosarToolFactory::CreateReferenceFromDomElement(port.firstChildElement("SHORT-NAME"));
                    for (int assemIter = 0; assemIter < assemblyConnectorRequesterPortRefs.size(); assemIter++) {
                        if (assemblyConnectorRequesterPortRefs.at(assemIter) == portAddress) {
                            QString tempRef = interfaceRef.text();
                            QDomElement referedInterface = AutosarToolFactory::GetDomElementFromReference(&tempRef, ecuExtract);
                            if (referedInterface.isNull()) {
                                std::cout << "Error: Unknown interface type " << interfaceRef.text().toStdString() << " @ Line:" << interfaceRef.lineNumber() << " in the extract.\n";
                                returnValue = false;
                                port = port.nextSiblingElement();
                                continue;
                            }
                            for (int ptIter = 0; ptIter < componentPrototypes.at(swcIter).size(); ptIter++) {
                                QDomElement comp = componentPrototypes.at(swcIter).at(ptIter);
                                QDomElement prototype = referedInterface.firstChildElement("DATA-ELEMENTS").firstChildElement();
                                QString prototypeName, compProtoName;
                                while (!prototype.isNull()) {
                                    prototypeName = referedInterface.firstChildElement("SHORT-NAME").text();
                                    compProtoName = comp.firstChildElement("SHORT-NAME").text();
                                    QString protoRef = prototype.firstChildElement("TYPE-TREF").text();
                                    QString tempStr = protoRef.split("/").last() + " Rte_Buffer_" + compProtoName + "_" + port.firstChildElement("SHORT-NAME").text() + "_" + prototype.firstChildElement("SHORT-NAME").text() + ";";
                                    CREATE_SECTION(fileStream, compName, "_VAR", "_INIT", "_UNSPECIFIED", tempStr);
                                    prototype = prototype.nextSiblingElement();
                                }
                            }
                        }
                    }
                }
            }
            port = port.nextSiblingElement();
        }
    }

    fileStream << "\n// --- SERVER ACTIVATIONS ------------------------------------------------------------------\n";
    // Don't know what to generate

    QDomNodeList compositionComponentList = ecuExtract.elementsByTagName("COMPOSITION-SW-COMPONENT-TYPE");
    QDomElement composition = compositionComponentList.at(0).toElement();
    if (composition.isNull()) {
        std::cout << "Error: Unable to find the composition software component in the extract.\n";
        return false;
    }
    fileStream << "\n// --- FUNCTIONS ---------------------------------------------------------------------------\n";
    QString compFunctions;
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QString swcName = swcs.at(swcIter).firstChildElement("SHORT-NAME").text();
        compFunctions = "\n// === " + swcName + " =======================================================================";
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {
            QDomElement comp = componentPrototypes.at(swcIter).at(compIter);
            QString compName = comp.firstChildElement("SHORT-NAME").text();
            compFunctions += "\n// --- " + compName + " --------------------------------------------------------------------\n\n";
            QDomElement swc = swcs.at(swcIter);
            QDomElement port = swc.firstChildElement("PORTS").firstChildElement();
            QStringList prototypes;
            while (!port.isNull()) {
                prototypes.clear();
                QString portName = port.firstChildElement("SHORT-NAME").text();
                // Get all prototypes of the port
                compFunctions += "// ------ " + portName + "\n";
                QString portType;
                bool success;
                if (port.tagName() == "P-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, true);
                    portType = port.firstChildElement("PROVIDED-INTERFACE-TREF").attribute("DEST");
                } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("REQUIRED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, false);
                    portType = port.firstChildElement("REQUIRED-INTERFACE-TREF").attribute("DEST");
                } else {
                    std::cout << "Error: Unknown port type \"" << port.tagName().toStdString() << "\".\n";
                    return false;
                }
                if (!success) {
                    returnValue = false;
                }
                // Add created prototypes and their definition to the output
                for (int protoIter = 0; protoIter < prototypes.size(); protoIter++) {
                    compFunctions += prototypes.at(protoIter) + " {\n";
                    //qDebug() << "P: " << prototypes.at(protoIter);
                    if (portType == "CLIENT-SERVER-INTERFACE") {
                        QString targetOperationRef;
                        int numOfArgs = 0;
                        bool apiCreated = false;
                        if (port.tagName() == "P-PORT-PROTOTYPE") {
                            QString runEntName = "";
                            QDomNodeList events;
                            events = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("OPERATION-INVOKED-EVENT");
                            for (int eIter = 0; eIter < events.size(); eIter++) {
                                QDomElement opRef = events.at(eIter).firstChildElement("OPERATION-IREF");
                                QDomElement contextRef = opRef.firstChildElement("CONTEXT-P-PORT-REF");
                                if (contextRef.text().contains(portName)) {
                                    QString tempStr;
                                    tempStr = opRef.firstChildElement("TARGET-PROVIDED-OPERATION-REF").text().split("/").last();
                                    //qDebug() << "\tTO: " << tempStr;
                                    if (prototypes.at(protoIter).contains(tempStr)) {
                                        targetOperationRef = opRef.firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                                        runEntName = events.at(eIter).firstChildElement("START-ON-EVENT-REF").text().split("/").last();
                                        break;
                                    }
                                }
                            }
                            //qDebug() << "\t\tR: " << runEntName;
                            if (runEntName != "") {
                                // Create API
                                compFunctions += "    return Rte_" + compName + "_" + runEntName + "(";
                                // Add port API options
                                QDomNodeList portApis = port.parentNode().parentNode().firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("PORT-API-OPTION");
                                int apiIter;
                                for (apiIter = 0; apiIter < portApis.size(); apiIter++) {
                                    if (portApis.at(apiIter).firstChildElement("PORT-REF").text().split("/").last() == portName) {
                                        break;
                                    }
                                }
                                if (apiIter < portApis.size()) {
                                    QDomElement value = portApis.at(apiIter).firstChildElement("PORT-ARG-VALUES").firstChildElement();
                                    while (!value.isNull()) {
                                        if (numOfArgs > 0) {
                                            compFunctions += ", ";
                                        }
                                        compFunctions += value.firstChildElement("VALUE").firstChildElement().firstChildElement("VALUE").text();
                                        numOfArgs++;
                                        value = value.nextSiblingElement();
                                    }
                                }
                                apiCreated = true;
                            } else {
                                compFunctions += "    /* --- Unconnected */\n    return RTE_E_UNCONNECTED;\n";
                            }
                        } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                            QString trOp = "";
                            QDomNodeList callPoints = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("SYNCHRONOUS-SERVER-CALL-POINT");
                            for (int cpIter = 0; cpIter < callPoints.size(); cpIter++) {
                                QDomElement opRef = callPoints.at(cpIter).firstChildElement("OPERATION-IREF");
                                QDomElement contextRef = opRef.firstChildElement("CONTEXT-R-PORT-REF");
                                if (contextRef.text().contains(portName)) {
                                    QString tempStr = opRef.firstChildElement("TARGET-REQUIRED-OPERATION-REF").text().split("/").last();
                                    if (prototypes.at(protoIter).contains(tempStr)) {
                                        targetOperationRef = opRef.firstChildElement("TARGET-REQUIRED-OPERATION-REF").text();
                                        trOp = tempStr;
                                        //qDebug() << "\tTO: " << trOp;
                                        break;
                                    }
                                }
                            }
                            if (trOp != "") {
                                QString providerSwcName = "";
                                QString providerCompName = "";
                                QString providerPortName = "";
                                QString portRef = AutosarToolFactory::CreateReferenceFromDomElement(port.firstChildElement("SHORT-NAME"));
                                //qDebug() << "\t\tPortRef: " << portRef;
                                for (int connIter = 0; connIter < assemblyConnectors.size(); connIter++) {
                                    QString targetComp = assemblyConnectorRequesterComponentRefs.at(connIter).split("/").last();
                                    if (assemblyConnectorRequesterPortRefs.at(connIter) == portRef && targetComp == compName) {
                                        QStringList targetPPort = assemblyConnectorProviderPortRefs.at(connIter).split("/", QString::SkipEmptyParts);
                                        providerSwcName = targetPPort.at(targetPPort.size()-2);
                                        providerPortName = targetPPort.at(targetPPort.size()-1);
                                        providerCompName = assemblyConnectorProviderComponentRefs.at(connIter).split("/").last();
                                        //qDebug() << "\t\tTarget: " << targetPPort << ", op: " << trOp;
                                        break;
                                    }
                                }
                                if (providerCompName != "") {
                                    compFunctions += "    return Rte_Call_" + providerSwcName + "_" + providerCompName + "_" + providerPortName + "_" + trOp + "(";
                                    apiCreated = true;
                                } else {
                                    compFunctions += "    /* --- Unconnected */\n    return RTE_E_UNCONNECTED;\n";
                                }
                            }
                        }
                        if (apiCreated) {
                            // Add interface arguments
                            //qDebug() << "R: " << targetOperationRef;
                            QStringList dataTypes, variables;
                            bool rv = CreateCSArguments(dataTypes, variables, targetOperationRef, autosarModelModuleRoot, ecuExtract);
                            if (!rv) {
                                return false;
                            }
                            //qDebug() << "V: " << variables;
                            if (dataTypes.size() > 0) {
                                for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                                    if (argIter+numOfArgs > 0) {
                                        compFunctions += ", ";
                                    }
                                    compFunctions += variables.at(argIter);
                                }
                            }
                            compFunctions += ");\n";
                        }
                    } else if (portType == "SENDER-RECEIVER-INTERFACE") {
                        compFunctions += "\tStd_ReturnType retVal = RTE_E_OK;\n";
                        // Check if the variable access is connected
                        QDomNodeList variableAccesses = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("VARIABLE-ACCESS");
                        QString variableAccess = "";
                        for (int verIter = 0; verIter < variableAccesses.size(); verIter++) {
                            //qDebug() << "\t\tVariable access: " << variableAccesses.at(verIter).firstChildElement("SHORT-NAME").text();
                            QDomElement variableRef = variableAccesses.at(verIter).toElement().firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                            QString portPrototypeRef = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text();
                            //qDebug() << "\t\tPrototype ref: " << portPrototypeRef;
                            if (portPrototypeRef.contains(port.firstChildElement("SHORT-NAME").text())) {
                                QDomElement tempVar = AutosarToolFactory::GetDomElementFromReference(&portPrototypeRef, ecuExtract);
                                if (tempVar.isNull()) {
                                    //qDebug() << "\t\t\tNot found in ecuExtract.\n";
                                    tempVar = AutosarToolFactory::GetDomElementFromReference(&portPrototypeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                                }
                                if (!tempVar.isNull() && tempVar == port) {
                                    QString dataType = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text().split("/").last();
                                    //qDebug() << "\t\tDataType: " << dataType;
                                    if (prototypes.at(protoIter).contains(dataType)) {
                                        variableAccess = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
                                        break;
                                    }
                                }
                            }
                        }
                        // Check if the signal is connected
                        QString portRef = AutosarToolFactory::CreateReferenceFromDomElement(port.firstChildElement("SHORT-NAME"));
                        QString compRef = AutosarToolFactory::CreateReferenceFromDomElement(comp.firstChildElement("SHORT-NAME"));
                        // Check assembly connectors
                        int assemblyConnectorIndex = -1;
                        if (port.tagName() == "R-PORT-PROTOTYPE") {
                            for (int connIter = 0; connIter < assemblyConnectors.size(); connIter++) {
                                if (assemblyConnectorRequesterPortRefs.at(connIter) == portRef && assemblyConnectorRequesterComponentRefs.at(connIter) == compRef) {
                                    assemblyConnectorIndex = connIter;
                                    break;
                                }
                            }
                        } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                            for (int connIter = 0; connIter < assemblyConnectors.size(); connIter++) {
                                if (assemblyConnectorProviderPortRefs.at(connIter) == portRef && assemblyConnectorProviderComponentRefs.at(connIter) == compRef) {
                                    assemblyConnectorIndex = connIter;
                                    break;
                                }
                            }
                        }
                        // Check delgate connectors
                        int delegateMappingIndex = -1;
                        for (int connIter = 0; connIter < delegateConnectors.size(); connIter++) {
                            if (delegateConnectorRequesterPortRefs.at(connIter) == portRef && delegateConnectorRequesterComponentRefs.at(connIter) == compRef) {
                                // Find the signal mapping which matches the data prototype
                                for (int mIter = 0; mIter < signalMappingPortRefs.size(); mIter++) {
                                    if (signalMappingPortRefs.at(mIter) == delegateConnectorOuterPortRefs.at(connIter)) {
                                        if (prototypes.at(protoIter).contains(signalMappingDataPrototypeRefs.at(mIter).split("/").last())) {
                                            delegateMappingIndex = mIter;
                                            break;
                                        }
                                    }
                                }
                                if (delegateMappingIndex != -1) {
                                    break;
                                }
                            }
                        }
                        // Find the i-signal
                        int delgateSignalIndex = -1;
                        if (delegateMappingIndex != -1) {
                            for (int sIter = 0; sIter < iSignalSystemSignalRefs.size(); sIter++) {
                                if (iSignalSystemSignalRefs.at(sIter) == signalMappingSystemSignalRefs.at(delegateMappingIndex)) {
                                    delgateSignalIndex = sIter;
                                    break;
                                }
                            }
                        }
                        if (variableAccess != "" || delgateSignalIndex != -1 || assemblyConnectorIndex != -1) {
                            if (assemblyConnectorIndex != -1) {
                                if (port.tagName() == "R-PORT-PROTOTYPE") {
                                    compFunctions += "\t/* --- Receiver (" + assemblyConnectors.at(assemblyConnectorIndex).firstChildElement("SHORT-NAME").text() + ") */\n\t{\n";
                                } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                    compFunctions += "\t/* --- Sender (" + assemblyConnectors.at(assemblyConnectorIndex).firstChildElement("SHORT-NAME").text() + ") */\n\t{\n";
                                }
                                QString dataMove;
                                //qDebug() << "VA: " << variableAccess;
                                if (variableAccess != "") {
                                    QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&variableAccess, ecuExtract);
                                    if (dataPrototype.isNull()) {
                                        dataPrototype = AutosarToolFactory::GetDomElementFromReference(&variableAccess, autosarModelModuleRoot.ownerDocument().documentElement());
                                    }
                                    QString typeRef = dataPrototype.firstChildElement("TYPE-TREF").text();
                                    QDomElement dataType = AutosarToolFactory::GetDomElementFromReference(&typeRef, ecuExtract);
                                    if (dataType.isNull()) {
                                        dataType = AutosarToolFactory::GetDomElementFromReference(&typeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                                    }
                                    if (dataType.firstChildElement("SHORT-NAME").text() == "uint32") {
                                        if (port.tagName() == "R-PORT-PROTOTYPE") {
                                            dataMove = "\t\tSYS_CALL_AtomicCopy32(*data, Rte_Buffer_" + compName + "_" + portName + "_" + variableAccess.split("/").last() + ");\n";
                                        } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                            dataMove = "\t\tSYS_CALL_AtomicCopy32(Rte_Buffer_" + assemblyConnectorRequesterComponentRefs.at(assemblyConnectorIndex).split("/").last() +
                                                    "_" + assemblyConnectorRequesterPortRefs.at(assemblyConnectorIndex).split("/").last() + "_" + variableAccess.split("/").last() + ", data);\n";
                                        }
                                    } // TODO add more data types
                                    else {
                                        dataMove += "\t\tSYS_CALL_SuspendOSInterrupts();\n";
                                        if (port.tagName() == "R-PORT-PROTOTYPE") {
                                            dataMove += "\t\t*data = Rte_Buffer_" + compName + "_" + portName + "_" + variableAccess.split("/").last() + ";\n";
                                        } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                            dataMove += "\t\tRte_Buffer_" + assemblyConnectorRequesterComponentRefs.at(assemblyConnectorIndex).split("/").last() +
                                                    "_" + assemblyConnectorRequesterPortRefs.at(assemblyConnectorIndex).split("/").last() + "_" + variableAccess.split("/").last() + " = data;\n";
                                        }
                                        dataMove += "\t\tSYS_CALL_ResumeOSInterrupts();\n";
                                    }
                                } else {
                                    // Get the variable prototype
                                    QString interfaceRef;
                                    if (port.tagName() == "R-PORT-PROTOTYPE") {
                                        interfaceRef = port.firstChildElement("REQUIRED-INTERFACE-TREF").text();
                                    } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                        interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF").text();
                                    }
                                    QDomElement interface = AutosarToolFactory::GetDomElementFromReference(&interfaceRef, ecuExtract);
                                    if (interface.isNull()) {
                                        interface = AutosarToolFactory::GetDomElementFromReference(&interfaceRef, autosarModelModuleRoot.ownerDocument().documentElement());
                                    }
                                    QDomNodeList dataProtos = interface.elementsByTagName("VARIABLE-DATA-PROTOTYPE");
                                    QString proto = "";
                                    for (int pIter = 0; pIter < dataProtos.size(); pIter++) {
                                        if (prototypes.at(protoIter).contains(dataProtos.at(pIter).firstChildElement("SHORT-NAME").text())) {
                                            proto = dataProtos.at(pIter).firstChildElement("SHORT-NAME").text();
                                        }
                                    }
                                    dataMove += "\t\tSYS_CALL_SuspendOSInterrupts();\n";
                                    if (port.tagName() == "R-PORT-PROTOTYPE") {
                                        dataMove += "\t\t*data = Rte_Buffer_" + compName + "_" + portName + "_" + proto + ";\n";
                                    } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                        dataMove += "\t\tRte_Buffer_" + assemblyConnectorRequesterComponentRefs.at(assemblyConnectorIndex).split("/").last() +
                                                "_" + assemblyConnectorRequesterPortRefs.at(assemblyConnectorIndex).split("/").last() + "_" + proto + " = data;\n";
                                    }
                                    dataMove += "\t\tSYS_CALL_ResumeOSInterrupts();\n";
                                }
                                compFunctions += dataMove + "\t}\n";
                            } else if (delgateSignalIndex != -1) {
                                int pduIndex = -1, mappingIndex = -1;
                                for (int pduIter = 0; pduIter < iPduSignalRefs.size(); pduIter++) {
                                    for (int mapIter = 0; mapIter < iPduSignalRefs.at(pduIter).size(); mapIter++) {
                                        if (iPduSignalRefs.at(pduIter).at(mapIter).split("/").last() == iSignalNames.at(delgateSignalIndex)) {
                                            pduIndex = pduIter;
                                            mappingIndex = mapIter;
                                            break;
                                        }
                                    }
                                    if (pduIndex != -1) {
                                        break;
                                    }
                                }
                                if (pduIndex == -1) {
                                    std::cout << "Error: Unable to find signal to PDU mapping for signal \"" << iSignalNames.at(delgateSignalIndex).toStdString() << "\".\n";
                                    return false;
                                }
                                // Get com signal from the communication configuration
                                int comIndex = -1;
                                for (int sigIter = 0; sigIter < comSignals.size(); sigIter++) {
                                    QString ref = GET_VALUE_REF_OF_REF_WITH_RETURN(comSignals[sigIter], "ComSystemTemplateSystemSignalRef");
                                    if (ref == iPduMappingRefs.at(pduIndex).at(mappingIndex)) {
                                        comIndex = sigIter;
                                        break;
                                    }
                                }
                                if (comIndex == -1) {
                                    std::cout << "Error: Unable to find COM signal for mapping \"" << iPduMappingRefs.at(pduIndex).at(mappingIndex).toStdString() << "\".\n";
                                    return false;
                                }
                                QString signalName = comSignals.at(comIndex).firstChildElement("SHORT-NAME").text();
                                if (port.tagName() == "R-PORT-PROTOTYPE") {
                                    compFunctions += "\t/* --- Receiver (" + iSignalNames.at(delgateSignalIndex) + ") @req SWS_Rte_04505, @req SWS_Rte_06023 */\n";
                                    compFunctions += "\tretVal |= Com_ReceiveSignal(ComConf_ComSignal_" + signalName + ", data);\n";
                                } else if (port.tagName() == "P-PORT-PROTOTYPE") {
                                    compFunctions += "\t/* --- Sender (" + iSignalNames.at(delgateSignalIndex) + ") @req SWS_Rte_04505, @req SWS_Rte_06023 */\n";
                                    compFunctions += "\tretVal |= Com_SendSignal(ComConf_ComSignal_" + signalName + ", &data);\n";
                                }
                            }
                        } else {
                            compFunctions += "\t// --- Unconnected\n";
                        }
                        compFunctions += "\treturn retVal;\n";
                    } else if (portType == "MODE-SWITCH-INTERFACE") {
                        if (port.tagName() == "P-PORT-PROTOTYPE") {
                            // Get port mode group
                            QString modeGroupName = "";
                            // TODO: Code below is copied from CreateRteFunctionCallCPrototype(). Merge these codes.
                            QDomNodeList modeSwitchPoints = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("MODE-SWITCH-POINT");
                            for (int pointIter = 0; pointIter < modeSwitchPoints.size(); pointIter++) {
                                //qDebug() << "\t\tMode switch point: " << modeSwitchPoints.at(pointIter).firstChildElement("SHORT-NAME").text();
                                QDomElement modeGroupRef = modeSwitchPoints.at(pointIter).firstChildElement("MODE-GROUP-IREF");
                                QDomElement contextPortRef = modeGroupRef.firstChildElement("CONTEXT-P-PORT-REF");
                                //qDebug() << "\t\tContext port ref: " << contextPortRef.text();
                                if (contextPortRef.text().contains(port.firstChildElement("SHORT-NAME").text())) {
                                    QString tempText = contextPortRef.text();
                                    QDomElement tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, ecuExtract);
                                    if (tempContext.isNull()) {
                                        //qDebug() << "\t\t\tNot found in ecuExtract.\n";
                                        tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, autosarModelModuleRoot.ownerDocument().documentElement());
                                    }
                                    if (!tempContext.isNull() && tempContext == port) {
                                        modeGroupName = modeGroupRef.firstChildElement("TARGET-MODE-GROUP-REF").text().split("/").last();
                                        break;
                                    } else {
                                        tempContext.clear();
                                    }
                                } else {
                                    contextPortRef.clear();
                                }
                            }
                            if (modeGroupName == "") {
                                std::cout << "Error: Unknown to find mode group for port \"" << port.firstChildElement("SHORT-NAME").text().toStdString() << "\".\n";
                                return false;
                            }
                            QString modeMachine = swcName + "_ModeMachines." + compName + "." + portName + "_" + modeGroupName;
                            QString rteTransition = "RTE_TRANSITION_" + swcName + "_" + compName + "_" + portName + "_" + modeGroupName;

                            // Get assembly connectors of the port
                            QString portRef = AutosarToolFactory::CreateReferenceFromDomElement(port.firstChildElement("SHORT-NAME"));
                            QString providerPortType = port.firstChildElement("PROVIDED-INTERFACE-TREF").text();
                            QString compRef = AutosarToolFactory::CreateReferenceFromDomElement(comp.firstChildElement("SHORT-NAME"));
                            //qDebug() << "P: " << portRef << ", C: " << compRef;
                            QList <QDomElement> connectedComponents;
                            QList <QDomElement> connectedPorts;
                            for (int connIter = 0; connIter < assemblyConnectors.size(); connIter++) {
                                if (assemblyConnectorProviderPortRefs.at(connIter) == portRef && assemblyConnectorProviderComponentRefs.at(connIter) == compRef) {
                                    QString tempRef = assemblyConnectorRequesterComponentRefs.at(connIter);
                                    QDomElement requesterComponent = AutosarToolFactory::GetDomElementFromReference(&tempRef, ecuExtract);
                                    if (requesterComponent.isNull()) {
                                        std::cout << "Error: Unknown component prototype \"" << tempRef.toStdString() << "\" for port \"" << port.firstChildElement("SHORT-NAME").text().toStdString() << "\".\n";
                                        return false;
                                    }
                                    tempRef = requesterComponent.firstChildElement("TYPE-TREF").text();
                                    requesterComponent = AutosarToolFactory::GetDomElementFromReference(&tempRef, ecuExtract);
                                    if (requesterComponent.isNull()) {
                                        std::cout << "Error: Unknown component \"" << tempRef.toStdString() << "\" for port \"" << port.firstChildElement("SHORT-NAME").text().toStdString() << "\".\n";
                                        return false;
                                    }
                                    //qDebug() << "Requester Port Ref: " << assemblyConnectorRequesterPortRefs.at(connIter);
                                    tempRef = assemblyConnectorRequesterPortRefs.at(connIter);
                                    QDomElement requesterPort = AutosarToolFactory::GetDomElementFromReference(&tempRef, ecuExtract);
                                    if (requesterPort.isNull()) {
                                        std::cout << "Error: Unknown component port \"" << tempRef.toStdString() << "\" for port \"" << port.firstChildElement("SHORT-NAME").text().toStdString() << "\".\n";
                                        return false;
                                    }
                                    //qDebug() << "Requester Port: " << requesterPort.firstChildElement("SHORT-NAME").text();
                                    connectedPorts.push_back(requesterPort);
                                    connectedComponents.push_back(requesterComponent);
                                }
                            }
                            //qDebug() << "numConn: " << connectedComponents.size();
                            QString callActivate;
                            if (connectedComponents.size() > 0) {
                                // Get mode switch events of the requester components
                                QList <QDomElement> events;
                                for (int connIter = 0; connIter < connectedComponents.size(); connIter++) {
                                    QDomNodeList switchEvents = connectedComponents.at(connIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("SWC-MODE-SWITCH-EVENT");
                                    //qDebug() << "Comp: " << connectedComponents.at(connIter).firstChildElement("SHORT-NAME").text() << ", SwitchEvents: " << switchEvents.size();
                                    for (int evIter = 0; evIter < switchEvents.size(); evIter++) {
                                        QDomNodeList contextPortRefs = switchEvents.at(evIter).toElement().elementsByTagName("CONTEXT-MODE-DECLARATION-GROUP-PROTOTYPE-REF");
                                        for (int refIter = 0; refIter < contextPortRefs.size(); refIter++) {
                                            if (contextPortRefs.at(refIter).toElement().text().contains(providerPortType)) {
                                                events.push_back(switchEvents.at(evIter).toElement());
                                            }
                                        }
                                    }
                                }
                                QStringList eventRefs;
                                for (int eventIter = 0; eventIter < events.size(); eventIter++) {
                                    eventRefs.push_back(AutosarToolFactory::CreateReferenceFromDomElement(events.at(eventIter).firstChildElement("SHORT-NAME")));
                                }
                                //qDebug() << "Events: " << eventRefs;
                                QList <QDomElement> mappedEvents;
                                QList <QDomElement> mappedOsTasks, mappedOsEvents;
                                for (int evIter = 0; evIter < RteEventToTaskMapping.size(); evIter++) {
                                    QDomElement mapping = RteEventToTaskMapping.at(evIter);
                                    QString eventRef = GET_VALUE_REF_OF_REF(mapping, "RteEventRef");
                                    QDomElement event = AutosarToolFactory::GetDomElementFromReference(&eventRef, ecuExtract);
                                    if (event.tagName() == "SWC-MODE-SWITCH-EVENT") {
                                        //qDebug() << "EventRef: " << eventRef;
                                        QString portRef = event.firstChildElement("MODE-IREFS").firstChildElement().firstChildElement("CONTEXT-PORT-REF").text();
                                        //qDebug() << "PortRef: " << portRef;
                                        QDomElement referedPort = AutosarToolFactory::GetDomElementFromReference(&portRef, ecuExtract);
                                        if (!referedPort.isNull() && connectedPorts.contains(referedPort)) {
                                            mappedEvents.push_back(event);
                                            QString osTaskRef = GET_VALUE_REF_OF_REF(mapping, "RteMappedToTaskRef");
                                            QDomElement osTask = AutosarToolFactory::GetDomElementFromReference(&osTaskRef, osModuleRoot.ownerDocument().documentElement());
                                            if (osTask.isNull()) {
                                                std::cout << "Error: Mapped task \"" << osTaskRef.toStdString() << "\" not found.\n";
                                                return false;
                                            }
                                            mappedOsTasks.push_back(osTask);
                                            QString osEventRef = GET_VALUE_REF_OF_REF(mapping, "RteUsedOsEventRef");
                                            QDomElement osEvent = AutosarToolFactory::GetDomElementFromReference(&osEventRef, osModuleRoot.ownerDocument().documentElement());
                                            if (osEvent.isNull()) {
                                                std::cout << "Error: Mapped event \"" << osEventRef.toStdString() << "\" not found.\n";
                                                return false;
                                            }
                                            mappedOsEvents.push_back(osEvent);
                                        }
                                    }
                                }
                                //qDebug() << "MappedEvents: " << mappedEvents.size();
                                if (mappedEvents.size() != events.size()) {
                                    std::cout << "Error: All mode switch events are not mapped to tasks.\n";
                                    return false;
                                }
                                // Make sure that all the events are mapped to the same task
                                for (int tIter = 0; tIter < mappedOsTasks.size()-1; tIter++) {
                                    if (mappedOsTasks.at(tIter) != mappedOsTasks.at(tIter+1)) {
                                        std::cout << "Error: All mode switch events must be mapped to the same OS task.\n";
                                        return false;
                                    }
                                }
                                int taskIndex = osTasks.indexOf(mappedOsTasks.at(0));
                                if (isOsTaskExtended.at(taskIndex)) {             // Mapped task is extended
                                    callActivate = "SYS_CALL_SetEvent(TASK_ID_" + mappedOsTasks.at(0).firstChildElement("SHORT-NAME").text() + ", EVENT_MASK_" + mappedOsEvents.at(0).firstChildElement("SHORT-NAME").text() + ");";
                                } else {
                                    callActivate = "SYS_CALL_ActivateTask(TASK_ID_" + mappedOsTasks.at(0).firstChildElement("SHORT-NAME").text() + ");";
                                }
                            } else {
                                // No runnables for the event
                                callActivate = "// No runnables to activate";
                            }
                            //qDebug() << "\n";
                            compFunctions += "\tif (" + modeMachine + ".nextMode == " + rteTransition + ") {\n\t\t{\n\t\t\tSYS_CALL_SuspendOSInterrupts();\n\t\t\t"
                                    + modeMachine + ".nextMode = mode;\n\t\t\tSYS_CALL_ResumeOSInterrupts();\n\t\t}\n\t\t// Activate runnables\n";
                            compFunctions += "\t\t" + callActivate + "\n";
                            if (callActivate == "// No runnables to activate") {
                                compFunctions += "\t\tSYS_CALL_SuspendOSInterrupts();\n\t\t" + modeMachine + ".currentMode = " + modeMachine + ".nextMode;\n\t\t" + modeMachine + ".nextMode = " +
                                        rteTransition + ";\n\t\tSYS_CALL_ResumeOSInterrupts();\n";
                            }
                            compFunctions += "\t\treturn RTE_E_OK;\n\t} else {\n\t\treturn RTE_E_LIMIT;\n\t}\n";
                        }
                    }
                    compFunctions += "}\n";
                }
                if (prototypes.size() == 0) {
                    compFunctions += "\n";
                }
                port = port.nextSiblingElement();
            }
        }
        CREATE_SECTION(fileStream, "Rte", "_CODE", "", "", compFunctions);
    }
    fileStream << "\n";
    CREATE_SECTION(fileStream, "Rte", "_CODE", "", "", "void Rte_Internal_Init_Buffers(void) {\n\t// Init communication buffers\n\n\t// Init mode machine queues\n\n}\n");

    file.close();
    return returnValue;
}

bool RteGenerator::CreateCSArguments(QStringList &dataType, QStringList &variable, QString &referedTargetRef, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract) {
    QDomElement referedTarget = AutosarToolFactory::GetDomElementFromReference(&referedTargetRef, ecuExtract);
    if (referedTarget.isNull()) {
        referedTarget = AutosarToolFactory::GetDomElementFromReference(&referedTargetRef, autosarModelModuleRoot.ownerDocument().documentElement());
        if (referedTarget.isNull()) {
            std::cout << "Error: Unknown operation type " << referedTargetRef.toStdString() << " in the extract.\n";
            return false;
        }
    }
    QDomElement arg = referedTarget.firstChildElement("ARGUMENTS").firstChildElement();
    bool isInModel;
    QString tempDataType;
    while (!arg.isNull()) {
        isInModel = false;
        QString dir = arg.firstChildElement("DIRECTION").text();
        tempDataType = "/*" + dir + "*/";
        QString implTypeRef = arg.firstChildElement("TYPE-TREF").text();
        QDomElement referedType = AutosarToolFactory::GetDomElementFromReference(&implTypeRef, ecuExtract);
        if (referedType.isNull()) {
            referedType = AutosarToolFactory::GetDomElementFromReference(&implTypeRef, autosarModelModuleRoot.ownerDocument().documentElement());
            if (referedType.isNull()) {
                std::cout << "Error: Target data type " << implTypeRef.toStdString() << " not found. Error Code: 00002.\n";
                return false;
            }
            isInModel = true;
        }
        if ( ( referedType.firstChildElement("CATEGORY").text() == "ARRAY" || referedType.firstChildElement("CATEGORY").text() == "STRUCTURE" ) && dir == "IN")
            tempDataType += "const ";
        bool starAdded = false;
        if (isInModel) {
            tempDataType += implTypeRef.split("/",  QString::SkipEmptyParts).last() + " ";
        } else {
            if (!referedType.firstChildElement("SUB-ELEMENTS").isNull()) {
                QDomElement impTypeElem = referedType.firstChildElement("SUB-ELEMENTS").firstChildElement("IMPLEMENTATION-DATA-TYPE-ELEMENT");
                QDomElement implDataTypeRef = impTypeElem.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement("SW-DATA-DEF-PROPS-VARIANTS").firstChildElement("SW-DATA-DEF-PROPS-CONDITIONAL").firstChildElement("IMPLEMENTATION-DATA-TYPE-REF");
                tempDataType += implDataTypeRef.text().split("/").last() + " ";
                if (impTypeElem.firstChildElement("CATEGORY").text() == "TYPE_REFERENCE") {
                    tempDataType += "* ";
                    starAdded = true;
                }
            } else {
                tempDataType += referedType.firstChildElement("SHORT-NAME").text() + " ";
            }
        }
        if ((dir == "OUT" || (referedType.firstChildElement("CATEGORY").text() == "STRUCTURE" && dir == "IN")) && !starAdded) {
            tempDataType += "* ";
        }
        variable.push_back(arg.firstChildElement("SHORT-NAME").text());
        dataType.push_back(tempDataType);
        arg = arg.nextSiblingElement();
    }
    return true;
}

void RteGenerator::CreateSRArguments(QDomElement &Type, QString &outString, bool isRead) {
    QDomElement category = Type.firstChildElement("CATEGORY");
    //std::cout << "\tInfo: Data type " << category.text().toStdString() << ".\n";
    if (isRead) {
        outString += "/*OUT*/";
    } else {
        outString += "/*IN*/";
    }
    if (category.text() == "TYPE_REFERENCE") {
        category = Type.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement("IMPLEMENTATION-DATA-TYPE-REF");
        outString += Type.firstChildElement("SHORT-NAME").text();
        if (isRead)
            outString += " *";
        outString += " data";
    } else if (category.text() == "VALUE") {
        QString type = Type.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement("BASE-TYPE-REF").text().split("/").last();
        outString += type;
        if (isRead)
            outString += " *";
        outString += " data";
    } else {
        std::cout << "\tError: Unsupported data type " << category.text().toStdString() << ".\n";
        /*} else if (category.text() == "DATA_REFERENCE") {
            category = Type.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement().firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement();
            QDomElement implDataType = category.firstChildElement("IMPLEMENTATION-DATA-TYPE-REF");
            if (implDataType.isNull()) {
                implDataType = category.firstChildElement("BASE-TYPE-REF");
            }
            QDomElement implPolicy = category.firstChildElement("SW-IMPL-POLICY");
            if (!implPolicy.isNull()) {
                if (implPolicy.text() == "CONST") {
                    outString += "const ";
                } else {
                    std::cout << "Error: Unknown SW-IMPL-POLICY " << implPolicy.text().toStdString() << "\n";
                }
            }
            QString type = implDataType.text().split("/").last();
            outString += type + " * " + Type.firstChildElement("SHORT-NAME").text();
        } else if (category.text() == "ARRAY") {
            category = Type.firstChildElement("SUB-ELEMENTS").firstChildElement();
            QString type = Type.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement("BASE-TYPE-REF").text().split("/").last();
            outString += type + " * " + Type.firstChildElement("SHORT-NAME").text();*/
    }
}

bool RteGenerator::CreateRteInternalH(QString location, QList<QDomElement> &ModeMachineSwcs, QList<QList<QDomElement> > &ModeMachineComps,
                                      QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs,
                                      QList < QList <QDomElement> > &componentPrototypes)
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Internal.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Internal.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#ifndef RTE_INTERNAL_H_\n#define RTE_INTERNAL_H_\n\n#include <Rte_Fifo.h>\n#include <Rte_DataHandleType.h>\n\nvoid Rte_Internal_Init_Buffers(void);\n";
    fileStream << "\n// --- PORT STATUS TYPES -------------------------------------------------------------------\n";
    // Unknown
    fileStream << "\n// --- SERVER REQUEST TYPES ----------------------------------------------------------------\n";
    // Unknown
    fileStream << "\n// --- SERVER RESPONSE TYPES ---------------------------------------------------------------\ntypedef enum {\n\tRTE_NO_REQUEST_PENDING = 0,\n\tRTE_REQUEST_PENDING = 1,\n";
    fileStream << "\tRTE_RESPONSE_RECEIVED = 2\n} Rte_ResponseStatusType;\n";
    // Unknown
    fileStream << "\n// --- SERVER RETURN SIGNAL IDS ------------------------------------------------------------\n";
    // Unknown
    fileStream << "\n// --- MODE MACHINE TYPES ------------------------------------------------------------------\n";
    QStringList modeTransitions;
    QList <unsigned> modeTransitionValues;
    QStringList modes;
    QList <unsigned> modeValues;
    for (int mmsIter = 0; mmsIter < ModeMachineSwcs.size(); mmsIter++) {
        fileStream << "\ntypedef struct {\n";
        for (int mmcIter = 0; mmcIter < ModeMachineComps.at(mmsIter).size(); mmcIter++) {
            fileStream << "\tstruct {\n";
            QDomElement port = ModeMachineSwcs.at(mmsIter).firstChildElement("PORTS").firstChildElement();
            while (!port.isNull()) {
                if (port.tagName() == "P-PORT-PROTOTYPE") {
                    QDomElement interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF");
                    if (interfaceRef.attribute("DEST") == "MODE-SWITCH-INTERFACE") {
                        QString portRef = interfaceRef.text();
                        QDomElement referedPort = AutosarToolFactory::GetDomElementFromReference(&portRef, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (referedPort.isNull()) {
                            referedPort = AutosarToolFactory::GetDomElementFromReference(&portRef, ecuExtract);
                            if (referedPort.isNull()) {
                                std::cout << "Error: Port " << portRef.toStdString() << " not found.\n";
                                return false;
                            }
                        }
                        QDomElement modeGroup = referedPort.firstChildElement("MODE-GROUP");
                        QString modeGroupName = modeGroup.firstChildElement("SHORT-NAME").text();
                        fileStream << "\t\tstruct {\n\t\t\tuint8 nextMode;\n\t\t\tuint8 currentMode;\n\t\t\tuint8 previousMode;\n\t\t\tboolean transitionCompleted;\n\t\t} " <<
                                      port.firstChildElement("SHORT-NAME").text() << "_" << modeGroupName << ";\n";

                        // Prepare for transition and mode definitions
                        QString typeRef = modeGroup.firstChildElement("TYPE-TREF").text();
                        QDomElement mode = AutosarToolFactory::GetDomElementFromReference(&typeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (mode.isNull()) {
                            mode = AutosarToolFactory::GetDomElementFromReference(&typeRef, ecuExtract);
                            if (mode.isNull()) {
                                std::cout << "Error: Mode " << typeRef.toStdString() << " not found.\n";
                                return false;
                            }
                        }
                        unsigned modeCount = 0;
                        QDomElement modeDecl = mode.firstChildElement("MODE-DECLARATIONS").firstChildElement();
                        while (!modeDecl.isNull()) {
                            modes.push_back(ModeMachineSwcs.at(mmsIter).firstChildElement("SHORT-NAME").text() + "_" +
                                            ModeMachineComps.at(mmsIter).at(mmcIter).firstChildElement("SHORT-NAME").text() + "_" +
                                            port.firstChildElement("SHORT-NAME").text() + "_" +
                                            modeGroupName + "_" +
                                            modeDecl.firstChildElement("SHORT-NAME").text());
                            modeValues.push_back(modeCount);
                            modeDecl = modeDecl.nextSiblingElement();
                            modeCount++;
                        }
                        modeTransitions.push_back(ModeMachineSwcs.at(mmsIter).firstChildElement("SHORT-NAME").text() + "_" +
                                                  ModeMachineComps.at(mmsIter).at(mmcIter).firstChildElement("SHORT-NAME").text() + "_" +
                                                  port.firstChildElement("SHORT-NAME").text() + "_" +
                                                  modeGroupName);
                        modeTransitionValues.push_back(modeCount);
                    }
                }
                port = port.nextSiblingElement();
            }
            fileStream << "\t} " << ModeMachineComps.at(mmsIter).at(mmcIter).firstChildElement("SHORT-NAME").text() << ";\n";
        }
        fileStream << "} " << ModeMachineSwcs.at(mmsIter).firstChildElement("SHORT-NAME").text() << "_ModeMachinesType;\n";
    }
    fileStream << "\n// --- MODE MACHINE TRANSITION DEFINES -----------------------------------------------------\n";
    for (int mtIter = 0; mtIter < modeTransitions.size(); mtIter++) {
        fileStream << "#define RTE_TRANSITION_" << modeTransitions.at(mtIter) << " " << modeTransitionValues.at(mtIter) << "\n";
    }
    fileStream << "\n// --- MODE MACHINE VALUE DEFINES ----------------------------------------------------------\n";
    for (int mIter = 0; mIter < modes.size(); mIter++) {
        fileStream << "#define RTE_MODE_" << modes.at(mIter) << " " << modeValues.at(mIter) << "\n";
    }
    fileStream << "\n// --- EXCLUSIVE AREA TYPES ----------------------------------------------------------------\ntypedef struct {\n\tboolean entered;\n} ExclusiveAreaType;\n";
    // Unknown
    fileStream << "\n// --- EXPORTED FUNCTIONS ------------------------------------------------------------------\n";
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QDomElement swc = swcs.at(swcIter);
        fileStream << "\n// === " << swc.firstChildElement("SHORT-NAME").text() << " =======================================================================\n";
        for (int ptIter = 0; ptIter < componentPrototypes.at(swcIter).size(); ptIter++) {
            QDomElement comp = componentPrototypes.at(swcIter).at(ptIter);
            fileStream << "// --- " << comp.firstChildElement("SHORT-NAME").text() << " --------------------------------------------------------------------\n\n";
            QDomElement port = swc.firstChildElement("PORTS").firstChildElement();
            QStringList prototypes;
            while (!port.isNull()) {
                prototypes.clear();
                // Get all prototypes of the port
                fileStream << "// ------ " << port.firstChildElement("SHORT-NAME").text() << "\n";
                bool success;
                if (port.tagName() == "P-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, true);
                } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("REQUIRED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, false);
                } else {
                    std::cout << "Error: Unknown port type \"" << port.tagName().toStdString() << "\".\n";
                    return false;
                }
                if (!success) {
                    returnValue = false;
                }
                // Add created prototypes to the output
                for (int protoIter = 0; protoIter < prototypes.size(); protoIter++) {
                    fileStream << prototypes.at(protoIter) << ";\n";
                }
                if (prototypes.size() == 0) {
                    fileStream << "// Empty\n";
                }
                port = port.nextSiblingElement();
            }
        }
    }
    fileStream << "\n#endif // RTE_INTERNAL_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteFunctionCallCPrototype(QStringList &protoList, QString &interfaceRef, QDomElement &swc, QDomElement &componentPrototype, QDomElement &port, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, bool isPPort) {
    QDomElement Interface = AutosarToolFactory::GetDomElementFromReference(&interfaceRef, autosarModelModuleRoot.ownerDocument().documentElement());
    if (Interface.isNull()) {
        Interface = AutosarToolFactory::GetDomElementFromReference(&interfaceRef, ecuExtract);
        if (Interface.isNull()) {
            std::cout << "Error: Interface " << interfaceRef.toStdString() << " not found.\n";
            return false;
        }
    }
    if (Interface.tagName() == "CLIENT-SERVER-INTERFACE") {
        QDomNodeList eventsOrCallPoints;
        if (port.tagName() == "P-PORT-PROTOTYPE") {
            eventsOrCallPoints = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("OPERATION-INVOKED-EVENT");
        } else if (port.tagName() == "R-PORT-PROTOTYPE") {
            eventsOrCallPoints = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("SYNCHRONOUS-SERVER-CALL-POINT");
        }
        for (int eCpIter = 0; eCpIter < eventsOrCallPoints.size(); eCpIter++) {
            QDomElement contextRef = eventsOrCallPoints.at(eCpIter).firstChildElement("OPERATION-IREF");
            if (port.tagName() == "P-PORT-PROTOTYPE") {
                contextRef = contextRef.firstChildElement("CONTEXT-P-PORT-REF");
            } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                contextRef = contextRef.firstChildElement("CONTEXT-R-PORT-REF");
            }
            if (contextRef.text().contains(port.firstChildElement("SHORT-NAME").text())) {
                QString tempText = contextRef.text();
                QDomElement tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, ecuExtract);
                if (tempContext.isNull()) {
                    //qDebug() << "\t\t\tNot found in ecuExtract.\n";
                    tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, autosarModelModuleRoot.ownerDocument().documentElement());
                }
                if (!tempContext.isNull() && tempContext == port) {
                    if (port.tagName() == "P-PORT-PROTOTYPE") {
                        tempText = contextRef.parentNode().firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                    } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                        tempText = contextRef.parentNode().firstChildElement("TARGET-REQUIRED-OPERATION-REF").text();
                    }
                    QString tempProto;
                    tempProto = "Std_ReturnType Rte_Call_" + swc.firstChildElement("SHORT-NAME").text() + "_" +
                                  componentPrototype.firstChildElement("SHORT-NAME").text() + "_" +
                                  port.firstChildElement("SHORT-NAME").text() + "_" +
                                  tempText.split("/").last() + "(";
                    QStringList dataTypes, variables;
                    bool rv = CreateCSArguments(dataTypes, variables, tempText, autosarModelModuleRoot, ecuExtract);
                    if (!rv) {
                        return false;
                    }
                    if (dataTypes.size() > 0) {
                        for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                            if (argIter > 0) {
                                tempProto += ", ";
                            }
                            tempProto += dataTypes.at(argIter) + variables.at(argIter);
                        }
                    } else {
                        tempProto += "void";
                    }
                    tempProto += ")";
                    protoList.push_back(tempProto);
                }
            }
        }
    } else if (Interface.tagName() == "SENDER-RECEIVER-INTERFACE") {
        QDomNodeList variableAccesses = swc.firstChildElement("INTERNAL-BEHAVIORS").firstChildElement("SWC-INTERNAL-BEHAVIOR").firstChildElement("RUNNABLES").elementsByTagName("VARIABLE-ACCESS");
        QDomElement tempVar;
        for (int verIter = 0; verIter < variableAccesses.size(); verIter++) {
            //qDebug() << "\t\tVariable access: " << variableAccesses.at(verIter).firstChildElement("SHORT-NAME").text();
            QString portPrototypeRef = variableAccesses.at(verIter).toElement().firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF").firstChildElement("PORT-PROTOTYPE-REF").text();
            //qDebug() << "\t\tPrototype ref: " << portPrototypeRef;
            if (portPrototypeRef.contains(port.firstChildElement("SHORT-NAME").text())) {
                tempVar = AutosarToolFactory::GetDomElementFromReference(&portPrototypeRef, ecuExtract);
                if (tempVar.isNull()) {
                    //qDebug() << "\t\t\tNot found in ecuExtract.\n";
                    tempVar = AutosarToolFactory::GetDomElementFromReference(&portPrototypeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                }
                if (!tempVar.isNull() && tempVar == port) {
                    break;
                } else {
                    tempVar.clear();
                }
            }
        }
        if (!tempVar.isNull()) {
            QDomElement SRDataElement = Interface.firstChildElement("DATA-ELEMENTS").firstChildElement();
            while (!SRDataElement.isNull()) {
                QString tempProto;
                tempProto = "Std_ReturnType Rte_";
                if (isPPort)
                    tempProto += "Write";
                else
                    tempProto += "Read";
                tempProto += "_" + swc.firstChildElement("SHORT-NAME").text() + "_" +
                              componentPrototype.firstChildElement("SHORT-NAME").text() + "_" +
                              port.firstChildElement("SHORT-NAME").text() + "_" +
                              SRDataElement.firstChildElement("SHORT-NAME").text() + "(";
                QString TypeRef = SRDataElement.firstChildElement("TYPE-TREF").text();
                //std::cout << "\tInfo: Type Ref " << TypeRef.toStdString() << ".\n";
                QDomElement Type = AutosarToolFactory::GetDomElementFromReference(&TypeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                if (Type.isNull()) {
                    Type = AutosarToolFactory::GetDomElementFromReference(&TypeRef, ecuExtract);
                    if (Type.isNull()) {
                        std::cout << "Error: SR Interface Type " << TypeRef.toStdString() << " not found.\n";
                        return false;
                    }
                }
                CreateSRArguments(Type, tempProto, !isPPort);
                tempProto += ")";
                protoList.push_back(tempProto);
                SRDataElement = SRDataElement.nextSiblingElement();
            }
        }
    } else if (Interface.tagName() == "MODE-SWITCH-INTERFACE") {
        if (isPPort) {
            QDomNodeList modeSwitchPoints = swc.firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("MODE-SWITCH-POINT");
            QDomElement tempContext, contextPortRef;
            for (int pointIter = 0; pointIter < modeSwitchPoints.size(); pointIter++) {
                //qDebug() << "\t\tMode switch point: " << modeSwitchPoints.at(pointIter).firstChildElement("SHORT-NAME").text();
                contextPortRef = modeSwitchPoints.at(pointIter).firstChildElement("MODE-GROUP-IREF").firstChildElement("CONTEXT-P-PORT-REF");
                //qDebug() << "\t\tContext port ref: " << contextPortRef.text();
                if (contextPortRef.text().contains(port.firstChildElement("SHORT-NAME").text())) {
                    QString tempText = contextPortRef.text();
                    tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, ecuExtract);
                    if (tempContext.isNull()) {
                        //qDebug() << "\t\t\tNot found in ecuExtract.\n";
                        tempContext = AutosarToolFactory::GetDomElementFromReference(&tempText, autosarModelModuleRoot.ownerDocument().documentElement());
                    }
                    if (!tempContext.isNull() && tempContext == port) {
                        break;
                    } else {
                        tempContext.clear();
                    }
                } else {
                    contextPortRef.clear();
                }
            }
            if (!tempContext.isNull()) {
                QDomElement ModeGroup = Interface.firstChildElement("MODE-GROUP");
                protoList.push_back("Std_ReturnType Rte_Switch_" + swc.firstChildElement("SHORT-NAME").text() + "_" +
                              componentPrototype.firstChildElement("SHORT-NAME").text() + "_" +
                              port.firstChildElement("SHORT-NAME").text() + "_" +
                              ModeGroup.firstChildElement("SHORT-NAME").text() + "(/*IN*/uint8 mode)");
            }
        }
    } else {
        std::cout << "Error: Unsupported Interface Type = " << Interface.tagName().toStdString() << ".\n";
        return false;
    }
    return true;
}

bool RteGenerator::CreateRteMainC(QString location, QList<QDomElement> &ModeMachineSwcs, QList< QList <QDomElement> > &ModeMachineComps,
                                  QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract)
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Main.c";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Main.c file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n#include <Rte_Internal.h>\n#include <Rte_Fifo.h>\n#include <Rte_Calprms.h>\n#include <Rte_Main.h>\n\n";
    for (int swcIter = 0; swcIter < ModeMachineSwcs.size(); swcIter++)
        fileStream << "extern " << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachinesType " << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines;\n";
    fileStream << "\nextern boolean RteInitialized;\n\n// === Lifecycle API ===\n\nStd_ReturnType Rte_Start(void) {\n\t// Initialize calibration parameters\n";
    fileStream << "\tRte_Init_Calprms();\n\n\t// Initialize buffers\n\tRte_Internal_Init_Buffers();\n\n\t// Initialize port status\n\t// Initialize update flags\n";
    fileStream << "\t// Initialize mode machines\n";
    for (int swcIter = 0; swcIter < ModeMachineSwcs.size(); swcIter++) {
        QDomElement port = ModeMachineSwcs.at(swcIter).firstChildElement("PORTS").firstChildElement();
        while (!port.isNull()) {
            if (port.tagName() == "P-PORT-PROTOTYPE") {
                QDomElement interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF");
                if (interfaceRef.attribute("DEST") == "MODE-SWITCH-INTERFACE") {
                    for (int compIter = 0; compIter < ModeMachineComps.at(swcIter).size(); compIter++) {
                        QString portRef = interfaceRef.text();
                        QDomElement referedPort = AutosarToolFactory::GetDomElementFromReference(&portRef, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (referedPort.isNull()) {
                            referedPort = AutosarToolFactory::GetDomElementFromReference(&portRef, ecuExtract);
                            if (referedPort.isNull()) {
                                std::cout << "Error: Port " << portRef.toStdString() << " not found.\n";
                                return false;
                            }
                        }
                        QDomElement modeGroup = referedPort.firstChildElement("MODE-GROUP");
                        QString typeRef = modeGroup.firstChildElement("TYPE-TREF").text();
                        QDomElement mode = AutosarToolFactory::GetDomElementFromReference(&typeRef, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (mode.isNull()) {
                            mode = AutosarToolFactory::GetDomElementFromReference(&typeRef, ecuExtract);
                            if (mode.isNull()) {
                                std::cout << "Error: Mode " << typeRef.toStdString() << " not found.\n";
                                return false;
                            }
                        }
                        QString initModeRef = mode.firstChildElement("INITIAL-MODE-REF").text().split("/").last();
                        unsigned initModeNumber = 0, modeCount = 0;
                        QDomElement modeDecl = mode.firstChildElement("MODE-DECLARATIONS").firstChildElement();
                        while (!modeDecl.isNull()) {
                            if (modeDecl.firstChildElement("SHORT-NAME").text() == initModeRef) {
                                initModeNumber = modeCount;
                            }
                            modeDecl = modeDecl.nextSiblingElement();
                            modeCount++;
                        }
                        //qDebug() << "init mode index = " << initModeNumber << ", count = " << modeCount;
                        // TODO replace numbers with identifiers defined in Rte_Internal.h
                        fileStream << "\t" << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines." <<
                                      ModeMachineComps.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text() << "." <<
                                      port.firstChildElement("SHORT-NAME").text() << "_" <<
                                      modeGroup.firstChildElement("SHORT-NAME").text() << ".currentMode = " <<
                                      initModeNumber << ";\n";
                        fileStream << "\t" << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines." <<
                                      ModeMachineComps.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text() << "." <<
                                      port.firstChildElement("SHORT-NAME").text() << "_" <<
                                      modeGroup.firstChildElement("SHORT-NAME").text() << ".nextMode = " <<
                                      modeCount << ";\n";
                        fileStream << "\t" << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines." <<
                                      ModeMachineComps.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text() << "." <<
                                      port.firstChildElement("SHORT-NAME").text() << "_" <<
                                      modeGroup.firstChildElement("SHORT-NAME").text() << ".previousMode = " <<
                                      modeCount << ";\n";
                        fileStream << "\t" << ModeMachineSwcs.at(swcIter).firstChildElement("SHORT-NAME").text() << "_ModeMachines." <<
                                      ModeMachineComps.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text() << "." <<
                                      port.firstChildElement("SHORT-NAME").text() << "_" <<
                                      modeGroup.firstChildElement("SHORT-NAME").text() << ".transitionCompleted = " <<
                                      "FALSE;\n";
                    }
                }
            }
            port = port.nextSiblingElement();
        }
    }
    fileStream << "\n\t// Initialize client-server sequence counters.\n\t// @req SWS_Rte_02655\n\n\tRteInitialized = TRUE;\n\treturn RTE_E_OK;\n}\n\n";
    fileStream << "Std_ReturnType Rte_Stop(void) {\n\tRteInitialized = FALSE;\n\treturn RTE_E_OK;\n}\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteMainH(QString location)   // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Main.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Main.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n/**\n * Lifecycle Header File\n *\n * @req SWS_Rte_01158\n */\n\n#ifndef RTE_MAIN_H_\n#define RTE_MAIN_H_\n\n";
    fileStream << "/** @req SWS_Rte_02569 @req SWS_Rte_01309 @req SWS_Rte_02585 @req SWS_Rte_01261 @req SWS_Rte_01262 */\n";
    fileStream << "Std_ReturnType Rte_Start(void);\n\n/** @req SWS_Rte_02570 @req SWS_Rte_01310 @req SWS_Rte_02584 @req SWS_Rte_01259 @req SWS_Rte_01260 */\n";
    fileStream << "Std_ReturnType Rte_Stop(void);\n\n/*\n 5.8.6 Rte_Init\n Purpose: SchedulesRunnableEntitys for initialization purpose.\n";
    fileStream << " Signature: [SWS_Rte_06749] d\n void Rte_Init_<InitContainer>(void)\n";
    fileStream << " Where<InitContainer>is the short name of the RteInitial-izationRunnableBatchcontainer.c(SRS_Rte_00240)\n";
    fileStream << " Existence: [SWS_Rte_06750] d An Rte_Init API shall be created\n for each RteInitializationRunnableBatch container.\n";
    fileStream << " c(SRS_Rte_00240)\n\n 5.8.7 Rte_StartTiming\n Purpose: Starts the triggering of recurrent events.\n Signature: [SWS_Rte_06754] d\n";
    fileStream << " void Rte_StartTiming(void)\n c(SRS_Rte_00240)\n Existence: [SWS_Rte_06755] dAnRte_StartTimingAPI shall be created if\n";
    fileStream << " anyRte_InitAPI is created.c(SRS_Rte_00240)\n */\n\n#endif /* RTE_MAIN_H_ */\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteMakeFile(QString location, QList<QDomElement> &swcs)   // Finished.
{
    bool returnValue = true;
    QString makeFileName = location + "/Rte.mk";
    QFile makeFile(makeFileName);
    if (!makeFile.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte.mk file.\n";
        returnValue = false;
    }
    QTextStream makeFileStream( &makeFile );
    makeFileStream << "\nMOD_USE += RTE\n\nobj-y += Rte.o\nobj-y += Rte_Main.o\nobj-y += Rte_Calprms.o\nobj-y += Rte_Internal.o\n";
    makeFileStream << "obj-y += Rte_Cbk.o\nobj-y += Rte_Fifo.o\n";
    for (int compIter = 0; compIter < swcs.size(); compIter++)
    {
        QDomElement shortName = swcs[compIter].firstChildElement("SHORT-NAME");
        if (shortName.isNull())
        {
            std::cout << "Error: Unable to get SHORT-NAME element of component\'s name.";
            returnValue = false;
        }
        QString componentName = shortName.text();
        if (componentName == "")
        {
            std::cout << "Error: Unable to get component name.";
            returnValue = false;
        }
        makeFileStream << "obj-y += Rte_" << componentName << ".o\n";
    }
    makeFileStream << "\n";
    makeFile.close();
    return returnValue;
}

bool RteGenerator::CreateRteTagsTxt(QString location)   // Incomplete but not critical.
{
    bool returnValue = true;
    QString fileName = location + "/RteTags.txt";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create RteTags.txt file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n//Not critical for working. Not generating anything.\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteTypeH(QString location, QList <QDomElement> &structureDataTypes, QList <QDomElement> &otherDataTypes, QDomElement &ecuExtract)       // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Type.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Type.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// RTE Types Header File\n// @req SWS_Rte_01161\n// @req SWS_Rte_01160\n\n#ifndef RTE_TYPE_H_\n#define RTE_TYPE_H_\n";
    fileStream << "// --- Includes ---\n// @req SWS_Rte_01163\n\n#include <Rte.h>\n#include <Rte_Type_Workarounds.h>\n\n";
    fileStream << "// @req SWS_Rte_08732\ntypedef struct {\n\tuint16 clientId;\n\tuint16 sequenceCounter;\n} Rte_Cs_TransactionHandleType;\n\n";
    QDomNodeList pims = ecuExtract.elementsByTagName("PER-INSTANCE-MEMORYS");
    for (int dtIter = 0; dtIter < pims.size(); dtIter++) {
        QDomElement pim = pims.at(dtIter).firstChildElement();
        while (!pim.isNull()) {
            QDomElement typeDef = pim.firstChildElement("TYPE-DEFINITION");
            if (typeDef.isNull()) {
                std::cout << "Error: TYPE-DEFINITION not found in " << pim.firstChildElement("SHORT-NAME").text().toStdString() << "\n";
            } else {
                fileStream << "typedef " << typeDef.text() << " Rte_PimType_" <<
                              pims.at(dtIter).parentNode().parentNode().parentNode().firstChildElement("SHORT-NAME").text() << "_" <<
                              pim.firstChildElement("TYPE").text() << ";\n";
            }
            pim = pim.nextSiblingElement();
        }
    }
    for (int dtIter = 0; dtIter < otherDataTypes.size(); dtIter++) {
        QDomElement category = otherDataTypes.at(dtIter).firstChildElement("CATEGORY");
        if (category.text() == "TYPE_REFERENCE") {
            category = otherDataTypes.at(dtIter).firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement("IMPLEMENTATION-DATA-TYPE-REF");
            QString type = category.text().split("/").last();
            fileStream << "typedef " << type << " " << otherDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << ";\n";
        } else if (category.text() == "DATA_REFERENCE") {
            category = otherDataTypes.at(dtIter).firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement().firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement();
            QDomElement implDataType = category.firstChildElement("IMPLEMENTATION-DATA-TYPE-REF");
            if (implDataType.isNull()) {
                implDataType = category.firstChildElement("BASE-TYPE-REF");
            }
            QDomElement implPolicy = category.firstChildElement("SW-IMPL-POLICY");
            fileStream << "typedef ";
            if (!implPolicy.isNull()) {
                if (implPolicy.text() == "CONST") {
                    fileStream << "const ";
                } else {
                    std::cout << "Error: Unknown SW-IMPL-POLICY " << implPolicy.text().toStdString() << "\n";
                }
            }
            QString type = implDataType.text().split("/").last();
            fileStream << type << " * " << otherDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << ";\n";
        } else if (category.text() == "ARRAY") {
            category = otherDataTypes.at(dtIter).firstChildElement("SUB-ELEMENTS").firstChildElement();
            QString size = category.firstChildElement("ARRAY-SIZE").text();
            QString type = category.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement().text().split("/").last();
            fileStream << "typedef " << type << " " << otherDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << "[" << size << "];\n";
        } else {
            std::cout << "Error: Unsupported data type " << category.text().toStdString() << ".\n";
        }
    }
    for (int dtIter = 0; dtIter < structureDataTypes.size(); dtIter++) {
        fileStream << "\ntypedef struct {\n";
        QDomElement dataType = structureDataTypes.at(dtIter).firstChildElement("SUB-ELEMENTS").firstChildElement();
        while (!dataType.isNull()) {
            // Make sure that the CATEGORY is TYPE_REFERENCE
            QDomElement category = dataType.firstChildElement("CATEGORY");
            if (category.text() != "TYPE_REFERENCE") {
                std::cout << "Error: Structure data element " << dataType.firstChildElement("SHORT-NAME").text().toStdString() << " is not TYPE_REFERENCE.\n";
            }
            QDomElement implDataType = dataType.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement().firstChildElement().firstChildElement();
            fileStream << "\t" << implDataType.text().split("/").last() << " " << dataType.firstChildElement("SHORT-NAME").text() << ";\n";
            dataType = dataType.nextSiblingElement();
        }
        fileStream << "} " << structureDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << ";\n";
    }
    fileStream << "\n#endif // RTE_TYPE_H_\n";
    file.close();
    return returnValue;
}

bool RteGenerator::CreateRteTypeWorkaroundsH(QString location, QList <QDomElement> &structureDataTypes, QList <QDomElement> &otherDataTypes)       // Finished.
{
    bool returnValue = true;
    QString fileName = location + "/Rte_Type_Workarounds.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create Rte_Type_Workarounds.h file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n// RTE Types Workarounds Header File\n// This file is used to fix some problems when using the RTE together\n";
    fileStream << "// with files produced elsewhere. Each workaround has its own documentation.\n\n#ifndef RTE_TYPE_WORKAROUNDS_H_\n";
    fileStream << "#define RTE_TYPE_WORKAROUNDS_H_\n\n// SWCs generated by Simulink have internal typedefs for types that are only used within the SWC.\n";
    fileStream << "// If another SWC (also generated by Simulink) uses the same type, but on a port, this type will be\n";
    fileStream << "// part of the AUTOSAR model and hence be generated in Rte_Type.h. To protect against double declaration\n";
    fileStream << "// each typedef in the Simulink files is guarded like this:\n\n// #ifndef _DEFINED_TYPEDEF_FOR_T_U_mV_1_1_s16_\n";
    fileStream << "// #define _DEFINED_TYPEDEF_FOR_T_U_mV_1_1_s16_\n// typedef int16_T T_U_mV_1_1_s16;\n// #endif\n\n";
    fileStream << "// By defining these macros we can make it easier to integrate Simulink SWCs with the ArcCore RTE.\n// Note that this is not AUTOSAR standard.\n\n";
    for (int dtIter = 0; dtIter < otherDataTypes.size(); dtIter++) {
        fileStream << "#define _DEFINED_TYPEDEF_FOR_" << otherDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << "_\n";
    }
    for (int dtIter = 0; dtIter < structureDataTypes.size(); dtIter++) {
        fileStream << "#define _DEFINED_TYPEDEF_FOR_" << structureDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << "_\n";
    }
    fileStream << "\n#endif // RTE_TYPE_WORKAROUNDS_H_\n";
    file.close();
    return returnValue;
}


bool RteGenerator::CreateMemMapH(QString compName, QString location)
{
    bool returnValue = true;
    QString fileName = location + "/" + compName + "_MemMap.h";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        std::cout << "Error: Unable to create " << fileName.toStdString() << "file.\n";
        return false;
    }
    QTextStream fileStream( &file );
    fileStream << "\n/**\n * Memory map file for " << compName << "\n * Memory sections according to ASR 4.1.3 definitions of the Memory Mapping\n";
    fileStream << " * specifications. Note that deprecated memory sections are not generated.\n */\n\n#define " << compName << "_MEMMAP_ERROR\n";

    MemMapSectionEntry(fileStream, compName, "VAR");
    MemMapSectionEntry(fileStream, compName, "VAR_FAST");
    MemMapSectionEntry(fileStream, compName, "VAR_SLOW");
    MemMapSectionEntry(fileStream, compName, "INTERNAL_VAR");
    MemMapSectionEntry(fileStream, compName, "VAR_SAVED_ZONE");
    MemMapSectionPolicyEntry(fileStream, compName, "VAR_SAVED_ZONE");
    MemMapSectionAlignmentPolicyEntry(fileStream, compName, "CONST_SAVED_RECOVERY_ZONE");
    MemMapSectionAlignmentPolicyEntry(fileStream, compName, "CONST_SAVED_RECOVERY_ZONE", "_UNSPECIFIED");
    MemMapSectionPolicyEntry(fileStream, compName, "VAR_SAVED_RECOVERY_ZONE");
    MemMapSectionPolicyEntry(fileStream, compName, "CONST");
    MemMapSectionPolicyEntry(fileStream, compName, "CALIB");
    MemMapSectionPolicyEntry(fileStream, compName, "CONFIG_DATA");
    MemMapSectionAlignmentPolicyEntry(fileStream, compName, "CODE");
    MemMapSectionAlignmentPolicyEntry(fileStream, compName, "CODE_FAST");
    MemMapSectionAlignmentPolicyEntry(fileStream, compName, "CODE_SLOW");

    fileStream << "\n#ifdef " << compName << "_MEMMAP_ERROR\n#error \"" << compName << "_MemMap.h error, section not mapped\"\n#endif\n";
    file.close();
    return returnValue;
}

void RteGenerator::MemMapSectionEntry(QTextStream &file, QString &compName , QString section)
{
    MemMapSectionPolicyEntry(file, compName, section, "_INIT");
    MemMapSectionPolicyEntry(file, compName, section, "_NO_INIT");
    MemMapSectionPolicyEntry(file, compName, section, "_CLEARED");
    MemMapSectionPolicyEntry(file, compName, section, "_POWER_ON_CLEARED");
    MemMapSectionPolicyEntry(file, compName, section, "_POWER_ON_INIT");
}

void RteGenerator::MemMapSectionPolicyEntry(QTextStream &file, QString &compName, QString section, QString policy)
{
    MemMapSectionAlignmentPolicyEntry(file, compName, section, "_UNSPECIFIED", policy);
    MemMapSectionAlignmentPolicyEntry(file, compName, section, "_BOOLEAN", policy);
    MemMapSectionAlignmentPolicyEntry(file, compName, section, "_8", policy);
    MemMapSectionAlignmentPolicyEntry(file, compName, section, "_16", policy);
    MemMapSectionAlignmentPolicyEntry(file, compName, section, "_32", policy);
}

void RteGenerator::MemMapSectionAlignmentPolicyEntry(QTextStream &file, QString &compName, const QString section, QString alignment, QString policy)
{
    file << "\n#ifdef " << compName << "_START_SEC_" << section << policy << alignment << "\n#include \"MemMap.h\"\n";
    file << "#undef " << compName << "_MEMMAP_ERROR\n#undef " << compName << "_START_SEC_" << section << policy << alignment << "\n#endif\n";
    file << "\n#ifdef " << compName << "_STOP_SEC_" << section << policy << alignment << "\n#include \"MemMap.h\"\n";
    file << "#undef " << compName << "_MEMMAP_ERROR\n#undef " << compName << "_STOP_SEC_" << section << policy << alignment << "\n#endif\n";
}

bool RteGenerator::CreateRteSwcTypeH(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs)
{
    bool returnValue = true;
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QString swcName = swcs.at(swcIter).firstChildElement("SHORT-NAME").text();
        QString fileName = location + "/Rte_" + swcName + "_Type.h";
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create " << fileName.toStdString() << " file.\n";
            returnValue = false;
            break;
        }
        QTextStream fileStream( &file );
        fileStream << "\n/**\n * Application Types Header File\n *\n * @req SWS_Rte_07122\n */\n\n";
        fileStream << "/** === HEADER ====================================================================================\n */\n\n";
        fileStream << "#ifdef __cplusplus\nextern \"C\" {\n#endif /* __cplusplus */\n\n";
        fileStream << "#ifndef RTE_" << swcName.toUpper() << "_TYPE_H_\n#define RTE_" << swcName.toUpper() << "_TYPE_H_\n\n";
        fileStream << "/** --- Includes ----------------------------------------------------------------------------------\n";
        fileStream << " * @req SWS_Rte_07127\n */\n#include <Rte_Type.h>\n\n";
        fileStream << "/** === BODY ====================================================================================\n */\n\n";
        fileStream << "/** --- ENUMERATION DATA TYPES ------------------------------------------------------------------ */\n\n";
        //qDebug() << "SWC: " << swcName;
        // Get all variable access types
        QList <QDomElement> implDataTypes;
        QDomNodeList nodeList = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("VARIABLE-ACCESS");
        for (int vaIter = 0; vaIter < nodeList.size(); vaIter++) {
            QString tempStr = nodeList.at(vaIter).firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF").firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
            QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
            if (dataPrototype.isNull()) {
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                if (dataPrototype.isNull()) {
                    std::cout << "Error: Unable to find target data prototype \"" << tempStr.toStdString() << "\".\n";
                    return false;
                }
            }
            tempStr = dataPrototype.firstChildElement("TYPE-TREF").text();
            dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
            if (dataPrototype.isNull()) {
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                if (dataPrototype.isNull()) {
                    std::cout << "Error: Unable to find target data prototype \"" << tempStr.toStdString() << "\".\n";
                    return false;
                }
            }
            implDataTypes.push_back(dataPrototype);
        }
        // Get all port argument types
        nodeList = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("PORT-DEFINED-ARGUMENT-VALUE");
        for (int vaIter = 0; vaIter < nodeList.size(); vaIter++) {
            QString tempStr = nodeList.at(vaIter).firstChildElement("VALUE-TYPE-TREF").text();
            QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
            if (dataPrototype.isNull()) {
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                if (dataPrototype.isNull()) {
                    std::cout << "Error: Unable to find target data type \"" << tempStr.toStdString() << "\".\n";
                    return false;
                }
            }
            if (!implDataTypes.contains(dataPrototype))
                implDataTypes.push_back(dataPrototype);
        }
        // Get all operation invoked data types
        nodeList = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("OPERATION-INVOKED-EVENT");
        for (int vaIter = 0; vaIter < nodeList.size(); vaIter++) {
            QString tempStr = nodeList.at(vaIter).firstChildElement("OPERATION-IREF").firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
            QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
            if (dataPrototype.isNull()) {
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                if (dataPrototype.isNull()) {
                    std::cout << "Error: Unable to find client server operation \"" << tempStr.toStdString() << "\".\n";
                    return false;
                }
            }
            QDomElement argumentDataPrototype = dataPrototype.firstChildElement("ARGUMENTS").firstChildElement();
            while (!argumentDataPrototype.isNull()) {
                tempStr = argumentDataPrototype.firstChildElement("TYPE-TREF").text();
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                if (dataPrototype.isNull()) {
                    dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (dataPrototype.isNull()) {
                        std::cout << "Error: Unable to find target data type \"" << tempStr.toStdString() << "\".\n";
                        return false;
                    }
                }
                if (!implDataTypes.contains(dataPrototype)) {
                    implDataTypes.push_back(dataPrototype);
                }
                argumentDataPrototype = argumentDataPrototype.nextSiblingElement();
            }
        }
        // Get all server call point data types
        nodeList = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("SYNCHRONOUS-SERVER-CALL-POINT");
        for (int vaIter = 0; vaIter < nodeList.size(); vaIter++) {
            QString tempStr = nodeList.at(vaIter).firstChildElement("OPERATION-IREF").firstChildElement("TARGET-REQUIRED-OPERATION-REF").text();
            QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
            if (dataPrototype.isNull()) {
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                if (dataPrototype.isNull()) {
                    std::cout << "Error: Unable to find client server operation \"" << tempStr.toStdString() << "\".\n";
                    return false;
                }
            }
            QDomElement argumentDataPrototype = dataPrototype.firstChildElement("ARGUMENTS").firstChildElement();
            while (!argumentDataPrototype.isNull()) {
                tempStr = argumentDataPrototype.firstChildElement("TYPE-TREF").text();
                dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                if (dataPrototype.isNull()) {
                    dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (dataPrototype.isNull()) {
                        std::cout << "Error: Unable to find target data type \"" << tempStr.toStdString() << "\".\n";
                        return false;
                    }
                }
                if (!implDataTypes.contains(dataPrototype)) {
                    implDataTypes.push_back(dataPrototype);
                }
                argumentDataPrototype = argumentDataPrototype.nextSiblingElement();
            }
        }
        for (int dtIter = 0; dtIter < implDataTypes.size(); dtIter++) {
            //qDebug() << "T: " << implDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text();
            QString category = implDataTypes.at(dtIter).firstChildElement("CATEGORY").text();
            if (category == "TYPE_REFERENCE" || category == "VALUE" || category == "BOOLEAN") {
                //qDebug() << "\tIs Primitive or Derived.";
                QDomNodeList methodRefs = implDataTypes.at(dtIter).elementsByTagName("COMPU-METHOD-REF");
                if (methodRefs.size() == 0) {
                    continue;
                }
                QDomElement compuMethodRefElem = methodRefs.at(0).toElement();              // There will only be single compuMethod inside an implementationDataType
                if (!compuMethodRefElem.isNull()) {
                    //qDebug() << "\tCompuMethodRef exists.";
                    QString compuMethodRef = compuMethodRefElem.text();
                    QDomElement compuMethod = AutosarToolFactory::GetDomElementFromReference(&compuMethodRef, ecuExtract);
                    if (compuMethod.isNull()) {
                        compuMethod = AutosarToolFactory::GetDomElementFromReference(&compuMethodRef, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (compuMethod.isNull()) {
                            std::cout << "Error: Unable to find computation method \"" << compuMethodRef.toStdString() << "\" in data type \"" <<
                                         implDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text().toStdString() << "\".\n";
                            returnValue = false;
                            break;
                        }
                    }
                    //qDebug() << "\tCompuMethod found.";
                    if (compuMethod.firstChildElement("CATEGORY").text() == "TEXTTABLE") {
                        //qDebug() << "\tIsTexttable";
                        // Get all scales
                        QStringList compuScaleNames;
                        QList <int> compuScalesLower, compuScalesUpper;
                        QList <bool> compuScalesIsConst, compuScalesIsUnsigned;
                        QDomElement compuScale = compuMethod.firstChildElement("COMPU-INTERNAL-TO-PHYS").firstChildElement("COMPU-SCALES").firstChildElement();
                        bool ok;
                        while (!compuScale.isNull()) {
                            compuScaleNames.push_back(compuScale.firstChildElement("COMPU-CONST").firstChildElement("VT").text());
                            ok = false;
                            compuScalesLower.push_back(compuScale.firstChildElement("LOWER-LIMIT").text().toInt(&ok));
                            if (!ok) {
                                std::cout << "Error: Unable to convert " << compuScale.firstChildElement("LOWER-LIMIT").text().toStdString() << " to integer.\n";
                                returnValue = false;
                                break;
                            }
                            ok = false;
                            compuScalesUpper.push_back(compuScale.firstChildElement("UPPER-LIMIT").text().toInt(&ok));
                            if (!ok) {
                                std::cout << "Error: Unable to convert " << compuScale.firstChildElement("UPPER-LIMIT").text().toStdString() << " to integer.\n";
                                returnValue = false;
                                break;
                            }
                            if (compuScalesLower.at(compuScalesLower.size()-1) == compuScalesUpper.at(compuScalesLower.size()-1)) {
                                compuScalesIsConst.push_back(true);
                            } else {
                                compuScalesIsConst.push_back(false);
                            }
                            if (compuScalesLower.at(compuScalesLower.size()-1) >= 0 && compuScalesUpper.at(compuScalesLower.size()-1) >= 0) {
                                compuScalesIsUnsigned.push_back(true);
                            } else {
                                compuScalesIsUnsigned.push_back(false);
                            }
                            compuScale = compuScale.nextSiblingElement();
                        }
                        //qDebug() << "\tCompuScales:\n\t\t" << compuScaleNames << "\n\t\t" << compuScalesLower << "\n\t\t"  << compuScalesUpper << "\n\t\t" << compuScalesIsConst << "\n\t\t" << compuScalesIsUnsigned;
                        if (!compuScalesIsConst.contains(false)) {
                            // The type is enum. Add it to the header file.
                            fileStream << "/** Enum literals for " << implDataTypes.at(dtIter).firstChildElement("SHORT-NAME").text() << " */\n";
                            for (int csIter = 0; csIter < compuScaleNames.size(); csIter++) {
                                fileStream << "#ifndef " << compuScaleNames.at(csIter) << "\n";
                                fileStream << "#define " << compuScaleNames.at(csIter) << " " << compuScalesLower.at(csIter);
                                if (compuScalesIsUnsigned.at(csIter)) {
                                    fileStream << "U";
                                }
                                fileStream << "\n#endif /* " << compuScaleNames.at(csIter) << " */\n\n";
                            }
                        }
                    }
                }
            } else if (category == "STRUCTURE") {
                QDomElement implDataTypeElement = implDataTypes.at(dtIter).firstChildElement("SUB-ELEMENTS").firstChildElement();
                QString tempStr;
                while (!implDataTypeElement.isNull()) {
                    tempStr = implDataTypeElement.firstChildElement("SW-DATA-DEF-PROPS").firstChildElement("SW-DATA-DEF-PROPS-VARIANTS").firstChildElement("SW-DATA-DEF-PROPS-CONDITIONAL")
                            .firstChildElement("IMPLEMENTATION-DATA-TYPE-REF").text();
                    QDomElement dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                    if (dataPrototype.isNull()) {
                        dataPrototype = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (dataPrototype.isNull()) {
                            std::cout << "Error: Unable to find target data type \"" << tempStr.toStdString() << "\".\n";
                            return false;
                        }
                    }
                    if (!implDataTypes.contains(dataPrototype))
                        implDataTypes.push_back(dataPrototype);
                    implDataTypeElement = implDataTypeElement.nextSiblingElement();
                }
            }
        }
        if (!returnValue) {
            break;
        }
        fileStream << "/** --- MODE TYPES ------------------------------------------------------------------------------ */\n\n";

        QList <QDomElement> modeTypes;
        QStringList modeTypeDataTypes;
        QDomNodeList dataTypeMappingRefs = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName("DATA-TYPE-MAPPING-REF");
        QStringList mainElement = (QStringList() << "MODE-SWITCH-POINT" << "SWC-MODE-SWITCH-EVENT");
        QStringList subElement = (QStringList() << "MODE-GROUP-IREF" << "MODE-IREFS");
        QStringList subSubElement = (QStringList() << "TARGET-MODE-GROUP-REF" << "CONTEXT-MODE-DECLARATION-GROUP-PROTOTYPE-REF");
        for (int loopIter = 0; loopIter < 2; loopIter++) {
            QDomNodeList modeSwitchPoints = swcs.at(swcIter).firstChildElement("INTERNAL-BEHAVIORS").elementsByTagName(mainElement.at(loopIter));
            for (int mspIter = 0; mspIter < modeSwitchPoints.size(); mspIter++) {
                //qDebug() << "MSP: " << modeSwitchPoints.at(mspIter).firstChildElement("SHORT-NAME").text();
                QDomElement tempElem = modeSwitchPoints.at(mspIter).firstChildElement(subElement.at(loopIter));
                if (loopIter == 1) {
                    tempElem = tempElem.firstChildElement();            // Take the first mode ref. Not sure if more are allowed.
                }
                tempElem = tempElem.firstChildElement(subSubElement.at(loopIter));
                QString tempStr = tempElem.text();
                QDomElement modeGroup = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                if (modeGroup.isNull()) {
                    modeGroup = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (modeGroup.isNull()) {
                        std::cout << "Error: Unable to find mode group \"" << tempStr.toStdString() << "\".\n";
                        return false;
                    }
                }
                tempStr = modeGroup.firstChildElement("TYPE-TREF").text();
                //qDebug() << "TS: " << tempStr;
                QDomElement modeDeclGroup = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                if (modeDeclGroup.isNull()) {
                    modeDeclGroup = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (modeDeclGroup.isNull()) {
                        std::cout << "Error: Unable to find mode declaration group \"" << tempStr.toStdString() << "\".\n";
                        return false;
                    }
                }
                modeTypes.push_back(modeDeclGroup);
                for (int dtmIter = 0; dtmIter < dataTypeMappingRefs.size(); dtmIter++) {            // TODO: Take this portion out of the while loop
                    tempStr = dataTypeMappingRefs.at(dtmIter).toElement().text();
                    QDomElement dataTypeMapping = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                    if (dataTypeMapping.isNull()) {
                        dataTypeMapping = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (dataTypeMapping.isNull()) {
                            std::cout << "Error: Unable to find data type mapping \"" << tempStr.toStdString() << "\".\n";
                            return false;
                        }
                    }
                    QDomElement map = dataTypeMapping.firstChildElement("MODE-REQUEST-TYPE-MAPS").firstChildElement();
                    while (!map.isNull()) {
                        tempStr = map.firstChildElement("MODE-GROUP-REF").text();
                        QDomElement mg = AutosarToolFactory::GetDomElementFromReference(&tempStr, ecuExtract);
                        if (mg.isNull()) {
                            mg = AutosarToolFactory::GetDomElementFromReference(&tempStr, autosarModelModuleRoot.ownerDocument().documentElement());
                            if (mg.isNull()) {
                                std::cout << "Error: Unable to find data type mapping \"" << tempStr.toStdString() << "\".\n";
                                return false;
                            }
                        }
                        if (mg == modeDeclGroup) {
                            modeTypeDataTypes.push_back(map.firstChildElement("IMPLEMENTATION-DATA-TYPE-REF").text().split("/").last());
                            break;
                        }
                        map = map.nextSiblingElement();
                    }
                }
            }
        }
        if (modeTypeDataTypes.size() != modeTypes.size()) {
            std::cout << "Error: Unable to find data mapping for all mode types.\n";
            return false;
        }
        for (int mtIter = 0; mtIter < modeTypes.size(); mtIter++) {
            QStringList modeNames;
            QDomElement modeDeclaration = modeTypes.at(mtIter).firstChildElement("MODE-DECLARATIONS").firstChildElement();
            while (!modeDeclaration.isNull()) {
                modeNames.push_back(modeDeclaration.firstChildElement("SHORT-NAME").text());
                modeDeclaration = modeDeclaration.nextSiblingElement();
            }
            QString modeTypeName = modeTypes.at(mtIter).firstChildElement("SHORT-NAME").text();
            fileStream << "#ifndef RTE_MODETYPE_" << modeTypeName << "\n#define RTE_MODETYPE_" << modeTypeName << "\n";
            fileStream << "typedef " << modeTypeDataTypes.at(mtIter) << " Rte_ModeType_" << modeTypeName << ";\n#endif\n";
            fileStream << "\n#ifndef RTE_TRANSITION_" << modeTypeName << "\n#define RTE_TRANSITION_" << modeTypeName << " " << modeNames.size() << "U\n#endif\n\n";
            for (int modeIter = 0; modeIter < modeNames.size(); modeIter++) {
                fileStream << "#ifndef RTE_MODE_" << modeTypeName << "_" << modeNames.at(modeIter) << "\n";
                fileStream << "#define RTE_MODE_" << modeTypeName << "_" << modeNames.at(modeIter) << " " << modeIter << "U\n#endif\n\n";
            }
        }
        fileStream << "/** === FOOTER ====================================================================================\n */\n\n";
        fileStream << "#endif /* RTE_" << swcName.toUpper() << "_TYPE_H_ */\n\n/** @req SWS_Rte_07126 */";
        fileStream << "\n#ifdef __cplusplus\n} /* extern \"C\" */\n#endif /* __cplusplus */\n";
        file.close();
    }
    return returnValue;
}

bool RteGenerator::CreateRteSwcH(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes) {
    bool returnValue = true;
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QDomElement swc = swcs.at(swcIter);
        QString swcName = swc.firstChildElement("SHORT-NAME").text();
        //qDebug() << "\nSWCName: " << swc.firstChildElement("SHORT-NAME").text();
        QString fileName = location + "/Rte_" + swcName + ".h";
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create " << fileName.toStdString() << " file.\n";
            returnValue = false;
            break;
        }
        QTextStream fileStream( &file );
        fileStream << "\n/**\n * Application Header File\n *\n * @req SWS_Rte_01003\n */\n\n";
        fileStream << "/** === HEADER ====================================================================================\n */\n\n";
        fileStream << "/** --- C++ guard ---------------------------------------------------------------------------------\n * @req SWS_Rte_03709\n */\n";
        fileStream << "#ifdef __cplusplus\nextern \"C\" {\n#endif /* __cplusplus */\n\n";
        fileStream << "/** --- Normal include guard ----------------------------------------------------------------------\n */\n";
        fileStream << "#ifndef RTE_" << swcName.toUpper() << "_H_\n#define RTE_" << swcName.toUpper() << "_H_\n\n";
        fileStream << "/** --- Duplicate application include guard -------------------------------------------------------\n * @req SWS_Rte_01006\n */\n";
        fileStream << "#ifdef RTE_APPLICATION_HEADER_FILE\n#error Multiple application header files included.\n#endif\n#define RTE_APPLICATION_HEADER_FILE\n\n";
        fileStream << "/** --- Single runnable API -----------------------------------------------------------------------\n * @req SWS_Rte_02751\n */\n";
        QDomNodeList runnables = swc.elementsByTagName("RUNNABLE-ENTITY");
        fileStream << "#if ";
        for (int reIter = 0; reIter < runnables.size(); reIter++) {
            fileStream << "defined(RTE_RUNNABLEAPI_" << runnables.at(reIter).firstChildElement("SHORT-NAME").text() << ")";
            if (reIter != runnables.size() - 1) {
                fileStream << " || \\";
            }
            fileStream << "\n";
        }
        fileStream << "#define RTE_RUNNABLEAPI\n#endif\n";
        fileStream << "\n/** --- Includes ----------------------------------------------------------------------------------\n * @req SWS_Rte_02751\n * @req SWS_Rte_07131\n */\n";
        fileStream << "#include <Rte_DataHandleType.h>\n#include <Rte_" << swcName << "_Type.h>\n\n";
        fileStream << "/** --- Application Errors ------------------------------------------------------------------------\n * @req SWS_Rte_02575\n * @req SWS_Rte_02576\n * @req SWS_Rte_07143\n */\n";
        QList <QStringList> errorRefs;
        QStringList interfaceNames;
        QDomNodeList events;
        for (int loopIter = 0; loopIter < 2; loopIter++) {                  // Once for operation invoked event, and once for server call points
            if (loopIter == 0)
                events = swc.elementsByTagName("OPERATION-INVOKED-EVENT");
            else
                events = swc.elementsByTagName("SYNCHRONOUS-SERVER-CALL-POINT");
            for (int oieIter = 0; oieIter < events.size(); oieIter++) {
                QString targetOperationRef;
                if (loopIter == 0)
                    targetOperationRef = events.at(oieIter).firstChildElement("OPERATION-IREF").firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                else
                    targetOperationRef = events.at(oieIter).firstChildElement("OPERATION-IREF").firstChildElement("TARGET-REQUIRED-OPERATION-REF").text();
                QDomElement targetOperation = AutosarToolFactory::GetDomElementFromReference(&targetOperationRef, autosarModelModuleRoot.ownerDocument().documentElement());
                if (targetOperation.isNull()) {
                    targetOperation = AutosarToolFactory::GetDomElementFromReference(&targetOperationRef, ecuExtract.ownerDocument().documentElement());
                    if (targetOperation.isNull()) {
                        std::cout << "Error: Invalid reference \'" << targetOperationRef.toStdString() << "\'.\n";
                        return false;
                    }
                }
                QString interfaceName = targetOperation.parentNode().parentNode().firstChildElement("SHORT-NAME").text();
                int interfaceIndex = -1;
                if (!interfaceNames.contains(interfaceName))
                    interfaceNames.push_back(interfaceName);
                else
                    interfaceIndex = interfaceNames.indexOf(interfaceName);
                QStringList errorRefForInt;
                QDomElement error = targetOperation.firstChildElement("POSSIBLE-ERROR-REFS").firstChildElement();
                while (!error.isNull()) {
                    if (!errorRefForInt.contains(error.text()))
                        errorRefForInt.push_back(error.text());
                    error = error.nextSiblingElement();
                }
                if (interfaceIndex == -1)
                    errorRefs.push_back(errorRefForInt);
                else {
                    for (int eIter = 0; eIter < errorRefForInt.size(); eIter++) {
                        if (!errorRefs[interfaceIndex].contains(errorRefForInt.at(eIter))) {
                            errorRefs[interfaceIndex].push_back(errorRefForInt.at(eIter));
                        }
                    }
                }
            }
        }
        for (int intIter = 0; intIter < interfaceNames.size(); intIter++) {
            for (int erIter = 0; erIter < errorRefs.at(intIter).size(); erIter++) {
                QDomElement error = AutosarToolFactory::GetDomElementFromReference(&errorRefs[intIter][erIter], autosarModelModuleRoot.ownerDocument().documentElement());
                if (error.isNull()) {
                    error = AutosarToolFactory::GetDomElementFromReference(&errorRefs[intIter][erIter], ecuExtract.ownerDocument().documentElement());
                    if (error.isNull()) {
                        std::cout << "Error: Invalid reference \'" << errorRefs[intIter][erIter].toStdString() << "\'.\n";
                        return false;
                    }
                }
                fileStream << "#define RTE_E_" << interfaceNames.at(intIter) << "_" << error.firstChildElement("SHORT-NAME").text() << " " << error.firstChildElement("ERROR-CODE").text() << "U\n";
            }
        }
        fileStream << "\n";
        fileStream << "/** --- Initial Values ----------------------------------------------------------------------------\n * @SWS_Rte_05078\n */\n\n";
        fileStream << "/** --- PIM DATA TYPES ------------------------------------------------------------------------------ */\n";
        QDomNodeList pimTypes = swc.elementsByTagName("PER-INSTANCE-MEMORY");
        for (int tIter = 0; tIter < pimTypes.size(); tIter++) {
            fileStream << "typedef Rte_PimType_" << swcName << "_" << pimTypes.at(tIter).firstChildElement("TYPE").text() << " " << pimTypes.at(tIter).firstChildElement("TYPE").text() << ";\n";
        }
        fileStream << "\n/** === BODY ======================================================================================\n */\n\n";
        fileStream << "/** @req SWS_Rte_03731\n *  @req SWS_Rte_07137\n *  @req SWS_Rte_07138\n *  !req SWS_Rte_06523\n *  @req SWS_Rte_03730\n *  @req SWS_Rte_07677\n";
        fileStream << " *  @req SWS_Rte_02620\n *  @req SWS_Rte_02621\n *  @req SWS_Rte_01055\n *  @req SWS_Rte_03726 */\n";
        QDomNodeList indirectApis = swc.elementsByTagName("INDIRECT-API");
        //qDebug() << "IndirectApis: " << indirectApis.size();
        QList <QString> pdsStructNames, pdsPortNames, pdsTypeDefNames;
        for (int apiIter = 0; apiIter < indirectApis.size(); apiIter++) {
            QString portRef = indirectApis.at(apiIter).nextSiblingElement("PORT-REF").text();
            QDomElement port = AutosarToolFactory::GetDomElementFromReference(&portRef, ecuExtract);
            if (port.isNull()) {
                std::cout << "Error: Unable to find port \"" << portRef.toStdString() << "\" in SWC \"" << swcName.toStdString() << "\".\n";
                return false;
            }
            pdsStructNames.push_back("Rte_PDS_" + swcName + "_" + port.firstChildElement("PROVIDED-INTERFACE-TREF").text().split("/").last() + "_P");
            pdsTypeDefNames.push_back("Rte_PortHandle_" + port.firstChildElement("PROVIDED-INTERFACE-TREF").text().split("/").last() + "_P");
            pdsPortNames.push_back(port.firstChildElement("SHORT-NAME").text());
            fileStream << "struct " << pdsStructNames.back() << " {\n\t// SWS_Rte_07138 requires the existence of a PDS in the CDS. Even if it is empty.\n\tuint8 _dummy;\n};\n";
        }
        fileStream << "\n/** @req SWS_Rte_01343\n *  @req SWS_Rte_01342\n *  !req SWS_Rte_06524\n *  @req SWS_Rte_01053\n */\n\n";
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {                 // Not sure how to handle multiple prototypes: TODO
            QDomElement comp = componentPrototypes.at(swcIter).at(compIter);
            QStringList prototypes;
            // Get all prototypes
            QDomElement port = swc.firstChildElement("PORTS").firstChildElement();
            while (!port.isNull()) {
                bool success;
                if (port.tagName() == "P-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("PROVIDED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, true);
                } else if (port.tagName() == "R-PORT-PROTOTYPE") {
                    QString interfaceRef = port.firstChildElement("REQUIRED-INTERFACE-TREF").text();
                    success = CreateRteFunctionCallCPrototype(prototypes, interfaceRef, swc, comp, port, autosarModelModuleRoot, ecuExtract, false);
                } else {
                    std::cout << "Error: Unknown port type \"" << port.tagName().toStdString() << "\".\n";
                    return false;
                }
                if (!success) {
                    returnValue = false;
                }
                port = port.nextSiblingElement();
            }
            //qDebug() << "Protos: " << prototypes;
            // Separate internal and external prototypes
            QStringList externProtoRefs;
            for (int runIter = 0; runIter < runnables.size(); runIter++) {
                QDomElement tempElement = runnables.at(runIter).firstChildElement("MODE-SWITCH-POINTS").firstChildElement();
                while (!tempElement.isNull()) {
                    externProtoRefs.push_back(tempElement.firstChildElement("MODE-GROUP-IREF").firstChildElement("CONTEXT-P-PORT-REF").text().split("/").last() + "_" +
                                           tempElement.firstChildElement("MODE-GROUP-IREF").firstChildElement("TARGET-MODE-GROUP-REF").text().split("/").last());
                    tempElement = tempElement.nextSiblingElement();
                }
                tempElement = runnables.at(runIter).firstChildElement("SERVER-CALL-POINTS").firstChildElement();
                while (!tempElement.isNull()) {
                    externProtoRefs.push_back(tempElement.firstChildElement("OPERATION-IREF").firstChildElement("CONTEXT-R-PORT-REF").text().split("/").last() + "_" +
                                           tempElement.firstChildElement("OPERATION-IREF").firstChildElement("TARGET-REQUIRED-OPERATION-REF").text().split("/").last());
                    tempElement = tempElement.nextSiblingElement();
                }
                QDomNodeList variableAccesses = runnables.at(runIter).toElement().elementsByTagName("VARIABLE-ACCESS");
                for (int vaIter = 0; vaIter < variableAccesses.size(); vaIter++) {
                    if (variableAccesses.at(vaIter).parentNode().toElement().tagName() == "DATA-SEND-POINTS" ||
                            variableAccesses.at(vaIter).parentNode().toElement().tagName() == "DATA-RECEIVE-POINT-BY-ARGUMENTS" ||
                            variableAccesses.at(vaIter).parentNode().toElement().tagName() == "DATA-RECEIVE-POINT-BY-VALUE")
                        externProtoRefs.push_back(variableAccesses.at(vaIter).firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF").firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last() + "_" +
                                           variableAccesses.at(vaIter).firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF").firstChildElement("TARGET-DATA-PROTOTYPE-REF").text().split("/").last());
                }
            }
            //qDebug() << "ExternalProtoRefs: " << externProtoRefs;
            QStringList externalPrototypes;
            for (int protoIter = 0; protoIter < externProtoRefs.size(); protoIter++) {
                int pIter = 0;
                for (pIter = 0; pIter < prototypes.size(); pIter++) {
                    if (prototypes[pIter].contains(externProtoRefs.at(protoIter))) {
                        break;
                    }
                }
                if (pIter < prototypes.size()) {
                    externalPrototypes.push_back(prototypes.at(pIter));
                }
            }
            //qDebug() << "ExternalProtos: " << externalPrototypes;
            QStringList internalPrototypes = prototypes;
            for (int protoIter = 0; protoIter < externalPrototypes.size(); protoIter++) {
                if (internalPrototypes.contains(externalPrototypes.at(protoIter))) {
                    internalPrototypes.erase(internalPrototypes.begin() + internalPrototypes.indexOf(externalPrototypes.at(protoIter)));
                }
            }
            //qDebug() << "InternalProtos: " << internalPrototypes;
            // Add external prototypes to the output
            for (int protoIter = 0; protoIter < externalPrototypes.size(); protoIter++)
                fileStream << "extern " << externalPrototypes.at(protoIter) << ";\n";
            fileStream << "\n";
        }
        for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
            fileStream << "typedef struct " << pdsStructNames.at(apiIter) << " * " << pdsTypeDefNames.at(apiIter) << ";\n";
        }
        fileStream << "/** @req SWS_Rte_07132\n *  @req SWS_Rte_03714 \n *  @req SWS_Rte_03725 \n *\t@req SWS_Rte_03752\n *	@req SWS_Rte_02623\n */\n";
        fileStream << "typedef struct {\n";
        int addedVariables = 0;
        // Get all dataTypes
        QString ref;
        for (int loopIter = 0; loopIter < 2; loopIter++) {                      // Once for read accesses, once for write accesses
            for (int rIter = 0; rIter < runnables.size(); rIter++) {
                QDomNodeList variableAccesses;
                if (loopIter == 0)
                    variableAccesses = runnables.at(rIter).toElement().elementsByTagName("DATA-READ-ACCESSS");
                else
                    variableAccesses = runnables.at(rIter).toElement().elementsByTagName("DATA-WRITE-ACCESSS");
                if (variableAccesses.size() > 0) {
                    QDomElement variableAccess = variableAccesses.at(0).toElement().firstChildElement();            // to iterate over data read accesses only
                    QDomElement variable;
                    while (!variableAccess.isNull()) {
                        variable = variableAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                        ref = variable.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
                        QDomElement varDataProto = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                        if (varDataProto.isNull()) {
                            std::cout << "Error: Unable to find the reference \"" << ref.toStdString() << "\" in the ECU extract.\n";
                            return false;
                        }
                        fileStream << "\tRte_DE_" << varDataProto.firstChildElement("TYPE-TREF").text().split("/").last() << " * const " <<
                                            runnables.at(rIter).firstChildElement("SHORT-NAME").text() << "_" <<
                                      variable.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last() << "_" << ref.split("/").last() << ";\n";
                        addedVariables++;
                        variableAccess = variableAccess.nextSiblingElement();
                    }
                }
            }
        }
        // Get all pimtypes
        for (int tIter = 0; tIter < pimTypes.size(); tIter++) {
            fileStream << "\tRte_PimType_" << swcName << "_" << pimTypes.at(tIter).firstChildElement("TYPE").text() << " *Pim_" << pimTypes.at(tIter).firstChildElement("SHORT-NAME").text() << ";\n";
            addedVariables++;
        }
        // Get all pdsStructures
        if (pdsStructNames.size()) {
            for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
                fileStream << "\tstruct " << pdsStructNames.at(apiIter) << " " << pdsPortNames.at(apiIter) << ";\n";
                addedVariables++;
            }
        }
        if (addedVariables == 0) {
            fileStream << "\tuint8 _dummy;\n";
        }
        fileStream << "} Rte_CDS_" << swcName << ";\n";
        fileStream << "\n/** --- Instance handle type ---------------------------------------------------------------------- */\n";
        fileStream << "typedef Rte_CDS_" << swcName << " const * const Rte_Instance;\n\n";
        fileStream << "/** --- Singleton instance handle -----------------------------------------------------------------\n *  @req SWS_Rte_03793\n */\n";
        fileStream << "extern const Rte_Instance Rte_Inst_" << swcName << ";\n#define self (Rte_Inst_" << swcName << ")\n\n";
        fileStream << "/** --- Calibration API --------------------------------------------------------------------------- */\n\n";
        // No clue
        fileStream << "/** --- Per Instance Memory API ------------------------------------------------------------------- */\n";
        for (int tIter = 0; tIter < pimTypes.size(); tIter++) {
            fileStream << "static inline " << pimTypes.at(tIter).firstChildElement("TYPE").text() << " *Rte_Pim_" << pimTypes.at(tIter).firstChildElement("SHORT-NAME").text() << "(void) {\n";
            fileStream << "\treturn self->Pim_" << pimTypes.at(tIter).firstChildElement("SHORT-NAME").text() << ";\n}\n";
        }
        fileStream << "\n/** --- Indirect port API ------------------------------------------------------------------------- */\n";
        // Add all port apis
        for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
            fileStream << "static inline " << pdsTypeDefNames.at(apiIter) << " Rte_Port_" << pdsPortNames.at(apiIter) << "(void) {\n";
            fileStream << "\treturn (" << pdsTypeDefNames.at(apiIter) << ") &(self->" << pdsPortNames.at(apiIter) << ");\n}\n\n";
        }
        for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
            ref = pdsTypeDefNames[apiIter];
            fileStream << "static inline " << pdsTypeDefNames.at(apiIter) << " " << ref.replace("PortHandle", "Ports") << "(void) {\n";
            fileStream << "\treturn (" << pdsTypeDefNames.at(apiIter) << ") &(self->" << pdsPortNames.at(apiIter) << ");\n}\n\n";
        }
        for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
            ref = pdsTypeDefNames[apiIter];
            fileStream << "static inline uint8 " << ref.replace("PortHandle", "NPorts") << "(void) {\n";
            fileStream << "\treturn 1;\n}\n\n";
        }
        QString runnableApis = "";
        fileStream << "/** --- Single Runnable APIs ---------------------------------------------------------------------- */\n#if defined(RTE_RUNNABLEAPI)\n";
        QDomNodeList operationInvokedEvents = swc.elementsByTagName("OPERATION-INVOKED-EVENT");
        for (int rIter = 0; rIter < runnables.size(); rIter++) {
            fileStream << "/** --- " << runnables.at(rIter).firstChildElement("SHORT-NAME").text() << " */\n#if defined(RTE_RUNNABLEAPI_" <<
                          runnables.at(rIter).firstChildElement("SHORT-NAME").text() << ")\n\n";
            QString returnType = "void";
            QString arguments = "";
            for (int eventIndex = 0; eventIndex < operationInvokedEvents.size(); eventIndex++) {
                QDomElement event = operationInvokedEvents.at(eventIndex).toElement();
                ref = event.firstChildElement("START-ON-EVENT-REF").text();
                QDomElement targetRunnable = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                if (targetRunnable.isNull()) {
                    targetRunnable = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                    if (targetRunnable.isNull()) {
                        std::cout << "Error: Target runnable reference " << ref.toStdString() << " is not valid.\n";
                        return false;
                    }
                }
                if (targetRunnable == runnables.at(rIter)) {
                    // It is a server runnable
                    ref = event.firstChildElement("OPERATION-IREF").firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                    QDomElement targetOperation = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (targetOperation.isNull()) {
                        targetOperation = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                        if (targetOperation.isNull()) {
                            std::cout << "Error: Target operation reference " << ref.toStdString() << " is not valid.\n";
                            return false;
                        }
                    }
                    if (!targetOperation.firstChildElement("POSSIBLE-ERROR-REFS").isNull()) {
                        returnType = "Std_ReturnType";
                    }
                    int numOfPortArgs;
                    QDomElement operation = CreateServerPortArguments(event, numOfPortArgs, arguments);
                    QString targetRef = operation.firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                    QStringList dataTypes, variables;
                    bool rv = CreateCSArguments(dataTypes, variables, targetRef, autosarModelModuleRoot, ecuExtract);
                    if (!rv) {
                        return false;
                    }
                    if (dataTypes.size() > 0) {
                        for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                            if (argIter+numOfPortArgs > 0) {
                                arguments += ", ";
                            }
                            arguments += dataTypes.at(argIter) + variables.at(argIter);
                        }
                    }
                    break;
                }
            }
            if (arguments == "") {
                arguments = "void";
            }
            QString tempApis;
            tempApis = returnType + " " + runnables.at(rIter).firstChildElement("SYMBOL").text() + "(" + arguments + ");\n\n";
            // Get data receive (send) points (by arguments)
            for (int loopIter = 0; loopIter < 2; loopIter++) {                          // First for data receive point by arguments and second for data send points.
                QDomElement drpba;
                if (loopIter == 0)
                    drpba = runnables.at(rIter).firstChildElement("DATA-RECEIVE-POINT-BY-ARGUMENTS");
                else
                    drpba = runnables.at(rIter).firstChildElement("DATA-SEND-POINTS");
                if (!drpba.isNull()) {
                    drpba = drpba.firstChildElement();
                    while (!drpba.isNull()) {
                        QDomElement variableRef = drpba.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                        ref = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
                        QString funcCall = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last() + "_" + ref.split("/").last();
                        tempApis += "static inline Std_ReturnType Rte_";
                        if (loopIter == 0)
                            tempApis += "Read";
                        else
                            tempApis += "Write";
                        tempApis += "_" + funcCall + "(";
                        QDomElement Type = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (Type.isNull()) {
                            Type = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                            if (Type.isNull()) {
                                std::cout << "Error: Target data prototype reference " << ref.toStdString() << " is not valid.\n";
                                return false;
                            }
                        }
                        ref = Type.firstChildElement("TYPE-TREF").text();
                        Type = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (Type.isNull()) {
                            Type = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                            if (Type.isNull()) {
                                std::cout << "Error: Type reference " << ref.toStdString() << " is not valid.\n";
                                return false;
                            }
                        }
                        QString args;
                        if (loopIter == 0)
                            CreateSRArguments(Type, args, true);
                        else
                            CreateSRArguments(Type, args, false);
                        tempApis += args + ") {\n";
                        QStringList argVariables = args.split(",");
                        tempApis += "\treturn Rte_";
                        if (loopIter == 0)
                            tempApis += "Read";
                        else
                            tempApis += "Write";
                        tempApis += "_" + swcName + "_" + componentPrototypes.at(swcIter).at(0).firstChildElement("SHORT-NAME").text() // Assuming only one component per swcprototype
                                + "_" + funcCall + "(";
                        for (int argIter = 0; argIter < argVariables.size(); argIter++) {
                            if (argIter != 0) {
                                tempApis += ", ";
                            }
                            tempApis += argVariables.at(argIter).split(" ").last();
                        }
                        tempApis += ");\n}\n\n";
                        drpba = drpba.nextSiblingElement();
                    }
                }
            }
            // Get mode switch points
            QDomElement msp = runnables.at(rIter).firstChildElement("MODE-SWITCH-POINTS");
            if (!msp.isNull()) {
                msp = msp.firstChildElement();
                while (!msp.isNull()) {
                    QDomElement modeGroup = msp.firstChildElement("MODE-GROUP-IREF");
                    QString funcCall = modeGroup.firstChildElement("CONTEXT-P-PORT-REF").text().split("/").last() + "_" +
                            modeGroup.firstChildElement("TARGET-MODE-GROUP-REF").text().split("/").last();
                    tempApis += "static inline Std_ReturnType Rte_Switch_" + funcCall + "(/*IN*/uint8 mode) {\n";
                    tempApis += "\treturn Rte_Switch_" + swcName + "_" + componentPrototypes.at(swcIter).at(0).firstChildElement("SHORT-NAME").text() // Assuming only one component per swcprototype
                            + "_" + funcCall + "(mode);\n}\n\n";
                    msp = msp.nextSiblingElement();
                }
            }
            // Get all server calls
            QDomElement serverCallPoints = runnables.at(rIter).firstChildElement("SERVER-CALL-POINTS");
            if (!serverCallPoints.isNull()) {
                serverCallPoints = serverCallPoints.firstChildElement();
                while (!serverCallPoints.isNull()) {
                    QDomElement operation = serverCallPoints.firstChildElement("OPERATION-IREF");
                    QString ref = operation.firstChildElement("TARGET-REQUIRED-OPERATION-REF").text();
                    QString funcCall = operation.firstChildElement("CONTEXT-R-PORT-REF").text().split("/").last() + "_" +
                            ref.split("/").last();
                    tempApis += "static inline Std_ReturnType Rte_Call_" + funcCall + "(";
                    QStringList dataTypes, variables;
                    bool rv = CreateCSArguments(dataTypes, variables, ref, autosarModelModuleRoot, ecuExtract);
                    if (!rv) {
                        return false;
                    }
                    if (dataTypes.size() > 0) {
                        for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                            if (argIter > 0) {
                                tempApis += ", ";
                            }
                            tempApis += dataTypes.at(argIter) + variables.at(argIter);
                        }
                    } else {
                        tempApis += "void";
                    }
                    tempApis += ") {\n";
                    tempApis += "\treturn Rte_Call_" + swcName + "_" + componentPrototypes.at(swcIter).at(0).firstChildElement("SHORT-NAME").text() // Assuming only one component per swcprototype
                            + "_" + funcCall + "(";
                    if (dataTypes.size() > 0) {
                        for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                            if (argIter > 0) {
                                tempApis += ", ";
                            }
                            tempApis += variables.at(argIter);
                        }
                    }
                    tempApis += ");\n}\n\n";
                    serverCallPoints = serverCallPoints.nextSiblingElement();
                }
            }
            // Get all data read/right accesses
            for (int loopIter = 0; loopIter < 2; loopIter++) {                          // First for data read accesses and second for data write accesses.
                QDomElement dataAccess;
                if (loopIter == 0)
                    dataAccess = runnables.at(rIter).firstChildElement("DATA-READ-ACCESSS");
                else
                    dataAccess = runnables.at(rIter).firstChildElement("DATA-WRITE-ACCESSS");
                if (!dataAccess.isNull()) {
                    dataAccess = dataAccess.firstChildElement();
                    while (!dataAccess.isNull()) {
                        QDomElement variableRef = dataAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                        ref = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
                        QDomElement Type = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (Type.isNull()) {
                            Type = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                            if (Type.isNull()) {
                                std::cout << "Error: Target data prototype reference " << ref.toStdString() << " is not valid.\n";
                                return false;
                            }
                        }
                        QString funcCall = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last() + "_" + ref.split("/").last();
                        ref = Type.firstChildElement("TYPE-TREF").text();
                        tempApis += "static inline ";
                        if (loopIter == 0)
                            tempApis += ref.split("/").last() + " Rte_IRead";
                        else
                            tempApis += "void Rte_IWrite";
                        tempApis += "_" + runnables.at(rIter).firstChildElement("SHORT-NAME").text() + "_" + funcCall + "(";
                        Type = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (Type.isNull()) {
                            Type = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                            if (Type.isNull()) {
                                std::cout << "Error: Type reference " << ref.toStdString() << " is not valid.\n";
                                return false;
                            }
                        }
                        QString args;
                        if (loopIter == 0) {
                            tempApis += "void) {\n\t";
                            tempApis += "return self->" + runnables.at(rIter).firstChildElement("SHORT-NAME").text() + "_" + funcCall + "->value;\n";
                        } else {
                            CreateSRArguments(Type, args, false);
                            QStringList argSep;
                            QStringList indArgs = args.split(",");
                            for (int argIter = 0; argIter < indArgs.size(); argIter++) {
                                argSep = indArgs.at(argIter).split(" ");
                                argSep.pop_back();
                                indArgs[argIter] = argSep.join(" ") + " value";
                            }
                            tempApis += indArgs.join(",") + ") {\n\t";
                            tempApis += "self->" + runnables.at(rIter).firstChildElement("SHORT-NAME").text() + "_" + funcCall + "->value = value;\n";
                        }
                        tempApis += "}\n\n";
                        dataAccess = dataAccess.nextSiblingElement();
                    }
                }
            }
            fileStream << tempApis << "#endif\n\n";
            runnableApis += tempApis;
        }
        fileStream << "#endif\n\n/** --- All Runnable APIs ------------------------------------------------------------------------- */\n#if !defined(RTE_RUNNABLEAPI)\n";
        fileStream << runnableApis;
        fileStream << "#endif\n\n/** === FOOTER ====================================================================================\n */\n\n";
        fileStream << "#endif /* RTE_" << swcName.toUpper() << "_H_ */\n\n/** @req SWS_Rte_03710 */\n#ifdef __cplusplus\n} /* extern \"C\" */\n#endif /* __cplusplus */\n";
        file.close();
    }
    return returnValue;
}

bool RteGenerator::CreateRteSwcC(QString location, QDomElement &autosarModelModuleRoot, QDomElement &ecuExtract, QList<QDomElement> &swcs, QList<QList<QDomElement> > &componentPrototypes)
{
    bool returnValue = true;
    for (int swcIter = 0; swcIter < swcs.size(); swcIter++) {
        QDomElement swc = swcs.at(swcIter);
        QString swcName = swc.firstChildElement("SHORT-NAME").text();
        //qDebug() << "\nSWCName: " << swc.firstChildElement("SHORT-NAME").text();
        QString fileName = location + "/Rte_" + swcName + ".c";
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            std::cout << "Error: Unable to create " << fileName.toStdString() << " file.\n";
            returnValue = false;
            break;
        }
        QTextStream fileStream( &file );
        fileStream << "/** === HEADER ====================================================================================\n */\n\n";
        fileStream << "#include <Rte.h>\n\n#include <Os.h>\n";
        fileStream << "#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))\n";
        fileStream << "#error Os version mismatch\n#endif\n\n#include <Com.h>\n";
        fileStream << "#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))\n";
        fileStream << "#error Com version mismatch\n#endif\n\n#include <Rte_Hook.h>\n#include <Rte_Internal.h>\n#include <Rte_Calprms.h>\n\n";
        fileStream << "#include \"Rte_" << swcName << ".h\"\n";
        fileStream << "\n/** === Runnable Prototypes =======================================================================\n */\n";
        QDomNodeList runnables = swc.elementsByTagName("RUNNABLE-ENTITY");
        QMap <QString, QDomElement> eventToRunnableMap;
        QDomNodeList tempList = swc.elementsByTagName("EVENTS");
        QDomElement firstEvent;
        QString ref;
        QStringList runnableProtosReturnType, runnableProtos, runnableProtosArguments;
        QList <QDomElement> runnableProtoComponents, runnableProtoRunnables;
        if (tempList.size() > 0)
            firstEvent = tempList.at(0).firstChildElement();
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {
            QString compName = componentPrototypes.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text();
            fileStream << "\n/** ------ " << compName << " -----------------------------------------------------------------------\n */\n";
            for (int reIter = 0; reIter < runnables.size(); reIter++) {
                // Check if the runnable should be invoked by the RTE
                QDomElement tempEvent = firstEvent;
                bool eventFound = false;
                while(!tempEvent.isNull()) {
                    ref = tempEvent.firstChildElement("START-ON-EVENT-REF").text();
                    QDomElement runEnt = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                    if (runEnt.isNull()) {
                        runEnt = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                        if (runEnt.isNull()) {
                            std::cout << "Error: Target runnable reference " << ref.toStdString() << " is not valid.\n";
                            return false;
                        }
                    }
                    if (runEnt == runnables.at(reIter)) {
                        eventToRunnableMap.insert(runEnt.firstChildElement("SHORT-NAME").text(), runEnt);
                        eventFound = true;
                        break;
                    }
                    tempEvent = tempEvent.nextSiblingElement();
                }
                if (eventFound) {
                    QString returnType = "void";
                    QString arguments = "";
                    // If server runnable, get return type and arguments
                    ref = tempEvent.firstChildElement("OPERATION-IREF").firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                    if (ref != "") {
                        QDomElement targetOperation = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                        if (targetOperation.isNull()) {
                            targetOperation = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                            if (targetOperation.isNull()) {
                                std::cout << "Error: Target operation reference " << ref.toStdString() << " is not valid.\n";
                                return false;
                            }
                        }
                        if (!targetOperation.firstChildElement("POSSIBLE-ERROR-REFS").isNull()) {
                            returnType = "Std_ReturnType";
                        }
                        int numOfPortArgs;
                        QDomElement operation = CreateServerPortArguments(tempEvent, numOfPortArgs, arguments);
                        QString targetRef = operation.firstChildElement("TARGET-PROVIDED-OPERATION-REF").text();
                        QStringList dataTypes, variables;
                        bool rv = CreateCSArguments(dataTypes, variables, targetRef, autosarModelModuleRoot, ecuExtract);
                        if (!rv) {
                            return false;
                        }
                        if (dataTypes.size() > 0) {
                            for (int argIter = 0; argIter < dataTypes.size(); argIter++) {
                                if (argIter+numOfPortArgs > 0) {
                                    arguments += ", ";
                                }
                                arguments += dataTypes.at(argIter) + variables.at(argIter);
                            }
                        }
                    }
                    if (arguments == "") {
                        arguments = "void";
                    }
                    runnableProtosReturnType.push_back(returnType);
                    runnableProtos.push_back("Rte_" + compName + "_" + runnables.at(reIter).firstChildElement("SHORT-NAME").text());
                    runnableProtosArguments.push_back(arguments);
                    runnableProtoRunnables.push_back(runnables.at(reIter).toElement());
                    runnableProtoComponents.push_back(componentPrototypes.at(swcIter).at(compIter));
                    fileStream << runnableProtosReturnType.last() << " " << runnableProtos.last() << "(" << runnableProtosArguments.last() << ");\n";
                }
            }
        }
        fileStream << "\n/** === Inter-Runnable Variable Buffers ===========================================================\n */\n";
        // No clue
        fileStream << "\n/** === Inter-Runnable Variable Functions =========================================================\n */\n";
        // No clue
        fileStream << "\n/** === Implicit Buffer Instances =================================================================\n */\n";
        QString tempString;
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {
            QString compName = componentPrototypes.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text();
            QString runnablesBuffer;
            for (int rIter = 0; rIter < runnables.size(); rIter++) {
                QString runnableName = runnables.at(rIter).firstChildElement("SHORT-NAME").text();
                QMultiMap <QString, QString> tempPortBuffer;
                for (int loopIter = 0; loopIter < 2; loopIter++) {                          // First for data read accesses and second for data write accesses.
                    QDomElement dataAccess;
                    if (loopIter == 0)
                        dataAccess = runnables.at(rIter).firstChildElement("DATA-READ-ACCESSS");
                    else
                        dataAccess = runnables.at(rIter).firstChildElement("DATA-WRITE-ACCESSS");
                    if (!dataAccess.isNull()) {
                        dataAccess = dataAccess.firstChildElement();
                        while (!dataAccess.isNull()) {
                            QDomElement variableRef = dataAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                            ref = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text();
                            QDomElement Type = AutosarToolFactory::GetDomElementFromReference(&ref, autosarModelModuleRoot.ownerDocument().documentElement());
                            if (Type.isNull()) {
                                Type = AutosarToolFactory::GetDomElementFromReference(&ref, ecuExtract);
                                if (Type.isNull()) {
                                    std::cout << "Error: Target data prototype reference " << ref.toStdString() << " is not valid.\n";
                                    return false;
                                }
                            }
                            tempPortBuffer.insert(variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last(),
                                                  "Rte_DE_" + Type.firstChildElement("TYPE-TREF").text().split("/").last() + " " + Type.firstChildElement("SHORT-NAME").text() + ";");
                            dataAccess = dataAccess.nextSiblingElement();
                        }
                    }
                }
                if (!tempPortBuffer.isEmpty()) {
                    QStringList ports = tempPortBuffer.uniqueKeys();
                    QString tempPortVariables;
                    for (int pIter = 0; pIter < ports.size(); pIter++) {
                        QStringList variables = tempPortBuffer.values(ports.at(pIter));
                        QString tempVariables;
                        for (int vIter = 0; vIter < variables.size(); vIter++) {
                            tempVariables += "\t\t\t" + variables.at(vIter) + "\n";
                        }
                        tempPortVariables += "\t\tstruct {\n" + tempVariables + "\t\t} " + ports.at(pIter) + ";\n";
                    }
                    runnablesBuffer += "\tstruct {\n" + tempPortVariables + "\t} " + runnableName + ";\n";
                }
            }
            if (!runnablesBuffer.isEmpty()) {
                tempString = "\nstruct {\n";
                tempString += runnablesBuffer;
                tempString += "} ImplDE_" + compName + ";";
                CREATE_SECTION(fileStream, swcName, "_VAR", "_CLEARED", "_UNSPECIFIED", tempString);
            }
        }
        fileStream << "\n/** === Per Instance Memories =====================================================================\n */\n";
        QDomNodeList pimTypes = swc.elementsByTagName("PER-INSTANCE-MEMORY");
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {
            QString compName = componentPrototypes.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text();
            for (int tIter = 0; tIter < pimTypes.size(); tIter++) {
                CREATE_SECTION(fileStream, swcName, "_VAR", "_INIT", "_UNSPECIFIED", "Rte_PimType_" + swcName + "_" + pimTypes.at(tIter).firstChildElement("TYPE").text()
                               + " Rte_Pim_" + swcName + "_" + compName + "_" + pimTypes.at(tIter).firstChildElement("SHORT-NAME").text() + ";");
                if (tIter < pimTypes.size() - 1) {
                    fileStream << "\n";
                }
            }
        }
        fileStream << "\n/** === Component Data Structure Instances ========================================================\n */\n";
        for (int compIter = 0; compIter < componentPrototypes.at(swcIter).size(); compIter++) {
            QString compName = componentPrototypes.at(swcIter).at(compIter).firstChildElement("SHORT-NAME").text();
            QStringList initializations;
            // Get all data read/right accesses
            for (int rIter = 0; rIter < runnables.size(); rIter++) {
                for (int loopIter = 0; loopIter < 2; loopIter++) {                          // First for data read accesses and second for data write accesses.
                    QDomElement dataAccess;
                    if (loopIter == 0)
                        dataAccess = runnables.at(rIter).firstChildElement("DATA-READ-ACCESSS");
                    else
                        dataAccess = runnables.at(rIter).firstChildElement("DATA-WRITE-ACCESSS");
                    if (!dataAccess.isNull()) {
                        QString runnableName = runnables.at(rIter).firstChildElement("SHORT-NAME").text();
                        dataAccess = dataAccess.firstChildElement();
                        while (!dataAccess.isNull()) {
                            QDomElement variableRef = dataAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                            QString protoName = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text().split("/").last();
                            QString portName = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last();
                            initializations.push_back("\t." + runnableName + "_" + portName + "_" + protoName + " = &ImplDE_" + compName + "." + runnableName + "." + portName + "." + protoName);
                            dataAccess = dataAccess.nextSiblingElement();
                        }
                    }
                }
            }
            // Get all pims
            for (int tIter = 0; tIter < pimTypes.size(); tIter++) {
                initializations.push_back("\t.Pim_" + pimTypes.at(tIter).firstChildElement("SHORT-NAME").text() +
                                          " = &Rte_Pim_" + swcName + "_" + compName + "_" + pimTypes.at(tIter).firstChildElement("SHORT-NAME").text());
            }
            // Get all pds
            QDomNodeList indirectApis = swc.elementsByTagName("INDIRECT-API");
            QList <QString> pdsStructNames, pdsPortNames, pdsTypeDefNames;
            for (int apiIter = 0; apiIter < indirectApis.size(); apiIter++) {
                QString portRef = indirectApis.at(apiIter).nextSiblingElement("PORT-REF").text();
                QDomElement port = AutosarToolFactory::GetDomElementFromReference(&portRef, ecuExtract);
                if (port.isNull()) {
                    std::cout << "Error: Unable to find port \"" << portRef.toStdString() << "\" in SWC \"" << swcName.toStdString() << "\".\n";
                    return false;
                }
                pdsStructNames.push_back("Rte_PDS_" + swcName + "_" + port.firstChildElement("PROVIDED-INTERFACE-TREF").text().split("/").last() + "_P");
                pdsTypeDefNames.push_back("Rte_PortHandle_" + port.firstChildElement("PROVIDED-INTERFACE-TREF").text().split("/").last() + "_P");
                pdsPortNames.push_back(port.firstChildElement("SHORT-NAME").text());
            }
            if (pdsStructNames.size()) {
                for (int apiIter = 0; apiIter < pdsStructNames.size(); apiIter++) {
                    initializations.push_back("\t." + pdsPortNames.at(apiIter) + " = {\n\t\t._dummy = 0 // SWS_Rte_07138 requires the existence of a PDS in the CDS. Even if it is empty.\n\t}");
                }
            }
            tempString = "const Rte_CDS_" + swcName + " " + swcName + "_" + compName + " = {\n";
            if (initializations.size() > 0) {
                tempString += initializations.join(",\n") + "\n";
            } else {
                tempString += "\t._dummy = 0\n";
            }
            tempString += "};";
            CREATE_SECTION(fileStream, swcName, "_VAR", "_INIT", "_UNSPECIFIED", tempString);
            fileStream << "\n";
            CREATE_SECTION(fileStream, swcName, "_VAR", "_INIT", "_UNSPECIFIED", "const Rte_Instance Rte_Inst_" + swcName + " = &" + swcName + "_" + compName + ";\n");
        }
        // No clue
        fileStream << "\n/** === Runnables =================================================================================\n */\n";
        QString runnableDefinitions;
        for (int rpIter = 0; rpIter < runnableProtos.size(); rpIter++) {
            QString runnableName = runnableProtoRunnables.at(rpIter).firstChildElement("SHORT-NAME").text();
            QString compName = runnableProtoComponents.at(rpIter).firstChildElement("SHORT-NAME").text();
            if (rpIter == 0 || runnableProtoComponents.at(rpIter-1) != runnableProtoComponents.at(rpIter)) {
                runnableDefinitions += "\n/** ------ " + compName
                        + " -----------------------------------------------------------------------\n */\n";
            }
            runnableDefinitions += runnableProtosReturnType.at(rpIter) + " " + runnableProtos.at(rpIter) + "(" + runnableProtosArguments.at(rpIter) + ") {\n";
            if (runnableProtosReturnType.at(rpIter) == "Std_ReturnType") {
                runnableDefinitions += "\tStd_ReturnType retVal = RTE_E_OK;\n";
            }
            runnableDefinitions += "\n\t/* PRE */\n";
            // Get data read accesses
            QDomElement dataAccess = runnableProtoRunnables.at(rpIter).firstChildElement("DATA-READ-ACCESSS");
            if (!dataAccess.isNull()) {
                dataAccess = dataAccess.firstChildElement();
                while (!dataAccess.isNull()) {
                    QDomElement variableRef = dataAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                    QString protoName = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text().split("/").last();
                    QString portName = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last();
                    runnableDefinitions += "\tRte_Read_" + swcName + "_" + compName + "_" + portName + "_" + protoName +
                            "(&ImplDE_" + compName + "." + runnableName + "." + portName + "." + protoName + ".value);\n\n";
                    dataAccess = dataAccess.nextSiblingElement();
                }
            }
            runnableDefinitions += "\t/* MAIN */\n\n\t";
            if (runnableProtosReturnType.at(rpIter) == "Std_ReturnType") {
                runnableDefinitions += "retVal = ";
            }
            runnableDefinitions += runnableProtoRunnables.at(rpIter).firstChildElement("SYMBOL").text() + "(";
            if (runnableProtosArguments.at(rpIter) != "void") {
                QStringList strippedRunnableArguments = runnableProtosArguments.at(rpIter).split(",");
                for (int argIter = 0; argIter < strippedRunnableArguments.size(); argIter++) {
                    strippedRunnableArguments[argIter] = strippedRunnableArguments.at(argIter).split(" ").last();
                }
                runnableDefinitions += strippedRunnableArguments.join(", ");
            }
            runnableDefinitions += ");\n";
            runnableDefinitions += "\n\t/* POST */\n";
            // Get data write accesses
            dataAccess = runnableProtoRunnables.at(rpIter).firstChildElement("DATA-WRITE-ACCESSS");
            if (!dataAccess.isNull()) {
                dataAccess = dataAccess.firstChildElement();
                while (!dataAccess.isNull()) {
                    QDomElement variableRef = dataAccess.firstChildElement("ACCESSED-VARIABLE").firstChildElement("AUTOSAR-VARIABLE-IREF");
                    QString protoName = variableRef.firstChildElement("TARGET-DATA-PROTOTYPE-REF").text().split("/").last();
                    QString portName = variableRef.firstChildElement("PORT-PROTOTYPE-REF").text().split("/").last();
                    runnableDefinitions += "\tRte_Write_" + swcName + "_" + compName + "_" + portName + "_" + protoName +
                            "(ImplDE_" + compName + "." + runnableName + "." + portName + "." + protoName + ".value);\n\n";
                    dataAccess = dataAccess.nextSiblingElement();
                }
            }
            if (runnableProtosReturnType.at(rpIter) == "Std_ReturnType") {
                runnableDefinitions += "\treturn retVal;\n";
            }
            runnableDefinitions += "}\n";
        }
        CREATE_SECTION(fileStream, swcName, "_CODE", "", "", runnableDefinitions);
        file.close();
    }
    return returnValue;
}
