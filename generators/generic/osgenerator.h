/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef OSGENERATOR
#define OSGENERATOR

#include <QtXml>
#include <iostream>
#include "abstractcodegenerator.h"
#include "autosartoolfactory.h"

class OsGenerator : public AbstractCodeGenerator
{
public:
    bool RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location);
    bool CheckModuleValidity(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot);
    ~OsGenerator() { }

    void GetOwningApplication(QString& returnAppName, QString searchText, QList<QDomElement>& applications, QDomElement& referedElement, QDomElement &modeuleRoot);
    QString GetAccessingApplications(QString searchText, QDomElement& referedElement, QDomElement &modeuleRoot);

    static AbstractCodeGenerator *CreateGenerator() { return new OsGenerator(); }
};

#endif // OSGENERATOR

