/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "linif_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool LinIfGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of LinIf Module Configuration files does not exist.\n";
        return false;
    }

    try {
        // ----------------------------------------- Create Makefile -----------------------------------------
        QString fileName = location + "/LinIf.mk";
        QFile qFile(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create LinIf.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &qFile );
        makeFileStream << "MOD_USE += LINIF\n";
        qFile.close();

        // ----------------------------------------- Create LinIf_Cfg.h -----------------------------------------
        fileName = location + "/LinIf_Cfg.h";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create LinIf_Cfg.h file.\n";
            return false;
        }
        QTextStream headerStream( &qFile );
        headerStream << "#if !(((LINIF_SW_MAJOR_VERSION == 2) && (LINIF_SW_MINOR_VERSION == 1)) )\n";
        headerStream << "#error LinIf: Configuration file expected BSW module version to be 2.1.X*\n#endif\n\n";
        headerStream << "/* @req LINIF383 */\n#if !(((LINIF_AR_RELEASE_MAJOR_VERSION == 4) && (LINIF_AR_RELEASE_MINOR_VERSION == 0)) )\n";
        headerStream << "#error LinIf: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n";
        headerStream << "/* File: LinIf_Cfg.h\n * Declarations of the pre-compile time configurable parameters.\n */\n\n";
        headerStream << "#ifndef LINIF_CFG_H_\n#define LINIF_CFG_H_\n\n";
        headerStream << "#include \"Std_Types.h\"\n#include \"LinIf_Types.h\"\n#include \"Lin.h\"\n#include \"PduR.h\"\n#include \"PduR_PbCfg.h\" // for using PDUR Pdu IDs\n\n";
        headerStream << "/*===========[LinIfGeneral]===================================================*/\n\n";
        headerStream << "/* Switches the Development Error Detection and Notification ON or OFF. */\n";
        QDomElement LinIfGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinIfGeneral");
        QString parameterValue = GET_VALUE_OF_PARAM(LinIfGeneral, "LinIfDevErrorDetect");
        headerStream << "#define LINIF_DEV_ERROR_DETECT\t";
        if (parameterValue == "1") {
            headerStream << "STD_ON";
        } else {
            headerStream << "STD_OFF";
        }
        headerStream << "\n\n/* States if multiple drivers are included in the LIN Interface or not. The\n * reason for this parameter is to reduce the size of LIN Interface if multiple\n";
        headerStream << " * drivers are not used. */\n#define LINIF_MULTIPLE_DRIVER_SUPPORT STD_OFF\n\n";
        headerStream << "/* States if the node configuration commands Assign NAD and Conditional\n * Change NAD are supported. */\n";
        headerStream << "#define LINIF_OPTIONAL_REQUEST_SUPPORTED STD_OFF\n\n";
        headerStream << "/* States if the TP is included in the LIN Interface or not. The reason for this\n * parameter is to reduce the size of LIN Interface if the TP is not used. */\n";
        headerStream << "#define LINIF_TP_SUPPORTED STD_OFF\n\n";
        headerStream << "/* Switches the LinIf_GetVersionInfo function ON or OFF. */\n";
        parameterValue = GET_VALUE_OF_PARAM(LinIfGeneral, "LinIfVersionInfoApi");
        headerStream << "#define LINIF_VERSION_INFO_API ";
        if (parameterValue == "1") {
            headerStream << "STD_ON";
        } else {
            headerStream << "STD_OFF";
        }
        headerStream << "\n\n/*===========[Channel ID symbols]=============================================*/\n/* NOTE: The symbolic name cannot be used to index in the LinIf configuration. */\n";
        QDomElement LinIfGlobalConfig = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinIfGlobalConfig");
        QList <QDomElement> LinIfChannels;
        QList <QList <QDomElement> > LinIfScheduleTables;
        QList <QList <QDomElement> > LinIfFrames;
        int totalSchedulesCount = 0;
        AutosarToolFactory::FindReferencesToElement(&LinIfChannels, &LinIfGlobalConfig, "LinIfChannel");
        // Get ComM channel index (MODIFY_POINT when indices are modified).
        QDomElement document = projectModuleRoot->ownerDocument().documentElement();
        QDomElement ComM = AutosarToolFactory::FindFirstReferenceToElement(&document, "ComM");
        QDomElement ComMConfigSet = AutosarToolFactory::FindFirstReferenceToElement(&ComM, "ComMConfigSet");
        QList <QDomElement> ComMChannels;
        QList <QDomElement> ComMHandles;
        QList <int> ComMChannelIds;
        AutosarToolFactory::FindReferencesToElement(&ComMChannels, &ComMConfigSet, "ComMChannel");
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            parameterValue = GET_VALUE_REF_OF_REF(LinIfChannels[chIter], "LinIfComMNetworkHandleRef");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined reference LinIf::LinIfGlobalConfig::LinIfChannel::LinIfComMNetworkHandleRef in " <<
                             LinIfChannels[chIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                return false;
            }
            QDomElement LinIfComMNetworkHandle = AutosarToolFactory::GetDomElementFromReference(&parameterValue, projectModuleRoot->ownerDocument().documentElement());
            if (LinIfComMNetworkHandle.isNull()) {
                std::cout << "Error: Invalid reference LinIf::LinIfGlobalConfig::LinIfChannel::LinIfComMNetworkHandleRef in " <<
                             LinIfChannels[chIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                return false;
            }
            ComMHandles.push_back(LinIfComMNetworkHandle);
            int channelIndex = -1;
            for (int comChIter = 0; comChIter < ComMChannels.size(); comChIter++) {
                if (LinIfComMNetworkHandle == ComMChannels[comChIter]) {
                    channelIndex = comChIter;
                    break;
                }
            }
            if (channelIndex == -1) {
                std::cout << "Error: Unable to find ComMChannel \'" << parameterValue.toStdString() << "\' in ComM module.\n";
                return false;
            }
            ComMChannelIds.push_back(channelIndex);
            headerStream << "#define LinIfConf_LinIfChannel_" << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << " "
                         << channelIndex << "u/* ComMConf_ComMChannel_" << LinIfComMNetworkHandle.firstChildElement("SHORT-NAME").text() << " */\n";
            QList <QDomElement> tempSchedules;
            AutosarToolFactory::FindReferencesToElement(&tempSchedules, &LinIfChannels[chIter], "LinIfScheduleTable");
            LinIfScheduleTables.push_back(tempSchedules);
            QList <QDomElement> tempFrames;
            AutosarToolFactory::FindReferencesToElement(&tempFrames, &LinIfChannels[chIter], "LinIfFrame");
            LinIfFrames.push_back(tempFrames);
            totalSchedulesCount += tempSchedules.size();
        }
        headerStream << "\n/*===========[Schedule table indexes]=========================================*/\n/* Current implementation of LinIf does not support separate schedule tables\n";
        headerStream << " * per channel so these indexes are all used to access the array\n * LinIfScheduleTableCfg. The indexes beginn with 1 because index 0 is reserved\n";
        headerStream << " * for the NULL_SCHEDULE table which is autogenerated. */\n\n";
        headerStream << "#define NULL_SCHEDULE  ((LinIf_SchHandleType) 0)\n\n";
        QList < QList <int> > schedIndices;
        int schedCount = 0;
        int frameCount = 0;
        int runningIndex = 1;
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            QList <int> tempIndices;
            for (int schIter = 0; schIter < LinIfScheduleTables[chIter].size(); schIter++) {
                headerStream << "#define LinIfConf_LinIfScheduleTable_" << LinIfScheduleTables[chIter][schIter].firstChildElement("SHORT-NAME").text()
                             << " ((LinIf_SchHandleType) " << runningIndex << ")\n";
                tempIndices.push_back(runningIndex);
                runningIndex++;
                schedCount++;
            }
            frameCount += LinIfFrames[chIter].size();
            schedIndices.push_back(tempIndices);
        }
        headerStream << "\n\n/*===========[PDU definitions]================================================*/\n\n";
        headerStream << "enum\n{\n";
        QStringList pduNames;
        QStringList pduDirection;
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            for (int frIter = 0; frIter < LinIfFrames[chIter].size(); frIter++) {
                QDomElement LinIfPduDirection = AutosarToolFactory::FindFirstReferenceToElement(&LinIfFrames[chIter][frIter], "LinIfPduDirection");
                if (LinIfPduDirection.isNull()) {
                    std::cout << "Error: Direction for frame " << LinIfFrames[chIter][frIter].firstChildElement("SHORT-NAME").text().toStdString() << " must be specified.\n";
                    return false;
                }
                QDomElement LinIfRxPdu = AutosarToolFactory::FindFirstReferenceToElement(&LinIfPduDirection, "LinIfRxPdu");
                QDomElement LinIfTxPdu = AutosarToolFactory::FindFirstReferenceToElement(&LinIfPduDirection, "LinIfTxPdu");
                if (LinIfTxPdu.isNull() && LinIfRxPdu.isNull()) {
                    std::cout << "Error: Frame " << LinIfFrames[chIter][frIter].firstChildElement("SHORT-NAME").text().toStdString() << " direction must contain either LinIfRxPdu or LinIfTxPdu.\n";
                    return false;
                }
                if (LinIfRxPdu.isNull()) {
                    parameterValue = GET_VALUE_REF_OF_REF(LinIfTxPdu, "LinIfTxPduRef");
                    pduDirection.push_back("LinIfTxPdu");
                } else if (LinIfTxPdu.isNull()) {
                    parameterValue = GET_VALUE_REF_OF_REF(LinIfRxPdu, "LinIfRxPduRef");
                    pduDirection.push_back("LinIfRxPdu");
                }
                pduNames.push_back(parameterValue.split("/").last().toUpper());
                headerStream << "\tLINIF_PDU_ID_" << pduNames.last() << ",\n";
            }
        }
        headerStream << "};\n\n/*===========[Sizes of configuration tables]==================================*/\n\n/* Number of LinIf channels. */\n";
        headerStream << "#define LINIF_CHANNEL_COUNT\t" << LinIfChannels.size() << "\n\n";
        headerStream << "/* Number of referenced Lin controllers.\n * As every LinIf channel is referencing\n * a Lin channel, these numbers are equal. */\n";
        headerStream << "#define LINIF_CONTROLLER_CNT\tLINIF_CHANNEL_COUNT\n\n";
        headerStream << "/* Number of all schedule tables on all channels plus one.\n * This includes the autogenerated NULL_SCHEDULE table. */\n";
        headerStream << "#define LINIF_SCH_CNT\t" << schedCount + 1 << "\n\n";
        headerStream << "/* Number of all setup LinIf frames on all channels */\n";
        headerStream << "#define LINIF_FRAMES_CNT\t" << frameCount << "\n";
        headerStream << "/*===========[Configuration tables]===========================================*/\n\n/* Global module configuration */\n";
        headerStream << "extern const LinIf_ConfigType LinIf_Config;\n\n";
        headerStream << "#endif /* LINIF_CFG_H_ */\n";
        qFile.close();

        // ----------------------------------------- Create LinIf_Lcfg.c -----------------------------------------
        fileName = location + "/LinIf_Lcfg.c";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create LinIf_Lcfg.c file.\n";
            return false;
        }
        QTextStream lConfigStream( &qFile );
        lConfigStream << "\n/* File: LinIf_Lcfg.c\n * Link time configurable parameters.\n *\n";
        lConfigStream << " * Note: All time declarations are given in milliseconds.\n *       Time declarations are: LinIfTimeBase, LinIfDelay.\n */\n\n";
        lConfigStream << "#include \"LinIf.h\"\n";
        lConfigStream << "/*-----------[LinIf Frames]---------------------------------------------------*/\n\n";
        lConfigStream << "const LinIf_FrameType LinIfFrameCfg[LINIF_FRAMES_CNT] =\n{\n";
        int runningTotal = 0;
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            for (int frIter = 0; frIter < LinIfFrames[chIter].size(); frIter++) {
                lConfigStream << "\t/* " << LinIfFrames[chIter][frIter].firstChildElement("SHORT-NAME").text() << " on channel " << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n\t{\n";
                parameterValue = GET_VALUE_OF_PARAM(LinIfFrames[chIter][frIter], "LinIfChecksumType");
                lConfigStream << "\t\t.LinIfChecksumType = " << parameterValue << ",\n";
                lConfigStream << "\t\t.LinIfFrameName = NULL_PTR, // not used\n\t\t.LinIfFramePriority = 0,        // not used\n\t\t.LinIfFrameType = UNCONDITIONAL,\n";
                parameterValue = GET_VALUE_OF_PARAM(LinIfFrames[chIter][frIter], "LinIfLength");
                lConfigStream << "\t\t.LinIfLength = " << parameterValue << ",\n";
                parameterValue = GET_VALUE_OF_PARAM(LinIfFrames[chIter][frIter], "LinIfPid");
                lConfigStream << "\t\t.LinIfPid = " << parameterValue << ",\n";
                lConfigStream << "\t\t.LinIfTxTargetPduId = PDUR_PDU_ID_" << pduNames[runningTotal + frIter] << ",\n";
                lConfigStream << "\t\t.LinIfFixedFrameSdu = NULL_PTR, // not used\n";
                lConfigStream << "\t\t.LinIfPduDirection = " << pduDirection[runningTotal + frIter] << "\n";
                lConfigStream << "\t},\n";
            }
            runningTotal += LinIfFrames[chIter].size();
        }
        lConfigStream << "};\n\n";
        bool ok;
        lConfigStream << "/*-----------[Entries for each schedule table]--------------------------------*/\n\n";
        int schedIndex = 1;
        QList <QList <QDomElement> > LinIfEntries;
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            for (int schIter = 0; schIter < LinIfScheduleTables[chIter].size(); schIter++) {
                lConfigStream << "/* Entries in schedule table " << LinIfScheduleTables[chIter][schIter].firstChildElement("SHORT-NAME").text()
                              << " on channel " << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
                QList <QDomElement> tempLinIfEntries;
                AutosarToolFactory::FindReferencesToElement(&tempLinIfEntries, &LinIfScheduleTables[chIter][schIter], "LinIfEntry");
                LinIfEntries.push_back(tempLinIfEntries);
                lConfigStream << "const LinIfEntryType LinIfScheduleTable_Entries_" << schedIndex << "[" << tempLinIfEntries.size() << "] =\n{\n";
                for (int enIter = 0; enIter < tempLinIfEntries.size(); enIter++) {
                    parameterValue = GET_VALUE_OF_PARAM(tempLinIfEntries[enIter], "LinIfDelay");
                    float valueFloat = parameterValue.toFloat(&ok);
                    if (!ok)
                        valueFloat = 0;
                    int valueInt = int(valueFloat * 1000);
                    lConfigStream << "\t{\n\t\t.LinIfDelay = " << valueInt << ",\n";
                    parameterValue = GET_VALUE_OF_PARAM(tempLinIfEntries[enIter], "LinIfEntryIndex");
                    lConfigStream << "\t\t.LinIfEntryIndex = " << parameterValue << ",\n";
                    lConfigStream << "\t\t.LinIfCollisionResolvingRef = (uint16) -1, // not used\n";
                    parameterValue = GET_VALUE_REF_OF_REF(tempLinIfEntries[enIter], "LinIfFrameRef");
                    // Look for the frame
                    parameterValue = parameterValue.split("/").last();
                    int frIter;
                    for (frIter = 0; frIter < LinIfFrames[chIter].size(); frIter++) {
                        if (LinIfFrames[chIter][frIter].firstChildElement("SHORT-NAME").text() == parameterValue) {
                            break;
                        }
                    }
                    if (frIter >= LinIfFrames[chIter].size()) {
                        std::cout << "Error: Unable to find the LinIf Frame \'" << parameterValue.toStdString() << "\'.\n";
                        return false;
                    }
                    lConfigStream << "\t\t.LinIfFrameRef = " << frIter << "   // -> " << parameterValue << "\n";
                    lConfigStream << "\t},\n";
                }
                lConfigStream << "};\n";
                schedIndex++;
            }
        }
        lConfigStream << "\n\n/*-----------[Schedule tables]------------------------------------------------*/\n/* All schedule tables on all channels.\n";
        lConfigStream << " * The schedule table with index 0 is the NULL_SCHEDULE table which is\n * autogenerated. */\n\n";
        lConfigStream << "const LinIf_ScheduleTableType LinIfScheduleTableCfg[LINIF_SCH_CNT] =\n{\n";
        lConfigStream << "\t/* NULL_SCHEDULE */\n\t[NULL_SCHEDULE] =\t\n\t{\n";
        lConfigStream << "\t\t.LinIfResumePosition = START_FROM_BEGINNING,\n\t\t.LinIfRunMode = RUN_CONTINUOUS,\n\t\t.LinIfSchedulePriority = 1,\n\t\t.LinIfScheduleTableIndex = NULL_SCHEDULE,\n";
        lConfigStream << "\t\t.LinIfScheduleTableName = \"NULL_SCHEDULE\",\n\t\t/* Null schedule table has 0 entries */\n\t\t.LinIfEntry = NULL_PTR,\n\t\t.LinIfNofEntries = 0,\n\t},\n";
        schedIndex = 1;
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            for (int schIter = 0; schIter < LinIfScheduleTables[chIter].size(); schIter++) {
                QString schedName = LinIfScheduleTables[chIter][schIter].firstChildElement("SHORT-NAME").text();
                lConfigStream << "\t/* " << schedName << " on channel " << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
                lConfigStream << "\t[LinIfConf_LinIfScheduleTable_" << schedName << "] =\n\t{\n";
                lConfigStream << "\t\t.LinIfResumePosition = START_FROM_BEGINNING,\n\t\t.LinIfRunMode = RUN_CONTINUOUS,\n\t\t.LinIfSchedulePriority = 1,        // not used\n";
                lConfigStream << "\t\t.LinIfScheduleTableIndex = LinIfConf_LinIfScheduleTable_" << schedName << ",\n";
                lConfigStream << "\t\t.LinIfScheduleTableName = NULL_PTR, // not used\n";
                lConfigStream << "\t\t.LinIfEntry = LinIfScheduleTable_Entries_" << schedIndex << ",\n";
                lConfigStream << "\t\t.LinIfNofEntries = " << LinIfEntries[schIter].size();
                lConfigStream << "\n\t},\n";
            }
        }
        lConfigStream << "};\n\n/*-----------[Channels]-------------------------------------------------------*/\n/* LinIf Channel configurations.\n */\n\n";
        lConfigStream << "const LinIf_ChannelType LinIfChannelCfg[LINIF_CHANNEL_COUNT] =\n{\n";
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            parameterValue = GET_VALUE_REF_OF_REF(LinIfChannels[chIter], "LinIfChannelRef");
            lConfigStream << "\t/* " << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << ", referencing Lin channel " << parameterValue.split("/").last() << " */\n\t{\n";
            lConfigStream << "\t\t.LinIfLinChannelId = LinConf_LinChannel_" << parameterValue.split("/").last() << ",\n";
            lConfigStream << "\t\t.LinIfScheduleRequestQueueLength = 1,        // not used\n\t\t.LinIfFrame = NULL_PTR, // not used\n\t\t.LinIfMaster = {.LinIfJitter = 0},	//not used\n";
            lConfigStream << "\t\t.LinIfScheduleTable = &(LinIfScheduleTableCfg[NULL_SCHEDULE]),\n\t\t.LinIfSlave = NULL_PTR, // Master only\n\t\t.LinIfWakeUpSource = NULL_PTR,  // Not needed\n";
            lConfigStream << "\t\t.LinIfComMHandle = " << ComMChannelIds[chIter] << ",\n";
            parameterValue = GET_VALUE_OF_PARAM(LinIfChannels[chIter], "LinIfStartupState");
            lConfigStream << "\t\t.LinIfStartupState = LINIF_CHANNEL_" << parameterValue << "\n";
            lConfigStream << "\t},\n";
        }
        lConfigStream << "};\n\n/* Map ComM channel Id to the corresponding CanSM index */\n";
        lConfigStream << "const NetworkHandleType ComMHandleToLinIfIndex[" << ComMChannels.size() << "] = {\n";
        for (int chIter = 0; chIter < LinIfChannels.size(); chIter++) {
            lConfigStream << "\t[" << ComMChannelIds[chIter] << "u] = " << chIter << "u,/* "
                          << ComMHandles[chIter].firstChildElement("SHORT-NAME").text() << " -> " << LinIfChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
        }
        lConfigStream << "};\n\n/*-----------[Module configuration]-------------------------------------------*/\n\n";
        lConfigStream << "const LinIf_ConfigType LinIf_Config =\n{\n";
        parameterValue = GET_VALUE_OF_PARAM(LinIfGlobalConfig, "LinIfTimeBase");
        float valueFloat = parameterValue.toFloat(&ok);
        int valueInt = int(valueFloat * 1000);
        lConfigStream << "\t/* " << valueInt << " milliseconds timebase */\n";
        lConfigStream << "\t.LinIfTimeBase = " << valueInt << ",\n\n";
        lConfigStream << "\t/* list of all " << LinIfChannels.size() << " channels */\n";
        lConfigStream << "\t.LinIfChannel = LinIfChannelCfg,\n\n\t/*Pointer to Frame configuration */\n\t.LinIfFrameConfig = LinIfFrameCfg,\n\n\t.LinIfChannelMap = ComMHandleToLinIfIndex,\n";
        lConfigStream << "\t.LinIfChannelMapSize = " << ComMChannels.size() << ",\n};\n\n";
        qFile.close();

        // ----------------------------------------- Create LinIf_PBcfg.c -----------------------------------------
        fileName = location + "/LinIf_PBcfg.c";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create LinIf_PBcfg.c file.\n";
            return false;
        }
        QTextStream pbConfigStream( &qFile );
        pbConfigStream << "\n/* File: LinIf_PBcfg.c\n * Post build time configurable parameters.\n */\n\n";
        qFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool LinIfGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}

