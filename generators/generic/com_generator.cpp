/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "com_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool ComGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of COM Module Configuration files does not exist.\n";
        return false;
    }

    try {
			// ----------------------------------------- Create Makefile -----------------------------------------
            QString fileName = location + "/Com.mk";
            QFile qFile(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create Com.mk file.\n";
				  return false;
			}
            QTextStream makeFileStream( &qFile );
            makeFileStream << "MOD_USE += COM\n";
            qFile.close();

            // ----------------------------------------- Create Com_Cfg.h -----------------------------------------
            fileName = location + "/Com_Cfg.h";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create Com_Cfg.h file.\n";
				  return false;
			}
            QTextStream headerStream( &qFile );
            headerStream << "\n#ifndef COM_CFG_H\n#define COM_CFG_H\n\n";
            headerStream << "#if !(((COM_SW_MAJOR_VERSION == 1) && (COM_SW_MINOR_VERSION == 1)) )\n";
            headerStream << "#error Com: Configuration file expected BSW module version to be 1.1.*\n#endif\n\n";
            headerStream << "#if !(((COM_AR_RELEASE_MAJOR_VERSION == 4) && (COM_AR_RELEASE_MINOR_VERSION == 0)) )\n";
            headerStream << "#error Com: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n";
            QDomElement ComGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "ComGeneral");
            QString parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComConfigurationUseDet");
            headerStream << "#define COM_DEV_ERROR_DETECT\t";
            if (parameterValue == "1") {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n";
            parameterValue = GET_VALUE_OF_PARAM_WITH_RETURN(ComGeneral, "ArcComSignalGatewayEnable");
            headerStream << "#define COM_SIG_GATEWAY_ENABLE\t";
            if (parameterValue == "1") {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n";
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComVersionInfoApi");
            headerStream << "#define COM_VERSION_INFO_API\t";
            if (parameterValue == "1") {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n\n";
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComRamBufferSize");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ComRamBufferSize.\n";
                return false;
            } else {
                headerStream << "#define COM_MAX_BUFFER_SIZE\t" << parameterValue << "\n\n";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComSupportedIPdus");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ComSupportedIPdus.\n";
                return false;
            } else {
                headerStream << "#define COM_MAX_N_IPDUS\t" << parameterValue << "\n";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComSupportedSignals");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ComSupportedSignals.\n";
                return false;
            } else {
                headerStream << "#define COM_MAX_N_SIGNALS\t" << parameterValue << "\n";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComSupportedGroupSignals");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ComSupportedGroupSignals.\n";
                return false;
            } else {
                headerStream << "#define COM_MAX_N_GROUP_SIGNALS\t" << parameterValue << "\n";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ComSupportedIPduGroups");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ComSupportedIPduGroups.\n";
                return false;
            } else {
                headerStream << "#define COM_N_SUPPORTED_IPDU_GROUPS\t" << parameterValue << "\n";
            }
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ArcMaxNumberOfSupportedIPduCounters");
            headerStream << "#define COM_MAX_N_SUPPORTED_IPDU_COUNTERS\t" << parameterValue << "\n";
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ArcComSupportedGatewaySourceDescriptions");
            if (parameterValue.isEmpty()) {
                std::cout << "Error: Undefined COM::ComGeneral::ArcComSupportedGatewaySourceDescriptions.\n";
                return false;
            } else {
                headerStream << "#define COM_MAX_N_SUPPORTED_GWSOURCE_DESCRIPTIONS\t" << parameterValue << "\n";
            }
            headerStream << "\n";
            headerStream << "#define COM_E_INVALID_FILTER_CONFIGURATION\t101\n#define COM_E_INITIALIZATION_FAILED\t102\n#define COM_E_INVALID_SIGNAL_CONFIGURATION\t103\n";
            headerStream << "#define COM_INVALID_PDU_ID\t104\n#define COM_INVALID_SIGNAL_ID\t109\n#define COM_ERROR_SIGNAL_IS_SIGNALGROUP\t105\n\n";
            headerStream << "#define COM_E_TOO_MANY_IPDU\t106\n#define COM_E_TOO_MANY_SIGNAL\t107\n#define COM_E_TOO_MANY_GROUPSIGNAL\t108\n\n#define CPU_ENDIANESS\tCOM_BIG_ENDIAN\n\n";
            parameterValue = GET_VALUE_OF_PARAM(ComGeneral, "ArcMaxNumberOfSupportedIPduCounters");
            headerStream << "#define COM_IPDU_COUNTING_ENABLE\t";
            bool ok;
            int numOfIPduCounters = parameterValue.toInt(&ok);
            Q_ASSERT(ok);
            if (numOfIPduCounters > 0) {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n\n";
            headerStream << "//Invalid handle for gateway signal description\n#define INVALID_GWSIGNAL_DESCRIPTION_HANDLE\t0xFFFFu\n\n// Notifications\n";
            headerStream << "\n// Rx callouts\n";
            // No Clue
            headerStream << "\n// Tx callouts\n";
            // No Clue
            headerStream << "\n// Trigger transmit callouts\n\n";
            // No Clue
            headerStream << "#endif /*COM_CFG_H*/\n";
            qFile.close();

            // ----------------------------------------- Create Com_PbCfg.h -----------------------------------------
            fileName = location + "/Com_PbCfg.h";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create Com_PbCfg.h file.\n";
                  return false;
            }
            QTextStream pbHeaderStream( &qFile );
            pbHeaderStream << "\n#ifndef COM_PBCFG_H\n#define COM_PBCFG_H\n\n";
            pbHeaderStream << "#if !(((COM_SW_MAJOR_VERSION == 1) && (COM_SW_MINOR_VERSION == 1)) )\n";
            pbHeaderStream << "#error Com: Configuration file expected BSW module version to be 1.1.*\n#endif\n\n";
            pbHeaderStream << "#if !(((COM_AR_RELEASE_MAJOR_VERSION == 4) && (COM_AR_RELEASE_MINOR_VERSION == 0)) )\n";
            pbHeaderStream << "#error Com: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n#include \"Com_Types.h\"\n\n";
            pbHeaderStream << "// COM IPDU IDs\n";
            QDomElement ComConfig = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "ComConfig");
            QList <QDomElement> ComIPdus;
            AutosarToolFactory::FindReferencesToElement(&ComIPdus, &ComConfig, "ComIPdu");
            for (int pduIter = 0; pduIter < ComIPdus.size(); pduIter++) {
                pbHeaderStream << "#define ComConf_ComIPdu_" << ComIPdus.at(pduIter).firstChildElement("SHORT-NAME").text() << "\t" << pduIter << "\n";
            }
            pbHeaderStream << "\n// COM PDU GROUP IDs\n";
            QList <QDomElement> ComIPduGroups;
            AutosarToolFactory::FindReferencesToElement(&ComIPduGroups, &ComConfig, "ComIPduGroup");
            for (int pduGIter = 0; pduGIter < ComIPduGroups.size(); pduGIter++) {
                pbHeaderStream << "#define ComConf_ComIPduGroup_" << ComIPduGroups.at(pduGIter).firstChildElement("SHORT-NAME").text() << "\t" << pduGIter << "\n";
            }
            pbHeaderStream << "\n// COM SIGNAL IDs\n";
            QList <QDomElement> ComSignals;
            AutosarToolFactory::FindReferencesToElement(&ComSignals, &ComConfig, "ComSignal");
            for (int sigIter = 0; sigIter < ComSignals.size(); sigIter++) {
                pbHeaderStream << "#define ComConf_ComSignal_" << ComSignals.at(sigIter).firstChildElement("SHORT-NAME").text() << "\t" << sigIter << "\n";
            }

            pbHeaderStream << "\n// COM GROUP SIGNAL IDs\n";
            // No clue
            //pbHeaderStream << "#define " << "\t" << "\n";

            pbHeaderStream << "\n// COM GATEWAY SOURCE SIGNAL DESCRIPTION IDs\n";
            // No clue
            //pbHeaderStream << "#define " << "\t" << "\n";

            pbHeaderStream << "\n// COM GATEWAY DESTINATION SIGNAL DESCRIPTION IDs\n";
            // No clue
            //pbHeaderStream << "#define " << "\t" << "\n";

            pbHeaderStream << "\n// COM GATEWAY MAPPING IDs\n";
            // No clue
            //pbHeaderStream << "#define " << "\t" << "\n";

            pbHeaderStream << "\n#endif /* COM_PBCFG_H */\n";
            qFile.close();

            // ----------------------------------------- Com_Cfg.c -----------------------------------------
            fileName = location + "/Com_Cfg.c";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create Com_Cfg.c file.\n";
				  return false;
			}
            QTextStream configStream( &qFile );
            configStream << "\n#include \"Com.h\"\n\n// Notifications\nconst ComNotificationCalloutType ComNotificationCallouts [] = {\n";
            // No Clue
            configStream << "\tNULL\n};\n\n";
            configStream << "// Rx callouts\nconst ComRxIPduCalloutType ComRxIPduCallouts[] = {\n";
            // No Clue
            configStream << "\tNULL\n};\n\n";
            configStream << "// Tx callouts\nconst ComTxIPduCalloutType ComTxIPduCallouts[] = {\n";
            // No Clue
            configStream << "\tNULL\n};\n\n";
            configStream << "// Trigger transmit callouts\nconst ComTxIPduCalloutType ComTriggerTransmitIPduCallouts[] = {\n";
            // No Clue
            configStream << "\tNULL\n};\n\n";
            qFile.close();

            // ----------------------------------------- Com_PbCfg.c -----------------------------------------
            fileName = location + "/Com_PbCfg.c";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create Com_PbCfg.c file.\n";
                  return false;
            }
            QTextStream pbConfigStream( &qFile );
            pbConfigStream << "#if defined(USE_PDUR)\n#include \"PduR.h\"\n#include \"PduR_PbCfg.h\"\n#endif\n\n";
            pbConfigStream << "#if defined(USE_CANTP)\n#include \"CanTp.h\"\n#include \"CanTp_PBCfg.h\"\n#endif\n\n";
            pbConfigStream << "#include \"Com.h\"\n#include \"Com_PbCfg.h\"\n\n#include \"MemMap.h\"\n\n";
            int maxBufferSize = 0;
            QList <int> ipduLengths;
            for (int pduIter = 0; pduIter < ComIPdus.size(); pduIter++) {
                QString idRef = GET_VALUE_REF_OF_REF(ComIPdus[pduIter], "ComPduIdRef");
                if (idRef.isEmpty()) {
                    std::cout << "Error: Undefined reference Com::ComConfig::ComIPdu::ComPduIdRef in \'" << ComIPdus[pduIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                QDomElement pdu = AutosarToolFactory::GetDomElementFromReference(&idRef, projectModuleRoot->ownerDocument().documentElement());
                QString pduLength = GET_VALUE_OF_PARAM_WITH_RETURN(pdu, "PduLength");
                if (pduLength.isEmpty()) {
                    std::cout << "Error: Undefined PDU length in \'" << pdu.firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                int pduLen = pduLength.toInt(&ok);
                Q_ASSERT(ok);
                ipduLengths.push_back(pduLen);
                maxBufferSize += pduLen;
            }
            pbConfigStream << "#if (COM_MAX_BUFFER_SIZE < " << maxBufferSize << ")\n";
            pbConfigStream << "#error Com: The configured ram buffer size is less than required! (" << maxBufferSize << " bytes required)\n#endif\n";
            pbConfigStream << "#if (COM_MAX_N_IPDUS < " << ComIPdus.size() << ")\n";
            pbConfigStream << "#error Com: Configured maximum number of Pdus is less than the number of Pdus in configuration!\n#endif\n";
            int nSuppCounters = ComIPdus.size();
            for (int pduIter = 0; pduIter < ComIPdus.size(); pduIter++) {
                QString iPduCounter = GET_VALUE_OF_PARAM_WITH_RETURN(ComIPdus[pduIter], "ComIPduCounter");
                if (iPduCounter.isEmpty()) {
                    nSuppCounters--;
                }
            }
            pbConfigStream << "#if (COM_MAX_N_SUPPORTED_IPDU_COUNTERS < " << nSuppCounters << ")\n";
            pbConfigStream << "#error Com: Configured maximum number of Pdus with counter(ArcMaxNumberOfSupportedIPduCounters) is less than the number of Pdus configured with counter !\n#endif\n";
            pbConfigStream << "#if (COM_MAX_N_SIGNALS < " << ComSignals.size() << ")\n";
            pbConfigStream << "#error Com: Configured maximum number of signals is less than the number of signals in configuration!\n#endif\n";
            pbConfigStream << "#if (COM_MAX_N_GROUP_SIGNALS < 0)\n";
            pbConfigStream << "#error Com: Configured maximum number of groupsignals is less than the number of groupsignals in configuration!\n#endif\n";
            pbConfigStream << "#if (COM_MAX_N_SUPPORTED_GWSOURCE_DESCRIPTIONS < 0)\n";
            pbConfigStream << "#error Com: Configured maximum number of gateway source description is less than the number of ComGwSourceDescription in configuration!\n#endif\n\n";
            pbConfigStream << "/*\n * Signal init values.\n */\n";
            for (int sigIter = 0; sigIter < ComSignals.size(); sigIter++) {
                pbConfigStream << "SECTION_POSTBUILD_DATA const ";
                QString sigType = GET_VALUE_OF_PARAM_WITH_RETURN(ComSignals[sigIter], "ComSignalType");
                if (sigType.isEmpty()) {
                    pbConfigStream << "void";
                } else {
                    pbConfigStream << sigType.toLower();
                }
                pbConfigStream << " Com_SignalInitValue_" << ComSignals.at(sigIter).firstChildElement("SHORT-NAME").text() << " = ";
                QString sigInitValue = GET_VALUE_OF_PARAM_WITH_RETURN(ComSignals[sigIter], "ComSignalInitValue");
                if (sigInitValue.isEmpty()) {
                    pbConfigStream << "0";
                } else {
                    pbConfigStream << sigInitValue;
                }
                pbConfigStream << ";\n";
            }
            pbConfigStream << "\n\n/*\n * Group signal definitions\n */\nSECTION_POSTBUILD_DATA const ComGroupSignal_type ComGroupSignal[] = {\n\t{\n\t\t.Com_Arc_EOL = 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * SignalGroup GroupSignals lists.\n */\n\n";
            // No Clue
            pbConfigStream << "/*\n * Signal group masks.\n */\n\n";
            // No Clue
            pbConfigStream << "/*\n * Signal definitions\n */\n\nSECTION_POSTBUILD_DATA const ComSignal_type ComSignal[] = {\n";
            QDomElement tempElem;
            QString tempStr;
            for (int sigIter = 0; sigIter < ComSignals.size(); sigIter++) {
                tempStr = ComSignals.at(sigIter).firstChildElement("SHORT-NAME").text();
                pbConfigStream << "\n\t{ // " << tempStr << "\n";
                pbConfigStream << "\t\t.ComHandleId\t= ComConf_ComSignal_" << tempStr << ",\n";
                QDomElement pdu;
                for (int pduIter = 0; pduIter < ComIPdus.size(); pduIter++) {
                    QList<QDomElement> refs;
                    AutosarToolFactory::FindReferencesToElement(&refs, &ComIPdus[pduIter], "ComIPduSignalRef");
                    for (int refIter = 0; refIter < refs.size(); refIter++) {
                        tempElem = refs.at(refIter).firstChildElement("VALUE-REF");
                        if (tempElem.isNull()) {
                            continue;
                        }
                        QString valueRef = tempElem.text();
                        if (valueRef.isEmpty()) {
                            continue;
                        }
                        if (valueRef.split("/").last() == tempStr) {
                            pdu = ComIPdus[pduIter];
                            break;
                        }
                    }
                    if (!pdu.isNull()) {
                        break;
                    }
                }
                pbConfigStream << "\t\t.ComIPduHandleId\t= ComConf_ComIPdu_" << pdu.firstChildElement("SHORT-NAME").text() << ",\n";
                QString pduDirection = GET_VALUE_OF_PARAM_WITH_RETURN(pdu, "ComIPduDirection");
                if (pduDirection == "RECEIVE") {
                    pbConfigStream << "\t\t// @req COM292\n";
                }
                pbConfigStream << "\t\t.ComFirstTimeoutFactor\t= 0,\n";     // No clue how to calculate
                pbConfigStream << "\t\t.ComNotification\t= COM_NO_FUNCTION_CALLOUT,\n";
                pbConfigStream << "\t\t.ComTimeoutFactor\t= 0,\n";     // No clue how to calculate
                pbConfigStream << "\t\t.ComTimeoutNotification\t= COM_NO_FUNCTION_CALLOUT,\n";
                pbConfigStream << "\t\t.ComErrorNotification\t= COM_NO_FUNCTION_CALLOUT,\n";
                pbConfigStream << "\t\t.ComTransferProperty\t= COM_PENDING,\n";
                pbConfigStream << "\t\t.ComUpdateBitPosition\t= 0,\n";
                pbConfigStream << "\t\t.ComSignalArcUseUpdateBit\t= FALSE,\n";
                pbConfigStream << "\t\t.ComSignalInitValue\t= &Com_SignalInitValue_" << ComSignals.at(sigIter).firstChildElement("SHORT-NAME").text() << ",\n";
                QString bitPosition = GET_VALUE_OF_PARAM(ComSignals[sigIter], "ComBitPosition");
                pbConfigStream << "\t\t.ComBitPosition\t= " << bitPosition << ",\n";
                QString ComBitSize = GET_VALUE_OF_PARAM_WITH_RETURN(ComSignals[sigIter], "ComBitSize");
                if (ComBitSize.isEmpty()) {
                    std::cout << "Error: Undefined parameter Com::ComConfig::ComSignal::ComBitSize in \'" << ComSignals[sigIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                pbConfigStream << "\t\t.ComBitSize\t= " << ComBitSize << ",\n";
                QString endianness = GET_VALUE_OF_PARAM(ComSignals[sigIter], "ComSignalEndianness");
                pbConfigStream << "\t\t.ComSignalEndianess\t= COM_" << endianness << ",\n";
                QString signalType = GET_VALUE_OF_PARAM_WITH_RETURN(ComSignals[sigIter], "ComSignalType");
                if (signalType.isEmpty()) {
                    std::cout << "Error: Undefined parameter Com::ComConfig::ComSignal::ComSignalType in \'" << ComSignals[sigIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                pbConfigStream << "\t\t.ComSignalType\t= COM_" << signalType << ",\n";
                pbConfigStream << "\t\t.Com_Arc_IsSignalGroup\t= 0,\n\t\t.ComGroupSignal\t= NULL,\n";
                QString rxTimeoutAction = GET_VALUE_OF_PARAM_WITH_RETURN(ComSignals[sigIter], "ComRxDataTimeoutAction");
                if (rxTimeoutAction.isEmpty() || rxTimeoutAction == "NONE") {
                    pbConfigStream << "\t\t.ComRxDataTimeoutAction\t= COM_TIMEOUT_DATA_ACTION_NONE,\n";
                } else {
                    // No clue
                }
                pbConfigStream << "\t\t.ComSigGwRoutingReq\t= FALSE,\n";
                pbConfigStream << "\t\t.Com_Arc_EOL\t= 0\n\t},\n";
            }
            pbConfigStream << "\n\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * Gateway source signal description definitions\n */\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const ComGwSrcDesc_type ComGwSourceDescs[] = {\n";
            pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * Gateway destination signal description definitions\n */\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const ComGwDestnDesc_type ComGwDestinationDescs[] = {\n";
            pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * Gateway Destination signal references\n */\n\n";
            pbConfigStream << "/*\n * Gateway mappings\n */\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const ComGwMapping_type ComGwMapping[] = {\n";
            pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * I-PDU group definitions\n */\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const ComIPduGroup_type ComIPduGroup[] = {\n";
            for (int grIter = 0; grIter < ComIPduGroups.size(); grIter++) {
                pbConfigStream << "\t{\n\t\t.ComIPduGroupHandleId\t= ComConf_ComIPduGroup_" << ComIPduGroups.at(grIter).firstChildElement("SHORT-NAME").text() << ",\n\t\t.Com_Arc_EOL\t= 0\n\t},\n";
            }
            pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "/*\n * IPdu signal lists.\n */\n";
            for (int ipduIter = 0; ipduIter < ComIPdus.size(); ipduIter++) {
                pbConfigStream << "\nSECTION_POSTBUILD_DATA const ComSignal_type * const ComIPduSignalRefs_" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text() << "[] = {\n";
                QList<QDomElement> refs;
                AutosarToolFactory::FindReferencesToElement(&refs, &ComIPdus[ipduIter], "ComIPduSignalRef");
                for (int refIter = 0; refIter < refs.size(); refIter++) {
                    tempElem = refs.at(refIter).firstChildElement("VALUE-REF");
                    if (tempElem.isNull()) {
                        continue;
                    }
                    QString valueRef = tempElem.text();
                    if (valueRef.isEmpty()) {
                        continue;
                    }
                    pbConfigStream << "\t&ComSignal[ComConf_ComSignal_" << valueRef.split("/").last() << "],\n";
                }
                pbConfigStream << "\tNULL\n};\n";
            }
            pbConfigStream << "\n/*\n * IPdu Counter list\n */\n\n";
            pbConfigStream << "/*\n * I-PDU group ref lists\n */\n";
            for (int ipduIter = 0; ipduIter < ComIPdus.size(); ipduIter++) {
                pbConfigStream << "SECTION_POSTBUILD_DATA const ComIPduGroup_type ComIpduGroupRefs_" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text() << "[] = {\n";
                QString idRef = GET_VALUE_REF_OF_REF(ComIPdus[ipduIter], "ComIPduGroupRef");
                if (idRef.isEmpty()) {
                    std::cout << "Error: Undefined reference Com::ComConfig::ComIPdu::ComIPduGroupRef in \'" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                pbConfigStream << "\t{\n\t\t.ComIPduGroupHandleId\t= ComConf_ComIPduGroup_" << idRef.split("/").last() << ",\n\t\t.Com_Arc_EOL\t= 0\n\t},\n";
                pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n";
            }
            pbConfigStream << "/*\n * Gateway signal description handle\n */\n\n";
            pbConfigStream << "/*\n * I-PDU definitions\n */\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const ComIPdu_type ComIPdu[] = {\n";
            for (int ipduIter = 0; ipduIter < ComIPdus.size(); ipduIter++) {
                pbConfigStream << "\t{ // " << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text() << "\n";
                QString pduDirection = GET_VALUE_OF_PARAM(ComIPdus[ipduIter], "ComIPduDirection");
                QString ComPduIdRef = GET_VALUE_REF_OF_REF(ComIPdus[ipduIter], "ComPduIdRef");
                if (pduDirection == "RECEIVE") {
                    pbConfigStream << "\t\t.ArcIPduOutgoingId\t= PDUR_REVERSE_PDU_ID_" << ComPduIdRef.split("/").last().toUpper() << ",\n";
                } else {
                    pbConfigStream << "\t\t.ArcIPduOutgoingId\t= PDUR_PDU_ID_" << ComPduIdRef.split("/").last().toUpper() << ",\n";
                }
                pbConfigStream << "\t\t.ComRxIPduCallout\t= COM_NO_FUNCTION_CALLOUT,\n";
                pbConfigStream << "\t\t.ComTxIPduCallout\t= COM_NO_FUNCTION_CALLOUT,\n";
                QString triggerCallout = GET_VALUE_OF_PARAM_WITH_RETURN(ComIPdus[ipduIter], "ComIPduTriggerTransmitCallout");
                if (triggerCallout == "NULL") {
                    pbConfigStream << "\t\t.ComTriggerTransmitIPduCallout = COM_NO_FUNCTION_CALLOUT,\n";
                } else {
                    // No Clue
                }
                QString sigProc = GET_VALUE_OF_PARAM_WITH_RETURN(ComIPdus[ipduIter], "ComIPduSignalProcessing");
                pbConfigStream << "\t\t.ComIPduSignalProcessing\t= COM_" << sigProc << ",\n";
                pbConfigStream << "\t\t.ComIPduSize\t= " << ipduLengths[ipduIter] << ",\n";
                pbConfigStream << "\t\t.ComIPduDirection\t= COM_" << pduDirection << ",\n";
                pbConfigStream << "\t\t.ComIPduGroupRefs\t= ComIpduGroupRefs_" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text() << ",\n";
                pbConfigStream << "\t\t.ComTxIPdu = {\n";
                if (pduDirection == "RECEIVE") {
                    pbConfigStream << "\t\t\t.ComTxIPduMinimumDelayFactor\t= 0,\n";
                    pbConfigStream << "\t\t\t.ComTxIPduUnusedAreasDefault\t= 0,\n";
                    pbConfigStream << "\t\t\t.ComTxModeTrue = {\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeMode\t= COM_NONE,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeNumberOfRepetitions\t= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeRepetitionPeriodFactor\t= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeTimeOffsetFactor\t= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeTimePeriodFactor\t= 0,\n\t\t\t},\n";
                } else {
                    pbConfigStream << "\t\t\t.ComTxIPduMinimumDelayFactor\t= 0,\n";
                    QDomElement txIpdu = AutosarToolFactory::FindFirstReferenceToElement(&ComIPdus[ipduIter], "ComTxIPdu");
                    QString unusedAreas = GET_VALUE_OF_PARAM(txIpdu, "ComTxIPduUnusedAreasDefault");
                    if (unusedAreas.isEmpty()) {
                        pbConfigStream << "\t\t\t.ComTxIPduUnusedAreasDefault\t= 0,\n";
                    } else {
                        pbConfigStream << "\t\t\t.ComTxIPduUnusedAreasDefault\t= " << unusedAreas << ",\n";
                    }
                    pbConfigStream << "\t\t\t.ComTxModeTrue = {\n";
                    QDomElement modeTrue = AutosarToolFactory::FindFirstReferenceToElement(&txIpdu, "ComTxModeTrue");
                    if (modeTrue.isNull()) {
                        std::cout << "Error: Undefined mode Com::ComConfig::ComIPdu::ComTxIPdu::ComTxModeTrue in \'" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                        return false;
                    }
                    QDomElement mode = AutosarToolFactory::FindFirstReferenceToElement(&modeTrue, "ComTxMode");
                    if (mode.isNull()) {
                        std::cout << "Error: Undefined mode Com::ComConfig::ComIPdu::ComTxIPdu::ComTxModeTrue::ComTxMode in \'" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                        return false;
                    }
                    QString modeMode = GET_VALUE_OF_PARAM(mode, "ComTxModeMode");
                    if (modeMode.isEmpty()) {
                        std::cout << "Error: Undefined parameter Com::ComConfig::ComIPdu::ComTxIPdu::ComTxModeTrue::ComTxMode::ComTxModeMode in \'" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                        return false;
                    }
                    pbConfigStream << "\t\t\t\t.ComTxModeMode\t= COM_" << modeMode << ",\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeNumberOfRepetitions\t= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeRepetitionPeriodFactor	= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeTimeOffsetFactor\t= 0,\n";
                    pbConfigStream << "\t\t\t\t.ComTxModeTimePeriodFactor\t= 0,\n\t\t\t},\n";
                }
                pbConfigStream << "\t\t},\n";
                pbConfigStream << "\t\t.ComIPduSignalRef\t= ComIPduSignalRefs_" << ComIPdus[ipduIter].firstChildElement("SHORT-NAME").text() << ",\n";
                pbConfigStream << "\t\t.ComIPduDynSignalRef\t= NULL,\n";
                pbConfigStream << "\t\t.ComIpduCounterRef\t= NULL,\n";
                pbConfigStream << "\t\t.ComIPduGwMapSigDescHandle\t= NULL,\n";
                pbConfigStream << "\t\t.ComIPduGwRoutingReq\t= FALSE,\n";
                pbConfigStream << "\t\t.Com_Arc_EOL\t= 0\n\t},\n\n";
            }
            pbConfigStream << "\t{\n\t\t.Com_Arc_EOL\t= 1\n\t}\n};\n\n";
            pbConfigStream << "SECTION_POSTBUILD_DATA const Com_ConfigType ComConfiguration = {\n";
            pbConfigStream << "\t.ComConfigurationId\t= 1,\n";
            pbConfigStream << "\t.ComNofIPdus\t= " << ComIPdus.size() << ",\n";
            pbConfigStream << "\t.ComNofSignals\t= " << ComSignals.size() << ",\n";
            pbConfigStream << "\t.ComNofGroupSignals\t= 0,\n";
            pbConfigStream << "\t.ComIPdu\t= ComIPdu,\n\t.ComIPduGroup\t= ComIPduGroup,\n\t.ComSignal\t= ComSignal,\n\t.ComGroupSignal\t= ComGroupSignal,\n";
            pbConfigStream << "\t.ComGwMappingRef\t= ComGwMapping,\n\t.ComGwSrcDesc\t= ComGwSourceDescs,\n\t.ComGwDestnDesc\t= ComGwDestinationDescs\n};\n\n";
            qFile.close();

    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool ComGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
		bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
					QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
						if (tempElem.isNull()) {
						    std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
				                         AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
						    returnValue = false;
						}
				}
    }

    return returnValue;
}

