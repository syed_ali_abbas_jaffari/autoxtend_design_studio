/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "linsm_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool LinSMGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of LinSM Module Configuration files does not exist.\n";
        return false;
    }

    try {
			// ----------------------------------------- Create Makefile -----------------------------------------
            QString fileName = location + "/LinSM.mk";
            QFile qFile(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create LinSM.mk file.\n";
				  return false;
			}
            QTextStream makeFileStream( &qFile );
            makeFileStream << "MOD_USE += LINSM\n";
            qFile.close();

            // ----------------------------------------- Create LinSM_Cfg.h -----------------------------------------
            fileName = location + "/LinSM_Cfg.h";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create LinSM_Cfg.h file.\n";
				  return false;
			}
            QTextStream headerStream( &qFile );
            headerStream << "\n#if !(((LINSM_SW_MAJOR_VERSION == 2) && (LINSM_SW_MINOR_VERSION == 4)) )\n";
            headerStream << "#error LinSM: Configuration file expected BSW module version to be 2.4.X*\n#endif\n\n";
            headerStream << "/* @req LINSM209 */\n#if !(((LINSM_AR_RELEASE_MAJOR_VERSION == 4) && (LINSM_AR_RELEASE_MINOR_VERSION == 0)) )\n";
            headerStream << "#error LinSM: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n";
            headerStream << "/* File: LinSM_Cfg.h\n * Declarations of the pre-compile time configurable parameters.\n */\n\n";
            headerStream << "#ifndef LINSM_CFG_H_\n#define LINSM_CFG_H_\n\n";
            headerStream << "#include \"LinSM_ConfigTypes.h\"\n\n";
            headerStream << "/*===========[LinSMGeneral]===================================================*/\n\n";
            headerStream << "/* Switches the Development Error Detection and Notification ON or OFF. */\n";
            QDomElement LinSMGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinSMGeneral");
            headerStream << "#define LINSM_DEV_ERROR_DETECT ";
            QString parameterValue = GET_VALUE_OF_PARAM(LinSMGeneral, "LinSMDevErrorDetect");
            if (parameterValue == "1") {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n\n/* Switches the LINSM_GetVersionInfo function ON or OFF. */\n";
            headerStream << "#define LINSM_VERSION_INFO_API ";
            parameterValue = GET_VALUE_OF_PARAM(LinSMGeneral, "LinSMVersionInfoApi");
            if (parameterValue == "1") {
                headerStream << "STD_ON";
            } else {
                headerStream << "STD_OFF";
            }
            headerStream << "\n\n/* LinSMMainProcessingPeriod in milliseconds */\n";
            parameterValue = GET_VALUE_OF_PARAM_WITH_RETURN(LinSMGeneral, "LinSMMainProcessingPeriod");
            if (parameterValue.isEmpty())
                parameterValue = "0";
            bool ok;
            float duration = parameterValue.toFloat(&ok);
            Q_ASSERT(ok);
            headerStream << "#define LINSM_MAIN_PROCESSING_PERIOD " << duration * 1000 << "\n\n";
            headerStream << "/*===========[Constants and sizes]============================================*/\n\n";
            headerStream << "/* Number of all setup LinSm channels */\n";
            QDomElement LinSMConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinSMConfigSet");
            QList <QDomElement> LinSMChannels;
            AutosarToolFactory::FindReferencesToElement(&LinSMChannels, &LinSMConfigSet, "LinSMChannel");
            headerStream << "#define LINSM_CHANNEL_CNT " << LinSMChannels.size() << "\n\n";
            headerStream << "/* Number of all setup LinSMSchedule objects */\n";
            QList < QList <QDomElement> > LinSMSchedules;
            int totalSchedulesCount = 0;
            for (int chIter = 0; chIter < LinSMChannels.size(); chIter++) {
                QList <QDomElement> tempSchedules;
                AutosarToolFactory::FindReferencesToElement(&tempSchedules, &LinSMChannels[chIter], "LinSMSchedule");
                totalSchedulesCount += tempSchedules.size();
                LinSMSchedules.push_back(tempSchedules);
            }
            headerStream << "#define LINSM_SCHEDULE_CNT " << totalSchedulesCount << "\n\n";
            headerStream << "/*===========[LinSMSchedule IDs]==============================================*/\n\n";
            headerStream << "/* Symbolic name reference to LinIfScheduleTable. */\n\n";
            for (int chIter = 0; chIter < LinSMChannels.size(); chIter++) {
                headerStream << "/* LinSMSchedule objects assigned to channel " << LinSMChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
                for (int schIter = 0; schIter < LinSMSchedules[chIter].size(); schIter++) {
                    parameterValue = GET_VALUE_REF_OF_REF(LinSMSchedules[chIter][schIter], "LinSMScheduleIndexRef");
                    if (parameterValue.isEmpty()) {
                        std::cout << "Error: Undefined reference LinSM::LinSMConfigSet::LinSMChannel::LinSMSchedule::LinSMScheduleIndexRef in " <<
                                     LinSMSchedules[chIter][schIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                        return false;
                    }
                    headerStream << "#define LinSMConf_LinSMSchedule_" << LinSMSchedules[chIter][schIter].firstChildElement("SHORT-NAME").text() << " LinIfConf_LinIfScheduleTable_"
                                 << parameterValue.split("/").last() << "\n";
                }
            }
            headerStream << "\n/*===========[LinSM configuration data]=======================================*/\n\n";
            headerStream << "/* LinSM module configuration. */\nextern const LinSM_ConfigType LinSM_Config;\n\n";
            headerStream << "#endif /*LINSM_CFG_H_*/\n";
            qFile.close();

            // ----------------------------------------- Create LinSM_Lcfg.c -----------------------------------------
            fileName = location + "/LinSM_Lcfg.c";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create LinSM_Lcfg.c file.\n";
				  return false;
			}
            QTextStream lConfigStream( &qFile );
            lConfigStream << "\n/* File: LinSM_Lcfg.c\n * Link time configurable parameters.\n */\n\n";
            lConfigStream << "#include \"LinSM_ConfigTypes.h\" /* Configuration types */\n#include \"LinSM.h\" /* symbolic indexes */\n#include \"LinIf.h\"\n\n";
            lConfigStream << "/*===========[LinSMSchedules]=================================================*/\n\n";
            for (int chIter = 0; chIter < LinSMChannels.size(); chIter++) {
                lConfigStream << "/* LinSMSchedule objects assigned to channel " << LinSMChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
                lConfigStream << "const LinSM_ScheduleType LinSMSchedules_" << LinSMChannels[chIter].firstChildElement("SHORT-NAME").text() << "[" << LinSMSchedules[chIter].size() << "] =\n{\n";
                for (int schIter = 0; schIter < LinSMSchedules[chIter].size(); schIter++) {
                    parameterValue = GET_VALUE_REF_OF_REF(LinSMSchedules[chIter][schIter], "LinSMScheduleIndexRef");
                    lConfigStream << "\t{\n\t\t.LinSMScheduleIndex = LinIfConf_LinIfScheduleTable_" << parameterValue.split("/").last() << "\n\t},\n";
                }
                lConfigStream << "};\n";
            }
            lConfigStream << "\n/*===========[LinSMChannels]==================================================*/\n";
            lConfigStream << "const LinSM_ChannelType LinSMChannels[LINSM_CHANNEL_CNT] =\n{\n";
            // Get ComM channel index (MODIFY_POINT when indices are modified).
            QDomElement document = projectModuleRoot->ownerDocument().documentElement();
            QDomElement ComM = AutosarToolFactory::FindFirstReferenceToElement(&document, "ComM");
            QDomElement ComMConfigSet = AutosarToolFactory::FindFirstReferenceToElement(&ComM, "ComMConfigSet");
            QList <QDomElement> ComMChannels;
            AutosarToolFactory::FindReferencesToElement(&ComMChannels, &ComMConfigSet, "ComMChannel");
            for (int chIter = 0; chIter < LinSMChannels.size(); chIter++) {
                /*«LET channel.LinSMComMNetworkHandleRef.value
                         .getLinIfChannelByComMChannel(channel.LinSMSchedules.first().LinSMScheduleIndexRef.value.parent.parent.LinIfChannels)
                         AS LinIfChannel -»
                        «ENDLET -»*/
                lConfigStream << "\t/* " << LinSMChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n\t{\n";
                parameterValue = GET_VALUE_OF_PARAM(LinSMChannels[chIter], "LinSMConfirmationTimeout");
                duration = parameterValue.toFloat(&ok);
                Q_ASSERT(ok);
                lConfigStream << "\t\t.LinSMConfirmationTimeout = " << duration * 1000 << ",\n";
                lConfigStream << "\t\t.LinSMSleepSupport = FALSE,\n\t\t.LinSMTransceiverPassiveMode = FALSE,\n";
                parameterValue = GET_VALUE_REF_OF_REF(LinSMChannels[chIter], "LinSMComMNetworkHandleRef");
                if (parameterValue.isEmpty()) {
                    std::cout << "Error: Undefined reference LinSM::LinSMConfigSet::LinSMChannel::LinSMComMNetworkHandleRef in " <<
                                 LinSMChannels[chIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                    return false;
                }
                QDomElement LinSMComMNetworkHandle = AutosarToolFactory::GetDomElementFromReference(&parameterValue, projectModuleRoot->ownerDocument().documentElement());
                if (LinSMComMNetworkHandle.isNull()) {
                    std::cout << "Error: Invalid reference LinSM::LinSMConfigSet::LinSMChannel::LinSMComMNetworkHandleRef in " <<
                                 LinSMChannels[chIter].firstChildElement("SHORT-NAME").text().toStdString() << ".\n";
                    return false;
                }
                int channelIndex = -1;
                for (int comChIter = 0; comChIter < ComMChannels.size(); comChIter++) {
                    if (LinSMComMNetworkHandle == ComMChannels[comChIter]) {
                        channelIndex = comChIter;
                        break;
                    }
                }
                if (channelIndex == -1) {
                    std::cout << "Error: Unable to find ComMChannel \'" << parameterValue.toStdString() << "\' in ComM module.\n";
                    return false;
                }
                lConfigStream << "\t\t.LinSMComMNetworkHandleRef = " << channelIndex << ",/*ComMConf_ComMChannel_" << LinSMComMNetworkHandle.firstChildElement("SHORT-NAME").text() << " */\n";
                lConfigStream << "\t\t.LinSMSchedule_Cnt = " << LinSMSchedules[chIter].size() << ",\n";
                lConfigStream << "\t\t.LinSMSchedules = LinSMSchedules_" << LinSMChannels[chIter].firstChildElement("SHORT-NAME").text() << ",\n\t},\n";
            }
            lConfigStream << "};\n\n/*===========[LinSM configuration]============================================*/\n";
            lConfigStream << "const LinSM_ConfigType LinSM_Config =\n{\n\t.LinSMChannels = LinSMChannels,\n\t.LinSMChannels_Size = LINSM_CHANNEL_CNT\n};\n\n";
            qFile.close();

            // ----------------------------------------- Create LinSM_Cfg.c -----------------------------------------
            fileName = location + "/LinSM_Cfg.c";
            qFile.setFileName(fileName);
            if (!qFile.open(QIODevice::WriteOnly)) {
                  std::cout << "Error: Unable to create LinSM_Cfg.c file.\n";
                  return false;
            }
            QTextStream configStream( &qFile );
            configStream << "/* File: LinSM_Cfg.c\n * Pre compile parameters that are \"const\".\n */\n\n/* Empty file */\n\n";
            qFile.close();

    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool LinSMGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}

