/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "pdur_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool PdurGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of PduR Module Configuration files does not exist.\n";
        return false;
    }

    try {
        // ----------------------------------------- Create Makefile -----------------------------------------
        QString fileName = location + "/PduR.mk";
        QFile qFile(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create PduR.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &qFile );
        makeFileStream << "MOD_USE += PDUR\n";
        qFile.close();

        // ----------------------------------------- Create PduR_Cfg.h -----------------------------------------
        fileName = location + "/PduR_Cfg.h";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create PduR_Cfg.h file.\n";
            return false;
        }
        QTextStream cfgHeaderStream( &qFile );
        cfgHeaderStream << "\n#ifndef PDUR_CFG_H_\n#define PDUR_CFG_H_\n\n";
        cfgHeaderStream << "#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 2)) )\n#error PduR: Configuration file expected BSW module version to be 2.2.*\n#endif\n\n";
        cfgHeaderStream << "#if !(((PDUR_AR_RELEASE_MAJOR_VERSION == 4) && (PDUR_AR_RELEASE_MINOR_VERSION == 0)) )\n#error PduR: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n";
        cfgHeaderStream << "#include \"PduR_Types.h\"\n\n";

        // Check for support for different BSW modules
        QList<QDomElement> bswModules;
        QStringList bswModuleNames;
        AutosarToolFactory::FindReferencesToElement(&bswModules, projectModuleRoot, "PduRBswModules");
        bool hasCanIfSupport = false, hasCanTpSupport = false, hasCanNmSupport = false, hasFrIfSupport = false, hasFrTpSupport = false, hasFrNmSupport = false, hasLinIfSupport = false;
        bool hasLinTpSupport = false, hasComSupport = false, hasDcmSupport = false, hasIpduMSupport = false, hasSoAdSupport = false, hasUdpNmSupport = false, hasJ1939TpSupport = false;
        bool hasCddLinSlvIfSupport = false, hasCddPduRSupport = false, hasLdComSupport = false;
        for (int modIter = 0; modIter < bswModules.size(); modIter++)
        {
            QString moduleRef = GET_VALUE_REF_OF_REF_WITH_RETURN(bswModules[modIter], "PduRBswModuleRef");
            if (moduleRef.isEmpty()) {
                std::cout << "Error: Reference for the BSW module not defined in " << AutosarToolFactory::CreateReferenceFromDomElement(bswModules[modIter].firstChildElement()).toStdString() << ".\n";
                return false;
            }
            QDomElement module = AutosarToolFactory::GetDomElementFromReference(&moduleRef, projectModuleRoot->ownerDocument().documentElement());
            if (module.isNull()) {
                std::cout << "Error: BSW module reference in " << AutosarToolFactory::CreateReferenceFromDomElement(bswModules[modIter].firstChildElement()).toStdString() << " is not valid.\n";
                return false;
            }
            bswModuleNames.push_back(module.firstChildElement("DEFINITION-REF").text().split("/").last());
            if (bswModuleNames.last() == "CanIf") {
                hasCanIfSupport = true;
            } else if (bswModuleNames.last() == "CanTp") {
                hasCanTpSupport = true;
            } else if (bswModuleNames.last() == "CanNm") {
                hasCanNmSupport = true;
            } else if (bswModuleNames.last() == "FrIf") {
                hasFrIfSupport = true;
            } else if (bswModuleNames.last() == "FrTp") {
                hasFrTpSupport = true;
            } else if (bswModuleNames.last() == "FrNm") {
                hasFrNmSupport = true;
            } else if (bswModuleNames.last() == "LinIf") {
                hasLinIfSupport = true;
            } else if (bswModuleNames.last() == "LinTp") {
                hasLinTpSupport = true;
            } else if (bswModuleNames.last() == "Com") {
                hasComSupport = true;
            } else if (bswModuleNames.last() == "Dcm") {
                hasDcmSupport = true;
            } else if (bswModuleNames.last() == "IpduM") {
                hasIpduMSupport = true;
            } else if (bswModuleNames.last() == "SoAd") {
                hasSoAdSupport = true;
            } else if (bswModuleNames.last() == "UdpNm") {
                hasUdpNmSupport = true;
            } else if (bswModuleNames.last() == "J1939Tp") {
                hasJ1939TpSupport = true;
            } else if (bswModuleNames.last() == "CDD_LinSlvIf") {
                hasCddLinSlvIfSupport = true;
            } else if (bswModuleNames.last() == "CddPduR") {
                hasCddPduRSupport = true;
            } else if (bswModuleNames.last() == "LdCom") {
                hasLdComSupport = true;
            }
        }
        cfgHeaderStream << "#define PDUR_CANIF_SUPPORT\t" << (hasCanIfSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_CANTP_SUPPORT\t" << (hasCanTpSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_CANNM_SUPPORT\t" << (hasCanNmSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_FRIF_SUPPORT\t" << (hasFrIfSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_FRTP_SUPPORT\t" << (hasFrTpSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_FRNM_SUPPORT\t" << (hasFrNmSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_LINIF_SUPPORT\t" << (hasLinIfSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_LINTP_SUPPORT\t" << (hasLinTpSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_COM_SUPPORT\t" << (hasComSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_DCM_SUPPORT\t" << (hasDcmSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_IPDUM_SUPPORT\t" << (hasIpduMSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_SOAD_SUPPORT\t" << (hasSoAdSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_UDPNM_SUPPORT\t" << (hasUdpNmSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_J1939TP_SUPPORT\t" << (hasJ1939TpSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_CDD_LINSLV_SUPPORT\t" << (hasCddLinSlvIfSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_CDDPDUR_SUPPORT\t" << (hasCddPduRSupport ? "STD_ON" : "STD_OFF") << "\n";
        cfgHeaderStream << "#define PDUR_LDCOM_SUPPORT\t" << (hasLdComSupport ? "STD_ON" : "STD_OFF") << "\n\n";

        QDomElement PduRGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PduRGeneral");
        QString parameterValue = GET_VALUE_OF_PARAM(PduRGeneral, "PduRDevErrorDetect");
        cfgHeaderStream << "#define PDUR_DEV_ERROR_DETECT\t";
        if (parameterValue == "1") {
            cfgHeaderStream << "STD_ON";
        } else {
            cfgHeaderStream << "STD_OFF";
        }
        cfgHeaderStream << "\n";
        parameterValue = GET_VALUE_OF_PARAM(PduRGeneral, "PduRVersionInfoApi");
        cfgHeaderStream << "#define PDUR_VERSION_INFO_API\t";
        if (parameterValue == "1") {
            cfgHeaderStream << "STD_ON";
        } else {
            cfgHeaderStream << "STD_OFF";
        }
        cfgHeaderStream << "\n";
        parameterValue = GET_VALUE_OF_PARAM(PduRGeneral, "PduRZeroCostOperation");
        cfgHeaderStream << "#define PDUR_ZERO_COST_OPERATION\t";
        if (parameterValue == "1") {
            cfgHeaderStream << "STD_ON";
        } else {
            cfgHeaderStream << "STD_OFF";
        }
        cfgHeaderStream << "\n\n";
        cfgHeaderStream << "#define PDUR_GATEWAY_OPERATION\tSTD_ON\n";
        cfgHeaderStream << "#define PDUR_SB_TX_BUFFER_SUPPORT\tSTD_ON\n";
        cfgHeaderStream << "#define PDUR_FIFO_TX_BUFFER_SUPPORT\tSTD_OFF\n\n";
        cfgHeaderStream << "#define PDUR_MULTICAST_TOIF_SUPPORT\tSTD_ON\n";
        cfgHeaderStream << "#define PDUR_MULTICAST_FROMIF_SUPPORT\tSTD_ON\n";
        cfgHeaderStream << "#define PDUR_MULTICAST_TOTP_SUPPORT\tSTD_ON\n";
        cfgHeaderStream << "#define PDUR_MULTICAST_FROMTP_SUPPORT\tSTD_ON\n\n";
        cfgHeaderStream << "/* Minimum routing not supported.\n#define PDUR_MINIMUM_ROUTING_UP_MODULE\tCOM\n#define PDUR_MINIMUM_ROUTING_LO_MODULE\tCAN_IF\n";
        cfgHeaderStream << "#define PDUR_MINIMUM_ROUTING_UP_RXPDUID\t((PduIdType)100)\n#define PDUR_MINIMUM_ROUTING_LO_RXPDUID\t((PduIdType)255)\n";
        cfgHeaderStream << "#define PDUR_MINIMUM_ROUTING_UP_TXPDUID\t((PduIdType)255)\n#define PDUR_MINIMUM_ROUTING_LO_TXPDUID\t((PduIdType)255)\n */\n\n";
        QList<QDomElement> routingPathGroups;
        AutosarToolFactory::FindReferencesToElement(&routingPathGroups, projectModuleRoot, "PduRRoutingPathGroup");
        cfgHeaderStream << "#define PDUR_MAX_NOF_ROUTING_PATH_GROUPS\t" << routingPathGroups.size() << "\n\n";
        cfgHeaderStream << "#define PDUR_NO_BUFFER 0xFFFF\n\n";
        cfgHeaderStream << "// Tx buffer IDs (sorted in the same order as PduRTxBuffers array)\n\n";
        // No clue
        QDomElement PduRRoutingTables = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PduRRoutingTables");
        QString routeCount = GET_VALUE_OF_PARAM(PduRRoutingTables, "ArcPduRMaxRouteCount");
        if (routeCount.isEmpty()) {
            std::cout << "Error: Required parameter \'ArcPduRMaxRouteCount\' not found in PduRRoutingTables.\n";
            return false;
        }
        QString configId = GET_VALUE_OF_PARAM(PduRRoutingTables, "PduRConfigurationId");
        if (configId.isEmpty()) {
            std::cout << "Error: Required parameter \'PduRConfigurationId\' not found in PduRRoutingTables.\n";
            return false;
        }
        cfgHeaderStream << "/* Maximum number of routing paths:\n * " << routeCount << "\n */\n\n";
        // No clue
        cfgHeaderStream << "/* Routing Path Group IDs */\n\n";
        // No clue
        cfgHeaderStream << "#if PDUR_ZERO_COST_OPERATION == STD_ON\n// Zero cost operation support active.\n/* @req PDUR287 */\n";
        cfgHeaderStream << "#if PDUR_CANIF_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_CanIfRxIndication Com_RxIndication\n#define PduR_CanIfTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#else\n#define PduR_CanIfRxIndication(... )\n#define PduR_CanIfTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_CANNM_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_CanNmRxIndication Com_RxIndication\n#define PduR_CanNmTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#else\n#define PduR_CanNmRxIndication(... )\n#define PduR_CanNmTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_UDPNM_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_UdpNmRxIndication Com_RxIndication\n#define PduR_UdpNmTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#else\n#define PduR_UdpNmRxIndication(... )\n#define PduR_UdpNmTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_CANTP_SUPPORT == STD_ON\n#include \"Dcm.h\"\n#define PduR_CanTpProvideRxBuffer Dcm_ProvideRxBuffer\n#define PduR_CanTpRxIndication Dcm_RxIndication\n";
        cfgHeaderStream << "#define PduR_CanTpProvideTxBuffer Dcm_ProvideTxBuffer\n#define PduR_CanTpTxConfirmation Dcm_TxConfirmation\n#else\n";
        cfgHeaderStream << "#define PduR_CanTpProvideRxBuffer(...)\n#define PduR_CanTpRxIndication(...)\n#define PduR_CanTpProvideTxBuffer(...)\n#define PduR_CanTpTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_LINIF_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_LinIfRxIndication Com_RxIndication\n#define PduR_LinIfTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#define PduR_LinIfTriggerTransmit Com_TriggerTransmit\n#else\n#define PduR_LinIfRxIndication(...)\n#define PduR_LinIfTxConfirmation(...)\n";
        cfgHeaderStream << "#define PduR_LinIfTriggerTransmit(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_CDD_LINSLV_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_LinSlvIfRxIndication Com_RxIndication\n#define PduR_LinSlvIfTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#define PduR_LinSlvIfTriggerTransmit Com_TriggerTransmit\n#else\n#define PduR_LinSlvIfRxIndication(...)\n#define PduR_LinSlvIfTxConfirmation(...)\n";
        cfgHeaderStream << "#define PduR_LinSlvIfTriggerTransmit(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_CDDPDUR_SUPPORT == STD_ON\n#error NOT SUPPORTED\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_SOAD_SUPPORT == STD_ON\n#include \"Dcm.h\"\n#define PduR_SoAdTpProvideRxBuffer Dcm_ProvideRxBuffer\n#define PduR_SoAdTpRxIndication Dcm_RxIndication\n";
        cfgHeaderStream << "#define PduR_SoAdTpProvideTxBuffer Dcm_ProvideTxBuffer\n#define PduR_SoAdTpTxConfirmation Dcm_TxConfirmation\n#else\n#define PduR_SoAdProvideRxBuffer(...)\n";
        cfgHeaderStream << "#define PduR_SoAdRxIndication(...)\n#define PduR_SoAdProvideTxBuffer(...)\n#define PduR_SoAdTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_J1939TP_SUPPORT == STD_ON\n#include \"Dcm.h\"\n#define PduR_J1939TpProvideRxBuffer Dcm_ProvideRxBuffer\n#define PduR_J1939TpRxIndication Dcm_RxIndication\n";
        cfgHeaderStream << "#define PduR_J1939TpProvideTxBuffer Dcm_ProvideTxBuffer\n#define PduR_J1939TpTxConfirmation Dcm_TxConfirmation\n#else\n#define PduR_J1939TpProvideRxBuffer(...)\n";
        cfgHeaderStream << "#define PduR_J1939TpRxIndication(...)\n#define PduR_J1939TpProvideTxBuffer(...)\n#define PduR_J1939TpTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_FRIF_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_FrIfRxIndication Com_RxIndication\n#define PduR_FrIfTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#define PduR_FrIfTriggerTransmit Com_TriggerTransmit\n#else\n#define PduR_FrIfRxIndication(... )\n#define PduR_FrIfTxConfirmation(...)\n";
        cfgHeaderStream << "#define PduR_FrIfTriggerTransmit(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_FRNM_SUPPORT == STD_ON\n#include \"Com.h\"\n#define PduR_FrNmRxIndication Com_RxIndication\n#define PduR_FrNmTxConfirmation Com_TxConfirmation\n";
        cfgHeaderStream << "#define PduR_FrNmTriggerTransmit Com_TriggerTransmit\n#else\n#define PduR_FrNmRxIndication(... )\n#define PduR_FrNmTxConfirmation(...)\n";
        cfgHeaderStream << "#define PduR_FrNmTriggerTransmit(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_FRTP_SUPPORT == STD_ON\n#include \"Dcm.h\"\n#define PduR_FrTpProvideRxBuffer Dcm_ProvideRxBuffer\n#define PduR_FrTpRxIndication Dcm_RxIndication\n";
        cfgHeaderStream << "#define PduR_FrTpProvideTxBuffer Dcm_ProvideTxBuffer\n#define PduR_FrTpTxConfirmation Dcm_TxConfirmation\n#else\n";
        cfgHeaderStream << "#define PduR_FrTpProvideRxBuffer(...)\n#define PduR_FrTpRxIndication(...)\n#define PduR_FrTpProvideTxBuffer(...)\n#define PduR_FrTpTxConfirmation(...)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_COM_SUPPORT == STD_ON\n";
        QList <int> lowerModuleIndices;
        for (int modIter = 0; modIter < bswModules.size(); modIter++)
        {
            QString lowerModule = GET_VALUE_OF_PARAM(bswModules[modIter], "PduRLowerModule");
            if (lowerModule == "1") {
                lowerModuleIndices.push_back(modIter);
            }
        }
        if (lowerModuleIndices.size() == 1) {
            cfgHeaderStream << "#include \"" << bswModuleNames.at(lowerModuleIndices.at(0)) << ".h\"\n#define PduR_ComTransmit " << bswModuleNames.at(lowerModuleIndices.at(0)) << "_Transmit\n";
        } else {
            cfgHeaderStream << "#define PduR_ComTransmit NULL\n";
        }
        cfgHeaderStream << "#else\n#define PduR_ComTransmit(...)\t(E_OK)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_DCM_SUPPORT == STD_ON\n";
        if (lowerModuleIndices.size() == 1) {
            cfgHeaderStream << "#include \"" << bswModuleNames.at(lowerModuleIndices.at(0)) << ".h\"\n#define PduR_DcmTransmit " << bswModuleNames.at(lowerModuleIndices.at(0)) << "_Transmit\n";
        } else {
            cfgHeaderStream << "#define PduR_DcmTransmit NULL\n";
        }
        cfgHeaderStream << "#else\n#define PduR_DcmTransmit(...)\t(E_OK)\n#endif\n\n";
        cfgHeaderStream << "#if PDUR_LDCOM_SUPPORT == STD_ON\n";
        if (lowerModuleIndices.size() == 1) {
            cfgHeaderStream << "#include \"" << bswModuleNames.at(lowerModuleIndices.at(0)) << ".h\"\n#define PduR_LdComTransmit " << bswModuleNames.at(lowerModuleIndices.at(0)) << "_Transmit\n";
        } else {
            cfgHeaderStream << "#define PduR_LdComTransmit NULL\n";
        }
        cfgHeaderStream << "#else\n#define PduR_LdComTransmit(...)\t(E_OK)\n#endif\n\n#endif // endif PDUR_ZERO_COST_OPERATION\n\n";
        cfgHeaderStream << "#endif // PDUR_CFG_H_\n\n";
        qFile.close();

        // ----------------------------------------- Create PduR_PbCfg.h -----------------------------------------
        fileName = location + "/PduR_PbCfg.h";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create PduR_PbCfg.h file.\n";
            return false;
        }
        QTextStream pbcfgHeaderStream( &qFile );
        pbcfgHeaderStream << "\n#ifndef PDUR_PBCFG_H_\n#define PDUR_PBCFG_H_\n\n";
        pbcfgHeaderStream << "#include \"PduR.h\"\n\n#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 2)) )\n";
        pbcfgHeaderStream << "#error PduR: Configuration file expected BSW module version to be 2.2.*\n#endif\n\n";
        pbcfgHeaderStream << "#if !(((PDUR_AR_RELEASE_MAJOR_VERSION == 4) && (PDUR_AR_RELEASE_MINOR_VERSION == 0)) )\n";
        pbcfgHeaderStream << "#error PduR: Configuration file expected AUTOSAR version to be 4.0.*\n#endif\n\n#include \"PduR_Types.h\"\n\n";
        pbcfgHeaderStream << "extern const PduR_PBConfigType PduR_Config;\n\n// Symbolic names for PduR Src Pdus\n";
        QList <QDomElement> routingPaths;
        AutosarToolFactory::FindReferencesToElement(&routingPaths, &PduRRoutingTables, "PduRRoutingPath");
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            QDomElement srcPdu = AutosarToolFactory::FindFirstReferenceToElement(&routingPaths[rpIter], "PduRSrcPdu");
            if (srcPdu.isNull()) {
                std::cout << "Error: Source Pdu for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\' not defined.\n";
                return false;
            }
            pbcfgHeaderStream << "#define PduRConf_Pdu_" << srcPdu.firstChildElement("SHORT-NAME").text() << " " << rpIter << "u\n";
        }
        pbcfgHeaderStream << "\n//  PduR Polite Defines.\n";
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            QDomElement srcPdu = AutosarToolFactory::FindFirstReferenceToElement(&routingPaths[rpIter], "PduRSrcPdu");
            QString srcPduRef = GET_VALUE_REF_OF_REF_WITH_RETURN(srcPdu, "PduRSrcPduRef");
            if (srcPduRef.isEmpty()) {
                std::cout << "Error: Source Pdu reference for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\' not defined.\n";
                return false;
            }
            QDomElement actualSrcPdu = AutosarToolFactory::GetDomElementFromReference(&srcPduRef, projectModuleRoot->ownerDocument().documentElement());
            if (actualSrcPdu.isNull()) {
                std::cout << "Error: Unable to find source Pdu \'" << srcPduRef.toStdString() << "\' for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                return false;
            }
            pbcfgHeaderStream << "#define PDUR_PDU_ID_" << actualSrcPdu.firstChildElement("SHORT-NAME").text().toUpper() << "\t" << rpIter << "\n";
            QList <QDomElement> dstPdus;
            AutosarToolFactory::FindReferencesToElement(&dstPdus, &routingPaths[rpIter], "PduRDestPdu");
            for (int dIter = 0; dIter < dstPdus.size(); dIter++) {
                QString destPduRef = GET_VALUE_REF_OF_REF_WITH_RETURN(dstPdus[dIter], "PduRDestPduRef");
                if (destPduRef.isEmpty()) {
                    std::cout << "Error: Destination Pdu reference for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\' not defined.\n";
                    return false;
                }
                QDomElement actualDestPdu = AutosarToolFactory::GetDomElementFromReference(&destPduRef, projectModuleRoot->ownerDocument().documentElement());
                if (actualDestPdu.isNull()) {
                    std::cout << "Error: Unable to find destination Pdu \'" << destPduRef.toStdString() << "\' for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                pbcfgHeaderStream << "#define PDUR_REVERSE_PDU_ID_" << actualDestPdu.firstChildElement("SHORT-NAME").text().toUpper() << "\t" << rpIter << "\n";
            }
            pbcfgHeaderStream << "\n";
        }
        pbcfgHeaderStream << "\n#endif // PDUR_PBCFG_H_\n\n";
        qFile.close();

        // ----------------------------------------- Create PduR_PbCfg.c -----------------------------------------
        fileName = location + "/PduR_PbCfg.c";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create PduR_PbCfg.c file.\n";
            return false;
        }
        QTextStream pbcfgCStream( &qFile );
        pbcfgCStream << "\n#include \"PduR.h\"\n#include \"MemMap.h\"\n\n";
        pbcfgCStream << "#if PDUR_CANIF_SUPPORT == STD_ON\n#include \"CanIf.h\"\n#include \"CanIf_PBCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_CANNM_SUPPORT == STD_ON\n#include \"CanNm.h\"\n#include \"CanNm_PBCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_UDPNM_SUPPORT == STD_ON\n#include \"UdpNm.h\"\n#include \"UdpNm_PBCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_CANTP_SUPPORT == STD_ON\n#include \"CanTp.h\"\n#include \"CanTp_PBCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_LINIF_SUPPORT == STD_ON\n#include \"LinIf.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_CDD_LINSLV_SUPPORT == STD_ON\n#include \"CDD_LinSlv.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_CDDPDUR_SUPPORT == STD_ON\n#include \"CddPduR.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_COM_SUPPORT == STD_ON\n#include \"Com.h\"\n#include \"Com_PbCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_DCM_SUPPORT == STD_ON\n#include \"Dcm.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_SOAD_SUPPORT == STD_ON\n#include \"SoAd.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_J1939TP_SUPPORT == STD_ON\n#include \"J1939Tp.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_IPDUM_SUPPORT == STD_ON\n#include \"IpduM.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_FRIF_SUPPORT == STD_ON\n#include \"FrIf.h\"\n#include \"FrIf_PBcfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_FRNM_SUPPORT == STD_ON\n#include \"FrNm.h\"\n#include \"FrNm_Cfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_FRTP_SUPPORT == STD_ON\n#include \"FrTp.h\"\n#include \"FrTp_PBCfg.h\"\n#endif\n";
        pbcfgCStream << "#if PDUR_LDCOM_SUPPORT == STD_ON\n#include \"LdCom.h\"\n#include \"LdCom_PBCfg.h\"\n#endif\n\n// Destinations\n";
        QList <QDomElement> ComIPdus;
        QDomElement projectRoot = projectModuleRoot->ownerDocument().documentElement();
        AutosarToolFactory::FindReferencesToElement(&ComIPdus, &projectRoot, "ComIPdu");
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            QList <QDomElement> dstPdus;
            AutosarToolFactory::FindReferencesToElement(&dstPdus, &routingPaths[rpIter], "PduRDestPdu");
            for (int dIter = 0; dIter < dstPdus.size(); dIter++) {
                QString destPduRef = GET_VALUE_REF_OF_REF_WITH_RETURN(dstPdus[dIter], "PduRDestPduRef");
                QDomElement actualDestPdu = AutosarToolFactory::GetDomElementFromReference(&destPduRef, projectModuleRoot->ownerDocument().documentElement());
                pbcfgCStream << "\nSECTION_POSTBUILD_DATA const PduRDestPdu_type PduRDestination_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() <<
                                "_" << dstPdus[dIter].firstChildElement("SHORT-NAME").text() << " = {\n";
                QString destinationModule = GET_VALUE_OF_PARAM_WITH_RETURN(dstPdus[dIter], "ArcOverridenDestModule").toUpper();
                pbcfgCStream << "\t.DestModule 	= ARC_PDUR_" << destinationModule << ",\n\t.DestPduId		= ";
                if (destinationModule == "COM") {
                    // Find comipdu which references the same pdu
                    int pIter;
                    for (pIter = 0; pIter < ComIPdus.size(); pIter++) {
                        QString pduRef = GET_VALUE_REF_OF_REF(ComIPdus[pIter], "ComPduIdRef");
                        if (pduRef == destPduRef) {
                            break;
                        }
                    }
                    if (pIter >= ComIPdus.size()) {
                        std::cout << "Error: Unable to find ComIPdu for the PDU \'" << destPduRef.toStdString() << "\' for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                        return false;
                    }
                    pbcfgCStream << "ComConf_ComIPdu_" << ComIPdus[pIter].firstChildElement("SHORT-NAME").text().toUpper() << ",\n";            // Check if simiilar to the one below!
                } else {
                    pbcfgCStream << destinationModule << "_PDU_ID_" << actualDestPdu.firstChildElement("SHORT-NAME").text().toUpper() << ",\n";
                }
                pbcfgCStream << "\t.NofGroupRefs = 0,\n\t.RoutingPathGroupRefs = NULL,\n";
                QString dataProvision = GET_VALUE_OF_PARAM_WITH_RETURN(dstPdus[dIter], "PduRDestPduDataProvision");
                pbcfgCStream << "\t.DataProvision	= " << dataProvision << ",\n";
                pbcfgCStream << "\t.TxBufferId		= PDUR_NO_BUFFER /* No buffer */\n};\n";
            }
        }
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            pbcfgCStream << "\nSECTION_POSTBUILD_DATA const PduRDestPdu_type * const PduRDestinations_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() << "[] = {\n";
            QList <QDomElement> dstPdus;
            AutosarToolFactory::FindReferencesToElement(&dstPdus, &routingPaths[rpIter], "PduRDestPdu");
            for (int dIter = 0; dIter < dstPdus.size(); dIter++) {
                pbcfgCStream << "\t&PduRDestination_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() <<
                                "_" << dstPdus[dIter].firstChildElement("SHORT-NAME").text() << ",\n";
            }
            pbcfgCStream << "\tNULL\n};\n";
        }
        pbcfgCStream << "\n// Routing paths\n";
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            pbcfgCStream << "\nSECTION_POSTBUILD_DATA const PduRRoutingPath_type PduRRoutingPath_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() << " = {\n";
            QDomElement srcPdu = AutosarToolFactory::FindFirstReferenceToElement(&routingPaths[rpIter], "PduRSrcPdu");
            QString srcPduRef = GET_VALUE_REF_OF_REF_WITH_RETURN(srcPdu, "PduRSrcPduRef");
            QDomElement actualSrcPdu = AutosarToolFactory::GetDomElementFromReference(&srcPduRef, projectModuleRoot->ownerDocument().documentElement());
            QString sourceModuleName = GET_VALUE_OF_PARAM_WITH_RETURN(srcPdu, "ArcOverridenSrcModule").toUpper();
            pbcfgCStream << "\t.SrcModule = ARC_PDUR_" << sourceModuleName << ",\n\t.SrcPduId = ";
            if (sourceModuleName == "COM") {
                // Find comipdu which references the same pdu
                int pIter;
                for (pIter = 0; pIter < ComIPdus.size(); pIter++) {
                    QString pduRef = GET_VALUE_REF_OF_REF(ComIPdus[pIter], "ComPduIdRef");
                    if (pduRef == srcPduRef) {
                        break;
                    }
                }
                if (pIter >= ComIPdus.size()) {
                    std::cout << "Error: Unable to find ComIPdu for the PDU \'" << srcPduRef.toStdString() << "\' for routing path \'" << routingPaths.at(rpIter).firstChildElement("SHORT-NAME").text().toStdString() << "\'.\n";
                    return false;
                }
                pbcfgCStream << "ComConf_ComIPdu_" << ComIPdus[pIter].firstChildElement("SHORT-NAME").text().toUpper() << ",\n";
            } else {
                pbcfgCStream << sourceModuleName << "_PDU_ID_" << actualSrcPdu.firstChildElement("SHORT-NAME").text().toUpper() << ",\n";
            }
            pbcfgCStream << "\t.PduRDestPdus = PduRDestinations_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() << ",\n";
            pbcfgCStream << "\t.PduRTpThreshld = 0\n};\n";
        }

        pbcfgCStream << "\n\nSECTION_POSTBUILD_DATA const PduRRoutingPath_type * const PduRRoutingPaths[] = {\n";
        for (int rpIter = 0; rpIter < routingPaths.size(); rpIter++) {
            pbcfgCStream << "\t&PduRRoutingPath_" << routingPaths[rpIter].firstChildElement("SHORT-NAME").text() << ",\n";
        }
        pbcfgCStream << "\tNULL\n};\n\n// Default values\n\n";
        // No clue
        pbcfgCStream << "\n// Tx buffer default value pointer list (sorted in the same order as Tx buffer IDs)\n";
        pbcfgCStream << "SECTION_POSTBUILD_DATA const uint8 * const PduRTxBufferDefaultValues[] = {\n\tNULL\n};\n\n";
        pbcfgCStream << "// Tx buffer default value length list (sorted in the same order as Tx buffer IDs)\n";
        pbcfgCStream << "SECTION_POSTBUILD_DATA const uint32 * const PduRTxBufferDefaultValueLengths[] = {\n\tNULL\n};\n\n";
        pbcfgCStream << "// Exported config\nSECTION_POSTBUILD_DATA const PduR_PBConfigType PduR_Config = {\n";
        pbcfgCStream << "\t.PduRConfigurationId = " << configId << ",\n";
        pbcfgCStream << "\t.RoutingPaths = PduRRoutingPaths,\n";
        pbcfgCStream << "\t.NRoutingPaths = " << routingPaths.size() << ",\n";
        pbcfgCStream << "\t.DefaultValues = PduRTxBufferDefaultValues,\n";
        pbcfgCStream << "\t.DefaultValueLengths = PduRTxBufferDefaultValueLengths,\n";
        pbcfgCStream << "\t.NofRoutingPathGroups = " << routingPathGroups.size() << ",\n";
        pbcfgCStream << "\t.RoutingPathGroups = NULL,\n};\n\n";
        qFile.close();

        // ----------------------------------------- Create PduR_Cfg.c -----------------------------------------
        fileName = location + "/PduR_Cfg.c";
        qFile.setFileName(fileName);
        if (!qFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create PduR_Cfg.c file.\n";
            return false;
        }
        QTextStream cfgCStream( &qFile );
        cfgCStream << "\n#include \"PduR.h\"\n\n";
        cfgCStream << "PduRTpBufferInfo_type PduRTpBuffers[] = {\n";
        // TODO add buffers
        cfgCStream << "\t{\n\t\t.pduInfoPtr = NULL,\n\t\t.status = PDUR_BUFFER_NOT_ALLOCATED_FROM_UP_MODULE,\n\t\t.bufferSize = 0\n\t}\n};\n\n";
        cfgCStream << "PduRTpBufferInfo_type *PduRTpRouteBufferPtrs[] = {\n";
        bool ok;
        int maxRouteCount = routeCount.toInt(&ok);
        Q_ASSERT(ok);
        for (int rIter = 0; rIter < maxRouteCount; rIter++) {
            cfgCStream << "\tNULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers\n";
        }
        cfgCStream << "\tNULL\n};\n\n";
        cfgCStream << "const PduR_RamBufCfgType PduR_RamBufCfg = {\n\t.TpBuffers = PduRTpBuffers,\n\t.TpRouteBuffers = PduRTpRouteBufferPtrs,\n";
        QDomElement TxBufferTable = AutosarToolFactory::FindFirstReferenceToElement(&PduRRoutingTables, "PduRTxBufferTable");
        if (!TxBufferTable.isNull()) {
            std::cout << "Error: PduRTxBufferTable is not supported by the generator.\n";
            return false;
        }
        QDomElement TpBufferTable = AutosarToolFactory::FindFirstReferenceToElement(&PduRRoutingTables, "PduRTpBufferTable");
        if (!TpBufferTable.isNull()) {
            std::cout << "Error: PduRTpBufferTable is not supported by the generator.\n";
            return false;
        }
        cfgCStream << "\t.TxBuffers = NULL,\n\t.NTpBuffers = 1, // Including 1 non-allocated buffer place holder for Upper module allocated buffer\n";
        cfgCStream << "\t.NTpRouteBuffers = " << maxRouteCount << ",\n\t.NTxBuffers = 0\n};\n\n";
        qFile.close();

    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool PdurGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++) {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "") {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull()) {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}

