/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/generic/det_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool DetGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists())
    {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Det Module Configuration files does not exist.\n";
        return false;
    }

    try
    {
		// ----------------------------------------- Create Makefile -----------------------------------------
		QString makeFileName = location + "/Det.mk";
		QFile makeFile(makeFileName);
		if (!makeFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Det.mk file.\n";
		    return false;
		}
		QTextStream makeFileStream( &makeFile );
		makeFileStream << "MOD_USE += DET\n";
		makeFile.close();

		// ----------------------------------------- Create Det_Cfg.h -----------------------------------------
		QString configHeaderName = location + "/Det_Cfg.h";
		QFile headerFile(configHeaderName);
		if (!headerFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Det_Cfg.h file.\n";
		    return false;
		}
		QTextStream headerStream( &headerFile );
		headerStream << "\n#ifndef DET_CFG_H_\n#define DET_CFG_H_\n\n";
        QDomElement DetGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "DetGeneral");
        QString DetEnableCallbacks = GET_VALUE_OF_PARAM(DetGeneral, "DetEnableCallbacks");
        headerStream << "// @req DET101\n#define DET_ENABLE_CALLBACKS\t\t" << GetStdOnOff(DetEnableCallbacks) << endl;
        QString DetForwardToDlt = GET_VALUE_OF_PARAM_WITH_RETURN(DetGeneral, "DetForwardToDlt");
        if (DetForwardToDlt != "")
            headerStream << "#define DET_FORWARD_TO_DLT\t\t" << GetStdOnOff(DetForwardToDlt) << endl;
        else
            headerStream << "#define DET_FORWARD_TO_DLT\t\tSTD_OFF\n";
        QString DetUseRamlog = GET_VALUE_OF_PARAM(DetGeneral, "DetUseRamlog");
        headerStream << "#define DET_USE_RAMLOG\t\t" << GetStdOnOff(DetUseRamlog) << endl;
        QString DetWrapRamlog = GET_VALUE_OF_PARAM(DetGeneral, "DetWrapRamlog");
        headerStream << "#define DET_WRAP_RAMLOG\t\t" << GetStdOnOff(DetWrapRamlog) << endl;
        QString DetUseStdErr = GET_VALUE_OF_PARAM(DetGeneral, "DetUseStdErr");
        headerStream << "#define DET_USE_STDERR\t\t" << GetStdOnOff(DetUseStdErr) << endl;
        QString DetDeInitAPI = GET_VALUE_OF_PARAM(DetGeneral, "DetDeInitAPI");
        headerStream << "#define DET_DEINIT_API\t\t" << GetStdOnOff(DetDeInitAPI) << endl;
        QString DetVersionInfoApi = GET_VALUE_OF_PARAM(DetGeneral, "DetVersionInfoApi");
        headerStream << "#define DET_VERSIONINFO_API\t\t" << GetStdOnOff(DetVersionInfoApi) << endl;
        QString DetRamlogSize = GET_VALUE_OF_PARAM(DetGeneral, "DetRamlogSize");
        headerStream << "#define DET_RAMLOG_SIZE\t\t(" << DetRamlogSize << ")\n\n";
        QString DetNumberOfCallbacks = GET_VALUE_OF_PARAM(DetGeneral, "DetNumberOfCallbacks");
        headerStream << "#define DET_NUMBER_OF_CALLBACKS\t\t(" << DetNumberOfCallbacks << ")\n\n";
        headerStream << "#define DET_USE_STATIC_CALLBACKS\t\t";
        QDomElement DetNotification = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "DetNotification");
        QList<QDomElement> DetErrorHooks;
        AutosarToolFactory::FindReferencesToElement(&DetErrorHooks, &DetNotification, "DetErrorHook");
        if (DetErrorHooks.size() > 0)
            headerStream << "STD_ON\n";
        else
            headerStream << "STD_OFF\n";
        headerStream << "#define DET_NUMBER_OF_STATIC_CALLBACKS\t\t" << DetErrorHooks.size() << "\n\n";
        headerStream << "#endif // DET_CFG_H_\n";
		headerFile.close();

		// ----------------------------------------- Create Det_Cfg.c -----------------------------------------
		QString configFileName = location + "/Det_Cfg.c";
		QFile configFile(configFileName);
		if (!configFile.open(QIODevice::WriteOnly))
		{
            std::cout << "Error: Unable to create Det_Cfg.c file.\n";
		    return false;
		}
		QTextStream configStream( &configFile );
        configStream << "\n#include \"Det.h\"\n\n// No static Dethooks configured\n\n";
		configFile.close();
    }
    catch (QString err)
    {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool DetGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
		{
			QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
		    if (tempElem.isNull())
		    {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
		    }
		}
    }
    return returnValue;
}

