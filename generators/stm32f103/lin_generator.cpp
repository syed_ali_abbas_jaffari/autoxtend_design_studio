/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/stm32f103/lin_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool STM32F103LinGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Lin Module Configuration files does not exist.\n";
        return false;
    }

    try {
        // ----------------------------------------- Create Makefile -----------------------------------------
        QString fileName = location + "/Lin.mk";
        QFile makeFile(fileName);
        if (!makeFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Lin.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += LIN\n";
        makeFile.close();

        // ----------------------------------------- Create Cfg.h -----------------------------------------
        fileName = location + "/Lin_Cfg.h";
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Lin_Cfg.h file.\n";
            return false;
        }
        QTextStream headerStream( &file );
        headerStream << "#if !(((LIN_SW_MAJOR_VERSION == 2) && (LIN_SW_MINOR_VERSION == 1)) )\n#error Lin: Configuration file expected BSW module version to be 2.1.X*\n#endif\n\n";
        headerStream << "#if !(((LIN_AR_RELEASE_MAJOR_VERSION == 4) && (LIN_AR_RELEASE_MINOR_VERSION == 1)) )\n#error Lin: Configuration file expected AUTOSAR version to be 4.1.*\n#endif\n\n";
        headerStream << "/* File: Lin_Cfg.h\n * Declarations of the pre-compile time configurable parameters.\n */\n\n";
        headerStream << "#ifndef LIN_CFG_H_\n#define LIN_CFG_H_\n\n";
        headerStream << "#include \"Lin_ConfigTypes.h\"\n\n/*===========[LinGeneral]=====================================================*/\n\n";
        headerStream << "/* Switches the Development Error Detection and Notification ON or OFF. */\n";
        QDomElement LinGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinGeneral");
        QString string = GET_VALUE_OF_PARAM(LinGeneral, "LinDevErrorDetect");
        headerStream << "#define LIN_DEV_ERROR_DETECT " << GetStdOnOff(string) << "\n\n\n";
        headerStream << "/* Specifies the InstanceId of this module instance. If only one instance is\n * present it shall have the Id 0. */\n";
        headerStream << "#define LIN_INDEX\t0 //One instance of Lindriver for all LinHwControllers\n\n\n";
        headerStream << "/* Specifies the maximum number of loops for blocking function until a timeout\n * is raised in short term wait loops. */\n";
        headerStream << "#define LIN_TIMEOUT_DURATION\t0\t// Not supported\n\n\n";
        headerStream << "/* Switches the Lin_GetVersionInfo function ON or OFF. */\n";
        string = GET_VALUE_OF_PARAM(LinGeneral, "LinVersionInfoApi");
        headerStream << "#define LIN_VERSION_INFO_API " << GetStdOnOff(string) << "\n\n";
        headerStream << "#define LIN_POSTBUILD_VARIANT\tSTD_OFF\n\n";
        headerStream << "/*===========[Lin channel identifiers]========================================*/\n\n";
        headerStream << "/* Symbolic names of Lin Channels. Only one channel per Lin controller is allowed. This is a simplified\n"
                        "approach and sufficient for most of the use cases. LinChannel here directly corresponds to the LinController used.\n */\n\n";
        QDomElement LinGlobalConfig = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "LinGlobalConfig");
        QList<QDomElement> LinChannels;
        AutosarToolFactory::FindReferencesToElement(&LinChannels, &LinGlobalConfig, "LinChannel");
        for (int chIter = 0; chIter < LinChannels.size(); chIter++) {

            headerStream << "#define LinConf_LinChannel_" << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << " " << chIter << "\n";
            headerStream << "#define Lin_" << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << " LinConf_LinChannel_" << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << "\n";
        }
        headerStream << "\n/* Number of all setup Lin channels */\n";
        headerStream << "#define LIN_CONTROLLER_CNT\t" << LinChannels.size() << "\n\n";
        headerStream << "/*===========[External data]==================================================*/\n\n/* Channel configuration data defined in Lin_Lcfg.c */\n\n";
        headerStream << "extern const Lin_ConfigType Lin_Config;\n\n";
        headerStream << "/* Configuration Set Handles */\n";
        headerStream << "#define LinGlobalConfig (Lin_Config)\n";
        headerStream << "#define Lin_LinGlobalConfig (Lin_Config)\n\n";
        headerStream << "#endif /* LIN_CFG_H_ */\n";
        file.close();

        // ----------------------------------------- Create Lcfg.c -----------------------------------------
        fileName = location + "/Lin_Lcfg.c";
        QFile lFile(fileName);
        if (!lFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Lin_Lcfg.c file.\n";
            return false;
        }
        QTextStream lcfgStream( &lFile );
        lcfgStream << "\n/* File: Lin_Lcfg.c */\n\n/** @tagSettings DEFAULT_ARCHITECTURE=STM32F103 */\n/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */\n\n";
        lcfgStream << "#include \"Mcu.h\"\n#include \"Lin.h\"\n\n";
        lcfgStream << "/*===========[Lin channels configuration data]================================*/\n\n";
        lcfgStream << "/* This table stores the configuration data of every setup LinChannel.\n * It is indexed by Lin channel identifier.\n */\n/** @req SWS_Lin_00011 */\n";
        lcfgStream << "const Lin_ChannelConfigType LinChannelConfigData[LIN_CONTROLLER_CNT] =\n{\n";
        int maxId = 0;
        bool okFlag;
        for (int chIter = 0; chIter < LinChannels.size(); chIter++) {
            lcfgStream << "\t/* " << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << " */\n";
            lcfgStream << "\t[LinConf_LinChannel_" << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << "] =\n\t{\n";
            string = GET_VALUE_OF_PARAM(LinChannels[chIter], "LinChannelBaudRate");
            lcfgStream << "\t\t.LinChannelBaudRate\t=\t" << string << ",\n";
            lcfgStream << "\t\t.LinChannelWakeUpSupport\t=\tFALSE,\t//Not supported\n";
            lcfgStream << "\t\t.LinChannelEcuMWakeUpSource = 0,\t//not used\n\t\t/*lint -e{641} */\n";
            string = GET_VALUE_OF_PARAM(LinChannels[chIter], "LinChannelId");
            int tempId = string.toInt(&okFlag);
            if (!okFlag) {
                std::cout << "Error: Unable to convert string \'" << string.toStdString() << "\' to integer.\n";
                return false;
            }
            if (tempId > maxId) {
                maxId = tempId;
            }
            lcfgStream << "\t\t.LinChannelId\t=\t" << string << ",\n\t},\n";
        }
        lcfgStream << "};\n\nconst uint8 Lin_HwId2Channel[" << maxId + 1 << "] = {\n";
        for (int chIter = 0; chIter < LinChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(LinChannels[chIter], "LinChannelId");
            lcfgStream << "\t[" << string << "] = LinConf_LinChannel_" << LinChannels[chIter].firstChildElement("SHORT-NAME").text() << ",\n";
        }
        lcfgStream << "};\n\n";
        lcfgStream << "const Lin_ConfigType Lin_Config =\n{\n\t.LinChannelConfig\t=\tLinChannelConfigData,\n\t.Lin_HwId2ChannelMap\t=\tLin_HwId2Channel\n};\n";
        lFile.close();

        // ----------------------------------------- Create PBcfg.c -----------------------------------------
        fileName = location + "/Lin_PBcfg.c";
        QFile pbcfgFile(fileName);
        if (!pbcfgFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Lin_PBcfg.c file.\n";
            pbcfgFile.close();
            return false;
        }
        QTextStream pbcfgStream( &pbcfgFile );
        pbcfgStream << "\n/* File: Lin_PBcfg.c */\n";
        pbcfgFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool STM32F103LinGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}
