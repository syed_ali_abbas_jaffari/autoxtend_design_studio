/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/stm32f103/port_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool STM32F103PortGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Port Module Configuration files does not exist.\n";
        return false;
    }

    try {
		// ----------------------------------------- Create Port_Cfg.h -----------------------------------------
		QString configHeaderName = location + "/Port_Cfg.h";
		QFile headerFile(configHeaderName);
        if (!headerFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Port_Cfg.h file.\n";
		    return false;
		}
		QTextStream headerStream( &headerFile );
		headerStream << "\n#ifndef PORT_CFG_H_\n#define PORT_CFG_H_\n\n";
        headerStream << "#include \"Std_Types.h\"\n#include \"Port_ConfigTypes.h\"\n#include \"Port.h\"\n\n";
        headerStream << "#if !(((PORT_SW_MAJOR_VERSION == 5) && (PORT_SW_MINOR_VERSION == 0)) )\n#error Port: Configuration file expected BSW module version to be 5.0.*\n#endif\n\n";
        headerStream << "#if !(((PORT_AR_RELEASE_MAJOR_VERSION == 4) && (PORT_AR_RELEASE_MINOR_VERSION == 1)) )\n#error Port: Configuration file expected AUTOSAR version to be 4.1.*\n#endif\n\n";
        QDomElement PortGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortGeneral");
        QString PortDevErrorDetect = GET_VALUE_OF_PARAM(PortGeneral, "PortDevErrorDetect");
        QString PortSetPinDirectionApi = GET_VALUE_OF_PARAM(PortGeneral, "PortSetPinDirectionApi");
        QString PortSetPinModeApi = GET_VALUE_OF_PARAM(PortGeneral, "PortSetPinModeApi");
        QString PortVersionInfoApi = GET_VALUE_OF_PARAM(PortGeneral, "PortVersionInfoApi");
        headerStream << "#define PORT_VERSION_INFO_API\t\t" << GetStdOnOff(PortVersionInfoApi) << "\n";
        headerStream << "#define PORT_DEV_ERROR_DETECT\t\t" << GetStdOnOff(PortDevErrorDetect) << "\n";
        headerStream << "#define PORT_SET_PIN_DIRECTION_API\t\t" << GetStdOnOff(PortSetPinDirectionApi) << "\n";
        headerStream << "/** Allow Pin mode changes during runtime (not avail on this CPU) */\n";
        headerStream << "#define PORT_SET_PIN_MODE_API\t\t" << GetStdOnOff(PortSetPinModeApi) << "\n";
        headerStream << "#define PORT_POSTBUILD_VARIANT STD_OFF\n\n";
        QDomElement PortConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortConfigSet");
        QList <QDomElement> PortContainers;
        AutosarToolFactory::FindReferencesToElement(&PortContainers, &PortConfigSet, "PortContainer");
        for (int contIter = 0; contIter < PortContainers.size(); contIter++) {
            QList <QDomElement> PortPins;
            AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
            for (int pinIter = 0; pinIter < PortPins.size(); pinIter++) {
                QString PortPinId = GET_VALUE_OF_PARAM(PortPins[pinIter], "PortPinId");
                QString PortPinName = AutosarToolFactory::GetTextOfFirstChild(&PortPins[pinIter], "SHORT-NAME");
                headerStream << "#define PortConf_PortPin_" << PortPinName << "\t((Port_PinType)" << PortPinId << ")\n";
                headerStream << "#define Port_" << PortPinName << " PortConf_PortPin_" << PortPinName << "\n";
            }
        }
        headerStream << "/** Instance of the top level configuration container */\nextern const Port_ConfigType PortConfigData;\n\n";
        headerStream << "/* Configuration Set Handles */\n#define " << PortConfigSet.firstChildElement("SHORT-NAME").text() << " (PortConfigData)\n";
        headerStream << "#define Can_" << PortConfigSet.firstChildElement("SHORT-NAME").text() << " (PortConfigData)\n\n";
        headerStream << "#endif /* PORT_CFG_H_ */\n";
		headerFile.close();

		// ----------------------------------------- Create Port_PBcfg.c -----------------------------------------
		QString configFileName = location + "/Port_PBcfg.c";
		QFile configFile(configFileName);
        if (!configFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Port_PBcfg.c file.\n";
		    return false;
		}
		QTextStream configStream( &configFile );
        configStream << "#include \"Port.h\"\n#include \"stm32f10x_gpio.h\"\n\n";
        configStream << "const uint32 remaps[] = {\n";
        QList <QDomElement> PortRemaps;
        AutosarToolFactory::FindReferencesToElement(&PortRemaps, &PortConfigSet, "ArcPortPinRemap");
        for (int rmIter = 0; rmIter < PortRemaps.size(); rmIter++) {
            configStream << "\t" << PortRemaps[rmIter].firstChildElement("VALUE").text();
            if (rmIter != PortRemaps.size() - 1)
                configStream << ",";
            configStream << endl;
        }
        configStream << "};\n\n";
        QList <QDomElement> PortPins;
        QList <int> PortPinIds;
        QString PortPinId;
        bool okFlag;
        for (int contIter = 0; contIter < PortContainers.size(); contIter++) {
            QList <QDomElement> PortPinsTemp;
            AutosarToolFactory::FindReferencesToElement(&PortPinsTemp, &PortContainers[contIter], "PortPin");
            for (int pinIter = 0; pinIter < PortPinsTemp.size(); pinIter++) {
                PortPinId = GET_VALUE_OF_PARAM(PortPinsTemp[pinIter], "PortPinId");
                int PortPinIdInt = PortPinId.toInt(&okFlag);
                if (okFlag == false) {
                    std::cout << "Error: Unable to convert port pin number " << PortPinId.toStdString() << " to integer.\n";
                    return false;
                }
                // Find index i such that PortPinIds[i] < PortPinIdInt < PortPinIds[i+1]
                int i = -1;
                for (int iter = 0; iter < PortPinIds.size()-1; iter++) {
                    if (iter == 0 && PortPinIdInt < PortPinIds[iter]) {
                        i = 0;
                        break;
                    }
                    if (PortPinIds[iter] < PortPinIdInt && PortPinIdInt < PortPinIds[iter+1]) {
                        i = iter+1;
                        break;
                    }
                }
                if (i == -1) {
                    PortPinIds.push_back(PortPinIdInt);
                    PortPins.push_back(PortPinsTemp[pinIter]);
                } else {
                    PortPinIds.insert(i, PortPinIdInt);
                    PortPins.insert(i, PortPinsTemp[pinIter]);
                }
            }
        }
        //qDebug() << "PortPins: " << PortPinIds;
        configStream << "const GpioPinCnfMode_Type GPIOConf[] = {\n";
        char portName = 'A';
        int pinNumber = 0;
        for (int contIter = 0; contIter < 7; contIter++) {      // Ports range from A to G (each containing 16 pins)
            configStream << "\t/*GPIO" << portName << "*/\n\t{\n";
            for (int pinIter = 0; pinIter < 16; pinIter++) {
                if (pinNumber < PortPinIds.size() && PortPinIds[pinNumber] == contIter*16 + pinIter) {
                    configStream << "\t\t.GpioPinCnfMode_" << pinIter << " = ";
                    QString PortPinDirection = GET_VALUE_OF_PARAM(PortPins[pinNumber], "PortPinDirection");
                    if (PortPinDirection == "PORT_PIN_OUT") {
                        configStream << "GPIO_OUTPUT_";
                        QString ArcPortPinSlewRate = GET_VALUE_OF_PARAM(PortPins[pinNumber], "ArcPortPinSlewRate");
                        if (ArcPortPinSlewRate == "PORT_PIN_SLEW_RATE_MAX")
                            configStream << "50MHz_MODE";
                        else if (ArcPortPinSlewRate == "PORT_PIN_SLEW_RATE_MED")
                            configStream << "10MHz_MODE";
                        else
                            configStream << "2MHz_MODE";
                    } else if (PortPinDirection == "PORT_PIN_IN") {
                        configStream << "GPIO_INPUT_MODE";
                    }
                    configStream << " | ";
                    QString ArcPortPinGpioMode = GET_VALUE_OF_PARAM(PortPins[pinNumber], "ArcPortPinGpioMode");
                    if (ArcPortPinGpioMode.isEmpty()) {
                        std::cout << "Error: ArcPortPinGpioMode not defined for pin number " << PortPinIds[pinNumber] << " to integer.\n";
                        return false;
                    }
                    configStream << ArcPortPinGpioMode << ",\n";
                    pinNumber++;
                } else {
                    configStream << "\t\t.GpioPinCnfMode_" << pinIter << " = GPIO_INPUT_MODE | GPIO_FLOATING_INPUT_CNF, /* Not configured */\n";
                }
            }
            configStream << "\t},\n";
            portName++;
        }
        configStream << "};\n\n";
        configStream << "const GpioPinOutLevel_Type GPIOOutConf[] = {\n";
        portName = 'A';
        pinNumber = 0;
        for (int contIter = 0; contIter < 7; contIter++) {      // Ports range from A to G (each containing 16 pins)
            configStream << "\t/*GPIO" << portName << "*/\n\t{\n";
            for (int pinIter = 0; pinIter < 16; pinIter++) {
                if (pinNumber < PortPinIds.size() && PortPinIds[pinNumber] == contIter*16 + pinIter) {
                    configStream << "\t\t.GpioPinOutLevel_" << pinIter << " = ";
                    QString PortPinLevelValue = GET_VALUE_OF_PARAM(PortPins[pinNumber], "PortPinLevelValue");
                    if (PortPinLevelValue == "PORT_PIN_LEVEL_HIGH") {
                        configStream << "GPIO_OUTPUT_HIGH";
                    } else {
                        configStream << "GPIO_OUTPUT_LOW";
                    }
                    configStream << ",\n";
                    pinNumber++;
                } else {
                    configStream << "\t\t.GpioPinOutLevel_" << pinIter << " = GPIO_OUTPUT_LOW,\n";
                }
            }
            configStream << "\t},\n";
            portName++;
        }
        configStream << "};\n\n";
        configStream << "const Port_ConfigType PortConfigData =\n{\n";
        configStream << "\t.padCnt = 7,\n";
        configStream << "\t.padConfig = GPIOConf,\n";
        configStream << "\t.outConfig = GPIOOutConf,\n";
        configStream << "\t.remapCount = sizeof(remaps) / sizeof(uint32),\n";
        configStream << "\t.remaps = &remaps[0]\n";
        configStream << "};\n\n";
        configFile.close();

		// ----------------------------------------- Create Port_Lcfg.c -----------------------------------------
		QString lConfigFileName = location + "/Port_Lcfg.c";
		QFile lConfigFile(lConfigFileName);
        if (!lConfigFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Port_Lcfg.c file.\n";
		    return false;
		}
		QTextStream lConfigStream( &lConfigFile );
        lConfigStream << "\n#include \"Port.h\"\n/* This file is not used - Port_PBcfg.c contains the configuration regardless\n";
        lConfigStream << " * if post build is used or not. \n */";
		lConfigFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Port.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Port.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += PORT\n";
        makeFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool STM32F103PortGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);
    bool returnValue = true;

    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }
    // Make sure that port pin correspand to the respective function
    QDomElement PortConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PortConfigSet");
    QList <QDomElement> PortContainers;
    AutosarToolFactory::FindReferencesToElement(&PortContainers, &PortConfigSet, "PortContainer");
    QStringList pins;
    for (int contIter = 0; contIter < PortContainers.size(); contIter++) {
        QList <QDomElement> PortPins;
        AutosarToolFactory::FindReferencesToElement(&PortPins, &PortContainers[contIter], "PortPin");
        for (int pinIter = 0; pinIter < PortPins.size(); pinIter++) {
            QString PortPinId;
            PortPinId = GET_VALUE_OF_PARAM_WITH_RETURN(PortPins[pinIter], "PortPinId");
            if (PortPinId.isEmpty()) {
                std::cout << "Error: Port::PortContainer::PortPin::PortPinId cannot be empty.\n";
                returnValue = false;
            }
            // Make sure that the port pin numbers are unique
            for (int pinIter = 0; pinIter < pins.size(); pinIter++) {
                if (pins[pinIter] == PortPinId) {
                    std::cout << "Error: Port::PortContainer::PortPin::PortPinId " + PortPinId.toStdString() + " is not unique.\n";
                    returnValue = false;
                }
            }
            pins << PortPinId;
        }
    }
    return returnValue;
}

