/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/stm32f103/pwm_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define CLOCK_FREQ 72000000.0       // Hz

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool STM32F103PwmGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Pwm Module Configuration files does not exist.\n";
        return false;
    }

    bool okFlag;
    try {
		// ----------------------------------------- Create Pwm_Cfg.h -----------------------------------------
        QString name = location + "/Pwm_Cfg.h";
        QFile file(name);
        if (!file.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Pwm_Cfg.h file.\n";
		    return false;
		}
        QTextStream headerStream( &file );
        headerStream << "\n#ifndef PWM_CFG_H_\n#define PWM_CFG_H_\n\n";
        headerStream << "#if !(((PWM_SW_MAJOR_VERSION == 2) && (PWM_SW_MINOR_VERSION == 0)) )\n#error PWM: Configuration file expected BSW module version to be 2.0.*\n#endif\n\n";
        headerStream << "#if !(((PWM_AR_RELEASE_MAJOR_VERSION == 4) && (PWM_AR_RELEASE_MINOR_VERSION == 1)) )\n#error PWM: Configuration file expected AUTOSAR version to be 4.1.*\n#endif\n\n";
        headerStream << "#include \"Pwm_ConfigTypes.h\"\n\n";
        headerStream << "/****************************************************************************\n * Global configuration options and defines\n */\n\n";
        headerStream << "// PWM003\n";
        QDomElement PwmGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PwmGeneral");
        QString string = GET_VALUE_OF_PARAM(PwmGeneral, "PwmDevErrorDetect");
        headerStream << "#define PWM_DEV_ERROR_DETECT\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(PwmGeneral, "PwmNotificationSupported");
        headerStream << "#define PWM_NOTIFICATION_SUPPORTED\t\t\t" << GetStdOnOff(string) << "\n\n";
        headerStream << "// PWM132\n";
        headerStream << "#define PWM_DUTYCYCLE_UPDATED_ENDPERIOD\t\t\tSTD_OFF\n";
        headerStream << "#define PWM_PERIOD_UPDATED_ENDPERIOD\t\t\tSTD_OFF\n\n";
        headerStream << "// Define what functions to enable.\n";
        headerStream << "#define PWM_GET_OUTPUT_STATE_API\t\t\tSTD_OFF\n";
        QDomElement PwmConfigurationOfOptApiServices = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PwmConfigurationOfOptApiServices");
        string = GET_VALUE_OF_PARAM(PwmConfigurationOfOptApiServices, "PwmSetPeriodAndDuty");
        headerStream << "#define PWM_SET_PERIOD_AND_DUTY_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(PwmConfigurationOfOptApiServices, "PwmDeInitApi");
        headerStream << "#define PWM_DE_INIT_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(PwmConfigurationOfOptApiServices, "PwmSetDutyCycle");
        headerStream << "#define PWM_SET_DUTY_CYCLE_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(PwmConfigurationOfOptApiServices, "PwmSetOutputToIdle");
        headerStream << "#define PWM_SET_OUTPUT_TO_IDLE_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(PwmConfigurationOfOptApiServices, "PwmVersionInfoApi");
        headerStream << "#define PWM_VERSION_INFO_API\t\t\t" << GetStdOnOff(string) << "\n\n";
        headerStream << "#define PWM_POSTBUILD_VARIANT\t\t\tSTD_OFF\n\n";
        headerStream << "// Legacy module support\n";
        headerStream << "#define PWM_STATICALLY_CONFIGURED\t\t\tSTD_OFF\n\n";
        headerStream << "typedef enum {\n";

        headerStream << "\tPWM_TIM1_CHANNEL1 = PWM_CHANNEL_11,	// TIM1 Channel 1\n";
        headerStream << "\tPWM_TIM1_CHANNEL2 = PWM_CHANNEL_12,\n";
        headerStream << "\tPWM_TIM1_CHANNEL3 = PWM_CHANNEL_13,\n";
        headerStream << "\tPWM_TIM1_CHANNEL4 = PWM_CHANNEL_14,\n";
        headerStream << "\tPWM_TIM2_CHANNEL1 = PWM_CHANNEL_21,     // TIM2 Channel 1\n";
        headerStream << "\tPWM_TIM2_CHANNEL2 = PWM_CHANNEL_22,\n";
        headerStream << "\tPWM_TIM2_CHANNEL3 = PWM_CHANNEL_23,\n";
        headerStream << "\tPWM_TIM2_CHANNEL4 = PWM_CHANNEL_24,\n";
        headerStream << "\tPWM_TIM3_CHANNEL1 = PWM_CHANNEL_31,     // TIM3 Channel 1\n";
        headerStream << "\tPWM_TIM3_CHANNEL2 = PWM_CHANNEL_32,\n";
        headerStream << "\tPWM_TIM3_CHANNEL3 = PWM_CHANNEL_33,\n";
        headerStream << "\tPWM_TIM3_CHANNEL4 = PWM_CHANNEL_34,\n";
        headerStream << "\tPWM_TIM4_CHANNEL1 = PWM_CHANNEL_41,     // TIM4 Channel 1\n";
        headerStream << "\tPWM_TIM4_CHANNEL2 = PWM_CHANNEL_42,\n";
        headerStream << "\tPWM_TIM4_CHANNEL3 = PWM_CHANNEL_43,\n";
        headerStream << "\tPWM_TIM4_CHANNEL4 = PWM_CHANNEL_44,\n";
        headerStream << "} PwmNamedChannelMapping;\n\n";
        headerStream << "/*\n * Setting to ON freezes the current output state of a PWM channel when in\n * debug mode.\n */\n";
        headerStream << "#define PWM_FREEZE_ENABLE\t\tSTD_ON\n\n";
        QDomElement PwmChannelConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "PwmChannelConfigSet");
        QList<QDomElement> PwmChannels;
        AutosarToolFactory::FindReferencesToElement(&PwmChannels, &PwmChannelConfigSet, "PwmChannel");
        for (int chIter = 0; chIter < PwmChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmChannelId");
            headerStream << "#define PwmConf_PwmChannel_" << PwmChannels[chIter].firstChildElement("SHORT-NAME").text() << " PWM_CHANNEL_" << string << "\n";
            headerStream << "#define Pwm_" << PwmChannels[chIter].firstChildElement("SHORT-NAME").text()
                         << " " << "PwmConf_PwmChannel_" << PwmChannels[chIter].firstChildElement("SHORT-NAME").text() << "\n";
        }
        headerStream << "\n#define PWM_NUMBER_OF_CHANNELS " << PwmChannels.size() << "\n\n";
        headerStream << "typedef struct {\n";
        headerStream << "\tPwm_ChannelConfigurationType Channels[PWM_NUMBER_OF_CHANNELS];\n";
        headerStream << "#if (PWM_NOTIFICATION_SUPPORTED==STD_ON)\n";
        headerStream << "\tPwm_NotificationHandlerType NotificationHandlers[PWM_NUMBER_OF_CHANNELS];\n";
        headerStream << "#endif\n#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON\n";
        headerStream << "\tPwm_ChannelClassType  ChannelClass[PWM_NUMBER_OF_CHANNELS];\n#endif\n} Pwm_ConfigType;\n\n";
        headerStream << "/* Configuration Set Handles */\n";
        headerStream << "#define PwmChannelConfigSet (PwmConfig)\n";
        headerStream << "#define Pwm_PwmChannelConfigSet (PwmConfig)\n\n";
        headerStream << "#endif /* PWM_CFG_H_ */\n";
        file.close();

        // ----------------------------------------- Create Pwm_Lcfg.c -----------------------------------------
        QString lConfigFileName = location + "/Pwm_Lcfg.c";
		QFile lConfigFile(lConfigFileName);
        if (!lConfigFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Pwm_Lcfg.c file.\n";
		    return false;
		}
        QTextStream configStream( &lConfigFile );
        configStream << "\n#include \"Pwm.h\"\n\n";
		lConfigFile.close();

        // ----------------------------------------- Create Pwm_PBcfg.c -----------------------------------------
        lConfigFileName = location + "/Pwm_PBcfg.c";
        QFile pbConfigFile(lConfigFileName);
        if (!pbConfigFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Pwm_PBcfg.c file.\n";
            return false;
        }
        QTextStream pStream( &pbConfigFile );
        pStream << "\n#include \"Pwm.h\"\n\n";
        pStream << "/*\n * Notification routines are defined elsewhere but need to be linked from here,\n * so we define the routines as external here.\n */\n";
        for (int chIter = 0; chIter < PwmChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmNotification");
            if (!string.isEmpty()) {
                std::cout << "Error: Don't know how to define externals of the pwm notifications.\n";
                return false;
            }
        }
        pStream << "\nconst Pwm_ConfigType PwmConfig = {\n\t.Channels = {\n";
        for (int chIter = 0; chIter < PwmChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmPeriodDefault");
            float period = string.toFloat(&okFlag);
            if (!okFlag) {
                std::cout << "Error: Unable to convert PwmPeriodDefault of " << string.toStdString() << " to float.\n";
                return false;
            }
            // To understand how to calculate prescaler and period, see http://www.microcontroller-project.com/stm32-pwm-generation-using-timers.html
            int prescaler = (int)ceil((period * CLOCK_FREQ / 65536) - 1);       // PWM Resolution = max value of 16-bit register
            int periodInt = (int)(period * (CLOCK_FREQ / (prescaler+1)));
            pStream << "\t\tPWM_CHANNEL_CONFIG(\n";
            pStream << "\t\t\t\t\t\tPwmConf_PwmChannel_" << PwmChannels[chIter].firstChildElement("SHORT-NAME").text() << ",\n";
            pStream << "\t\t\t\t\t\t// Period in ticks. Target: " << period << " Actual: " << period << "\n";           // Don't want to spend time on calculating the error
            pStream << "\t\t\t\t\t\t" << periodInt << ",\n";
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmDutycycleDefault");
            pStream << "\t\t\t\t\t\t// Duty cycle (0 ~> 0%, 0x8000 ~> 100%)\n\t\t\t\t\t\t" << string << ",\n";
            pStream << "\t\t\t\t\t\t// Local prescaler zero base register value\n\t\t\t\t\t\t" << prescaler << ",\n";
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmPolarity");
            pStream << "\t\t\t\t\t\t// Polarity\n\t\t\t\t\t\t" << string << "),\n";
        }
        pStream << "\t},\n#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON\n";
        pStream << "\t.ChannelClass={\n";
        for (int chIter = 0; chIter < PwmChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmChannelClass");
            pStream << "\t\t" << string << ",\n";
        }
        pStream << "\t},\n#endif\n#if PWM_NOTIFICATION_SUPPORTED==STD_ON\n";
        pStream << "\t.NotificationHandlers = {\n";
        for (int chIter = 0; chIter < PwmChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(PwmChannels[chIter], "PwmNotification");
            pStream << "\t\t// Notification routine for Pwm_" << PwmChannels[chIter].firstChildElement("SHORT-NAME").text() << "\n";
            if (!string.isEmpty())
                pStream << "\t\t" << string << ",\n";
            else
                pStream << "\t\tNULL,\n";
        }
        pStream << "\t}\n#endif\n};\n\n";
        pbConfigFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Pwm.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Pwm.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += PWM\n";
        makeFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool STM32F103PwmGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);

    bool returnValue = true;
    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}

