/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "generators/stm32f103/adc_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool STM32F103AdcGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location)
{
    Q_UNUSED(projectModuleRoot);
    Q_UNUSED(autosarModelModuleRoot);

    // Check if the generation location is valid
    if (!QDir(location).exists()) {
        std::cout << "Error: Folder " + location.toStdString() + " for generation of Adc Module Configuration files does not exist.\n";
        return false;
    }

    try {
		// ----------------------------------------- Create Adc_Cfg.h -----------------------------------------
        QString headerName = location + "/Adc_Cfg.h";
        QFile file(headerName);
        if (!file.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Adc_Cfg.h file.\n";
		    return false;
		}
        QTextStream headerStream( &file );
        headerStream << "\n#if !(((ADC_SW_MAJOR_VERSION == 2) && (ADC_SW_MINOR_VERSION == 0)) )\n#error Adc: Configuration file expected BSW module version to be 2.0.*\n#endif\n\n";
        headerStream << "#if !(((ADC_AR_RELEASE_MAJOR_VERSION == 4) && (ADC_AR_RELEASE_MINOR_VERSION == 1)) )\n#error Adc: Configuration file expected AUTOSAR version to be 4.1.*\n#endif\n\n";
        headerStream << "#ifndef ADC_CFG_H_\n#define ADC_CFG_H_\n\n";
        QDomElement AdcGeneral = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "AdcGeneral");
        QString string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcDeInitApi");
        headerStream << "#define ADC_DEINIT_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcDevErrorDetect");
        headerStream << "#define ADC_DEV_ERROR_DETECT" << GetStdOnOff(string) << "\n";
        headerStream << "#define ADC_ENABLE_QUEUING\t\t\tSTD_ON\n";
        string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcEnableStartStopGroupApi");
        headerStream << "#define ADC_ENABLE_START_STOP_GROUP_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcGrpNotifCapability");
        headerStream << "#define ADC_GRP_NOTIF_CAPABILITY\t\t\t" << GetStdOnOff(string) << "\n";
        headerStream << "#define ADC_HW_TRIGGER_API\tSTD_OFF\t\t\t/* Not implemented. */\n";
        headerStream << "#define ADC_PRIORITY_IMPLEMENTATION\t\t\tSTD_OFF\t/* Not implemented. */\n";
        string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcReadGroupApi");
        headerStream << "#define ADC_READ_GROUP_API\t\t\t" << GetStdOnOff(string) << "\n";
        string = GET_VALUE_OF_PARAM(AdcGeneral, "AdcVersionInfoApi");
        headerStream << "#define ADC_VERSION_INFO_API\t\t\t" << GetStdOnOff(string) << "\n";
        headerStream << "#define ADC_POSTBUILD_VARIANT\t\t\tSTD_OFF\n\n";
        headerStream << "#include \"Adc_ConfigTypes.h\"\n\n";
        headerStream << "#define ADC_NOF_GROUP_PER_CONTROLLER 100\n";
        QDomElement AdcConfigSet = AutosarToolFactory::FindFirstReferenceToElement(projectModuleRoot, "AdcConfigSet");
        QList<QDomElement> AdcHwUnits;
        AutosarToolFactory::FindReferencesToElement(&AdcHwUnits, &AdcConfigSet, "AdcHwUnit");
        QList<QDomElement> channels;
        headerStream << "#define ADC_ARC_CTRL_CONFIG_CNT " << AdcHwUnits.size() << "\n\n";
        int channelNumber = 0;
        QList<QDomElement> AdcGroups;
        QList<QDomElement> AdcGroupDefinitions;
        for (int hwIter = 0; hwIter < AdcHwUnits.size(); hwIter++) {
            QString hwUnitName = AdcHwUnits[hwIter].firstChildElement("SHORT-NAME").text().toUpper();
            AutosarToolFactory::FindReferencesToElement(&AdcGroups, &AdcHwUnits[hwIter], "AdcGroup");
            for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
                QString groupName = AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper();
                AutosarToolFactory::FindReferencesToElement(&AdcGroupDefinitions, &AdcGroups[grIter], "AdcGroupDefinition");
                headerStream << "#define ADC_NBR_OF_" << hwUnitName << "_" << groupName << "_CHANNELS\t" << AdcGroupDefinitions.size() << "\n";
                for (int chIter = 0; chIter < AdcGroupDefinitions.size(); chIter++) {
                    string = AdcGroupDefinitions[chIter].firstChildElement("VALUE-REF").text();
                    QDomElement channel = AutosarToolFactory::GetDomElementFromReference(&string, projectModuleRoot->ownerDocument().documentElement());
                    channels.push_back(channel);
                    headerStream << "#define ADC_" << hwUnitName << "_" << groupName << "_" << channel.firstChildElement("SHORT-NAME").text().toUpper()
                                 << " ((Adc_ArcChannelWithinGroupType)" << channelNumber << ")\n";
                    channelNumber++;
                }
                string = GET_VALUE_OF_PARAM_WITH_RETURN(AdcGroups[grIter], "AdcNotification");
                if (!string.isEmpty())
                    headerStream << "extern void " << string << "(void);\n";
            }
        }
        headerStream << "\ntypedef uint16 Adc_ArcChannelWithinGroupType;\n\n";
        headerStream << "/* These are only defined here to be used by IoHwAb */\n";
        for (int chIter = 0; chIter < channels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(channels[chIter], "AdcChannelId");
            headerStream << "#define " << channels[chIter].firstChildElement("SHORT-NAME").text() << " " << string << "\n";
        }
        headerStream << "\ntypedef enum\n{\n";
        for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
            headerStream << "\tADC_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper() << " = " << grIter << ",\n";
        }
        headerStream << "}Adc_GroupType;\n\n/* Generated symbolic names */\n";
        for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
            headerStream << "#define\tAdcConf_AdcGroup_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text() << " ADC_"
                         << AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper() << "\n";
            headerStream << "#define Adc_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text() << " AdcConf_AdcGroup_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text() << "\n";
        }
        headerStream << "\n#define ADC_NBR_OF_GROUPS " << AdcGroups.size() << "\n\n/* Configuration Set Handles */\n";
        headerStream << "#define AdcConfigSet (AdcConfig)\n#define Adc_AdcConfigSet (AdcConfig)\n\n";
        headerStream << "#endif /* ADC_CFG_H_ */\n";
        file.close();

		// ----------------------------------------- Create Adc_Cfg.c -----------------------------------------
		QString lConfigFileName = location + "/Adc_Cfg.c";
		QFile lConfigFile(lConfigFileName);
        if (!lConfigFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Adc_Cfg.c file.\n";
		    return false;
		}
        bool okFlag;
        QTextStream configStream( &lConfigFile );
        QString firstHwUnitName = AdcHwUnits[0].firstChildElement("SHORT-NAME").text();
        configStream << "\n#include \"Adc.h\"\n#include \"Adc_Cfg.h\"\n#include \"stm32f10x_adc.h\"\n\n";
        configStream << "Adc_GroupStatus AdcGroupStatus_" << firstHwUnitName << "[" << AdcGroups.size() << "];\n\n";
        configStream << "const Adc_HWConfigurationType AdcHWUnitConfiguration_" << firstHwUnitName << " =\n{\n";
        string = GET_VALUE_OF_PARAM(AdcHwUnits[0], "AdcHwUnitId");
        if (string != "ADC_1") {
            std::cout << "Error: Don't know what to do......\n";
            return false;
        }
        configStream << "\t.hwUnitId = 0,\n";
        string = GET_VALUE_OF_PARAM(AdcHwUnits[0], "AdcPrescale");
        if (!string.isEmpty()) {
            int num = string.toInt(&okFlag);
            if (!okFlag) {
                std::cout << "Error: Unable to convert \'" << string.toStdString() << "\' to an integer.\n";
                return false;
            }
            if (num % 2 != 0) {
                std::cout << "Error: Adc::AdcConfigSet::AdcHwUnit::AdcPrescale must be an integer divisible by 2.\n";
                return false;
            }
        } else {
            std::cout << "Error: Adc::AdcConfigSet::AdcHwUnit::AdcPrescale cannot be empty.\n";
            return false;
        }
        configStream << "\t.adcPrescale = ADC_SYSTEM_CLOCK_DIVIDE_FACTOR_" << string << ",\n";
        configStream << "\t.clockSource = ADC_SYSTEM_CLOCK,\n};\n\n";
        configStream << "const Adc_ChannelConfigurationType AdcChannelConfiguration_" << firstHwUnitName << "[] =\n{\n";
        QList<QDomElement> AdcChannels;
        AutosarToolFactory::FindReferencesToElement(&AdcChannels, &AdcHwUnits[0], "AdcChannel");
        for (int chIter = 0; chIter < AdcChannels.size(); chIter++) {
            string = GET_VALUE_OF_PARAM(AdcChannels[chIter], "ArcAdcChannelConversionTime");
            QStringList sampleTime = string.split("_");
            configStream << "\t{ .adcChannelConvTime = ADC_SampleTime_" << sampleTime[0] << "Cycles" << sampleTime[1] << " },\n";
        }
        configStream << "};\n\n";
        channelNumber = 0;
        for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
            configStream << "const Adc_ChannelType Adc_" << firstHwUnitName << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text()
                         << "_ChannelList[ADC_NBR_OF_" << firstHwUnitName.toUpper() << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper() << "_CHANNELS] =\n{\n";
            for (int chIter = 0; chIter < channels.size(); chIter++) {
                string = GET_VALUE_OF_PARAM(channels[chIter], "AdcChannelId");
                configStream << "\tADC_CH" << string;
                if (chIter != channels.size()-1)
                    configStream << ",\n";
                else
                    configStream << "\n";
            }
            configStream << "};\n";
        }
        configStream << "\n";
        for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
            configStream << "Adc_ValueGroupType Adc_" << firstHwUnitName << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text()
                         << "_Buffer[ADC_NBR_OF_" << firstHwUnitName.toUpper() << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper() << "_CHANNELS];\n";
        }
        configStream << "\n";
        configStream << "const Adc_GroupDefType AdcGroupConfiguration_" << firstHwUnitName << "[] =\n{\n";
        for (int grIter = 0; grIter < AdcGroups.size(); grIter++) {
            string = GET_VALUE_OF_PARAM(AdcGroups[grIter], "AdcGroupConversionMode");
            configStream << "\t{\n\t\t.conversionMode\t=\t" << string << ",\n";
            string = GET_VALUE_OF_PARAM(AdcGroups[grIter], "AdcGroupTriggSrc");
            configStream << "\t\t.triggerSrc\t=\t" << string << ",\n";
            string = GET_VALUE_OF_PARAM_WITH_RETURN(AdcGroups[grIter], "AdcNotification");
            if (!string.isEmpty()) {
                configStream << "\t\t.groupCallback\t=\t" << string << ",\n";
            } else {
                configStream << "\t\t.groupCallback\t=\tNULL,\n";
            }
            configStream << "\t\t.channelList\t=\tAdc_" << firstHwUnitName << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text() << "_ChannelList,\n";
            configStream << "\t\t.resultBuffer\t=\tAdc_" << firstHwUnitName << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text() << "_Buffer,\n";
            configStream << "\t\t.numberOfChannels\t=\tADC_NBR_OF_" << firstHwUnitName.toUpper() << "_" << AdcGroups[grIter].firstChildElement("SHORT-NAME").text().toUpper() << "_CHANNELS,\n";
            configStream << "\t\t.status\t=\t&AdcGroupStatus_" << firstHwUnitName << "[" << grIter << "],\n\t},\n";
        }
        configStream << "};\n\n";
        configStream << "const Adc_ConfigType AdcConfig[] =\n{\n";
        for (int hwIter = 0; hwIter < AdcHwUnits.size(); hwIter++) {
            configStream << "\t{\n\t\t.hwConfigPtr\t=\t&AdcHWUnitConfiguration_" << AdcHwUnits[hwIter].firstChildElement("SHORT-NAME").text() << ",\n";
            configStream << "\t\t.channelConfigPtr = AdcChannelConfiguration_" << AdcHwUnits[hwIter].firstChildElement("SHORT-NAME").text() << ",\n";
            configStream << "\t\t.nbrOfChannels\t=\t" << channels.size() << ",\n";
            configStream << "\t\t.groupConfigPtr\t=\tAdcGroupConfiguration_" << AdcHwUnits[hwIter].firstChildElement("SHORT-NAME").text() << ",\n";
            configStream << "\t\t.nbrOfGroups\t=\t" << AdcGroups.size() << "\n\t},\n";
        }
        configStream << "};\n\n";
		lConfigFile.close();

        // ----------------------------------------- Create Makefile -----------------------------------------
        QString makeFileName = location + "/Adc.mk";
        QFile makeFile(makeFileName);
        if (!makeFile.open(QIODevice::WriteOnly)) {
            std::cout << "Error: Unable to create Adc.mk file.\n";
            return false;
        }
        QTextStream makeFileStream( &makeFile );
        makeFileStream << "MOD_USE += ADC\n";
        makeFile.close();
    } catch (QString err) {
        std::cout << err.toStdString() << "\n";
        return false;
    }

    return true;
}

bool STM32F103AdcGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
    Q_UNUSED(autosarModelModuleRoot);

    bool returnValue = true;
    // Make sure that all the references within the module are valid
    QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
    for (int refIter = 0; refIter < refs.size(); refIter++)
    {
        QString ref = refs.at(refIter).toElement().text();
        if (ref != "")
        {
            QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
            if (tempElem.isNull())
            {
                std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
                             AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
                returnValue = false;
            }
        }
    }

    return returnValue;
}

