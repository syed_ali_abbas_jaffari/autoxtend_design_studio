/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "x_generator.h"
#include "autosartoolfactory.h"
#include <QFileInfo>

#define GET_VALUE_OF_PARAM(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__)
#define GET_VALUE_REF_OF_REF_WITH_RETURN(p1, p2) GetValueRefOfReference(p1, p2, __FILE__, __LINE__, false)
#define GET_VALUE_OF_PARAM_WITH_RETURN(p1, p2) GetValueOfParameter(p1, p2, __FILE__, __LINE__, false)

bool TemplateGenerator::RunCodeGenerator(QDomElement* projectModuleRoot, QDomElement* autosarModelModuleRoot, QString location) {
	Q_UNUSED(projectModuleRoot);
	Q_UNUSED(autosarModelModuleRoot);

	// Check if the generation location is valid
	if (!QDir(location).exists()) {
		  std::cout << "Error: Folder " + location.toStdString() + " for generation of X Module Configuration files does not exist.\n";
		  return false;
	}

	try {
		// ----------------------------------------- Create Makefile -----------------------------------------
		QString fileName = location + "/x.mk";
		QFile qFile(fileName);
		if (!qFile.open(QIODevice::WriteOnly)) {
				std::cout << "Error: Unable to create x.mk file.\n";
				return false;
		}
		QTextStream makeFileStream( &qFile );
		makeFileStream << "MOD_USE += X\n";
		qFile.close();

		// ----------------------------------------- Create x_Cfg.h -----------------------------------------
		fileName = location + "/x_Cfg.h";
		qFile.setFileName(fileName);
		if (!qFile.open(QIODevice::WriteOnly)) {
				std::cout << "Error: Unable to create x_Cfg.h file.\n";
				return false;
		}
		QTextStream headerStream( &qFile );
		headerStream << "\n#ifndef X_CFG_H_\n#define X_CFG_H_\n\n";


		headerStream << "#endif // X_CFG_H_\n";
		qFile.close();

		// ----------------------------------------- Create x_Cfg.c -----------------------------------------
		fileName = location + "/x_Cfg.c";
		qFile.setFileName(fileName);
		if (!qFile.open(QIODevice::WriteOnly)) {
				std::cout << "Error: Unable to create x_Cfg.c file.\n";
				return false;
		}
		QTextStream configStream( &qFile );
		configStream << "";
		qFile.close();
	} catch (QString err) {
		  std::cout << err.toStdString() << "\n";
		  return false;
	}

	return true;
}

bool TemplateGenerator::CheckModuleValidity(QDomElement *projectModuleRoot, QDomElement* autosarModelModuleRoot)
{
  Q_UNUSED(autosarModelModuleRoot);
	bool returnValue = true;

  // Make sure that all the references within the module are valid
  QDomNodeList refs = projectModuleRoot->elementsByTagName("VALUE-REF");
  for (int refIter = 0; refIter < refs.size(); refIter++) {
    QString ref = refs.at(refIter).toElement().text();
    if (ref != "") {
			QDomElement tempElem = AutosarToolFactory::GetDomElementFromReference(&ref, projectModuleRoot->ownerDocument().documentElement());
			if (tempElem.isNull()) {
			  std::cout << "Error: Invalid reference \'" + refs.at(refIter).toElement().text().toStdString() + "\' in " <<
		                     AutosarToolFactory::CreateReferenceFromDomElement(refs.at(refIter).toElement()).toStdString() << ".\n";
			  returnValue = false;
			}
		}
  }

  return returnValue;
}

