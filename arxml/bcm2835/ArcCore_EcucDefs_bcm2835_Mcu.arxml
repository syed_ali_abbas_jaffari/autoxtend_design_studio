<?xml version="1.0" encoding="UTF-8" ?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://autosar.org/schema/r4.0 autosar_4-1-1.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE>
      <SHORT-NAME>ArcCore</SHORT-NAME>
      <AR-PACKAGES>
        <AR-PACKAGE>
          <SHORT-NAME>EcucDefs</SHORT-NAME>
          <AR-PACKAGES>
            <AR-PACKAGE>
              <SHORT-NAME>bcm2835</SHORT-NAME>
              <ELEMENTS>
                <ECUC-MODULE-DEF UUID="ECUC:3be785ac-217c-4816-af2c-395509aa44ec">
                  <SHORT-NAME>Mcu</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">Configuration of the Mcu (Microcontroller Unit) module.</L-2>
                  </DESC>
                  <ADMIN-DATA>
                    <DOC-REVISIONS>
                      <DOC-REVISION>
                        <REVISION-LABEL>4.4.0</REVISION-LABEL>
                        <ISSUED-BY>AUTOSAR</ISSUED-BY>
                        <DATE>2013-10-31</DATE>
                      </DOC-REVISION>
                    </DOC-REVISIONS>
                  </ADMIN-DATA>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                  <SUPPORTED-CONFIG-VARIANTS>
                    <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD</SUPPORTED-CONFIG-VARIANT>
                    <SUPPORTED-CONFIG-VARIANT>VARIANT-PRE-COMPILE</SUPPORTED-CONFIG-VARIANT>
                  </SUPPORTED-CONFIG-VARIANTS>
                  <CONTAINERS>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:d7751157-e2f5-4d68-bdb4-6435f4579625">
                      <SHORT-NAME>McuGeneralConfiguration</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">This container contains the configuration (parameters) of the MCU driver.</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                      <MULTIPLE-CONFIGURATION-CONTAINER>false</MULTIPLE-CONFIGURATION-CONTAINER>
                      <PARAMETERS>
                        <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:76739a47-838a-4d23-869b-cdec117258d0">
                          <SHORT-NAME>McuDevErrorDetect</SHORT-NAME>
                          <DESC>
                            <L-2 L="EN">Pre-processor switch for enabling the development error detection and reporting.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                          <SCOPE>LOCAL</SCOPE>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                          <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                          <DEFAULT-VALUE>0</DEFAULT-VALUE>
                        </ECUC-BOOLEAN-PARAM-DEF>
                        <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:5b79ff39-ace6-403d-a8aa-179ce92ab4de">
                          <SHORT-NAME>McuInitClock</SHORT-NAME>
                          <DESC>
                            <L-2 L="EN">If this parameter is set to FALSE, the clock initialization has to be disabled from the MCU driver. This concept applies when there are some write once clock registers and a bootloader is present. If this parameter is set to TRUE, the MCU driver is responsible of the clock initialization.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                          <SCOPE>LOCAL</SCOPE>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                          <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                          <DEFAULT-VALUE>1</DEFAULT-VALUE>
                        </ECUC-BOOLEAN-PARAM-DEF>
                      </PARAMETERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:99a9afb6-7290-4e31-83ff-87e62ee312b0">
                      <SHORT-NAME>McuModuleConfiguration</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">This container contains the configuration (parameters) of the MCU driver</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                      <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
                      <REFERENCES>
                        <ECUC-REFERENCE-DEF UUID="ECUC:293b68d8-3cca-4343-abf2-9bdcf91e6770">
                          <SHORT-NAME>ArcDefaultClockRef</SHORT-NAME>
                          <DESC>
                            <L-2 L="EN">This parameter contains a reference to the default clock that will be used at initialization</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>ARCCORE_ECUC</ORIGIN>
                          <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/ArcCore/EcucDefs/bcm2835/Mcu/McuModuleConfiguration/McuClockSettingConfig</DESTINATION-REF>
                        </ECUC-REFERENCE-DEF>
                      </REFERENCES>
                      <SUB-CONTAINERS>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:6405e155-4b6e-4ed5-8a59-02b55c751f9c">
                          <SHORT-NAME>McuClockSettingConfig</SHORT-NAME>
                          <DESC>
                            <L-2 L="EN">This container contains the configuration (parameters) for the Clock settings of the MCU. Please see MCU031 for more information on the MCU clock settings.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY-INFINITE>true</UPPER-MULTIPLICITY-INFINITE>
                          <MULTIPLE-CONFIGURATION-CONTAINER>false</MULTIPLE-CONFIGURATION-CONTAINER>
                          <PARAMETERS>
                            <ECUC-INTEGER-PARAM-DEF UUID="ECUC:047b9310-fc58-4447-97c7-54d19121862d">
                              <SHORT-NAME>McuClockSettingId</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">The Id of this McuClockSettingConfig to be used as argument for the API call "Mcu_InitClock".</L-2>
                              </DESC>
                              <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                              <SCOPE>LOCAL</SCOPE>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                              <SYMBOLIC-NAME-VALUE>true</SYMBOLIC-NAME-VALUE>
                              <MAX>255</MAX>
                              <MIN>0</MIN>
                              <DEFAULT-VALUE>0</DEFAULT-VALUE>
                            </ECUC-INTEGER-PARAM-DEF>
                            <ECUC-INTEGER-PARAM-DEF UUID="ECUC:6e4df4d0-6cdc-4a2d-8476-3593e04ddcaa">
                              <SHORT-NAME>McuClockReferencePointFrequency</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">This is the frequency for the specific instance of the McuClockReferencePoint container. It shall be given in Hz.</L-2>
                              </DESC>
                              <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                              <SCOPE>ECU</SCOPE>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                              <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                              <MAX>4294967295</MAX>
                              <MIN>0</MIN>
                              <DEFAULT-VALUE>250000000</DEFAULT-VALUE>
                            </ECUC-INTEGER-PARAM-DEF>
                          </PARAMETERS>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:44ef2f9f-d880-4a47-8157-46da58bf513e">
                          <SHORT-NAME>McuModeSettingConf</SHORT-NAME>
                          <DESC>
                            <L-2 L="EN">This container contains the configuration (parameters) for the Mode setting of the MCU. Please see MCU035 for more information on the MCU mode settings.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY-INFINITE>true</UPPER-MULTIPLICITY-INFINITE>
                          <MULTIPLE-CONFIGURATION-CONTAINER>false</MULTIPLE-CONFIGURATION-CONTAINER>
                          <PARAMETERS>
                            <ECUC-INTEGER-PARAM-DEF UUID="ECUC:b5a03083-c304-4420-90b8-fd19d2a60724">
                              <SHORT-NAME>McuMode</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">The parameter represents the MCU Mode settings.</L-2>
                              </DESC>
                              <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                              <SCOPE>LOCAL</SCOPE>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                              <SYMBOLIC-NAME-VALUE>true</SYMBOLIC-NAME-VALUE>
                              <MAX>255</MAX>
                              <MIN>0</MIN>
                            </ECUC-INTEGER-PARAM-DEF>
                          </PARAMETERS>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                      </SUB-CONTAINERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                  </CONTAINERS>
                </ECUC-MODULE-DEF>
              </ELEMENTS>
            </AR-PACKAGE>
          </AR-PACKAGES>
        </AR-PACKAGE>
      </AR-PACKAGES>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
