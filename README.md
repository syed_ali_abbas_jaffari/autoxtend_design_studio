
**NOTE: To get an introduction of the AUTO-XTEND toolchain, please refer to the [main repository](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend).**

# Introduction to AUTOSAR Design Studio

The tool AUTOSAR Design Studio enables the user to configure ECU (i.e., define module configurations, map runnables to tasks, configure run-time environment) and automatically generate ECU configuration code. The tool also provides command-line operation which enables bulk code generation to diversify evaluation test-cases.

# Getting Started

Note: Not tested on Windows. There might be problems due to different path conventions. In future, we intend to fix these issues.

## Compile & Run
Requirements: Opengl package for Qt (Linux: libqt5opengl5-dev, Windows: TODO) and UUID for c++ (Linux: uuid-dev, Windows: TODO).

Install Qt Creator on the desired operating system. Open project file in Qt Creator. If the compiler is detected by the Qt Creator, 'Configure Project' tab will appear. Press the 'Configure Project' button. This will parse and build compile hierarchy. Compile using Run button.

## Using GUI

When the GUI opens up for the first time, a message box appears reporting that the meta-model files (in the folder 'arxml') are not found in the build folder. Either locate the meta-model folder from the local copy of the repository (Does not work now, check why) or copy the folder in the build folder manually. To check if the meta-model files are properly loaded in the tool, check if the model hierarchy is not empty by going to Options->Meta-Model Viewer. 

To test code generation, open the example project file by going to File->Open and locating the examples/LinExample.arxml file from the local copy of the repository. From the toolbox, click on 'Generate All' button. It will report that the generation failed since the folder where generated files are supposed to be stored does not exist. Either create the required folder (location in the log) or modify the GENERATOR_OUTPUT_DIRECTORY from the project properties in the 'MyEcu' container in Module Configuration tree. Press the 'Generate All' button again and the module configuration files will be generated in the defined folder.

## Using Command-line

Command-line options: --help, --open (opens an ARXML project file), --import (adds other ARXML project files to the opened project), --generate (generates configuration files for all modules in the opened project)

**Example on Linux (Tested on Ubuntu 16.04)**

./AutosarGenerator --open <repository location>/examples/LinExample.arxml --generate

**Example on Windows**

TODO

# Details on how the main application works:
- At the start of the program, the AUTOSAR meta-model files are read from the folder 'arxml'.
- When a project is opened or newly created, its configurations are searched and put in tree view. 
- When a configuration is selected from the tree view, if no child parameter/reference is found, properties tab is cleared (i.e. the formView displays 'No parameter/reference found for configuration X' (Where X is the name of selected configuration)). Else all the parameters/references are extracted and form is made with the defined configuration type as labels and values from extracted parameters/references as lineEdits/comboboxes/buttons.
- Upon edition of above created layout, the respective parameter must be saved (temporarily) in the opened project file.
- When a parameter is modified, the project is marked as dirty. If user tries to create new or open an existing project, user is prompted to save.
- When a configuration is right-clicked, a menu appears which provide option to add possible configurations types or remove selected configurations (if possible).

# Limitations

1. No support for code generation languages (e.g. Xpand/Xtend). In other words, the code generators so far use file streams to write code based on the model. Naturally, this limitation can be resolved once such languages are ported to QT.
2. Module refinement is not allowed. In other words, MCAL modules will never be included in generic modules (i.e. Port and Eth modules should not be generic as in Arctic Studio).
3. So far the MCAL code generators are only available for the BCM2835 SOC (and a few STM32F103 modules, which are useless since the core cannot be compiled for this microcontroller). New code generators can be created by copying existing ones, however the platform dependent details have to be filled manually. Note that the generic module code generators can be used with any MCAL.

For a list of todos and known bugs, please see plan.txt.

# Procedures
- **Open an example project file from ArcCore12 in AUTOSAR Design Studio:**

1. Modify all references of root element AUTOSAR to ArcCore.
2. Open the file as explained above.

- **Modifying an ARXML meta-model file from ArcCore before importing in AUTOSAR Design Studio:**

1. Replace all /AUTOSAR/EcucDefs/ references with /ArcCore/EcucDefs/Generic/, if the reference is made to a non MCAL module (See folder in core/arxml/generic in the AUTOSAR Core from ArcCore). Otherwise replace it with /ArcCore/EcucDefs/Mcal/.
2. Format all text in the INTRODUCTION and DESC tags with C escape sequences (e.g. \n, \t, ...).
3. Module refinement (other than refinement for a specific MCAL) is not allowed. The REFINED-MODULE-DEF-REF tags can be removed (are ignored otherwise). If a refinement is strictly required, the module can be made generic (i.e. shifted to generic package, IOW out of the MCAL packages) and all mcal based modules need to define their own refinement if they use this module.

Problem: "Unable to find the implementation /ArcCore/EcucDefs/Generic/Implementation/NvM in the AUTOSAR model."
Solution: If after starting the AUTOSAR design studio the above error is shown, replace all the occurrences of /ArcCore/EcucDefs/Generic/Implementation/NvM with /ArcCore/EcucDefs/Generic/Implementations/NvM.

- **Create a new code generator for module X:**

1. Make sure that the module X has an ARXML configuration file and an implementation (BSW-IMPLEMENTATION) with correct values, references and UUIDs.
2. Create a copy of template_generator.h and template_generator.cpp from <ProjectRoot>/generators to <ProjectRoot>/generators/generic/, if the module is a generic module. Else, to <ProjectRoot>/generators/<MCAL>/.
3. Rename the copied files to X_generator.h/cpp. In the h/cpp files, modify the guard and the class name. Start the generator class name (and the file name) with the MCAL name, if generator is for an MCAL. 
4. Either import the new files in the project using Qt Creator or add the files to the Qt project file's SOURCES/HEADERS tags.
5. Include the new header in codegeneratorfactory.cpp. Register the generator with the information from the implementation (BSW-IMPLEMENTATION).
6. Start writing code for the generator in cpp file for validation and generation of code. Note, AutosarToolFactory and AbstractCodeGenerator classes provides useful functions. Also, errors and warnings can be written to the output window on the GUI by writing to the stdout.

# Wish to contribute?

You are welcome to contribute in improving and extending the software. To do that, you can request for a branch or report bugs (with procedure to reproduce the bug) to the author, Ali Syed @ [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com). 

# Copyright and License

Copyright (C) 2016, Ali Syed, Germany
Contact: [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com)

AUTO-XTEND is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. AUTO-XTEND is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU General Public License](https://www.gnu.org/licenses/gpl.txt) for more details.

**Disclaimer:** The rights for the operating system core from ArcCore and AUTOSAR meta-model provided as part of this toolchain are reserved by ArcCore AB, Sweden. The licensing statement from ArcCore is provided as under:

Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
Contact: [contact@arccore.com](mailto:contact@arccore.com)
You may ONLY use this file:

1. if you have a valid commercial ArcCore license and then in accordance with the terms contained in the written license agreement between you and ArcCore, or alternatively
2. if you follow the terms found in GNU General Public License version 2 as published by the Free Software Foundation and appearing in the file LICENSE.GPL included in the packaging of this file or [here](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).

