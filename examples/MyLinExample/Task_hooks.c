#include "Os.h"
//#define USE_LDEBUG_PRINTF // Uncomment this to turn debug statements on.
#include "debug.h"

// Hooks
#define ERROR_LOG_SIZE 20

struct LogBad_s {
    uint32_t param1;
    uint32_t param2;
    uint32_t param3;
    TaskType taskId;
    OsServiceIdType serviceId;
    StatusType error;
};

void ErrorHook ( StatusType Error ) {

    TaskType task;
    //static struct LogBad_s LogBad[ERROR_LOG_SIZE];
    static uint8_t ErrorCount = 0;
    GetTaskID(&task);
    //OsServiceIdType service = OSErrorGetServiceId();

    LDEBUG_PRINTF("## ErrorHook err=%d\n",Error);

    /* Log the errors in a buffer for later review */
    /*LogBad[ErrorCount].param1 = os_error.param1;
    LogBad[ErrorCount].param2 = os_error.param2;
    LogBad[ErrorCount].param3 = os_error.param3;
    LogBad[ErrorCount].serviceId = service;
    LogBad[ErrorCount].taskId = task;
    LogBad[ErrorCount].error = Error;*/

    ErrorCount++;

    // Stall if buffer is full.
    while(ErrorCount >= ERROR_LOG_SIZE){}

}

void PostTaskHook ( void ) {
    TaskType task;
    GetTaskID(&task);
    LDEBUG_PRINTF("## PreTaskHook, taskid=%d\n",task);
}

void PreTaskHook ( void ) {
    TaskType task;
    GetTaskID(&task);
    LDEBUG_PRINTF("## PreTaskHook, taskid=%d\n",task);
}

void ShutdownHook ( StatusType Error ) {
    LDEBUG_PRINTF("## ShutdownHook\n");
    while(1){}
    (void) Error;
}

void StartupHook ( void ) {
    LDEBUG_PRINTF("## StartupHook\n");
}
