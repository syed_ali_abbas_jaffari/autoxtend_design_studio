/*
* Configuration of module: Os (Tasks.c)
*
* Configured for (MCU):    BCM2835
*/

#include "Os.h"
#include "Lin.h"
#include "LinIf.h"
#include "LinSM.h"
#include "PduR.h"
#include "Com.h"
#include "ComM.h"
#include "bcm2835.h"

uint8 Signal1, Signal2;

extern const Lin_ConfigType Lin_Config;
extern const LinIf_ConfigType LinIf_Config;
extern const LinSM_ConfigType LinSM_Config;
extern const Com_ConfigType ComConfiguration;


void Initializer( void ) {

    Com_IpduGroupVector groupVector;

    Signal1 = Signal2;
    Lin_Init(&Lin_Config);
    LinIf_Init(&LinIf_Config);
    PduR_Init(&PduR_Config);
    LinSM_Init(&LinSM_Config);
    Com_Init(&ComConfiguration);
    LinSM_RequestComMode(ComMConf_ComMChannel_ComMChannel1,COMM_FULL_COMMUNICATION);
    LinIf_MainFunction();
    LinSM_ScheduleRequest(ComMConf_ComMChannel_ComMChannel1,LinSMConf_LinSMSchedule_LinSMSchedule1);
    LinIf_MainFunction();

    Com_ClearIpduGroupVector(groupVector);
    Com_SetIpduGroup(groupVector, ComConf_ComIPduGroup_LIN, TRUE);
    Com_IpduGroupControl(groupVector, FALSE);
    TerminateTask();

}

void Generator( void ) {
	int count = 0;

	while (1) {
		WaitEvent(EVENT_MASK_OsEvent);

		if (count == 0) {
			Signal1 = 65;
			Signal2 = 66;
			Com_SendSignal(ComConf_ComSignal_Speed_Signal, &Signal1);
			Com_SendSignal(ComConf_ComSignal_Turn_Signal, &Signal2);
		} else if (count == 3) {
			Signal1 = 67;
			Signal2 = 68;
			Com_SendSignal(ComConf_ComSignal_Speed_Signal, &Signal1);
			Com_SendSignal(ComConf_ComSignal_Turn_Signal, &Signal2);
		} else if (count == 4) {
			Signal1 = 69;
			Signal2 = 70;
			Com_SendSignal(ComConf_ComSignal_Speed_Signal, &Signal1);
			Com_SendSignal(ComConf_ComSignal_Turn_Signal, &Signal2);
		}
		count++;
		if (count == 5) {
			count = 0;
		}
		LinIf_MainFunction();
		//Com_MainFunctionTx();Com_MainFunctionRx();
		ClearEvent(EVENT_MASK_OsEvent);
	}

}

void OsIdle( void ) {
    while(1){}
}

