/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "autosargenerator.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AutosarGenerator w;
    QCommandLineParser parser;
    parser.setApplicationDescription("AUTOSAR Design Studio");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption openOption(QStringList() << "open", "Open an ARXML file.", "ARXML file");
    parser.addOption(openOption);
    QCommandLineOption importOption(QStringList() << "import", "Import an ARXML file.", "ARXML file");
    parser.addOption(importOption);
    QCommandLineOption generateOption(QStringList() << "generate", "Generate all modules.");
    parser.addOption(generateOption);

    // Process the actual command line arguments given by the user
    parser.process(a);
    if (!parser.isSet(openOption))
    {
        w.runGui(true);
        w.show();
        return a.exec();
    }
    else
    {
        std::cout << "\n";
        w.runGui(false);
        if (parser.isSet(openOption)) {
            QFileInfo info(parser.value(openOption));
            std::cout << "Opening file: " << parser.value(openOption).toStdString() << "\n";
            w.SetCommandLineFileName(info.absoluteFilePath());
            w.on_action_Open_triggered();
        }
        if (parser.isSet(importOption)) {
            QFileInfo info(parser.value(importOption));
            std::cout << "Importing file: " << parser.value(importOption).toStdString() << "\n";
            w.SetCommandLineFileName(info.absoluteFilePath());
            w.on_actionImport_ARXML_triggered();
        }
        if (parser.isSet(generateOption)) {
            std::cout << "Generating all modules...\n";
            w.on_actionGenerate_triggered();
        }
        return 0;
    }
}
