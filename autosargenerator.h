/* ----------------------------- AUTOSAR Design Studio---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * AUTOSAR Design Studio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * AUTOSAR Design Studio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- AUTOSAR Design Studio ---------------------------*/
/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef AUTOSARGENERATOR_H
#define AUTOSARGENERATOR_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtXml>

#define VERSION "0.9.0"

enum ParameterType
{
    Uninitialized,
    Function,
    Reference,
    ForeignReference,
    Enumeration,
    Boolean,
    Integer,
    Float,
    String,
    SymbolicNameReference,
    ExternalFileReference,
    LinkerSymbol,
    ChoiceReference
};

#define InvalidMultiplicity -2
#define Infinite -1

namespace Ui {
class AutosarGenerator;
}

class AutosarGenerator : public QMainWindow
{
    Q_OBJECT

public:
    explicit AutosarGenerator(QWidget *parent = 0);
    ~AutosarGenerator();

    // Functions for handling non-GUI operations
    void runGui(bool v);
    void SetCommandLineFileName(QString clfn);

public slots:
    // Slots for menu items
    void on_action_Open_triggered();
    void on_actionImport_ARXML_triggered();
    void on_actionGenerate_triggered();

private slots:

    // Slots for menu items
    void on_actionE_xit_triggered();
    void on_action_New_triggered();
    void on_action_Save_triggered();
    void on_actionS_ave_As_triggered();
    void on_actionMeta_Model_Editor_triggered();
    void on_actionA_bout_triggered();

    // Slots for GUI buttons
    void on_ExpandAllButton_clicked();
    void on_CollapseAllButton_clicked();
    void on_actionValidate_triggered();

    // Slots for other UI items
    void configurationSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void propertiesTextChanged(QString text);
    void propertiesButtonClicked(bool value);
    void propertiesRefClicked(bool value);
    void onConfigurationContextMenuItemClicked(QAction *action);
    void onConfigurationContextMenu(const QPoint &point);
    void on_moduleSplitter_splitterMoved(int pos, int index);
    void on_propertiesSplitter_splitterMoved(int pos, int index);

private:
    Ui::AutosarGenerator *ui;
    QMenu moduleContextMenu;

    QStandardItemModel *autosarQItemModel, *projectQItemModel;
    QDomDocument autosarDomModel;
    QList<QDomDocument> projectDomModel;
    QSortFilterProxyModel *proxyModel;
    QList<QDomNode> bswImplementations;
    QList<QDomNode> bswImplementationRefs;

    void Initialize();
    bool eventFilter(QObject* object, QEvent* event);
    void closeEvent (QCloseEvent *event);
    void ManualImmMatchRefDomModel(QList<QDomElement> *returnItemList, QDomElement* root, QString refLastItem);
    void ClearLayout(QLayout *layout);
    void AddRecursiveItemFromMetaModel(QString reference, QDomElement elementToAdd, QString *projectXml, QString parentRef);
    void ParseArxmlAndCreateReferences(QDomNode *element, int parentIndex, QStringList *refs, QList<QDomElement> *domElems);
    void CreateQModel(QStringList *references, int refIndex, QModelIndex modelIndex, QStandardItemModel *model, QList<QDomElement> *domElems, int fileIndex = -1);
    void ReadAndAppendImplementations(QDomElement* fileRootElement, bool allImplementations = true);
    void OptimizeArxml(QDomElement *node);
    void AppendArxmls(QDomElement *destinationRootElem, QDomElement *sourceRootElem);
    void GetMetaModelIndicators(QDomElement *modelIndex);
    void GetLiterals(QStringList *enums, QDomElement childElement);
    void GetChoiceReferences(QStringList *refs, QDomElement childElement);
    void ImportModules(QDomDocument *domElement);
    void SaveFile(int fileIter);
    void ParseSdgs(QDomElement sdgsElement, QList<QDomElement> *extensionKeys, QList<QDomElement> *values);
    void GetAllAvailableModulesInMM(QString targetLocation, QStringList *availableModules);
    QString ConstructGeneratorString(QDomNode bswImplementation);
    void RunAllGenerators(bool generate);
    void ReplaceMcalInReference(QString &reference);
    QString GenerateUUID();
    void AddSpaces(QString &string);

    // Variables for handling non-GUI operations
    bool guiMode;
    QString commandLineFileName;

    QList<bool> openedFilesSaved;
    QStringList openedFileNames;
    QList<QDomElement> editingValues;

    QStringList genericModules;
    QStringList mcalModules;
    QString mcalName;
    QString generatorOutputDirectory;
    int selectedContainersFileIndex;
    QStringList availableMcals;

    // Temporary variables for parsing meta-model parameters
    int upperMultiplicity, lowerMultiplicity;
    QString description, introduction, destReference, destReferenceType, definitionReference, definitionReferenceType, shortName;
    bool hasReferences, hasChoices, hasParameters, hasContainers;
    ParameterType parameterType;
    double floatMin, floatMax;
    qlonglong intMin;
    qulonglong intMax;
};

#endif // AUTOSARGENERATOR_H
